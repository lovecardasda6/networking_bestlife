<h1 style="font-family:creamCake; font-size: 5em;">Cashout</h1>
<br />
<?php
if (isset($_GET['cashout']) && $_GET['cashout'] == 2) :
?>
    <div style="background-color:#af0000; padding: .8em; border-radius:10px; color:white;">
        Transaction failed. Please try again.
    </div>
<?php
elseif (isset($_GET['cashout']) && $_GET['cashout'] == 3) :
?>
    <div style="background-color:#af0000; padding: .8em; border-radius:10px; color:white;">
        Amount is below minimum withdrawal amount.
    </div>
<?php
elseif (isset($_GET['cashout']) && $_GET['cashout'] == 4) :
?>
    <div style="background-color:#af0000; padding: .8em; border-radius:10px; color:white;">
        Invalid entered amount. Please try again.
    </div>
<?php
elseif (isset($_GET['error']) && $_GET['error'] == 1) :
?>
    <div style="background-color:#af0000; padding: .8em; border-radius:10px; color:white;">
        <?= base64_decode($_GET['details']) ?>
    </div>
<?php
endif;
?>


<div>
    <!--------------------------------------------------------------------->
    <div>
        <?php
        if ($banks != NULL) :
            foreach ($banks as $bank_center) :
        ?>
                <form method="POST">
                    <input type="hidden" name="transaction" value="Bank">
                    <input type="hidden" name="center" value="<?= $bank_center['bank_name'] ?>">
                    <div class="tiles">
                        <div id="title" onclick="toogle('<?= $bank_center['bank_name'] ?>')">
                            <img src="/assets/logo/<?= Encashment::fetch_logo("Bank", $bank_center['bank_name']) ?>" style="width: 65px;" />
                            <?= $bank_center['bank_name'] ?>
                            <i class="fa fa-chevron-down" aria-hidden="true"></i>

                        </div>
                        <div style="display:none" id="<?= $bank_center['bank_name'] ?>">
                            <hr />
                            <div class="form-group">
                                <label for="lastname">Account Name <label style="color:red;"> * </label>:</label>
                                <input type="text" class="form-control" value="<?= ($bank_center['account_name']) ? $bank_center['account_name'] : '' ?>" name="lastname" required readonly disabled>
                            </div>
                            <div class="form-group">
                                <label for="firstname">Account Number <label style="color:red;"> * </label>:</label>
                                <input type="text" class="form-control" value="<?= ($bank_center['account_number']) ? $bank_center['account_number'] : '' ?>" name="firstname" required readonly disabled>
                            </div>
                            <div style="text-align:right">
                                <input type="submit" name="continue_transaction" value="Continue" class="btn btn-default">
                            </div>
                        </div>
                    </div>
                </form>
        <?php
            endforeach;
        else :
            echo "No bank account information found. <br> 
    <a href='?page=account'>Please click here to add your bank account informations. </a><Br/><Br/>";
        endif;
        ?>
    </div>
    <!--------------------------------------------------------------------->
    <!--------------------------------------------------------------------->
    <div>
        <?php
        if ($remittances != null) :
            foreach ($remittances as $remittance) :
        ?>
                <form method="POST">
                    <input type="hidden" name="transaction" value="Remittance">
                    <input type="hidden" name="center" value="<?= $remittance['remittance_center'] ?>">
                    <div class="tiles">
                        <div id="title" onclick="toogle('<?= $remittance['remittance_center'] ?>')">
                            <img src="/assets/logo/<?= Encashment::fetch_logo("Remittance", $remittance['remittance_center']) ?>" style="width: 65px;" />
                            <?= $remittance['remittance_center'] ?>
                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </div>

                        <div style="display:none" id="<?= $remittance['remittance_center'] ?>">
                            <hr />
                            <div class="form-group">
                                <label for="lastname">Receiver's Name <label style="color:red;"> * </label>:</label>
                                <input type="text" class="form-control" value="<?= ($remittance['receivers_name']) ? $remittance['receivers_name'] : '' ?>" name="lastname" required readonly disabled>
                            </div>
                            <div class="form-group">
                                <label for="firstname">Contact Number <label style="color:red;"> * </label>:</label>
                                <input type="text" class="form-control" value="<?= ($remittance['contact_number']) ? $remittance['contact_number'] : '' ?>" name="firstname" required readonly disabled>
                            </div>
                            <div style="text-align:right">
                                <input type="submit" name="continue_transaction" value="Continue" class="btn btn-default">
                            </div>
                        </div>
                    </div>
                </form>
        <?php
            endforeach;
        else :
            echo "No Remittance account information found. <br> <a href='?page=account'>Click here to add your Remittance Information.</a><br/><Br/>";
        endif;
        ?>
    </div>
    <!--------------------------------------------------------------------->
    <!--------------------------------------------------------------------->
    <div>
        <?php
        if ($ewallets != null) :
            foreach ($ewallets as $ewallet_center) :
        ?>
                <form method="POST">
                    <input type="hidden" name="transaction" value="EWallet">
                    <input type="hidden" name="center" value="<?= $ewallet_center['ewallet_center'] ?>">
                    <div class="tiles">
                        <div id="title" onclick="toogle('<?= $ewallet_center['ewallet_center'] ?>')">
                            <img src="/assets/logo/<?= Encashment::fetch_logo("EWallet", $ewallet_center['ewallet_center']) ?>" style="width: 65px;" />
                            <?= $ewallet_center['ewallet_center'] ?>
                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </div>
                        <div style="display:none" id="<?= $ewallet_center['ewallet_center'] ?>">
                            <hr />
                            <div class="form-group">
                                <label for="firstname">EWallet Number <label style="color:red;"> * </label>:</label>
                                <input type="text" class="form-control" value="<?= ($ewallet_center['ewallet_number']) ? $ewallet_center['ewallet_number'] : '' ?>" name="firstname" required readonly disabled>
                            </div>
                            <div style="text-align:right">
                                <input type="submit" name="continue_transaction" value="Confirm" class="btn btn-default">
                            </div>
                        </div>
                    </div>
                </form>
        <?php
            endforeach;
        else :
            echo "No EWallet account information found. <br> <a href='?page=account'>Click here to add your EWallet number Information.</a>";
        endif;
        ?>
    </div>
    <!--------------------------------------------------------------------->
</div>


<script>
    function toogle(displayId) {
        var divTag = document.getElementById(displayId).style.display;

        if (divTag == "none") {
            document.getElementById(displayId).style.display = "block";
        } else {
            document.getElementById(displayId).style.display = "none";
        }
    }
</script>