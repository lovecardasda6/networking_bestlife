<h1 style="font-family:creamCake; font-size: 5em;">Cashout Amount</h1>
<br />

<?php
    $in_php = $balance;
    $is_usd = $balance / $php_to_usd_rate;
?>

<form method="POST" action="?page=cashout">
    <input type="hidden" name="currency" value="<?= $_GET['encashment_center'] ?>">
    <input type="hidden" name="encashment_mode" value="<?= $_GET['encashment_mode'] ?>">
    <input type="hidden" name="encashment_center" value="<?= $_GET['encashment_center'] ?>">
    <div class="row">
        <div class="tiles">
            <br />
            <div style="text-align:center;">
                <label style="font-size:1.5em;">Amount (<?= ($currency == NULL || $currency  == 'PHP') ? "PHP" : "USD" ?>)</label>

                <?php
                if ($currency == NULL || $currency == 'PHP') :
                ?>
                    <input type="number" id="cashoutAmount" step="any" placeholder="0.00" name="amount" value="<?= $in_php ?>" class="cashout" max="<?= $in_php ?>" />
                <?php
                else :
                ?>
                    <input type="number" id="cashoutAmount" step="any" placeholder="0.00" name="amount" value="<?= $is_usd ?>" class="cashout" max="<?= $is_usd ?>" />
                <?php
                endif;
                ?>

                <hr>
                <label>Wallet Balance</label>
                <div style="font-size: 2.5em;">
                    <?php
                    if ($currency == NULL || $currency == 'PHP') :
                        echo "&#8369;" . number_format($in_php, 2);
                    else :
                        echo "&#36;" . number_format($is_usd, 2);
                    endif;
                    ?>
                </div>
                <br />
            </div>

            <div style="text-align:right;">
                <a href="?page=cashout_center">
                    <input type="button" class="btn btn-primary" name="bank" value="Cancel" />
                </a> &nbsp;
                <input type="submit" class="btn btn-default" name="confirmAmount" value="Preview" style="float:right;" />
            </div>
        </div>
    </div>
</form>


<style>
    .cashout {
        border: none;
        outline: none;
        width: 100%;
        text-align: center;
        font-size: 5em;
        font-weight: 1px;
    }

    input[type="number"]::-webkit-inner-spin-button,
    input[type="number"]::-webkit-outer-spin-button {
        -moz-appearance: none;
        -webkit-appearance: none;
        appearance: none;
        margin: 0;
    }
</style>