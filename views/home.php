<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2" style="text-align:center;">

        <label style="font-family:-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;font-weight: 500; font-size: 500%; color: #036b07; text-shadow: 2px 2px black;">
            WELCOME,
        </label>

        <br />
        <label style="color:#3a3c3a;font-weight: 600; font-size: 200%; font-family:-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif; ">
            <?= $name ?>
        </label>
        <br />
        <br />
        <br />
        <img src="./assets/logo2.png" style="width: 70%; margin-top:-5em;" />

    </div>
</div>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<?php if ($contact == null) : ?>
    <div class="modal fade" id="myNotice" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="border-bottom:none">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">NOTICE</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Please complete you account information in the Account section or <a href="?page=account">click here</a> for the following purpose below and no more.

                        <ul>
                            <li>
                                Contact Number - for faster password reset and password delivery
                            </li>
                            <li>
                                Document - for easily and faster identifying account ownership
                            </li>
                        </ul>
                        <br />
                        Please also note to change your password if you are a new member and to change your password regularly.
                        <br />
                        <br />
                        For more information, please look and read our about section for more privacy and policy agreement.
                    </p>
                    </br>
                    </br>
                    <p>
                        Administrator
                    </p>
                </div>
            </div>

        </div>
    </div>


    <script>
        $(document).ready(function() {
            $("#myNotice").modal('show');
        });
    </script>

<?php endif; ?>