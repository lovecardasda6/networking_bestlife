<?php

require_once __DIR__ . '/../functions/redirect.php';
$referral_commision = new referral_commision($acc_id);
?>

<h1>Direct Referral Transactions</h1>

<table class="table">
    <thead>
        <tr>
            <td>Name</td>
            <td>Package</td>
            <td>Commision</td>
            <td>Gift Check</td>
            <td>Date</td>
        </tr>
    </thead>


    <tbody>
        <?= $referral_commision->commision() ?>
    </tbody>
</table>