<?php

require_once __DIR__ . '/../functions/redirect.php';
$indirect_bonus_income_transaction = new indirect_bonus_income_transaction($acc_id);
?>

<h1>Indirect Bonus Transactions</h1>

<table class="table">
    <thead>
        <tr>
            <td>Name</td>
            <td>Package</td>
            <td>Commision</td>
            <td>Date</td>
        </tr>
    </thead>


    <tbody>
        <?php
        $indirect_bonus_income_transaction->commision();
        ?>
    </tbody>
</table>