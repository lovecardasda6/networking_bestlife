<div class="col-md-12">

<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li class="active" style="color:white">Indirect Bonus</li>
</ol>

    <div class="row">

    <div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
        Indirect Link Bonus
        </div>
        <div class="panel-body">
            <?php
            
            $package = null;
            $counter = 1;
            $query = "SELECT packages.id as `packages_id`, package FROM indirect_level INNER JOIN packages ON packages.id = indirect_level.package_id ORDER BY packages.price ASC;";
            $res = $con->query($query);
            while($row = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                if($counter == 1){
                    $counter += 1;
                    $package = isset($_GET['package']) ? $_GET['package']  :$row['packages_id'];
                }
                echo "<a href='?page=indirect_bonus&package={$row['packages_id']}'>{$row['package']}</a> &nbsp; &nbsp; &nbsp; ";
            }
            ?>

            <br/>
            <br/>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>Level</td>
                        <td>Earnings (Percentage %)</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    <?php

                        $query = "SELECT indirect_bonuses.id as `indirect_bonuses_id`, packages.id as `package_id`, package, level, percent FROM indirect_bonuses INNER JOIN packages ON packages.id = indirect_bonuses.package_id WHERE packages.id = '{$package}' ORDER BY packages.price ASC;";
                        $res = mysqli_query($con, $query);
                        while($row = mysqli_fetch_assoc($res))
                        {
                    ?>
                        <tr>
                            <td><?= $row['level'] ?></td>
                            <td><?= $row['percent'] ?></td>
                            <td>
                                <a href="?page=indirect_bonus&package=<?= $row['package_id'] ?>&id=<?= $row['indirect_bonuses_id']?>&action=modify&package_id=<?= $row['package_id'] ?>&percent=<?= $row['percent'] ?>">Modify</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="panel panel-default">
        <?php
        if(isset($_GET['action']) && $_GET['action'] === "modify")
        {
        ?>
        <div class="panel-heading">
        Form Update
        </div>
        <div class="panel-body">
            <!---UPDATE FORM--->
            <form method="post">
                <input type="hidden" name="id" value="<?= $_GET['id'] ?>" />
                <div class="form-group">
                    <label for="package">Package :</label>
                    <select class="form-control" name="package">
                        <?php
                            $query = "SELECT * FROM packages WHERE id = '{$package}' ORDER BY price ASC";
                            $res = mysqli_query($con, $query);
                            while($row = mysqli_fetch_assoc($res)){
                        ?>
                            <option value="<?= $row['id']?>"><?= $row['package']?></option>
                        <?php } ?>
                    </select>
                </div>
                                
                <div class="form-group">
                    <label for="percent">Earnings (%)  :</label>
                    <input  class="form-control" type="number" step=".01" name="percent" placeholder="Earnings (Percentage %)" value="<?= $_GET['percent'] ?>"/>
                </div>

                <input class="btn btn-default" type="submit" name="update" value="Update"/>
                &nbsp; &nbsp; &nbsp;
                <a class="btn btn-default" href="?page=indirect_bonus">Cancel</a>
            </form>
        </div>
        <?php 
            }
        ?>
    </div>
    </div>

    </div>
</div>