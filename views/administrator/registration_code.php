<div class="col-md-12">

    <ol class="breadcrumb" style="background-color: #00674b;">
        <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
        <li class="active" style="color:white">Registration code</li>
    </ol>

    <div class="row">

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Registration Codes - <?= @$_GET['package']  ?>
                </div>

                <div class="panel-body">

                    <a href="?page=unpaid_registration_codes">Click here to view free codes</a>
                    <br />
                    <br />

                    <?php
                    $query = "SELECT * FROM packages ORDER BY price ASC";
                    $res = mysqli_query($con, $query);
                    while ($row = mysqli_fetch_assoc($res)) {
                    ?>
                        <a href="?page=registration_code&package_id=<?= $row['id'] ?>&package=<?= $row['package'] ?>&status=Unused"><?= $row['package'] ?></a> &nbsp; | &nbsp;
                    <?php
                    }
                    ?>

                    <?php
                    if (isset($_GET['package_id']) && isset($_GET['status'])) {
                    ?>

                        <br />
                        <br />

                        <a href="?page=registration_code&package_id=<?= $_GET['package_id'] ?>&package=<?= $_GET['package'] ?>&status=Unused">Unused</a>
                        &nbsp; | &nbsp;
                        <a href="?page=registration_code&package_id=<?= $_GET['package_id'] ?>&package=<?= $_GET['package'] ?>&status=Used">Used</a>

                        <br />
                        <br />

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Package</td>
                                    <td>Registration Code</td>
                                    <td>Status</td>
                                    <td>QR Code</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $query = "SELECT registration_codes.id as `id`, package_id, registration_code, status FROM registration_codes INNER JOIN packages ON packages.id = registration_codes.package_id WHERE status = '" . $_GET['status'] . "' AND package_id = '" . $_GET['package_id'] . "' AND type = 'Paid' ORDER BY id ASC;";
                                $res = mysqli_query($con, $query);
                                while ($row = mysqli_fetch_assoc($res)) {
                                ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $_GET['package'] ?></td>
                                        <td>
                                            <?php
                                            $code = str_split($row['registration_code'], 4);
                                            $code = htmlspecialchars(implode('-', $code));
                                            echo $code;
                                            ?>
                                        </td>
                                        <td><?= $row['status'] ?></td>
                                        <td>
                                            <button>Generate</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Generate Codes </div>

                <div class="panel-body">
                    <!---GENERATE FORM--->
                    <form method="post">

                        <div class="form-group">
                            <label for="package">Package:</label>
                            <select name="package" class="form-control">
                                <?php
                                $query = "SELECT * FROM packages ORDER BY price ASC";
                                $res = mysqli_query($con, $query);
                                while ($row = mysqli_fetch_assoc($res)) {
                                ?>
                                    <option value="<?= $row['id'] ?>" <?= (isset($_GET['package_id']) && $_GET['package_id'] == $row['id']) ? "selected" : "" ?>><?= $row['package'] ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="type">Type:</label>
                            <select name="type" class="form-control">
                                <option value="Paid">Paid</option>
                                <option value="Unpaid">Unpaid</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="quantity">Quantity:</label>
                            <input class="form-control" type="number" name="quantity" placeholder="Quantity (PHP)" />

                        </div>

                        <input type="submit" class="btn btn-default" name="generate" value="Generate" />
                    </form>
                    <br />
                    <?= (isset($_GET['add']) && $_GET['add'] == 1) ? "<h3>Codes successfully generated.</h3>" : "" ?>
                </div>
            </div>
        </div>

    </div>