
<div class="col-md-12">

    <ol class="breadcrumb" style="background-color: #00674b;">
        <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
        <li class="active" style="color:white">Referral Bonus</li>
    </ol>

    <div class="row">

    <div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            Referral Bonus
        </div>
        <div class="panel-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>Package</td>
                    <td>Income(PHP)</td>
                    <td>Product(PHP)</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query = "SELECT direct_referral_bonuses.id as `id`, package_id, earnings, product, package FROM direct_referral_bonuses INNER JOIN packages ON packages.id = direct_referral_bonuses.package_id ORDER BY earnings ASC;";
                    $res = mysqli_query($con, $query);
                    while($row = mysqli_fetch_assoc($res))
                    {
                ?>
                    <tr>
                        <td><?= $row['package'] ?></td>
                        <td><?= $row['earnings'] ?></td>
                        <td><?= $row['product'] ?></td>
                        <td>
                            <a href="?page=referral_bonus&id=<?= $row['id']?>&action=modify&package=<?= $row['package'] ?>&earnings=<?= $row['earnings'] ?>&product=<?= $row['product'] ?>&package_id=<?= $row['package_id'] ?>">Modify</a>
                            <!-- | -->
                            <!-- <a href="?page=referral_bonus&id=<?= $row['id']?>&action=remove">Remove</a>    -->
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="panel panel-default">
        
    <?php
        if(isset($_GET['action']) && $_GET['action'] === "modify")
        {
    ?>
        <div class="panel-heading">
        Form Update
        </div>

        <div class="panel-body">
        <!---UPDATE FORM--->
        <form method="post">
            <input type="hidden" name="id" value="<?= $_GET["id"] ?> "/>
            <div class="form-group">
                <label for="package">Package:</label>
                <select class="form-control" name="package">
                    <?php
                        $query = "SELECT * FROM packages ORDER BY price ASC";
                        $res = mysqli_query($con, $query);
                        while($row = mysqli_fetch_assoc($res)){
                    ?>
                        <option <?= ($_GET['package_id'] == $row['id'])? "selected" : "" ?>  value="<?= $row['id']?>"><?= $row['package']?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="earnings">Income (PHP):</label>
                <input class="form-control" type="text" name="earnings" placeholder="Income (PHP)" value="<?= $_GET['earnings'] ?>"/>
            </div>
                            
            <div class="form-group">
                <label for="product">Product (PHP):</label>
                <input class="form-control" type="text" name="product" placeholder="Product (PHP)" value="<?= $_GET['product'] ?>"/>
            </div>

            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 

            <a  class="btn btn-default" href="?page=referral_bonus">Cancel</a>
            &nbsp; &nbsp; &nbsp; | &nbsp; &nbsp; &nbsp; 
            <input class="btn btn-default" type="submit" name="update" value="Update"/>
        </form>
        </div>
    <?php 
        }
        else
        {
    ?>
    <div class="panel-heading">
        Form Add
        </div>
        
        <div class="panel-body">
        <!---ADD FORM--->
        <form method="post">
            <div class="form-group">
                <label for="package">Package (PHP):</label>
                <select class="form-control" name="package">
                <?php
                    $query = "SELECT * FROM packages ORDER BY price ASC";
                    $res = mysqli_query($con, $query);
                    while($row = mysqli_fetch_assoc($res)){
                ?>
                    <option value="<?= $row['id']?>"><?= $row['package']?></option>
                <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="earnings">Income (PHP) :</label>
                <input class="form-control" type="text" name="earnings" placeholder="Income (PHP)" />
            </div>
            
            <div class="form-group">
                <label for="product">Product (PHP) :</label>
                <input class="form-control" type="text" name="product" placeholder="Product (PHP)" />
            </div>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 

            <input class="btn btn-default" type="submit" name="submit" value="Save" />
        </form>
        </div>
    <?php 
        }
    ?>
    </div>
    </div>
    </div>
</div>

    