<div class="col-md-12">

    <ol class="breadcrumb" style="background-color: #00674b;">
        <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
        <li class="active" style="color:white">Announcement</li>
    </ol>

    <form method="post">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <h4>Announcement </h4>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <label>From <label style="color:red;"> * </label>:</label>
                            <input type="date" class="form-control" name="from" required>
                        </div>
                        <div class="col-md-6">
                            <label>Until <label style="color:red;"> * </label>:</label>
                            <input type="date" class="form-control" name="until" required>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <input type="text" class="form-control" name="subject" placeholder="Subject" required>
                </div>
                <div class="form-group">
                    <textarea class="form form-control" name="content" style="height: 300px;" required></textarea>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="signed_by" placeholder="Signed by" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="position" placeholder="Position" required>
                </div>

                <hr/>
                <div class="form-group" style="text-align: right;">
                    <input type="submit" class="btn btn-default" name="save_announcement" value="Save Announcement">
                </div>
            </div>
        </div>
    </form>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
</div>