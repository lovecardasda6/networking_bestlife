
<div class="col-md-12">

<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li class="active" style="color:white">Pairing Bonus</li>
</ol>

    <div class="row">

    <div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
        Direct Link Bonus
        </div>
        <div class="panel-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>Package</td>
                    <td>Income (Percentage %)</td> 
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query = "SELECT direct_bonuses.id as `id`, package_id, percentage, package FROM direct_bonuses INNER JOIN packages ON packages.id = direct_bonuses.package_id ORDER BY id ASC;";
                    $res = mysqli_query($con, $query);
                    while($row = mysqli_fetch_assoc($res))
                    {
                ?>
                    <tr>
                        <td><?= $row['package'] ?></td>
                        <td><?= $row['percentage'] ?></td>
                        <td>
                            <a href="?page=direct_bonus&id=<?= $row['id']?>&action=modify&percentage=<?= $row['percentage'] ?>&package_id=<?= $row['package_id'] ?>">Modify</a>
                            <!-- | -->
                            <!-- <a href="?page=direct_bonus&id=<?= $row['id']?>&action=remove">Remove</a>    -->
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        </div>
    </div>
    </div>

    <div class="col-md-6">
    <div class="panel panel-default">
        <?php
            if(isset($_GET['action']) && $_GET['action'] === "modify")
            {
        ?>
        <div class="panel-heading">
        Form Update
        </div>
        <div class="panel-body">
        <!---UPDATE FORM--->
        <form method="post">
            <input type="hidden" name="id" value="<?= $_GET['id'] ?>" />
            <div class="form-group">
                <label for="value_points">Package :</label>
                <select class="form-control" name="package">
                    <?php
                        $query = "SELECT * FROM packages ORDER BY price ASC";
                        $res = mysqli_query($con, $query);
                        while($row = mysqli_fetch_assoc($res)){
                    ?>
                        <option <?= ($_GET['package_id'] == $row['id'])? "selected" : "" ?>  value="<?= $row['id']?>"><?= $row['package']?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="value_points">Income (Percentage %) :</label>
                <input class="form-control" type="number" step=".01" name="percentage" placeholder="Income (Percentage %)" value="<?= $_GET['percentage'] ?>"/>
            </div>
           
            <a class="btn btn-default" href="?page=direct_bonus">Cancel</a>
            &nbsp; &nbsp; &nbsp; | &nbsp; &nbsp; &nbsp; 
            <input class="btn btn-default" type="submit" name="update" value="Update"/>
        </form>
        </div>
    <?php 
        }
        else
        {
    ?>
        <!---ADD FORM--->
        <div class="panel-heading">
        Form Add
        </div>
        <div class="panel-body">
        <form method="post">
            <div class="form-group">
                <label for="value_points">Package :</label>
                <select class="form-control" name="package">
                    <?php
                        $query = "SELECT * FROM packages ORDER BY price ASC";
                        $res = mysqli_query($con, $query);
                        while($row = mysqli_fetch_assoc($res)){
                    ?>
                        <option value="<?= $row['id']?>"><?= $row['package']?></option>
                    <?php } ?>
                </select>
            </div>
            
            <div class="form-group">
                <label for="value_points">Income (Percentage %) :</label>
                <input class="form-control" type="number" step=".01" name="percentage" step="any" placeholder="Cash (Percentage %)" />
            </div>
            
            <input class="btn btn-default"  type="submit" name="submit" value="Save" />
        </form>
        </div>
        <?php 
            }
        ?>
    </div>
    </div>
    </div>
   
</div>

    