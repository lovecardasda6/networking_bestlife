<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Encashment</a></li>
</ol>


<?php if (isset($_GET['add']) && $_GET['add'] == 1 && !$error) : ?>
    <div class="breadcrumb" style="background-color: #00674b;">
        <span style="color:white">
            Encashment center successfully added.
        </span>
    </div>
<?php elseif (isset($_GET['update']) && $_GET['update'] == 1 && !$error) : ?>
    <div class="breadcrumb" style="background-color: #00674b;">
        <span style="color:white">
            Item successfully save.
        </span>
    </div>
<?php elseif (isset($_GET['remove']) && $_GET['remove'] == 1 && !$error) : ?>
    <div class="breadcrumb" style="background-color: #00674b;">
        <span style="color:white">
            Item successfully remove.
        </span>
    </div>
<?php elseif ($error) : ?>
    <div class="breadcrumb" style="background-color: #770202;">
        <span style="color:white">
            <?= $message ?>
        </span>
    </div>
<?php endif; ?>


<div class="row">
    <div class="col-md-7">

        <ul class="nav nav-tabs">
            <li class="active" id="nav_tabs"><a data-toggle="tab" href="#all" style="font-size: .9em;">All</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#bank" style="font-size: .9em;">Bank</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#remittance" style="font-size: .9em;">Remittance</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#ewallet" style="font-size: .9em;">EWallet</a></li>
        </ul>
        <br />
        <div class="tab-content">
            <div id="all" class="tab-pane fade in active">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>Encashment</td>
                                    <td>Center</td>
                                    <td>Actions</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($encashments != null) :
                                    foreach ($encashments as $encashment) :
                                ?>
                                        <tr>
                                            <td><?= $encashment['encashment'] ?></td>
                                            <td><?= $encashment['encashment_center'] ?></td>
                                            <td>
                                                <a href="?page=encashment&action=modify&encashment_id=<?= $encashment['id'] ?>&mode=<?= $encashment['encashment'] ?>&center=<?= $encashment['encashment_center'] ?>&logo=<?= $encashment['logo'] ?>">
                                                    Modify
                                                </a>
                                                &nbsp; &nbsp; | &nbsp; &nbsp;
                                                <a href="?page=encashment&action=remove&encashment_id=<?= $encashment['id'] ?>" onclick="removeItem()">
                                                    Remove
                                                </a>
                                            </td>
                                        </tr>
                                <?php
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="bank" class="tab-pane">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>Bank</td>
                                    <td>Actions</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($encashments != null) :
                                    foreach ($encashments as $encashment) :
                                        if ($encashment['encashment'] == "Bank") :
                                ?>
                                            <tr>
                                                <td><?= $encashment['encashment_center'] ?></td>
                                                <td>
                                                    <a href="?page=encashment&action=modify&encashment_id=<?= $encashment['id'] ?>&mode=<?= $encashment['encashment'] ?>&center=<?= $encashment['encashment_center'] ?>&logo=<?= $encashment['logo'] ?>">
                                                        Modify
                                                    </a>
                                                    &nbsp; &nbsp; | &nbsp; &nbsp;
                                                    <a href="?page=encashment&action=remove&encashment_id=<?= $encashment['id'] ?>" onclick="removeItem()">
                                                        Remove
                                                    </a>
                                                </td>
                                            </tr>
                                <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="remittance" class="tab-pane">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>Remittance</td>
                                    <td>Actions</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($encashments != null) :
                                    foreach ($encashments as $encashment) :
                                        if ($encashment['encashment'] == "Remittance") :
                                ?>
                                            <tr>
                                                <td><?= $encashment['encashment_center'] ?></td>
                                                <td>
                                                    <a href="?page=encashment&action=modify&encashment_id=<?= $encashment['id'] ?>&mode=<?= $encashment['encashment'] ?>&center=<?= $encashment['encashment_center'] ?>&logo=<?= $encashment['logo'] ?>">
                                                        Modify
                                                    </a>
                                                    &nbsp; &nbsp; | &nbsp; &nbsp;
                                                    <a href="?page=encashment&action=remove&encashment_id=<?= $encashment['id'] ?>" onclick="removeItem()">
                                                        Remove
                                                    </a>
                                                </td>
                                            </tr>
                                <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="ewallet" class="tab-pane">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>EWallet</td>
                                    <td>Actions</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($encashments != null) :
                                    foreach ($encashments as $encashment) :
                                        if ($encashment['encashment'] == "EWallet") :
                                ?>
                                            <tr>
                                                <td><?= $encashment['encashment_center'] ?></td>
                                                <td>
                                                    <a href="?page=encashment&action=modify&encashment_id=<?= $encashment['id'] ?>&mode=<?= $encashment['encashment'] ?>&center=<?= $encashment['encashment_center'] ?>&logo=<?= $encashment['logo'] ?>">
                                                        Modify
                                                    </a>
                                                    &nbsp; &nbsp; | &nbsp; &nbsp;
                                                    <a href="?page=encashment&action=remove&encashment_id=<?= $encashment['id'] ?>" onclick="removeItem()">
                                                        Remove
                                                    </a>
                                                </td>
                                            </tr>
                                <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-5">
        <div class="panel panel-default">


            <?php
            if (isset($_GET['action']) && $_GET['action'] == "modify" && isset($_GET['encashment_id']) && isset($_GET['mode']) && isset($_GET['center'])) {
            ?>
                <div class="panel-heading">
                    Form Update
                </div>

                <div class="panel-body">
                    <!--UPDATE FORM-->
                    <form method="post" enctype="multipart/form-data">
                        <input type="hidden" name="encashment_id" value="<?= $_GET['encashment_id'] ?>" />
                        <input type="hidden" name="logo" value="<?= $_GET['logo'] ?>" />
                        <div class="form-group">
                            <label>Encashment:</label>
                            <select class="form-control" name="encashment">
                                <option value="Bank" <?= $_GET['mode'] == "Bank" ? "selected" : '' ?>>Bank</option>
                                <option value="Remittance" <?= $_GET['mode'] == "Remittance" ? "selected" : '' ?>>Remittance</option>
                                <option value="EWallet" <?= $_GET['mode'] == "EWallet" ? "selected" : '' ?>>EWallet</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Center:</label>
                            <input class="form-control" type="text" name="center" placeholder="Center" value="<?= $_GET['center'] ?>" required />
                        </div>

                        <div class="form-group">
                            <label for="middlename">Logo <label style="color:red;"> * </label>:</label>
                            <input type="file" class="form-control" name="logo" accept="image/*">
                        </div>

                        <div style="text-align:right">
                            <a href="?page=encashment">
                                <input class="btn" type="button" value="Cancel" />
                            </a>
                            <input class="btn btn-default" type="submit" name="update" value="Update" />
                        </div>
                    </form>
                </div>
            <?php

            } else {
            ?>

                <!--ADD FORM-->
                <div class="panel-heading">
                    Form Add
                </div>

                <div class="panel-body">
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Encashment:</label>
                            <select class="form-control" name="encashment">
                                <option value="Bank" <?= $logs != null && $logs->encashment_mode == "Bank" ? "selected" : '' ?>>Bank</option>
                                <option value="Remittance" <?= $logs != null && $logs->encashment_mode == "Remittance" ? "selected" : '' ?>>Remittance</option>
                                <option value="EWallet" <?= $logs != null && $logs->encashment_mode == "EWallet" ? "selected" : '' ?>>EWallet</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Center:</label>
                            <input class="form-control" type="text" name="center" placeholder="Center" value="<?= $logs != null ? $logs->encashment_center : '' ?>" required />
                        </div>

                        <div class="form-group">
                            <label for="middlename">Logo <label style="color:red;"> * </label>:</label>
                            <input type="file" class="form-control" name="logo" accept="image/*" required>
                        </div>

                        <div style="text-align:right">
                            <input class="btn btn-default" type="submit" name="add" value="Add" />
                        </div>
                    </form>

                <?php
            }
                ?>
                </div>
        </div>
    </div>

</div>
</div>