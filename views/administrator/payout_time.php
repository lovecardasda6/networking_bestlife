<div class="col-md-12">

<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li class="active" style="color:white">Payout Time</li>
</ol>

    <div class="row">

    <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
        Payout Time
        </div>
        <div class="panel-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>Package</td>
                    <td>Status</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query = "SELECT * FROM payout_time;";
                    $res = mysqli_query($con, $query);
                    while($row = mysqli_fetch_assoc($res))
                    {
                ?>
                    <tr>
                        <td><?= $row['day'] ?></td>
                        <td><?= ($row['status'] != "Active")? '' : 'Active' ?></td>
                        <td>
                            <?php
                                if($row['status'] != "Active")
                                {
                            ?>
                                <a href="?page=payout_time&id=<?= $row['id']?>&action=activate">
                                    Activate
                                </a>  
                            <?php
                                }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
</div>

