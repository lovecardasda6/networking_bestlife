<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li><a href="#" style="color:white">Accounts</a></li>
    <li class="active" style="color:white">Members</li>
</ol>

<?php
if (isset($_GET['document']) && $_GET['document'] == 1) :
?>
    <div style="background-color:#003302; padding: 10px; border-radius:5px; color:white;">
        Document confirmed.<br />
    </div>
    <BR/>
<?php
endif;
?>

<div class="row">
    <div class="col-md-8">
        <form method="POST">
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form form-control" name="input" placeholder="Search Account">
                </div>
                <input type="submit" value="Search" name="search" class="btn btn-default" />
                <a href="?page=leaders">
                    <input type="button" value="Cancel" name="search" class="btn btn-primary" />
                </a>
            </div>
        </form>
    </div>

    <div class="col-md-4" style="text-align: right;">
        <a href="?page=create_members_account">
            <input type="button" class="btn btn-default" value="Register new account">
        </a>
    </div>
</div>

<hr />

<table class="table">
    <thead>
        <tr>
            <td>Acc. ID</td>
            <td>Lastname</td>
            <td>Firstname</td>
            <td>Middlename</td>
            <td>Contact No.</td>
            <td>Package</td>
            <td>Registered</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $Accounts = Account::all_accounts();
        if ($Accounts != null) :
            foreach ($Accounts as $account) :
        ?>
                <tr>
                    <td><?= $account['account_id'] ?></td>
                    <td><?= $account['lastname'] ?></td>
                    <td><?= $account['firstname'] ?></td>
                    <td><?= $account['middlename'] ?></td>
                    <td><?= $account['contact'] ?></td>
                    <td>
                        <?= $account['package'] ?>
                        ( <?= $account['type'] ?> )
                    </td>
                    <td><?= $account['registered_on'] ?></td>
                    <td>
                        <a href="?page=view_members_account&account_id=<?= $account['account_id'] ?>">
                            View
                        </a>
                    </td>
                </tr>
        <?php
            endforeach;
        endif;
        ?>
    </tbody>
</table>