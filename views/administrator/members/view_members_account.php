<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li><a href="#" style="color:white">Accounts</a></li>
    <li><a href="?page=members" style="color:white">Members</a></li>
    <li><a href="#" style="color:white">View</a></li>
</ol>

<?php
$account_id = isset($_GET['account_id']) ? $_GET['account_id'] : 0;
$account = Account::fetch_account($account_id);
if ($account == null) :
?>
    <div style="background-color:#800505; padding: .8em; border-radius:5px; color:white;">
        Account information not found.
    </div>
    <br />
    <a href="?page=members">Click here to go back.</a>
    <?= exit; ?>
<?php
endif;
?>
<!----------------------------------------------->
<?php if ($error) : ?>
    <div style="background-color:#800505; padding: .8em; border-radius:5px; color:white;">
        <?= $message ?>
    </div>
    <br />
<?php endif; ?>
<!----------------------------------------------->
<?php if (isset($_GET['details']) && isset($_GET['success']) && $_GET['success'] == '1') : ?>
    <div style="background-color:green; padding: .8em; border-radius:5px; color:white;">
        <?= base64_decode($_GET['details']) ?>
    </div>
    <br />
<?php endif; ?>
<!----------------------------------------------->

<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#personal_information">Personal Information</a></li>
    <li><a data-toggle="tab" href="#account_information">Account Information</a></li>
    <li><a data-toggle="tab" href="#login_information">Login Information</a></li>
    <li><a data-toggle="tab" href="#documents">Documents</a></li>
    <li><a data-toggle="tab" href="#update_account">Update Personal Information</a></li>
</ul>

<br />


<div class="tab-content">
    <div id="personal_information" class="tab-pane fade in active">
        <div class="panel panel-default">
            <div class="panel-heading">
                Personal Information
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Lastname &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['lastname'] ?>" name="username" readonly disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Firstname &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['firstname'] ?>" name="username" readonly disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Middlename &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['middlename'] ?>" name="username" readonly disabled>
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Contact No. &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['contact'] ?>" name="username" readonly disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Gender &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['gender'] ?>" name="username" readonly disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Address &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['address'] ?>" name="username" readonly disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Date of birth &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="date" class="form-control" value="<?= $account['date_of_birth'] ?>" name="username" readonly disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Place of birth &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['place_of_birth'] ?>" name="username" readonly disabled>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="account_information" class="tab-pane fade">
        <div class="panel panel-default">
            <div class="panel-heading">
                Account Information
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Package &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['package'] ?>" name="package" readonly disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="login_information" class="tab-pane fade">
        <div class="panel panel-default">
            <div class="panel-heading">
                Login Information
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="username">Username &nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['username'] ?>" readonly disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="email">Email Address&nbsp; &nbsp; :</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="<?= $account['email'] ?>" readonly disabled>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <form method="post">
                                    <div class="col-md-12" style="text-align: right;">
                                        <input type="hidden" name="user_id" value="<?= $account['user_id'] ?>" />
                                        <input type="hidden" name="account_id" value="<?= $account['account_id'] ?>" />
                                        <button type="submit" name="reset_password" class="btn btn-default">Reset Password</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="documents" class="tab-pane fade">
        <div class="panel panel-default">
            <div class="panel-heading">
                Documents
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <?php
                        $document = notifications::fetch_documents($account_id);
                        ?>
                        <?php if ($document != null) : ?>
                            <h4>
                                <?= $document['type'] ?>
                            </h4>
                            <hr />
                            <a href="/../assets/documents/<?= $document['path'] ?>" target="_blank">
                                <img src="/../assets/documents/<?= $document['path'] ?>" style="width: 100%;" />
                            </a>
                            <hr />
                            <?php if ($document['status'] == "Pending") : ?>
                                <div style="text-align:right">
                                    <form method="post">
                                        <input type="hidden" name="account_id" value="<?= $account['account_id'] ?>" />
                                        <input type="hidden" name="document_id" value="<?= $document['id'] ?>" />
                                        <input type="submit" name="confirm_document" class="btn btn-default" value="Confirm Document" />
                                    </form>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="update_account" class="tab-pane fade">
        <form method="post" autocomplete="off">
            <input type="hidden" class="form-control" value="<?= $_GET['id'] ?>" name="account_id">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Update Personal Information
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="email">Lastname &nbsp; &nbsp; :</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" value="<?= $logs ? $logs->lastname : $account['lastname'] ?>" name="lastname">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="email">Firstname &nbsp; &nbsp; :</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" value="<?= $logs ? $logs->firstname : $account['firstname'] ?>" name="firstname">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="email">Middlename &nbsp; &nbsp; :</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" value="<?= $logs ? $logs->middlename : $account['middlename'] ?>" name="middlename">
                                    </div>
                                </div>
                            </div>

                            <hr />

                            <div class="form-group">
                                <div class="row">
                                    <form method="post">
                                        <div class="col-md-12" style="text-align: right;">
                                            <input type="hidden" name="account_id" value="<?= $account['account_id'] ?>" />
                                            <button type="submit" name="update_personal_information" class="btn btn-default">Update Information</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />