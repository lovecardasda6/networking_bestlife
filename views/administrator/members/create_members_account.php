<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li><a href="#" style="color:white">Accounts</a></li>
    <li><a href="?page=members" style="color:white">Members</a></li>
    <li><a href="#" style="color:white">Register</a></li>
</ol>


<?php
if ($failed) :
?>
    <div style="background-color:#b70000; padding: 1em; border-radius:5px; color:white;">
        <?= $message ?>
    </div>
<?php
endif;
if (isset($_GET['success']) && $_GET['success'] == 1 && !$failed) :
?>
    <div style="background-color:#003302; padding: 10px; border-radius:5px; color:white;">
        Account successfully registered.<br />
    </div>
<?php
endif;
?>

<form method="post">
    <h3>Register new account</h3>
    <hr>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="lastname">Lastname <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="lastname" value="<?= ($log)? $log->lastname : '' ?>" required>
                </div>
                <div class="form-group">
                    <label for="firstname">Firstname <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="firstname" value="<?= ($log)? $log->firstname : '' ?>" required>
                </div>
                <div class="form-group">
                    <label for="middlename">Middlename :</label>
                    <input type="text" class="form-control" name="middlename" value="<?= ($log)? $log->middlename : '' ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="registration_code">Registration code <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="registration_code" value="<?= ($log)? $log->registration_code : '' ?>" required>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">Username <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="username" value="<?= ($log)? $log->username : '' ?>" required>
                </div>
                <div class="form-group">
                    <label for="email">Email Address:</label>
                    <input type="email" class="form-control" value="<?= ($log)? $log->email : '' ?>" name="email">
                </div>
                <div class="form-group">
                    <label for="password">Password <label style="color:red;"> * </label>:</label>
                    <input type="password" class="form-control" name="password" required>
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirm password <label style="color:red;"> * </label>:</label>
                    <input type="password" class="form-control" name="confirm_password" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <input type="submit" name="submit" value="Submit" class="btn btn-default" />
                <a href="?page=binarytree" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </div>
    </div>
    <br>
</form>