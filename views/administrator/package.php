<div class="col-md-12">

    <ol class="breadcrumb" style="background-color: #00674b;">
        <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
        <li class="active" style="color:white">Package</li>
    </ol>

    <?php if ($error) : ?>
        <ol class="breadcrumb" style="background-color: #9a0000; color:white;">
            <?= $message ?>
        </ol>
    <?php endif; ?>

    <?php if (isset($_GET['success']) && $_GET['success'] == 1 && isset($_GET['action']) && $_GET['action'] == 'add') : ?>
        <ol class="breadcrumb" style="background-color: #00674b; color:white;">
            Package successfully added.
        </ol>
    <?php endif; ?>

    <?php if (isset($_GET['success']) && $_GET['success'] == 1 && isset($_GET['action']) && $_GET['action'] == 'update') : ?>
        <ol class="breadcrumb" style="background-color: #00674b; color:white;">
            Package successfully updated.
        </ol>
    <?php endif; ?>

    <?php if (isset($_GET['success']) && $_GET['success'] == 1 && isset($_GET['action']) && $_GET['action'] == 'remove') : ?>
        <ol class="breadcrumb" style="background-color: #00674b; color:white;">
            Package successfully remove.
        </ol>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Package
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>Package</td>
                                <td>Price</td>
                                <td>Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $retrieve = "SELECT * FROM packages ORDER BY price ASC";
                            $res = mysqli_query($con, $retrieve);
                            while ($row = mysqli_fetch_assoc($res)) {
                            ?>
                                <tr>
                                    <td><?= $row["package"] ?></td>
                                    <td><?= $row["price"] ?></td>
                                    <td>
                                        <a href="?page=package&id=<?= $row["id"] ?>&action=modify&package=<?= $row["package"] ?>&price=<?= $row['price'] ?>">Modify</a>
                                        <label>&nbsp; | &nbsp; </label>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">


                <?php
                if (isset($_GET['action']) && $_GET['action'] === "modify" && $_GET['page'] === "package") {
                ?>
                    <div class="panel-heading">
                        Form Update
                    </div>

                    <div class="panel-body">
                        <!--UPDATE FORM-->
                        <form method="POST">
                            <input type="hidden" name="package_id" value="<?= $_GET['id'] ?>" />
                            <div class="form-group">
                                <label for="package">Package:</label>
                                <input class="form-control" type="text" name="package" placeholder="Package" value="<?= $_GET['package'] ?>" />
                            </div>

                            <div class="form-group">
                                <label for="price">Price:</label>
                                <input class="form-control" type="text" name="price" placeholder="Price" value="<?= $_GET['price'] ?>" />
                            </div>


                            <input type="submit" class="btn btn-default" name="update" value="Update" />
                            &nbsp; &nbsp; &nbsp;
                            <a href="?page=package" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                <?php

                } else {
                ?>

                    <!--ADD FORM-->
                    <div class="panel-heading">
                        Form Add
                    </div>

                    <div class="panel-body">
                        <form method="POST">
                            <div class="form-group">
                                <label for="package">Package:</label>
                                <input class="form-control" type="text" name="package" placeholder="Package" />
                            </div>

                            <div class="form-group">
                                <label for="price">Price:</label>
                                <input class="form-control" type="text" name="price" placeholder="Price" />
                            </div>

                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <input class="btn btn-default" type="submit" name="submit" value="Save" />
                        </form>

                    <?php
                }
                    ?>
                    </div>
            </div>
        </div>

    </div>
</div>