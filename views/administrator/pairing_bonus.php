<div class="col-md-12">

    <ol class="breadcrumb" style="background-color: #00674b;">
        <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
        <li class="active" style="color:white">Pairing Bonus</li>
    </ol>
    <br />

    <!----------------------------------------------------------------------->
    <?php
    if ($error) :
    ?>
        <div style="background-color:#af0000; padding: .8em; border-radius:10px; color:white;">
            <?= $message ?>
        </div>
    <?php
    endif;
    ?>
    <!----------------------------------------------------------------------->
    <?php
    if (isset($_GET['action']) && $_GET['action'] == 'update' && isset($_GET['success']) && $_GET['success'] == 1) :
    ?>
        <div style="background-color:green; padding: .8em; border-radius:5px; color:white;">
            Pairing bonus successfully updated.
        </div>
    <?php
    endif;
    ?>
    <!----------------------------------------------------------------------->
    <?php
    if (isset($_GET['action']) && $_GET['action'] == 'add' && isset($_GET['success']) && $_GET['success'] == 1) :
    ?>
        <div style="background-color:green; padding: .8em; border-radius:5px; color:white;">
            Pairing bonus successfully added.
        </div>
    <?php
    endif;
    ?>
    <!----------------------------------------------------------------------->
    <?php
    if (isset($_GET['action']) && $_GET['action'] == 'activate' && isset($_GET['success']) && $_GET['success'] == 1) :
    ?>
        <div style="background-color:green; padding: .8em; border-radius:5px; color:white;">
            Base value points successfully activated.
        </div>
    <?php
    endif;
    ?>
    <!----------------------------------------------------------------------->


    <br />
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Pairing Bonus
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>Package</td>
                                <td>Value Points</td>
                                <td>Pairs per day</td>
                                <td>Base VP</td>
                                <td>Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $query = "SELECT pairing_bonuses.id as `id`, package_id, value_points, package, pair_per_day, status FROM pairing_bonuses INNER JOIN packages ON packages.id = pairing_bonuses.package_id ORDER BY value_points ASC;";
                            $res = mysqli_query($con, $query);
                            while ($row = mysqli_fetch_assoc($res)) {
                            ?>
                                <tr>
                                    <td><?= $row['package'] ?></td>
                                    <td><?= $row['value_points'] ?></td>
                                    <td><?= $row['pair_per_day'] ?></td>
                                    <td><?= $row['status'] ?></td>
                                    <td>
                                        <a href="?page=pairing_bonus&pairing_id=<?= $row['id'] ?>&package_id=<?= $row['package_id'] ?>&action=active">Set as base VP</a>
                                        &nbsp; | &nbsp;
                                        <a href="?page=pairing_bonus&id=<?= $row['id'] ?>&action=modify&id=<?= $row['id'] ?>&value_points=<?= $row['value_points'] ?>&package_id=<?= $row['package_id'] ?>&pair_per_day=<?= $row['pair_per_day'] ?>">Modify</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="panel panel-default">
                <?php
                if (isset($_GET['action']) && $_GET['action'] === "modify") {
                ?>
                    <div class="panel-heading">
                        Form Update
                    </div>
                    <div class="panel-body">
                        <!---UPDATE FORM--->
                        <form method="post">
                            <input type="hidden" name="pairing_id" value="<?= $_GET['id'] ?>" />

                            <div class="form-group">
                                <label for="package">Package :</label>
                                <select name="package_id" class="form-control">
                                    <?php
                                    $query = "SELECT * FROM packages ORDER BY price ASC";
                                    $res = mysqli_query($con, $query);
                                    while ($row = mysqli_fetch_assoc($res)) {
                                    ?>
                                        <option <?= ($_GET['package_id'] == $row['id']) ? "selected" : "" ?> value="<?= $row['id'] ?>"><?= $row['package'] ?></option>
                                    <?php } ?>
                                </select>

                            </div>

                            <div class="form-group">
                                <label for="value_points">Value Points :</label>
                                <input class="form-control" type="number" name="value_points" placeholder="Value Points" value="<?= $_GET['value_points'] ?>" />
                            </div>

                            <div class="form-group">
                                <label for="pair_per_day">Pairs per day :</label>
                                <input class="form-control" type="number" name="pair_per_day" placeholder="Pair per day(250VP base)" value="<?= $_GET['pair_per_day'] ?>" />
                            </div>

                            <input class="btn btn-default" type="submit" name="update" value="Update" />
                            &nbsp; &nbsp;
                            <a class="btn btn-default" href="?page=pairing_bonus">Cancel</a>
                        </form>
                    </div>
                <?php
                } else {
                ?>
                    <div class="panel-heading">
                        Form Add
                    </div>
                    <div class="panel-body">
                        <!---ADD FORM--->
                        <form method="post">
                            <div class="form-group">
                                <label for="package">Package :</label>
                                <select name="package_id" class="form-control">
                                    <?php
                                    $query = "SELECT * FROM packages ORDER BY price ASC";
                                    $res = mysqli_query($con, $query);
                                    while ($row = mysqli_fetch_assoc($res)) {
                                    ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['package'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="value_points">Package :</label>
                                <input class="form-control" type="number" name="value_points" placeholder="Value Points" />
                            </div>

                            <div class="form-group">
                                <label for="pair_per_day">Pairs per day :</label>
                                <input class="form-control" type="number" name="pair_per_day" placeholder="Pair per day" />
                            </div>

                            <input class="btn btn-default" type="submit" name="submit" value="Save" />
                        </form>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>

</div>