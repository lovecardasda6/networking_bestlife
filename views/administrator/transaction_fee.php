
<div class="col-md-12">
<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li class="active" style="color:white">Transaction Fee</li>
</ol>
<div style="overflow:none;">
    <div style="width: 50%; float: left;">
        <h1>Transaction Fee</h1>
            <?= (isset($_GET['success']) && $_GET['success'] == 1)? "Transaction fee successfully added." : "" ?>
            <?= (isset($_GET['success']) && $_GET['success'] == 2)? "Transaction fee successfully updated." : "" ?>
            <?= (isset($_GET['success']) && $_GET['success'] == 3)? "Transaction fee successfully activated." : "" ?>
        <br /><br />
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>Trans. Fee (%)</td>
                    <td>Status</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
                <?= $transaction_fee->transaction_fees() ?>
            </tbody>
        </table>
    </div>

    <div style="width: 49%; float: right;">
        <div style="width: 75%; margin: 0 auto;">
            <?php if (isset($_GET['action']) && $_GET['action'] == "modify") { ?>

                <h1>Update Fee</h1>
                <br />
                <br />
                <form method="post">
                    <input type="hidden" name="id" value="<?= $_GET['id'] ?>">
                    <input type="hidden" name="status" value="<?= $_GET['status'] ?>">
                    <div class="form-group">
                        <label for="fee">Transaction Fee (%):</label>
                        <input type="number" step=".01" class="form-control" name="fee" value="<?= $_GET['fee'] ?>" required>
                    </div>

                    <div class="form-group">
                        <a href="?page=transaction_fee">
                            <button type="button" class="btn">Cancel</button>
                        </a>
                        <input type="submit" name="update" class="btn btn-default" value="Update" />
                    </div>

                </form>

            <?php } else {
            ?>

                <h1>Add Fee</h1>
                <br />
                <br />
                <form method="post">
                    <div class="form-group">
                        <label for="fee">Transaction Fee (%):</label>
                        <input type="number" step=".01" class="form-control" name="fee" required>
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add" class="btn btn-default" value="Add" />
                    </div>

                </form>

            <?php
            }
            ?>
        </div>
    </div>
    <div>
<div>