<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li><a href="#" style="color:white">Accounts</a></li>
    <li style="color:white"><a href="?page=administrator" style="color:white">Administrator</a></li>
    <li class="active" style="color:white">Create</li>
</ol>

<?php
if ($failedRegistration) :
?>
    <div style="background-color:#b70000; padding: 1em; border-radius:5px; color:white;">
        <?= $message ?>
    </div>
<?php
endif;
if (isset($_GET['success']) && $_GET['success'] == 1 && !$failedRegistration) :
?>
    <div style="background-color:#003302; padding: 10px; border-radius:5px; color:white;">
        Account successfully registered.<br />
    </div>
<?php
endif;
?>

<form method="post">
    <h3>Register new administrator</h3>
    <hr>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="lastname">Lastname <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="lastname" value="<?= ($failedLogs) ? $failedLogs->lastname : '' ?>" required>
                </div>
                <div class="form-group">
                    <label for="firstname">Firstname <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="firstname" value="<?= ($failedLogs) ? $failedLogs->firstname : '' ?>" required>
                </div>
                <div class="form-group">
                    <label for="middlename">Middlename :</label>
                    <input type="text" class="form-control" name="middlename" value="<?= ($failedLogs) ? $failedLogs->middlename : '' ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">Username <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="username" value="<?= ($failedLogs) ? $failedLogs->username : '' ?>" required>
                </div>
                <div class="form-group">
                    <label for="password">Password <label style="color:red;"> * </label>:</label>
                    <input type="password" class="form-control" name="password" required>
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirm password <label style="color:red;"> * </label>:</label>
                    <input type="password" class="form-control" name="confirm_password" required>
                </div>


                <div class="row">
                    <div class="col-md-12" style="text-align: right;">
                        <a href="?page=accounting" class="btn btn-primary">Cancel</a>
                        <input type="submit" name="submit" value="Continue" class="btn btn-default" />
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
    <br>
</form>