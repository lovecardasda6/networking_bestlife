<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li><a href="#" style="color:white">Accounts</a></li>
    <li class="active" style="color:white">Accounting</li>
</ol>
<div class="row">
    <div class="col-md-8">
        <form method="POST">
            <div class="row">
                <div class="col-md-8">
                    <input type="text" class="form form-control" name="input" placeholder="Search Account">
                </div>
                <input type="submit" value="Search" name="search" class="btn btn-default" />
                <a href="?page=accounting">
                    <input type="button" value="Cancel" name="search" class="btn btn-primary" />
                </a>
            </div>
        </form>
    </div>

    <div class="col-md-4" style="text-align: right;">
        <a href="?page=create_accounting_account">
            <input type="button" class="btn btn-default" value="Archive Acc.">
        </a>
        <a href="?page=create_accounting_account">
            <input type="button" class="btn btn-default" value="Create account">
        </a>
    </div>
</div>

<hr />

<?php
if (isset($_GET['success']) && isset($_GET['action']) && $_GET['success'] == 1 && $_GET['action'] == "disable") :
?>
    <div style="background-color:green; padding: .8em; border-radius:5px; color:white;">
        Accounting users successfully disabled.
    </div>
    <br />
<?php
endif;
?>

<table class="table">
    <thead>
        <tr>
            <td>Acc. ID</td>
            <td>Lastname</td>
            <td>Firstname</td>
            <td>Middlename</td>
            <td>Username</td>
            <td>Registered</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
        <?php
        $system_users = $system_user->system_users('Accounting');
        if ($system_users == null) {
            exit;
        }
        foreach ($system_users as $user) :
        ?>
            <tr>
                <td><?= $user['id'] ?></td>
                <td><?= $user['lastname'] ?></td>
                <td><?= $user['firstname'] ?></td>
                <td><?= $user['middlename'] ?></td>
                <td><?= $user['username'] ?></td>
                <td><?= $user['created'] ?></td>
                <!-- <td> -->
                    <!-- <a href="?page=accounting&action=disable&id=<?= $user['id'] ?>">Disable Account</a> -->
                <!-- </td> -->
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>