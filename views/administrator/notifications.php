

<ol class="breadcrumb" style="background-color: #00674b;">
    <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
    <li class="active" style="color:white">Notifications</li>
</ol>


<?php
$notification = $notifications->notification();
if ($notification != null) :
    foreach ($notification as $value) :
?>
        <div class="panel panel-default">
            <div class="panel-body">
                <i class="fa fa-user-o" aria-hidden="true" style="font-size: 1.5em; color:blue;"></i> &nbsp;
                <a href="?page=view_members_account&account_id=<?= $value['account_id'] ?>">
                    <?= $value['lastname'] ?>
                    <?= $value['firstname'] ?>
                    <?= $value['middlename'] ?>
                    uploaded a document file. Waiting for your confirmation.
                </a>
            </div>
        </div>

<?php
    endforeach;
endif;
?>