<?php
$user_id = $_GET['id'];
$con = connectionString();
$query = "SELECT accounts.id as `id`, lastname, firstname, middlename, package FROM accounts INNER JOIN packages ON packages.id = accounts.package_id WHERE accounts.id = '{$user_id}'";
$res = mysqli_query($con, $query);
$row = mysqli_fetch_array($res, MYSQLI_ASSOC);
list('id' => $id, 'lastname' => $lastname,  'firstname' => $firstname, 'middlename' => $middlename, 'package' => $package) = $row;

$downlineTree = new downlineTree();
?>
<h1 style="font-family:creamCake; font-size: 5em;">Binary Tree</h1>
<label style="font-size: 1.2em; font-weight:bold;">
    <?= $lastname . ', ' . $firstname . ' ' . $middlename ?>
</label>
<br />
<label style="font-size: .8em;">
    ID : <?= $id ?>
</label>

<div style="overflow-x:scroll;border-radius: 7px;background: silver;">
    <div style="width:2000px;">
        <div class="tree">
            <ul>
                <li>
                    <div class="card">
                        <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                        <p class="title"></p>
                        <p><?= ucwords(strtolower($lastname . ', ' . $firstname . ' ' . $middlename)) ?></p>
                        <p><button class="package-type"><?= ucwords(strtolower($package)) ?></button></p>
                    </div>
                    <?= $downlineTree->downline($id, $id) ?>
                </li>
            </ul>
        </div>
    </div>
</div>