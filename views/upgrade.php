<h1 style="font-family:creamCake; font-size: 5em;">Upgrade Account</h1>

<?php
if ($error) :
?>
    <div style="background-color:#b70000; padding: 1em; border-radius:5px; color:white;">
        <?= $message ?>
    </div>
<?php
elseif (isset($_GET['token']) && $_GET['token'] == 0) :
?>
    <div style="background-color:#b70000; padding: 10px; border-radius:5px; color:white;">
        Invalid security token. Please try again.
    </div>
<?php
elseif (isset($_GET['success']) && $_GET['success'] == 1) :
?>
    <div style="background-color:#003302; padding: 10px; border-radius:5px; color:white;">
        Account successfully upgraded.
    </div>
<?php
endif;
?>
<div class="row">
    <div class="tiles">
        <div id="title">
            Upgrade Account
        </div>
        <hr />
        <form method="post">
            <input type="hidden" name="csrf_token" value="<?= CsrfToken::generate_csrf_token() ?>" />
            <div>
                <div class="form-group">
                    <label for="lastname">Registration Code:</label>
                    <input class="form-control" type="text" name="registration_code" value="<?= ($logs) ? $logs : '' ?>" placeholder="Registration Code" />
                </div>
            </div>
            <hr>
            <a href="?page=wallet">
                <button class="btn btn-danger" type="button">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> Cancel
                </button>
            </a>
            <button type="submit" class="btn btn-default" name="upgrade"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i> Upgrade</a>
        </form>
    </div>
</div>