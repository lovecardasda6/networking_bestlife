<h1 style="font-weight:bold;">
    ABOUT US
</h1>
<div style="font-size: 1.3em;">
    <p>
    </p>
    <br />
</div>

<br />
<br />
<br />

<h1 style="font-weight:bold;">
    PRIVACY AGREEMENT
</h1>
<div style="font-size: 1.3em;">
    <p>
        Best Life Essential has created the following Privacy Policy to let you know what information we collect when you visit our site and/or use the Best Life Essential Services, why we collect it and how it is used and stored. We are committed to ensuring that your privacy is safeguarded, and we are transparent as to how we process your personal information. This Privacy Policy takes into consideration the rights and obligations as outlined under the Data Privacy Act 2012 (Republic Act 10173), its Implementing Regulations, and the laws of the Philippines.
    </p>
    <br />
    <p>
        Personal information simply means any information or data that can be used to distinguish, identify or contact you.
    </p>
    <br />
    <p>
        By using this website, you consent to the data practices prescribed in this statement. We may periodically make changes to this Privacy Policy that we will include on this page. It is your responsibility to review this Privacy Policy frequently and remain informed about any changes to it. We encourage you to visit this page often.
    </p>


    <br />
    <br />
    <h3 style="font-weight:bold;">WHY WE COLLECT PERSONAL INFORMATION AND HOW WE USE IT</h3>
    <p>
        Our primary purpose in collecting personal information is to provide you with a secure, efficient, and to identify who is using our website and/or services.
        <ul>
            <li>
                Process transactions and send notices about your transactions;
            </li>
            <li>
                Resolve disputes, collect fees, and troubleshoot problems;
            </li>
            <li>
                Prevent potentially prohibited or illegal activities;
            </li>
            <li>
                Customize, measure, and improve Best Life Essential services and the content and layout of our website and applications;
            </li>
        </ul>
    </p>
    <br />
    <p>
        We will not sell or rent your personal information to third-parties for their marketing purposes without your explicit consent.
    </p>

</div>