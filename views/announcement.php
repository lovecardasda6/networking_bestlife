<?php
$announcement = $AnnouncementController->fetch();
if ($announcement != null) :
?>
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="border-bottom:none">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">ANNOUNCEMENT</h4>
        </div>
        <div class="modal-body">
          <p><?= date('F d,Y') ?></p>
          <br>
          <p><?= $announcement['subject'] ?>,</p>
          <br>
          <pre style="white-space: pre-wrap; 
          background-color: rgba(0,0,0,0); 
          border: none;
          word-break:keep-all;
          padding:0px;
          margin:0px;
          font-family:Arial, Helvetica, sans-serif;"><?= $announcement['content'] ?></pre>
          </br>
          </br>
          <p><?= ucwords(strtolower($announcement['signed_by'])) ?></p>
          <p>
            <?= $announcement['position'] ?>
          </p>
        </div>
        <div class="modal-footer" style="border-top:none">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>

    </div>
  </div>

  <script>
    $(document).ready(function() {
      $("#myModal").modal('show');
    });
  </script>

<?php
endif;
?>