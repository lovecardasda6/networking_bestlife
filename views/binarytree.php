<h1 style="font-family:creamCake; font-size: 5em;">Binary Tree</h1>
<br />
<?php
if (isset($_GET['invalid']) && $_GET['invalid'] == 1) :
?>
    <div style="background-color:#af0000; padding: .8em; border-radius:10px; color:white;">
        Invalid selected position and is already taken.
    </div>
    <br />
<?php elseif (isset($_GET['token']) && $_GET['token'] == 0) : ?>
    <div style="background-color:#a91111; padding: 1em; border-radius:5px; color:white; width: 100%;">
        Invalid security token. Action anomaly detected. Please try again.
    </div>
    <br />
<?php
endif;
?>
<?php
if (isset($_GET['action']) && isset($_GET['success']) && $_GET['action'] == 'add' && $_GET['success'] == 1) :
?>
    <div style="background-color:green; padding: .8em; border-radius:5px; color:white;">
        Account successfully registered.
    </div>
    <br />
<?php
endif;
?>
<label style="font-size: 1em; font-weight:bold;">
    Account name : &nbsp;
</label><span><?= $name ?></span>
<br />
<label style="font-size: 1em; font-weight:bold;">
    Account ID : &nbsp;
</label><span><?= $acc_id ?></span>
<br />
<label style="font-size: .8em;">

</label>
<?php
$binaryTree = new binaryTrees();
?>

<div style="overflow-x:scroll;border-radius: 7px;background-color: #353535;">
    <div style="width:4000px;zoom:0.8">
        <div class="tree">
            <ul>
                <li>
                    <div class="card">
                        <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                        <p class="title"></p>
                        <p><?= ucwords(strtolower($name)) ?></p>
                        <p><button class="package-type"><?= $package ?></button></p>
                    </div>
                    <?php
                    $binaryTree->binaryTree($acc_id, $acc_id);
                    ?>
                </li>
            </ul>
        </div>
    </div>
</div>