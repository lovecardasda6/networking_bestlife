<?php
require_once __DIR__ . '/../functions/redirect.php';
$indirect = new indirect();
$datatable = $indirect->indirect_history($acc_id);
$no = 1;
?>

<a href="?page=binarytree">Pairing Bonus/</a>

<br />
<br />


<label style="font-size: 1.2em; font-weight:bold;">
    <?= $name ?>
</label>
<br />
<label style="font-size: .8em;">
    ID : <?= $id ?>
</label>

<Br />
<Br />

<div>
    <table>
        <thead>
            <th>No</th>
            <th>Transaction No</th>
            <th>Transaction Date</th>
            <th>From</th>
            <th>Amount</th>
        </thead>
        <tbody>
            <?php foreach ($datatable as $data) : ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $data['id'] ?></td>
                    <td><?php echo date('F d,Y', strtotime($data['created'])) ?></td>
                    <td><?php echo ucwords(strtolower($data['firstname'] . ' ' . $data['lastname'])) ?></td>
                    <td><?php echo number_format($data['id'], 2) ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>