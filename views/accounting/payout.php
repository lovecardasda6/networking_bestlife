<div class="col-md-12">
    <ol class="breadcrumb" style="background-color: #00674b;">
        <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
        <li class="active" style="color:white">Payout</li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <form method="POST">
                <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:1.2em; font-weight: 300;">Search : </label>
                    </div>
                    <div class="col-md-7">
                        <input class="form form-control" type="date" name="date" id="date" />
                    </div>
                    <div class="col-md-1">
                        <input class="btn btn-default" type="submit" name="search_transactions" value="Search">
                    </div>
                </div>
            </form>
        </div>
        <?php if ($search_date != null && $payouts != null) : ?>
            <div class="col-md-6" style="text-align: right;">
                <form method="POST">
                    <input type="hidden" name="date" value="<?= $search_date ?>" />
                    <input type="submit" class="btn btn-default" name="export_transactions" value="Export transactions" />
                </form>
            </div>
        <?php endif; ?>
    </div>
    <hr />
    <?php if (isset($_GET['success']) && $_GET['success'] == 1) : ?>
        <div style="background-color:#003302; padding: 10px; border-radius:5px; color:white;">
            Cashout successfully confirmed.<br />
        </div>
        <br />
    <?php endif; ?>
    <table class="table table-bordered">
        <thead>
            <tr>
                <td>Acc. ID</td>
                <td>Name</td>
                <td>Wallet Balance</td>
                <td>Cashout </td>
                <td>Trans. Fee(%)</td>
                <td>Deduction</td>
                <td>Net Amount</td>
                <td>Date</td>
                <td>Action</td>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($payouts != null) :
                foreach ($payouts as $payout) :
                    $cashout_id = $payout['cashout_id'];
                    $account_id = $payout['account_id'];
                    $lastname = $payout['lastname'];
                    $firstname = $payout['firstname'];
                    $middlename = $payout['middlename'];
                    $wallet = $payout['balance'];
                    $cashout_amount = $payout['amount'];
                    $fee = $payout['fee'];
                    $net_amount = $payout['receivable_amount'];
                    $deduction = $cashout_amount - $payout['receivable_amount'];
                    $created = $payout['created'];


            ?>
                    <tr>
                        <td><a href='?page=account_information&account_id={$account_id}'><?= $account_id ?></a></td>
                        <td><?= $lastname . ',&nbsp;' . $firstname . '&nbsp;' . $middlename ?></td>
                        <td>PHP <?= number_format($wallet, 2) ?></td>
                        <td>PHP <?= number_format($cashout_amount, 2) ?></td>
                        <td><?= $fee ?> (%)</td>
                        <td>PHP <?= number_format($deduction, 2) ?></td>
                        <td>PHP <?= number_format($net_amount, 2) ?></td>
                        <td><?= $created ?></td>
                        <td>
                            <a target='_blank' href='?page=confirm_payout&cashout_id=<?= base64_encode($cashout_id) ?>&account_id=<?= base64_encode($account_id) ?>'>
                                <button class='btn btn-primary' type='button' onclick='display()'>
                                    View
                                </button>
                            </a>
                        </td>
                    </tr>
            <?php endforeach;
            endif; ?>
        </tbody>
    </table>
</div>