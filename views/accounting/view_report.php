<h1>Transaction</h1>
<hr>

<div class="row">
    <div class="col-md-4">
        <h3>Account Information</h3>
        <hr />
        Account ID : <label for="name">
            <?= $_GET['account_id'] ?>
        </label>
        <Br />
        Account Name : <label for="name">
            <?= $account_details['lastname'] ?>, <?= $account_details['firstname'] ?> <?= $account_details['middlename'] ?>
        </label>
        <Br />
        Contact No. : <label for="name">
            <?= $account_details['contact'] ?>
        </label>
        <Br />
        Package: <label for="name">
            <?= $account_details['package'] ?>
        </label>

    </div>

    <div class="col-md-4">
        <h3>Encashment Details</h3>
        <hr />
        <?php
        $encashment_mode = $confirm_payout->encashment_mode();
        if ($encashment_mode['encashment_mode'] == "Bank") :
            $bank = $confirm_payout->bankEncashment();
        ?>
            Bank Center : <label for="name">
                <?= $bank['bank_name'] ?>
            </label>
            <Br />
            Account Name : <label for="name">
                <?= $bank['account_name'] ?>
            </label>
            <Br />
            Account Number : <label for="name">
                <?= $bank['account_number'] ?>
            </label>
            <Br />
        <?php
        elseif ($encashment_mode['encashment_mode'] == "Remittance") :
            $remittance = $confirm_payout->remittanceEncashment();
        ?>
            Remittance Center : <label for="name">
                <?= $remittance['remittance_center'] ?>
            </label>
            <Br />
            Receiver's Name : <label for="name">
                <?= $remittance['receivers_name'] ?>
            </label>
            <Br />
            Contact No. : <label for="name">
                <?= $remittance['contact_number'] ?>
            </label>
            <Br />
        <?php
        elseif ($encashment_mode['encashment_mode'] == "EWallet") :
            $ewallet = $confirm_payout->eWalletEncashment();
        ?>
            EWallet Center : <label for="name">
                <?= $ewallet['ewallet_center'] ?>
            </label>
            <Br />
            EWallet No. : <label for="name">
                <?= $ewallet['ewallet_number'] ?>
            </label>
            <Br />
        <?php
        endif;
        ?>

    </div>
</div>
<br />
<br />
<br />
<?php
$direct_referral_income_details = $confirm_payout->direct_referral_income_details();
var_dump($direct_referral_income_details);
exit;
$pairing_income_details = $confirm_payout->pairing_income_details();
$indirect_income_details = $confirm_payout->indirect_income_details();
$direct_income_details = $confirm_payout->direct_income_details();

?>

<h3>Income Details</h3>
<br />
<hr />
<div class="row">
    <div class="col-md-3">
        <h4>Direct Referral Income</h4>
        <hr />
        Direct Referral Income (Cash): <label for="name">
            PHP <?= number_format($direct_referral_income_details->amount, 2) ?>
        </label>
        <Br />
        Direct Referral Income (GC) : <label for="name">
            PHP <?= number_format($direct_referral_income_details->product, 2) ?>
        </label>
    </div>
    <div class="col-md-3">
        <h4>Pairing Bonus Income</h4>
        <hr />
        Income (Cash) : <label for="name">
            PHP <?= number_format($pairing_income_details->income_cash, 2) ?>
        </label>
        <Br />
        Income (GC) : <label for="name">
            PHP <?= number_format($pairing_income_details->income_gc, 2) ?>
        </label>
        <Br />
        Flushout : <label for="name">
            PHP <?= number_format($pairing_income_details->flushout, 2) ?>
        </label>
        <Br />
        Overall VP (Left): <label for="name">
            <?= number_format($pairing_income_details->left_vp) ?> Value Points
        </label>
        <Br />
        Overall VP (Right): <label for="name">
            <?= number_format($pairing_income_details->right_vp) ?> Value Points
        </label>
    </div>
    <div class="col-md-3">
        <h4>Indirect Bonus Income</h4>
        <hr />
        Indirect Bonus. : <label for="name">
            PHP <?= number_format($indirect_income_details->amount, 2) ?>
        </label>
    </div>
    <div class="col-md-3">
        <h4>Direct Bonus Income</h4>
        <hr />
        Direct Bonus: <label for="name">
            PHP<?= number_format($direct_income_details->amount, 2) ?>
        </label>
    </div>
</div>
<hr />
Total Income (Cash) : <label for="name">
    PHP<?= number_format(($direct_referral_income_details->amount +
            $pairing_income_details->income_cash +
            $indirect_income_details->amount +
            $direct_income_details->amount), 2) ?>
</label>
<Br />
Total Income (GC) : <label for="name">
    PHP<?= number_format(($direct_referral_income_details->product +
            $pairing_income_details->income_gc), 2) ?>
</label>
<br />
<br />
<br />
<h3>Encashment Details</h3>
<hr />
<form method="POST">
    <div class="row">
        <div class='col-md-12'>
            <table class="table">
                <thead>
                    <tr>
                        <td>Gross Income (PHP)</td>
                        <td>Wallet Balance (PHP)</td>
                        <td>Cashout Amount (PHP)</td>
                        <td>Transaction Fee (%)</td>
                        <td>Deduction (PHP)</td>
                        <td>Net Amount (PHP)</td>
                        <td>Remarks <label style='color:red'>*</label></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?= number_format(($wallet->fetch_wallet($account_id) + $payout_details->cashout_amount),2) ?>
                        </td>
                        <td><?= number_format($wallet->fetch_wallet($account_id),2) ?></td>
                        <td><?= number_format($payout_details->cashout_amount, 2) ?></td>
                        <td><?= $payout_details->transaction_fee ?></td>
                        <td><?= number_format($payout_details->deduction, 2) ?></td>
                        <td><?= number_format($payout_details->receivable_amount, 2) ?></td>
                        <td>
                            <textarea class="form-control" name="remarks"><?= isset($_SESSION['admin_name']) ? $_SESSION['admin_name'] : null ?></textarea>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div class='col-md-12' style="text-align:right;">
            <div class='form-group'>
                <a href="?page=payout">
                    <button type="button" class='btn btn-primary'>Cancel</button>
                </a>
                <input type='submit' class='btn btn-default' name='confirm_transaction' value='Confirm Transaction'>
            </div>
        </div>
    </div>
</form>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>