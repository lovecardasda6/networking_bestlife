<ol class="breadcrumb" style="background-color: #00674b;">
    <li class="active" style="color:white">Report</li>
</ol>
<div class="row">
    <div class="col-md-6">
        <form method="POST">
            <div class="row">
                <div class="col-md-8">
                    <input class="form form-control" type="date" name="date" placeholder="Date" />
                </div>
                <div class="col-md-2">
                    <input class="btn btn-default" type="submit" name="search" value="Search">
                </div>
            </div>
        </form>
    </div>
</div>
<hr />
<br />
<div class="row">
    <div class="col-md-12">
        <div class="col-md-2">
            <form action="?page=reports">
                <input class="btn btn-default" type="submit" name="search" value="View all reports">
            </form>
        </div>

        <div class="col-md-1">
            <input class="btn btn-default" type="submit" name="search" value="Search">
        </div>

        <div class="col-md-1">
            <form method="Post">
                <input type="hidden" name="date" value="<?= $date ?>" />
                <input class="btn btn-default" type="submit" name="export_to_excel" value="Export to excel">
            </form>
        </div>
    </div>
</div>
<hr />
<table class="table table-bordered">
    <thead>
        <tr>
            <td>Name</td>
            <td>Cashout </td>
            <td>Fee(%)</td>
            <td>Deduction</td>
            <td>Net Amount</td>
            <td>Approve On</td>
            <td>Approve By</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($reports != null) :
            foreach ($reports as $rep) :  ?>
                <tr>
                    <td><?= $rep['lastname'] ?>, <?= $rep['firstname'] ?> <?= $rep['middlename'] ?></td>
                    <td>Php <?= number_format($rep['amount'], 2) ?></td>
                    <td><?= $rep['fee'] ?> % </td>
                    <td>Php <?= number_format($rep['amount'] - $rep['receivable_amount'], 2) ?></td>
                    <td>Php <?= number_format($rep['receivable_amount'], 2) ?></td>
                    <td><?= $rep['updated'] ?></td>
                    <td>
                        <?php
                        $approved_by = $report->fetch_approved_by($rep['confirm_id']);
                        echo $approved_by['lastname'] . ', ' . $approved_by['firstname'] . ' ' . $approved_by['middlename'];
                        ?>
                    </td>
                    <td>
                        <a href="?page=view_report&account_id=<?= $rep['account_id'] ?>&id=<?= $rep['id'] ?>">
                            View Report
                        </a>
                    </td>
                </tr>
        <?php endforeach;
        endif; ?>
    </tbody>
</table>
</div>