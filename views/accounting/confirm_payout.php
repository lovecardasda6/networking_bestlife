<h1>Transaction</h1>
<hr>
<?php if ($error) : ?>
    <div style="background-color:#8a0e0e; padding: 10px; border-radius:5px; color:white;">
        Invalid cashout transaction. If you found this as a error. Please contact the administrator.
    </div>
    <br />
<?php endif; ?>
<div class="row">
    <div class="col-md-4">
        <h3>Account Information</h3>
        <hr />
        Account ID : <label for="name">
            <?= $cashout['account_id'] ?>
        </label>
        <Br />
        Account Name : <label for="name">
            <?= $cashout['lastname'] . ',&nbsp;' . $cashout['firstname'] . '&nbsp;' . $cashout['middlename'] ?>
        </label>
        <Br />
        Contact No. : <label for="name">
            <?= $cashout['contact'] ?>
        </label>
        <Br />
        Package: <label for="name">
            <?= $cashout['package'] ?>
        </label>

    </div>

    <div class="col-md-4">
        <h3>Encashment Details</h3>
        <hr />
        <?php
        $encashment_mode = $confirm_payout->encashment_mode();
        if ($encashment_mode['encashment_mode'] == "Bank") :
            $bank = $confirm_payout->bankEncashment();
        ?>
            Bank Center : <label for="name">
                <?= $bank['bank_name'] ?>
            </label>
            <Br />
            Account Name : <label for="name">
                <?= $bank['account_name'] ?>
            </label>
            <Br />
            Account Number : <label for="name">
                <?= $bank['account_number'] ?>
            </label>
            <Br />
        <?php
        elseif ($encashment_mode['encashment_mode'] == "Remittance") :
            $remittance = $confirm_payout->remittanceEncashment();
        ?>
            Remittance Center : <label for="name">
                <?= $remittance['remittance_center'] ?>
            </label>
            <Br />
            Receiver's Name : <label for="name">
                <?= $remittance['receivers_name'] ?>
            </label>
            <Br />
            Contact No. : <label for="name">
                <?= $remittance['contact_number'] ?>
            </label>
            <Br />
        <?php
        elseif ($encashment_mode['encashment_mode'] == "EWallet") :
            $ewallet = $confirm_payout->eWalletEncashment();
        ?>
            EWallet Center : <label for="name">
                <?= $ewallet['ewallet_center'] ?>
            </label>
            <Br />
            EWallet No. : <label for="name">
                <?= $ewallet['ewallet_number'] ?>
            </label>
            <Br />
        <?php
        endif;
        ?>

    </div>
</div>
<br />
<br />
<br />
<?php
$direct_referral_income_details = $confirm_payout->direct_referral_income_details();
$pairing_income_details = $confirm_payout->pairing_income_details();
$indirect_income_details = $confirm_payout->indirect_income_details();
$direct_income_details = $confirm_payout->direct_income_details();

?>
<div>
    <h3>Income Details</h3>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <h4>Direct Referral Income</h4>
            <hr />
            Direct Referral Income (Cash): <label for="name">
                PHP <?= ($direct_referral_income_details) ? number_format($direct_referral_income_details->amount, 2) : '0' ?>
            </label>
            <Br />
            Direct Referral Income (GC) : <label for="name">
                PHP <?= ($direct_referral_income_details) ? number_format($direct_referral_income_details->product, 2) : '0' ?>
            </label>
        </div>
        <div class="col-md-3">
            <h4>Pairing Bonus Income</h4>
            <hr />
            Income (Cash) : <label for="name">
                PHP <?= ($pairing_income_details) ? number_format($pairing_income_details->income_cash, 2) : '0' ?>
            </label>
            <Br />
            Income (GC) : <label for="name">
                PHP <?= ($pairing_income_details) ?  number_format($pairing_income_details->income_gc, 2) : '0' ?>
            </label>
            <Br />
            Flushout : <label for="name">
                PHP <?= ($pairing_income_details) ?  number_format($pairing_income_details->flushout, 2) : '0' ?>
            </label>
            <Br />
            Overall VP (Left): <label for="name">
                <?= ($confirm_payout->pairingBonusLeftVP()) ?  number_format($confirm_payout->pairingBonusLeftVP()) : '0' ?> Value Points
            </label>
            <Br />
            Overall VP (Right): <label for="name">
                <?= ($confirm_payout->pairingBonusRightVP()) ?  number_format($confirm_payout->pairingBonusRightVP()) : '0' ?> Value Points
            </label>
        </div>
        <div class="col-md-3">
            <h4>Indirect Bonus Income</h4>
            <hr />
            Indirect Bonus. : <label for="name">
                PHP <?= ($indirect_income_details) ? number_format($indirect_income_details->amount, 2) : '0' ?>
            </label>
        </div>
        <div class="col-md-3">
            <h4>Direct Bonus Income</h4>
            <hr />
            Direct Bonus: <label for="name">
                PHP<?= ($direct_income_details) ? number_format($direct_income_details->amount, 2) : '0' ?>
            </label>
        </div>
    </div>
    <hr />
    Total Income (Cash) : <label for="name">
        PHP<?= number_format((
                (($direct_referral_income_details) ? $direct_referral_income_details->amount : 0)
                +
                (($pairing_income_details) ? $pairing_income_details->income_cash : 0)
                +
                (($indirect_income_details) ? $indirect_income_details->amount : 0)
                +
                (($direct_income_details) ? $direct_income_details->amount : 0)), 2) ?>
    </label>
    <Br />
    Total Income (GC) : <label for="name">
        PHP<?= number_format((
                (($direct_referral_income_details) ? $direct_referral_income_details->product : 0)
                +
                (($pairing_income_details) ? $pairing_income_details->income_gc : 0)), 2) ?>
    </label>
</div>
<br />
<br />
<br />
<h3>Cashout Details</h3>
<label>Cashout On &nbsp;:&nbsp; <?= $cashout['cashout_created'] ?></label>
<hr />
<form method="POST">
    <input type="hidden" name="created_on" value="<?= $cashout['cashout_created'] ?>" />
    <div class="row">
        <div class='col-md-12'>
            <table class="table">
                <thead>
                    <tr>
                        <td>Gross Income (PHP)</td>
                        <td>Wallet Balance (PHP)</td>
                        <td>Cashout Amount (PHP)</td>
                        <td>Transaction Fee (%)</td>
                        <td>Deduction (PHP)</td>
                        <td>Net Amount (PHP)</td>
                        <td>Remarks <label style='color:red'>*</label></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?= number_format(($cashout['balance'] + $cashout['amount']), 2) ?>
                        </td>
                        <td><?= number_format($cashout['balance'], 2) ?></td>
                        <td><?= number_format($cashout['amount'], 2) ?></td>
                        <td><?= $cashout['fee'] ?></td>
                        <td>
                            <?= number_format(($cashout['amount'] - $cashout['receivable_amount']), 2) ?>
                        </td>
                        <td><?= number_format($cashout['receivable_amount'], 2) ?></td>
                        <td>
                            <textarea class="form-control" name="remarks"><?= isset($_SESSION['admin_name']) ? $_SESSION['admin_name'] : null ?></textarea>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
        <?php if ($cashout['status'] == 'Pending') : ?>
            <div class='col-md-12' style="text-align:right;">
                <div class='form-group'>
                    <a href="?page=payout">
                        <button type="button" class='btn btn-primary'>Cancel</button>
                    </a>
                    <input type='submit' class='btn btn-default' name='confirm_transaction' value='Confirm Transaction'>
                </div>
            </div>
        <?php endif; ?>
    </div>
</form>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />