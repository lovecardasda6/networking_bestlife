<?php


?>

<ol class="breadcrumb" style="background-color: #00674b;">
    <li class="active" style="color:white">Report</li>
</ol>
<div class="row">
    <div class="col-md-6">
        <form method="POST">
            <div class="row">
                <div class="col-md-8">
                    <input class="form form-control" type="date" name="date" placeholder="Date" />
                </div>
                <div class="col-md-2">
                    <input class="btn btn-default" type="submit" name="search" value="Search">
                </div>
            </div>
        </form>
    </div>

    <div class="col-md-6" style="text-align: right;">
        <a href="?page=reports">
            <input class="btn btn-default" type="button" name="search" value="View all reports">
        </a>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-md-1">
        <form method="Post">
            <input type="hidden" name="date" value="<?= $date ?>" />
            <input class="btn btn-default" type="submit" name="export_to_excel" value="Export to excel">
        </form>
    </div>
</div>
</div>
<hr />
<table class="table table-bordered">
    <thead>
        <tr>
            <td>Name</td>
            <td>Cashout </td>
            <td>Fee(%)</td>
            <td>Deduction</td>
            <td>Net Amount</td>
            <td>Approve On</td>
            <td>Approve By</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
        <?php
        if ($reports != null) :
            foreach ($reports as $info) : ?>
                <tr>
                    <td><?= $info['lastname'] ?>, <?= $info['firstname'] ?> <?= $info['middlename'] ?></td>
                    <td>Php <?= number_format($info['amount'], 2) ?></td>
                    <td><?= $info['fee'] ?> % </td>
                    <td>Php <?= number_format($info['amount'] - $info['receivable_amount'], 2) ?></td>
                    <td>Php <?= number_format($info['receivable_amount'], 2) ?></td>
                    <td><?= $info['updated'] ?></td>
                    <td>
                        <?php
                        $approved_by = $report->fetch_approved_by($info['confirm_id']);
                        echo $approved_by['lastname'] . ', ' . $approved_by['firstname'] . ' ' . $approved_by['middlename'];
                        ?>
                    </td>
                    <td>
                    </td>
                </tr>
        <?php endforeach;
        endif; ?>
    </tbody>
</table>
</div>