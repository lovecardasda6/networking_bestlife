<?php
$payout = new payout();
?>
<div class="col-md-12">

    <ol class="breadcrumb" style="background-color: #00674b;">
        <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
        <li class="active" style="color:white">Pending Transaction</li>
    </ol>
    <hr />
    <?php if (isset($_GET['success']) && $_GET['success'] == 1) : ?>
        <div style="background-color:#003302; padding: 10px; border-radius:5px; color:white;">
            Cashout successfully confirmed.<br />
        </div>
        <br />
    <?php endif; ?>
    <table class="table table-bordered">
        <thead>
            <tr>
                <td>Acc. ID</td>
                <td>Name</td>
                <td>Wallet</td>
                <td>Cashout </td>
                <td>Trans. Fee(%)</td>
                <td>Deduction</td>
                <td>Recievable Amount</td>
                <td>Date</td>
                <td>Status</td>
            </tr>
        </thead>
        <tbody>
            <?= $payout->pending_transactions() ?>
        </tbody>
    </table>
</div>