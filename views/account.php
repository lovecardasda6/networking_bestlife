<h1 style="font-family:creamCake; font-size: 5em;">My Account</h1>


<?php
?>

<br />

<?php if ($error) : ?>
    <div style="background-color:#a91111; padding: 1em; border-radius:5px; color:white; width: 100%;">
        <?= $message ?>
    </div>
    <br />
<?php elseif (isset($_GET['token']) && $_GET['token'] == 0) : ?>
    <div style="background-color:#a91111; padding: 1em; border-radius:5px; color:white; width: 100%;">
        Invalid security token. Please try again.
    </div>
    <br />
<?php elseif (isset($_GET['success']) && isset($_GET['details']) && $_GET['success'] == 1) : ?>
    <div style="background-color:green; padding: 1em; border-radius:5px; color:white; width: 100%;">
        <?= base64_decode($_GET['details']) ?>
    </div>
    <br />
<?php endif; ?>


<br />
<br />
<div class="row">
    <div class="col-sm-12 col-md-4 col-lg-4">
        <div class="panel panel-default" style="border-radius:6px;">
            <div class="panel-body profile" style="border-top-left-radius:6px;border-top-right-radius:6px;margin-bottom: 40px;background: linear-gradient(to bottom, #00674b 50%, white 50%);">
                <div style="text-align:center;">
                    <br />
                    <br />
                    <img src="./assets/user.png" class="img-rounded" alt="Cinque Terre" style="width:100px;margin-top:25px;background:#fff;border-radius:50px;box-shadow:0 4px 10px 0 rgba(0,0,0,0.2), 0 4px 20px 0 rgba(0,0,0,0.19)">
                    <h3><?= ucwords(strtolower($name)) ?></h3>
                    <h4 style="font-size: 150%;">(&nbsp;<?= $acc_id ?>&nbsp;)</h4>
                    <hr />
                    <h4 style="font-size: 150%;"><?= $package ?> Account</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-8 col-lg-8">
        <ul class="nav nav-tabs">
            <li id="nav_tabs" class="active"><a data-toggle="tab" href="#account_information">Personal Information</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#login_information">Login Information</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#encashment">Encashments</a></li>
            <?php if (!Document::fetch($acc_id)) : ?>
                <li id="nav_tabs"><a data-toggle="tab" href="#document">Document</a></li>
            <?php endif; ?>
        </ul>
        <br>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="tab-content">
                    <!------------------------------------------------------>
                    <div id="account_information" class="tab-pane fade in active">
                        <div class="col-md-12 col-lg-12">
                            <div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Personal Information</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label>Account Name</label>
                                            <input type="text" class="form-control" value="<?= $Account['lastname'] . ' ' . $Account['firstname'] . ' ' . $Account['middlename'] ?>" placeholder="Account Name" readonly disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Referral Name</label>
                                            <input type="text" class="form-control" value="<?= ($Referral) ? $Referral['lastname'] . ' ' . $Referral['firstname'] . ' ' . $Referral['middlename'] : '' ?>" placeholder="Referral Name" readonly disabled>
                                        </div>
                                        <form method="post">
                                            <div class="form-group">
                                                <label>Currency</label>
                                                <select name="currency" class="form form-control">
                                                    <option value="PHP" <?= ($AccountCurrency['currency'] == "PHP" || $AccountCurrency['currency'] == null) ? 'selected' : '' ?>>PHP</option>
                                                    <option value="USD" <?= ($AccountCurrency['currency'] == "USD") ? 'selected' : '' ?>>USD</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Contact Number</label>
                                                <input class="form form-control" name="contact" value="<?= $Account['contact'] ?>" />
                                            </div>
                                            <div class="form-group">
                                                <label>Gender</label>
                                                <select name="gender" class="form form-control">
                                                    <option value="Male" <?= ($Account['gender'] == "Male") ? "selected" : "" ?>>Male</option>
                                                    <option value="Female" <?= ($Account['gender'] == "Female") ? "selected" : "" ?>>Female</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input class="form form-control" name="address" value="<?= $Account['address'] ?>" type="text" />
                                            </div>
                                            <div class="form-group">
                                                <label>Date of Birth</label>
                                                <input class="form form-control" name="date_of_birth" value="<?= $Account['date_of_birth'] ?>" type="date" />
                                            </div>
                                            <div class="form-group">
                                                <label>Place of Birth</label>
                                                <input class="form form-control" name="place_of_birth" value="<?= $Account['place_of_birth'] ?>" type="text" />
                                            </div>
                                            <hr />
                                            <div style="text-align:right;">
                                                <button type="submit" name="update_basic_information" class="btn btn-default">
                                                    Save changes
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!------------------------------------------------------>
                    <!------------------------------------------------------>
                    <div id="login_information" class="tab-pane fade">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Email Address</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form method="POST">
                                            <div class="form-group" style="margin-bottom:0px;">
                                                <div class="panel panel-default " id="email-panel" style="border-color:transparent;-webkit-box-shadow:none;margin-bottom:0px;">
                                                    <div class="panel-body" style="padding:0px">
                                                        <a class="dropdown-btn dropdown-link" id="email" style="color:black!important;padding:0px">
                                                            <label class="control-label col-sm-3 col-md-2" style="text-align:left">Email &nbsp;&nbsp;:</label>
                                                            <label class="control-label col-sm-9 col-md-10" style="text-align:left"><?= $Account['email'] ?> &nbsp;&nbsp; <button class="btn btn-default btn-xs btn-email" type="button">Edit</button></label>
                                                        </a>
                                                        <div class="dropdown-container" style="margin-top: 50px;background:transparent!important">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Current email</label>
                                                                            <input type="email" class="form-control" id="email_old" value="<?= $Account['email'] ?>" readonly>
                                                                        </div>
                                                                        <form class="form-horizontal" method="post">
                                                                            <input type="hidden" name="user_id" value="<?= $Account['user_id'] ?>">
                                                                            <div class="form-group">
                                                                                <label>New email</label>
                                                                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter new email">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-sm-offset-6 col-sm-6">
                                                                                    <button type="submit" name="submit_email" value="submit" class="btn btn-default pull-right">Submit</button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Change Password</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form method="POST">
                                            <input type="hidden" name="csrf_token" value="<?= CsrfToken::generate_csrf_token() ?>" />
                                            <div class="form-group" style="margin-bottom:0px;">
                                                <div class="panel panel-default " id="password-panel" style="border-color:transparent;-webkit-box-shadow:none;margin-bottom:0px;">
                                                    <div class="panel-body" style="padding:0px">
                                                        <a class="dropdown-btn dropdown-link" id="password" style="color:black!important;padding:0px">
                                                            <div class="col-md-6">
                                                                <p>Change password</p>
                                                                <p>
                                                                    It's a good idea to use a strong password that you're not using elsewhere
                                                                </p>
                                                            </div>
                                                            <label class="control-label col-sm-2" style="text-align:left">
                                                                &nbsp;&nbsp;
                                                                <button class="btn btn-default btn-xs btn-password" type="button">
                                                                    Edit
                                                                </button>
                                                            </label>
                                                        </a>
                                                        <div class="dropdown-container" style="margin-top: 70px;background:transparent!important">
                                                            <BR />
                                                            <form method="post">
                                                                <input type="hidden" name="user_id" value="<?= $Account['user_id'] ?>">
                                                                <div class="form-group">
                                                                    <label>Current password</label>
                                                                    <input type="password" class="form-control" name="current" id="password_old">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>New password</label>
                                                                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter new number">
                                                                </div>


                                                                <div class="form-group">
                                                                    <label>Confirm password</label>
                                                                    <input type="password" class="form-control" name="confirm" id="password" placeholder="Enter new number">
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-offset-6 col-sm-5">
                                                                        <button type="submit" name="submit_password" value="submit" class="btn btn-default pull-right">Submit</button>
                                                                    </div>
                                                                </div>

                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!------------------------------------------------------>
                    <div id="encashment" class="tab-pane fade">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <!------------------------------------------BANK------------------------------------------>
                            <?php
                            if ($EncashmentModeBank != null) :
                                foreach ($EncashmentModeBank as $bank_mode) :
                            ?>
                                    <div class="tiles" style="border:0px;">
                                        <div id="title">
                                            <?= $bank_mode['encashment_center'] ?>
                                        </div>
                                        <hr />

                                        <?php
                                        $BankAccounts = $AccountController->fetch_banks($acc_id, $bank_mode['encashment_center']);
                                        if ($BankAccounts) {
                                        ?>
                                            <form method="POST">
                                                <input type="hidden" name="bank_id" value="<?= $BankAccounts['id'] ?>" />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastname">Account Name : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="name" value="<?= $BankAccounts['account_name'] ?>" placeholder="Account Name" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="firstname">Account Number : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="number" value="<?= $BankAccounts['account_number'] ?>" placeholder="Account Number" required>
                                                        </div>
                                                        <div style="text-align: right;">
                                                            <input name="update_bank_center" value="Update Account" type="submit" class="btn btn-default" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php
                                        } else {
                                        ?>
                                            <form method="POST">
                                                <input type="hidden" name="encashment_center" value="<?= $bank_mode['encashment_center'] ?>" />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastname">Account Name : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="name" placeholder="Account Name" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="firstname">Account Number : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="number" placeholder="Account Number" required>
                                                        </div>
                                                        <div style="text-align: right;">
                                                            <input name="add_bank_center" value="Create Account" type="submit" class="btn btn-default" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php
                                        }
                                        ?>
                                    </div>
                            <?php
                                endforeach;
                            endif;
                            ?>
                            <!-------------------------------------------BANK----------------------------------------->
                            <!---------------------------------------------------------------------------------------->
                            <!---------------------------------------REMITTANCE--------------------------------------->
                            <?php
                            if ($EncashmentModeRemittance != null) :
                                foreach ($EncashmentModeRemittance as $remittance_mode) :
                            ?>
                                    <div class="tiles" style="border:0px;">
                                        <div id="title">
                                            <?= $remittance_mode['encashment_center'] ?>
                                        </div>
                                        <hr />

                                        <?php
                                        $RemittanceAccounts = $AccountController->fetch_remittance($acc_id, $remittance_mode['encashment_center']);
                                        if ($RemittanceAccounts) {
                                        ?>
                                            <form method="POST">
                                                <input type="hidden" name="remittance_id" value="<?= $RemittanceAccounts['id'] ?>" />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastname">Receiver's Name : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="name" value="<?= $RemittanceAccounts['receivers_name'] ?>" placeholder="Receiver's Name" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="firstname">Contact Number : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="number" value="<?= $RemittanceAccounts['contact_number'] ?>" placeholder="Contact Number" required>
                                                        </div>
                                                        <div style="text-align: right;">
                                                            <input name="update_remittance_center" value="Update Account" type="submit" class="btn btn-default" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php
                                        } else {
                                        ?>
                                            <form method="POST">
                                                <input type="hidden" name="encashment_center" value="<?= $remittance_mode['encashment_center'] ?>" />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="lastname">Receiver's Name : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="name" placeholder="Receiver's Name" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="firstname">Contact Number : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="number" placeholder="Contact Number" required>
                                                        </div>
                                                        <div style="text-align: right;">
                                                            <input name="add_remittance_center" value="Create Account" type="submit" class="btn btn-default" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php
                                        }
                                        ?>
                                    </div>
                            <?php
                                endforeach;
                            endif;
                            ?>
                            <!---------------------------------------REMITTANCE--------------------------------------->
                            <!---------------------------------------------------------------------------------------->
                            <!-----------------------------------------EWALLET---------------------------------------->
                            <?php
                            if ($EncashmentModeEWallet != null) :
                                foreach ($EncashmentModeEWallet as $ewallet_mode) :
                            ?>
                                    <div class="tiles" style="border:0px;">
                                        <div id="title">
                                            <?= $ewallet_mode['encashment_center'] ?>
                                        </div>
                                        <hr />

                                        <?php
                                        $EWalletAccounts = $AccountController->fetch_ewallet($acc_id, $ewallet_mode['encashment_center']);
                                        if ($EWalletAccounts) {
                                        ?>
                                            <form method="POST">
                                                <input type="hidden" name="ewallet_id" value="<?= $EWalletAccounts['id'] ?>" />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="ewallet_number">EWallet Number : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="number" value="<?= $EWalletAccounts['ewallet_number'] ?>" placeholder="EWallet Number" required>
                                                        </div>
                                                        <div style="text-align: right;">
                                                            <input name="update_ewallet_center" value="Update Account" type="submit" class="btn btn-default" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php
                                        } else {
                                        ?>
                                            <form method="POST">
                                                <input type="hidden" name="encashment_center" value="<?= $ewallet_mode['encashment_center'] ?>" />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="ewallet_number">EWallet Number : <label style="color:red;"> * </label>:</label>
                                                            <input type="text" class="form-control" name="number" placeholder="EWallet Number" required>
                                                        </div>
                                                        <div style="text-align: right;">
                                                            <input name="add_ewallet_center" value="Create Account" type="submit" class="btn btn-default" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php
                                        }
                                        ?>
                                    </div>
                            <?php
                                endforeach;
                            endif;
                            ?>
                            <!-----------------------------------------EWALLET---------------------------------------->
                            <!---------------------------------------------------------------------------------------->
                        </div>
                    </div>
                    <!------------------------------------------------------>

                    <?php if (!Document::fetch($acc_id)) : ?>
                        <div id="document" class="tab-pane fade">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Documents</h3>
                                    </div>
                                    <div class="panel-body">
                                        <ul class="list-group">
                                            <form method="post" enctype="multipart/form-data">
                                                <li class="list-group-item">
                                                    <div class="form-group" style="margin-bottom:0px;">
                                                        <div class="row">
                                                            <div class="col-xsm-12 col-sm-12 col-md-2">
                                                                <label class="control-label col-sm-12 col-md-12" style="text-align:left">Document &nbsp;&nbsp;:</label>
                                                            </div>
                                                            <div class="col-xss-10 col-sm-10 col-md-4" style="margin:0px 10px;">
                                                                <select name="document_type" class="form form-control">
                                                                    <option value="Birth Certificate">Birth Certificate</option>
                                                                    <option value="Drivers License">Driver's License</option>
                                                                    <option value="Voter's ID">Voter's ID</option>
                                                                    <option value="Any">Any</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li class="list-group-item">
                                                    <div class="form-group" style="margin-bottom:0px;">
                                                        <div class="row">
                                                            <div class="col-xsm-12 col-sm-12 col-md-2">
                                                            </div>
                                                            <div class="col-xss-10 col-sm-10 col-md-4" style="margin:0px 10px;">
                                                                <input class="form form-control" name="file_image" type="file" accept="image/*" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li class="list-group-item">
                                                    <div class="form-group" style="margin-bottom:0px;">
                                                        <div class="row">
                                                            <div class="col-xsm-12 col-sm-12 col-md-2"></div>
                                                            <div class="col-xss-10 col-sm-10 col-md-4" style="margin:0px 10px;">
                                                                <label class="control-label col-sm-12 col-md-12" style="text-align:left">Upload size of 5MB</label>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>

                                                <li class="list-group-item">
                                                    <div style="text-align:right;">
                                                        <button type="submit" name="upload_document" class="btn btn-default">
                                                            Save changes
                                                        </button>
                                                    </div>
                                                </li>
                                            </form>


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!------------------------------------------------------>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(".dropdown-link").click(function() {
        var id = this.id;
        var text = $('.btn-' + id).text();
        if (text == 'Close') {
            $('.btn-' + id).text('Edit');
            document.getElementById(id + "-panel").style.backgroundColor = "transparent";
        } else {
            $('.btn-' + id).text('Close')
            document.getElementById(id + "-panel").style.backgroundColor = "rgb(242, 242, 242)";
        }
    });
</script>