<h1 style="font-family:creamCake; font-size: 5em;">Account Registration</h1>
<br />


<?php
if ($error) :
?>
    <div style="background-color:#af0000; padding: .8em; border-radius:10px; color:white;">
        <?= $message ?>
    </div>
<?php
endif;
?>
<?php if (!$success) : ?>
    <form method="post">
        <input type="hidden" name="csrf_token" value="<?= CsrfToken::generate_csrf_token() ?>" />
        <div class="row">
            <div class="col-md-12">
                <h3>Personal Informations</h3>
                <hr />
                <div class="form-group">
                    <label for="lastname">Lastname <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" value="<?= ($logs) ? $logs->lastname : '' ?>" name="lastname" required>
                </div>
                <div class="form-group">
                    <label for="firstname">Firstname <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" value="<?= ($logs) ? $logs->firstname : '' ?>" name="firstname" required>
                </div>
                <div class="form-group">
                    <label for="middlename">Middlename :</label>
                    <input type="text" class="form-control" value="<?= ($logs) ? $logs->middlename : '' ?>" name="middlename">
                </div>
                <div class="form-group">
                    <label for="contact">Contact no :</label>
                    <input type="number" class="form-control" value="<?= ($logs) ? $logs->contact : '' ?>" name="contact_no">
                </div>
                <div class="form-group">
                    <label for="middlename">Gender :</label>
                    <select class="form-control" name="gender">
                        <option value="Male" selected>Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="address">Address :</label>
                    <input type="text" class="form-control" value="<?= ($logs) ? $logs->address : '' ?>" name="address">
                </div>
                <div class="form-group">
                    <label for="date_of_birth">Date of birth :</label>
                    <input type="date" class="form-control" value="<?= ($logs) ? $logs->date_of_birth : '07/11/2020' ?>" name="date_of_birth">
                </div>
                <div class="form-group">
                    <label for="place_of_birth">Place of birth :</label>
                    <input type="text" class="form-control" value="<?= ($logs) ? $logs->place_of_birth : '' ?>" name="place_of_birth">
                </div>
            </div>



            <div class="col-md-12">
                <br />
                <h3>Referral Information</h3>
                <hr />
                <div class="form-group">
                    <label for="reference_id">Referral id <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="reference_id" value="<?= $account_id ?>" readonly required>
                </div>
                <div class="form-group">
                    <label for="reference_name">Referral name <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="reference_name" value="<?= $name ?>" readonly required>
                </div>
                <div class="form-group">
                    <label for="upline_id">Upline id <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="upline_id" value="<?= $_GET['upline'] ?>" readonly required>
                </div>
                <div class="form-group">
                    <label for="position">Position <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" name="position" value="<?= $_GET['position'] ?>" readonly required>
                </div>
                <div class="form-group">
                    <label for="registration_code">Registration code <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" value="<?= ($logs) ? $logs->registration_code : '' ?>" name="registration_code" required>
                    <!-- <label style="color:#b70000;">Note: Do not include hyphen (-) in typing a code.</label> -->
                </div>
            </div>


            <div class="col-md-12">
                <br />
                <h3>Login Information</h3>
                <hr />
                <div class="form-group">
                    <label for="email">Username <label style="color:red;"> * </label>:</label>
                    <input type="text" class="form-control" value="<?= ($logs) ? $logs->username : '' ?>" name="username" required>
                </div>
                <div class="form-group">
                    <label for="email">Email Address:</label>
                    <input type="email" class="form-control" value="<?= ($logs) ? $logs->email_address : '' ?>" name="email_address">
                </div>
                <div class="form-group">
                    <label for="password">Password <label style="color:red;"> * </label>:</label>
                    <input type="password" class="form-control" name="password" required>
                    <label style="color:#b70000;">Notice: Password must be at least 8 character long.</label>
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirm password <label style="color:red;"> * </label>:</label>
                    <input type="password" class="form-control" name="confirm_password" required>
                </div>
            </div>


            <div class="col-md-12" style="text-align:right;">
                <hr />
                <input type="submit" name="register_account" value="Register Account" class="btn btn-default" />
            </div>
        </div>

    </form>
<?php endif; ?>
<br />
<br />
<br />
<br />
<br />