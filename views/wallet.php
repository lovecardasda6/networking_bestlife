<h1 style="font-family:creamCake; font-size: 5em;"> My Journey</h1>
<br />

<style>
    #Bronze {
        background: linear-gradient(100deg, #543d28, #fd7a00);
        color: white;
    }

    #Silver {
        background: linear-gradient(100deg, #999, #100f0f);
        color: white;
    }

    #Gold {
        background: linear-gradient(100deg, #65622b, #e0cf00);
        color: white;
    }

    #Platinum {
        background: linear-gradient(100deg, #1f1f1f, #01385f);
        color: white;
    }

    .profile {
        background: #00674b;
        position: inherit;
        top: 0;
        left: 0;
        width: 100%;
        height: 16%;
    }
</style>

<?php
$wallet = $WalletController->fetch_wallet();
$daily_transaction = $WalletController->daily_transactions();
$direct_referral = $WalletController->fetch_direct_referral_income();
$pairing_income = $WalletController->fetch_pairing_bonus_income();
$indirect_income = $WalletController->fetch_indirect_bonus_income();
$direct_income = $WalletController->fetch_direct_bonus_income();
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="margin-bottom:0px;">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default" style="border-radius:6px;">
                    <div class="panel-body profile" style="border-top-left-radius:6px;border-top-right-radius:6px;margin-bottom: 10px; overflow:visible;background: linear-gradient(to bottom, #00674b 50%, white 50%)">
                        <div class="row">
                            <div style="text-align:center;">
                                <br />
                                <br />
                                <img src="./assets/user.png" class="img-rounded" alt="Cinque Terre" style="width:100px;margin-top:18px;background:#fff;border-radius:50px;box-shadow:0 4px 10px 0 rgba(0,0,0,0.2), 0 4px 20px 0 rgba(0,0,0,0.19)">
                            </div>
                            <div style="text-align: center;">
                                <h3><?= ucwords(strtolower($name)) ?></h3>

                                <h5>(&nbsp;<?= $acc_id ?>&nbsp;)</h5>
                                <h4><?= $package ?> Account</h4>
                            </div>
                        </div>


                        <div class="row" style="margin-top:20px;">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align:center; border-right:1px solid; margin-bottom:0.8em;">
                                <a href="?page=transactions"><i class="fa fa-download" aria-hidden="true"></i> Transactions</a>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align:center; border-left:1px solid;">
                                <a href="?page=upgrade"><i class="fa fa-arrow-up" aria-hidden="true"></i> Upgrade</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!------------------------------------------------------------------------------------------------------------------>
            <!----------------------------------->
            <div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div>
                        <div class="panel-body profile" style="border-top:1px solid white; border:1px solid;" id="<?= $package ?>">
                            <div id="title" style=" margin-bottom: 0.5em; text-weight:bold;color:White;">
                                Wallet &nbsp; - &nbsp; <?= $package ?> Account
                            </div>
                            <div style="border-bottom:1px solid #d4cccc;color:white; border:1px solid;"></div>
                            <Br />
                            <div style="text-align:center;">
                                <label id="amount">
                                    <?php
                                    $inc = ($wallet['balance']) ? $wallet['balance'] : 0;

                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" .  number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!----------------------------------->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top:3px;">
                    <div>
                        <div class="panel-body profile" style="border-top:1px solid white; background:linear-gradient(100deg , #1d1a00, #FFC107)">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label style="font-family:creamCake; font-size: 3em; color:white;">Gift Check</label>
                            </div>
                            <div style="text-align:center;">
                                <label id="amount" style="color:white;">
                                    <?php
                                    $inc = ($wallet['gc']) ? $wallet['gc'] : 0;
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" .   number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!----------------------------------->
            <!------------------------------------------------------------------------------------------------------------------>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden;">
                <div class="row" style="padding:0px; margin: 0.2em 0em 0.2em 0em;">
                    <?php if ($direct_referral['current_direct_referral_earned'] != 0) : ?>
                        <button type="button" onclick="toogle('directReferral')" style="background-color:#ca8001; color:white; padding: 8px; margin-top:5px; border:0px;">
                            Direct Referral
                            <span class="badge badge-light"><?= ($direct_referral['current_direct_referral_earned']) ?  $direct_referral['current_direct_referral_earned'] : 0 ?></span>
                        </button>
                    <?php endif;
                    if ($pairing_income['current_cash_counter'] != 0) : ?>
                        <button type="button" onclick="toogle('pairingIncomeCash')" style="background-color:#005800; color:white; border:0px;padding: 8px; margin-top:5px;">
                            Pairing Income
                            <span class="badge badge-light"><?= ($pairing_income['current_cash_counter']) ?  $pairing_income['current_cash_counter'] : 0 ?></span>
                        </button>
                    <?php endif;
                    if ($pairing_income['current_gc_counter'] != 0) : ?>
                        <button type="button" onclick="toogle('pairingIncomeGC')" class="" style="background-color:#08086b; color:white; color:white; border:0px; padding: 8px; margin-top:5px;">
                            Pairing GC
                            <span class="badge badge-light"><?= ($pairing_income['current_gc_counter']) ?  $pairing_income['current_gc_counter'] : 0 ?></span>
                        </button>
                    <?php endif;
                    if ($pairing_income['current_flushout_counter'] != 0) : ?>
                        <button type="button" onclick="toogle('pairingIncomeFlushout')" class="" style="background-color:#bf0000; color:white; color:white; border:0px; padding: 8px; margin-top:5px;">
                            Pairing Flushout
                            <span class="badge badge-light"><?= ($pairing_income['current_flushout_counter']) ?  $pairing_income['current_flushout_counter'] : 0 ?></span>
                        </button>
                    <?php endif;
                    if ($indirect_income['current_cash_counter'] != 0) : ?>
                        <button type="button" onclick="toogle('indirectBonus')" class="" style="background-color:#004580; color:white; color:white; border:0px; padding: 8px; margin-top:5px;">
                            Indirect Bonus
                            <span class="badge badge-light"><?= ($indirect_income['current_cash_counter']) ?  $indirect_income['current_cash_counter'] : 0 ?></span>
                        </button>
                    <?php endif;
                    if ($direct_income['current_cash_counter'] != 0) : ?>
                        <button type="button" onclick="toogle('directBonus')" class="" style="background-color:#530065; color:white; color:white; border:0px; padding: 8px; margin-top:5px;">
                            Direct Bonus
                            <span class="badge badge-light"><?= ($direct_income['current_cash_counter']) ?  $direct_income['current_cash_counter'] : 0 ?></span>
                        </button>
                    <?php endif; ?>




                    <div class="tiles" style="margin-bottom:1em; display:none; border-radius:0px;" id="directReferral">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Direct Referral
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4" style="border:1px">
                                Cash
                            </div>
                            <div class="col-md-8 col-lg-8" style="text-align:right;">
                                <label style="font-size:1.1em; text-align:right;">
                                    <?php
                                    $inc = ($direct_referral['total_current_direct_referral_earned_cash']) ?  $direct_referral['total_current_direct_referral_earned_cash'] : 0;
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" .  number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-4 col-lg-4" style="border:1px">
                                Gift Check
                            </div>
                            <div class="col-md-8 col-lg-8" style="text-align:right;">
                                <label style="font-size:1.1em; text-align:right;">
                                    <?php
                                    $inc = ($direct_referral['total_current_direct_referral_earned_gc']) ?  $direct_referral['total_current_direct_referral_earned_gc'] : 0;
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" .  number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none; border-radius:0px;" id="pairingIncomeCash">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Pairing Bonus - Income Cash
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = $pairing_income['current_cash'] ? $pairing_income['current_cash'] : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none; border-radius:0px;" id="pairingIncomeGC">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Pairing Bonus - Income GC
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = $pairing_income['current_gc'] ? $pairing_income['current_gc'] : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none; border-radius:0px;" id="pairingIncomeFlushout">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Pairing Bonus - Flushout
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = $pairing_income['current_flushout'] ? $pairing_income['current_flushout'] : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none; border-radius:0px;" id="indirectBonus">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Indirect Bonus
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = ($indirect_income['current_cash']) ? $indirect_income['current_cash']  : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none; border-radius:0px;" id="directBonus">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Direct Bonus
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = ($direct_income['current_cash']) ? $direct_income['current_cash']  : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>

                </div>
            </div>
            <!----------------------------------->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div>
                    <div class="panel-body profile">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                            <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                <div id="title" style="padding: 0.5em;background-color:#00674b; color:white">
                                    Accumulated Income
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px;">
                        <div style="text-align:right;">
                            <label style="font-size: 2em; font-weight: bold;">
                                <?php
                                $inc = $direct_referral['total_direct_referral_earned_cash'] + $pairing_income['total_pairing_cash'] + $indirect_income['total_indirect_cash'] + $direct_income['total_direct_cash'];
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" . number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                endif;
                                // 
                                ?>
                            </label>
                        </div>
                    </div>

                    <div>
                        <div class="panel-body profile" style="border-top:1px solid white;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                                <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                    <div style="padding: 0.7em;background-color:#00674b; color:white">
                                        Direct Referral
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px;">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            Income Cash
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <div style="text-align:right;">
                                                <label style="font-size: 2em; font-weight: bold;">
                                                    <?php
                                                    $inc = $direct_referral['total_direct_referral_earned_cash'] ?  $direct_referral['total_direct_referral_earned_cash'] : 0;
                                                    if ($currency == NULL || $currency == 'PHP') :
                                                        echo "&#8369;" . number_format($inc, 2);
                                                    else :
                                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                                    endif;
                                                    ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <hr />
                                </div>

                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            Gift check
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <div style="text-align:right;">
                                                <label style="font-size: 1.5em; font-weight: bold;">
                                                    <?php
                                                    $inc = $direct_referral['total_direct_referral_earned_gc'] ?  $direct_referral['total_direct_referral_earned_gc'] : 0;
                                                    if ($currency == NULL || $currency == 'PHP') :
                                                        echo "&#8369;" . number_format($inc, 2);
                                                    else :
                                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                                    endif;
                                                    ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="panel-body profile" style=" border-top:1px solid white;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                                <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                    <div style="padding: 0.7em;background-color:#00674b; color:white">
                                        Pairing Bonus
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px;">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            Income Cash
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <div style="text-align:right;">
                                                <label style="font-size: 2em; font-weight: bold;">
                                                    <?php
                                                    $inc = $pairing_income['total_pairing_cash'] ? $pairing_income['total_pairing_cash'] : 0;
                                                    if ($currency == NULL || $currency == 'PHP') :
                                                        echo "&#8369;" . number_format($inc, 2);
                                                    else :
                                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                                    endif;
                                                    ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <hr />
                                </div>

                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            Gift check
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <div style="text-align:right;">
                                                <label style="font-size: 1.5em; font-weight: bold;">
                                                    <?php
                                                    $inc = $pairing_income['total_pairing_gc'] ? $pairing_income['total_pairing_gc'] : 0;
                                                    if ($currency == NULL || $currency == 'PHP') :
                                                        echo "&#8369;" . number_format($inc, 2);
                                                    else :
                                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                                    endif;
                                                    ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <hr />
                                </div>

                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            flushout
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <div style="text-align:right;">
                                                <label style="font-size: 1.5em; font-weight: bold;">
                                                    <?php
                                                    $inc = $pairing_income['total_pairing_flushout'] ? $pairing_income['total_pairing_flushout'] : 0;
                                                    if ($currency == NULL || $currency == 'PHP') :
                                                        echo "&#8369;" . number_format($inc, 2);
                                                    else :
                                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                                    endif;
                                                    ?>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr />
                            <div class="row" style="margin-top: -0.5em;">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin: 0px; text-align:center;">
                                    <label style="font-size:.9em; font-weight: 100;">Left VP</label>
                                    <br />
                                    <?= $pairing_income['left_vp'] ?  number_format($pairing_income['left_vp']) : 0; ?>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin: 0px; text-align:center;">
                                    <label style="font-size:.9em; font-weight: 100;">Right VP</label>
                                    <br />
                                    <?= $pairing_income['right_vp'] ?  number_format($pairing_income['right_vp']) : 0; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="panel-body profile" style="border-top:1px solid white;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                                <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                    <div style="padding: 0.7em;background-color:#00674b; color:white">
                                        Indirect Bonus
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px;">
                            <div style="text-align:right;">
                                <label style="font-size: 2em; font-weight: bold;">
                                    <?php
                                    $inc = $indirect_income['total_indirect_cash'] ?  $indirect_income['total_indirect_cash'] : 0;
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" .  number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="panel-body profile" style="border-top:1px solid white;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                                <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                    <div style="padding: 0.7em;background-color:#00674b; color:white">
                                        Direct Bonus
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px;">
                            <div style="text-align:right;">
                                <label style="font-size: 2em; font-weight: bold;">
                                    <?php
                                    $inc = $direct_income['total_direct_cash'] ?  $direct_income['total_direct_cash'] : 0;
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" . number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($inc / $php_to_usd_rate, 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <ul class="nav nav-tabs">
            <li class="active" id="nav_tabs"><a data-toggle="tab" href="#daily_transactions" style="font-size: .9em;">Daily Transactions</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#referral" style="font-size: .9em;">Direct Referral</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#pairing" style="font-size: .9em;">Pairing Bonus</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#indirect" style="font-size: .9em;">Indirect Bonus</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#direct" style="font-size: .9em;">Direct Bonus</a></li>
        </ul>

        <div class="tab-content">
            <div id="daily_transactions" class="tab-pane fade in active">
                <?php
                if ($daily_transaction != null) :
                    foreach ($daily_transaction as $transaction) :
                        $date = date_create($transaction['created']);
                        $month_format = date_format($date, 'M');
                        $day = date_format($date, 'd');
                        $hours = date_format($date, 'h:i a');

                        $created_date = date_format($date, "Y-m-d");
                        $today = date("Y-m-d");

                        if ($created_date != $today) :
                            continue;
                        endif;
                ?>
                        <div class="tiles" style="border-radius:0px;">
                            <div class="row">
                                <div class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>
                                    <b><?= $month_format ?></b>
                                    <br />
                                    <b><?= $day ?></b>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-6 col-lg-6">
                                    <b>
                                        <?= $transaction['transaction_type'] ?>
                                    </b>
                                    <br />
                                    <b><?= $hours ?></b>
                                </div>
                            </div>
                        </div>
                <?php
                    endforeach;
                endif;
                ?>
            </div>
            <div id="referral" class="tab-pane fade">
                <?php
                $limiter = 0;
                if ($direct_referral['direct_referral_incomes'] != null) :
                    foreach ($direct_referral['direct_referral_incomes'] as $income) :
                        $limiter += 1;
                        if ($limiter == 15) :
                            break;
                        endif;

                        $date = date_create($income['created']);
                        $month_format = date_format($date, 'M');
                        $day = date_format($date, 'd');
                        $hours = date_format($date, 'h:i a');

                        $cash = null;
                        $gc = null;
                        if ($currency == NULL || $currency == 'PHP') :
                            $cash = "&#8369;" . number_format($income['amount'], 2);
                            $gc = "&#8369;" . number_format($income['gc'], 2);
                        else :
                            $cash =  "&#36;" . number_format($income['amount'] / $php_to_usd_rate, 2);
                            $gc =  "&#36;" . number_format($income['gc'] / $php_to_usd_rate, 2);
                        endif;
                ?>
                        <div class='tiles' style='border-radius:0px;'>
                            <div class='row'>
                                <div class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>
                                    <b><?= $month_format ?></b>
                                    <br />
                                    <b><?= $day ?></b>
                                </div>
                                <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>
                                    <b>
                                        DIRECT REFERRAL
                                    </b>
                                    <br />
                                    <b><?= $hours ?></b>
                                </div>
                                <div class='col-xs-2 col-sm-4 col-md-2 col-lg-4'>
                                    Cash &nbsp; - &nbsp; <b><?= $cash ?></b>
                                    <br />
                                    GC &nbsp; - &nbsp; <b><?= $gc ?></b>
                                </div>
                            </div>
                        </div>
                <?php
                    endforeach;
                endif;
                ?>
            </div>
            <div id="pairing" class="tab-pane fade">
                <?php
                $limiter = 0;
                if ($pairing_income['pairing_logs'] != null) :
                    foreach ($pairing_income['pairing_logs'] as $income) :
                        $limiter += 1;
                        if ($limiter == 15) :
                            break;
                        endif;

                        $date = date_create($income['created']);
                        $month_format = date_format($date, 'M');
                        $day = date_format($date, 'd');
                        $hours = date_format($date, 'h:i a');


                        if ($income['income'] != 0 && $income['remark'] != "FlushOut") :
                            if ($currency == NULL || $currency == 'PHP') :
                                $cash = "&#8369;" . number_format($income['income'], 2);
                            else :
                                $cash =  "&#36;" . number_format($income['income'] / $php_to_usd_rate, 2);
                            endif;
                ?>
                            <div class='tiles' style='border-radius:0px;'>
                                <div class='row'>
                                    <div class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>
                                        <b><?= $month_format ?></b>
                                        <br />
                                        <b><?= $day ?></b>
                                    </div>
                                    <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>
                                        <b>
                                            PAIRING BONUS
                                        </b>
                                        <br />
                                        <b><?= $hours ?></b>
                                    </div>
                                    <div class='col-xs-2 col-sm-4 col-md-2 col-lg-4'>
                                        <?= $income['remark'] ?> &nbsp; - &nbsp; <b><?= $cash ?></b>
                                    </div>
                                </div>
                            </div>
                <?php
                        endif;
                    endforeach;
                endif;
                ?>
            </div>
            <div id="indirect" class="tab-pane fade">
                <?php
                $limiter = 0;
                if ($indirect_income['indirect_income'] != null) :
                    foreach ($indirect_income['indirect_income'] as $income) :
                        $limiter += 1;
                        if ($limiter == 15) :
                            break;
                        endif;

                        $date = date_create($income['created']);
                        $month_format = date_format($date, 'M');
                        $day = date_format($date, 'd');
                        $hours = date_format($date, 'h:i a');

                        $cash = null;
                        if ($currency == NULL || $currency == 'PHP') :
                            $cash = "&#8369;" . number_format($income['amount'], 2);
                        else :
                            $cash =  "&#36;" . number_format($income['amount'] / $php_to_usd_rate, 2);
                        endif;
                ?>
                        <div class='tiles' style='border-radius:0px;'>
                            <div class='row'>
                                <div class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>
                                    <b><?= $month_format ?></b>
                                    <br />
                                    <b><?= $day ?></b>
                                </div>
                                <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>
                                    <b>
                                        INDIRECT BONUS
                                    </b>
                                    <br />
                                    <b><?= $hours ?></b>
                                </div>
                                <div class='col-xs-2 col-sm-4 col-md-2 col-lg-4'>
                                    Cash &nbsp; - &nbsp; <b><?= $cash ?></b>
                                </div>
                            </div>
                        </div>
                <?php
                    endforeach;
                endif;
                ?>
            </div>
            <div id="direct" class="tab-pane fade">
                <?php
                $limiter = 0;
                if ($direct_income['direct_income'] != null) :
                    foreach ($direct_income['direct_income'] as $income) :
                        $limiter += 1;
                        if ($limiter == 15) :
                            break;
                        endif;

                        $date = date_create($income['created']);
                        $month_format = date_format($date, 'M');
                        $day = date_format($date, 'd');
                        $hours = date_format($date, 'h:i a');

                        $cash = null;
                        if ($currency == NULL || $currency == 'PHP') :
                            $cash = "&#8369;" . number_format($income['amount'], 2);
                        else :
                            $cash =  "&#36;" . number_format($income['amount'] / $php_to_usd_rate, 2);
                        endif;
                ?>
                        <div class='tiles' style='border-radius:0px;'>
                            <div class='row'>
                                <div class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>
                                    <b><?= $month_format ?></b>
                                    <br />
                                    <b><?= $day ?></b>
                                </div>
                                <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>
                                    <b>
                                        DIRECT BONUS
                                    </b>
                                    <br />
                                    <b><?= $hours ?></b>
                                </div>
                                <div class='col-xs-2 col-sm-4 col-md-2 col-lg-4'>
                                    Cash &nbsp; - &nbsp; <b><?= $cash ?></b>
                                </div>
                            </div>
                        </div>
                <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>



<script>
    function toogle(element) {
        var elem = document.getElementById(element).style.display;

        if (element == "directReferral") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "pairingIncomeCash") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "pairingIncomeGC") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "pairingIncomeFlushout") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "indirectBonus") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "directBonus") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        }
    }
</script>