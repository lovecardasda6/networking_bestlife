<?php
$accounts = new accounts($acc_id);

$error = false;
$createError = false;
$updateError = false;

$success = false;
$createSuccess = false;
$updateSuccess =false;

if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $number = $_POST['number'];

    $res = $accounts->createRemmitanceAccount($name, $number, 'Cebuana Lhuillier');
    if($res){
        $success = true;
        $updateSuccess =true;
    }else{
        $error = true;
        $createError = true;
    }
}

if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $number = $_POST['number'];

    $res = $accounts->updateRemmitance($id, $name, $number);
    if($res){
        $success = true;
        $updateSuccess =true;
    }else{
        $error = true;
        $updateError = true;
    }
}
?>
<ol class="breadcrumb" style="background-color: #00674b;">
    <li class="?page=binarytree" style="color:white">Encashment</li>
    <li class="active" style="color:white">Cebuana Lhuillier</li>
</ol>

<div class="tiles" style="border:0px;">>
    <div id="title">
        Remmitance - Palawan Pawnshop
    </div>
    <hr />

    <?php
    if (!$accounts->fetchRemmitanceAccount('Cebuana Lhuillier')) {
    ?>

        <form method="POST">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="lastname">Receiver's Name : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="name" placeholder="Account Name" required>
                    </div>
                    <div class="form-group">
                        <label for="firstname">Contact Number : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="number" placeholder="Account Number" required>
                    </div>
                    <div style="text-align: right;">
                        <input type="submit" value="Create Account" name="submit" class="btn btn-default" />
                    </div>
                </div>
            </div>
        </form>
    <?php
    } else {
        $details = $accounts->fetchRemmitanceAccount('Cebuana Lhuillier');
    ?>
        
        <form method="POST">
            <input type="hidden" name="id" value="<?= $details->id ?>" />
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="lastname">Receiver's Name : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="name" value="<?= $details->receivers_name ?>" placeholder="Receiver's Name" required>
                    </div>
                    <div class="form-group">
                        <label for="firstname">Contact Number : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="number" value="<?= $details->contact_number ?>" placeholder="Contact Number" required>
                    </div>
                    <div style="text-align: right;">
                        <input type="submit" value="Update Account" name="update" class="btn btn-default" />
                    </div>
                </div>
            </div>
        </form>
    <?php
    }
    ?>
</div>