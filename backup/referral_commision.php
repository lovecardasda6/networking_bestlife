<?php

class referral_commision{
    protected $account_id;

    function __construct($user_id){
        $this->account_id = $user_id;
    }

    public function commision(){
        $con = connectionString();

        $commisionQuery = "SELECT CONCAT(lastname, ', ', firstname, ' ', middlename) as `name` , package, amount, gc, direct_referral_income.created FROM direct_referral_income INNER JOIN accounts ON accounts.id = direct_referral_income.invited_account_id INNER JOIN packages ON packages.id = accounts.package_id WHERE direct_referral_income.account_id = '{$this->account_id}' ORDER BY direct_referral_income.id DESC";
        $res = mysqli_query($con, $commisionQuery);

        while($rows = mysqli_fetch_array($res, MYSQLI_ASSOC)){
?>
            <tr id="<?php 
                Switch ($rows['package']){
                    case "Bronze":
                        echo "bronze";
                        break;

                    case "Silver":
                        echo "silver";
                        break;

                    case "Gold":
                        echo "gold";
                        break;

                    case "Platinum":
                        echo "platinum";
                        break;
                }
            ?>" >
                <td><?= $rows["name"] ?></td>
                <td><?= $rows["package"] ?></td>
                <td><?= $rows["amount"] ?></td>
                <td><?= $rows["gc"] ?></td>
                <td><?= $rows["created"] ?></td>
            </tr>
<?php
        }
    }
}