<h1 style="font-family:creamCake; font-size: 5em;">Cashout Preview</h1>
<br />

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="padding:0px;">
        <div class="tiles">
            <div id="title">
                Cashout
            </div>
            <hr />
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px; margin:0px;">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px;">
                    Amount
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px; text-align:right;">
                    PHP <?= number_format($amount, 2) ?>
                </div>
            </div>
            <br />
            <br />
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px; margin:0px;">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px;">
                    Transaction Fee
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px; text-align:right;">
                    <?= $fee ?>%
                </div>
            </div>
            <br />
            <br />
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px; margin:0px;">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px;">
                    Deduction
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px; text-align:right;">
                    PHP <?= number_format(($amount * ($fee / 100)), 2) ?>
                </div>
            </div>
            <br />
            <hr />
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0px; margin:0px;">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px;">
                    Net Amount
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding:0px; text-align:right;">
                    <?= number_format(($amount - ($amount * ($fee / 100))), 2) ?>
                </div>
            </div>
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="padding:0px;">
        <?php if ($transaction == "Bank") : $bank_informations = Bank::fetch($account_id, $center); ?>
            <form method="POST">
                <input type="hidden" name="transaction" value="<?= $transaction ?>" />
                <input type="hidden" name="center" value="<?= $center ?>" />
                <input type="hidden" name="amount" value="<?= $amount ?>" />
                <div class="tiles">
                    <div id="title">
                        Bank - <?= $center ?>
                    </div>
                    <hr />
                    <div class="form-group">
                        <label for="lastname">Account Name <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" value="<?= $bank_informations['account_name'] ?>" name="lastname" required readonly disabled>
                    </div>
                    <div class="form-group">
                        <label for="firstname">Account Number <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" value="<?= $bank_informations['account_number'] ?>" name="firstname" required readonly disabled>
                    </div>
                    <div style="text-align:right">
                        <input type="submit" name="confirm_transaction" value="Confirm" class="btn btn-default">
                    </div>
                </div>
            </form>
        <?php elseif ($transaction == "Remittance") :  $remittance_informations = Remittance::fetch($account_id, $center); ?>
            <form method="POST">
                <input type="hidden" name="transaction" value="<?= $transaction ?>" />
                <input type="hidden" name="center" value="<?= $center ?>" />
                <input type="hidden" name="amount" value="<?= $amount ?>" />
                <div class="tiles">
                    <div id="title">
                        Remittance - <?= $center ?>
                    </div>
                    <hr />
                    <div class="form-group">
                        <label for="lastname">Receiver's Name <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" value="<?= $remittance_informations['receivers_name'] ?>" name="lastname" required readonly disabled>
                    </div>
                    <div class="form-group">
                        <label for="firstname">Contact Number <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" value="<?= $remittance_informations['contact_number']?>" name="firstname" required readonly disabled>
                    </div>
                    <div style="text-align:right">
                        <input type="submit" name="confirm_transaction" value="Confirm" class="btn btn-default">
                    </div>
                </div>
            </form>
        <?php elseif ($transaction == "EWallet") :  $ewallet_informations = EWallet::fetch($account_id, $center); ?>
            <form method="POST">
                <input type="hidden" name="transaction" value="<?= $transaction ?>" />
                <input type="hidden" name="center" value="<?= $center ?>" />
                <input type="hidden" name="amount" value="<?= $amount ?>" />
                <div class="tiles">
                    <div id="title">
                        EWallet - <?= $center ?>
                    </div>
                    <hr />
                    <div class="form-group">
                        <label for="firstname">EWallet Number <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" value="<?= $ewallet_informations['ewallet_number'] ?>" name="firstname" required readonly disabled>
                    </div>
                    <div style="text-align:right">
                        <input type="submit" name="confirm_transaction" value="Confirm" class="btn btn-default">
                    </div>
                </div>
            </form>
        <?php endif; ?>
    </div>
</div>