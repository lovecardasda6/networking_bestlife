<?php

class direct_income_transaction{
    protected $account_id;
    protected $con;

    function __construct($user_id){
        $this->con = connectionString();
        $this->account_id = $user_id;
    }

    public function commision(){

        $query = "SELECT CONCAT(lastname, ', ',firstname, ' ', middlename) as `name`, package, amount, direct_bonus_income.created  FROM accounts INNER JOIN packages ON packages.id = accounts.package_id INNER JOIN direct_bonus_income ON accounts.id = direct_bonus_income.invited_account_id WHERE direct_bonus_income.account_id = '{$this->account_id}'";
        $res = mysqli_query($this->con, $query);

        while($rows = mysqli_fetch_array($res, MYSQLI_ASSOC)){
?>
            <tr id="<?php 
                Switch ($rows['package']){
                    case "Bronze":
                        echo "bronze";
                        break;

                    case "Silver":
                        echo "silver";
                        break;

                    case "Gold":
                        echo "gold";
                        break;

                    case "Platinum":
                        echo "platinum";
                        break;
                }
            ?>" >
                <td><?= $rows["name"] ?></td>
                <td><?= $rows["package"] ?></td>
                <td><?= $rows["amount"] ?></td>
                <td><?= $rows["created"] ?></td>
            </tr>
<?php
        }
    }
}