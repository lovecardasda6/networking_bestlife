<h1 style="font-family:creamCake; font-size: 5em;">Transactions</h1>


<?php
if (isset($_GET['cashout']) && $_GET['cashout'] == 1) :
?>
    <div style="background-color:#014204; padding: .8em; border-radius:10px; color:white;">
        Cashout request successfully send. Please wait for the confirmation.
    </div>
<?php
endif;
?>


<?php
if ($transactions != null) :
    foreach ($transactions as $transaction) :
?>
        <div class="tiles">
            <div id="title">
                <div class="row">
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <img src="/assets/logo/<?= Encashment::fetch_logo($transaction['encashment_mode'], $transaction['encashment_center']) ?>" style="width: 50px;" />
                        <?= $transaction['encashment_mode'] ?> (<?= $transaction['encashment_center'] ?>) - Cashout
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <label style="font-weight: 100;">
                            Transaction fee : <?= $transaction['fee'] ?> %
                        </label>
                    </div>
                </div>
            </div>
            <hr>
            <div>
                <label id="amount">
                    <?php
                    $amount = $transaction['receivable_amount'];
                    if ($currency == NULL || $currency == 'PHP') :
                        echo "&#8369;" . $amount;
                    else :
                        echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($amount), 2);
                    endif;
                    ?>
                </label>
            </div>
            <hr>
            <div>
                <?= ($transaction['status'] == "Confirmed") ? $transaction['updated'] : $transaction['created'] ?>
                <br />
                <?= ($transaction['status'] == "Confirmed") ? '<b style="color:green;">Confirmed</b>' : '<b style="color:red;">Pending</b>' ?>
                <br />
            </div>
        </div>
<?php
    endforeach;
endif;
?>