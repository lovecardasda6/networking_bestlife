<?php
class wallet{
    protected $con;
    protected $account_id;
    protected $date;

    //id sa nag pasulod kay para ma update ang iyang wallet
    function __construct($account_id){
        $this->con = connectionString();
        $this->account_id = $account_id;
        $this->date = date('Y-m-d H:i:s');
    }

    

    function updateWallet($amount, $product){
        $wallet = $this->retrieveWallet();
        $balance = (float) $wallet->balance + (float) $amount;
        $product = (float) $wallet->gc + (float) $product;
        $accumulated_income = (float) $wallet->accumulated_income + (float) $amount;

        $query = "UPDATE wallet SET balance = '{$balance}', gc = '{$product}', accumulated_income = '{$accumulated_income}', updated = '{$this->date}' WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if($res){
            return true;    
        }
        
        return false;
    }
    
    //kuhaon ang wallet information ani nga user
    function retrieveWallet(){
        $query = "SELECT balance, gc, accumulated_income FROM wallet WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);
        $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);

        $obj = new \stdClass();
        list('balance' => $balance, 'gc' => $gc, 'accumulated_income' => $accumulated_income) = $datas;
        $obj->balance = $balance;
        $obj->gc = $gc;
        $obj->accumulated_income = $accumulated_income;

        return $obj;

    }

    function deductWallet($amount, $product){
        $wallet = $this->retrieveWallet();
        $balance = (float) $wallet->balance - (float) $amount;

        $query = "UPDATE wallet SET balance = '{$balance}', updated = '{$this->date}' WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if($res){
            return true;    
        }
        
        return false;
    }
}