<?php


class pairing_bonus
{
    protected $con;
    protected $date;


    function __construct()
    {
        $this->con  = connectionString();
        $this->date = date('Y-m-d');
    }

    //ang gamit rasa new registered_account_id kay makuha ang value points
    function pairing_income($account_id, $invited_account_id, $new_registered_account_id)
    {
        $account_id = $account_id;////
        $invited_account_id = $invited_account_id;
        $new_registered_account_id = $new_registered_account_id;

        $account_package_id = $this->account_package_id($account_id);///
        $validateForPairing = $this->validateForPairing($account_package_id);

        if (!$validateForPairing) {

            $this->bonuses($account_id, $invited_account_id, $new_registered_account_id, 0);///
            return;
        }

        $new_registered_account_package_id = $this->account_package_id($new_registered_account_id);
        $new_registered_account_value_points = $this->value_points($new_registered_account_package_id);
        $invited_account_position = $this->invited_account_reside_position($invited_account_id);

   
        $latest_transaction = $this->latest_transaction($account_id);////
        $pairing_id = $latest_transaction->pairing_id;
        $balance = $latest_transaction->balance;
        $heavy_side = $latest_transaction->heavy_side;
        $paired_this_day = $latest_transaction->paired_this_day;

        $transaction = $this->transaction($balance, $new_registered_account_value_points, $heavy_side, $invited_account_position);
        $income = $transaction->income;
        $remaining_vp = $transaction->remaining_vp;
        $heavy_side = $transaction->heavy_side;

        $paired = $this->paired($account_id);////
        $package_pair_per_day = $this->package_pair_per_day($account_package_id);
        $remark = null;

        if ($income != 0) {
            $config_file = parse_ini_file("./../config/config.ini");
            $baseVP = $config_file['BASE_VP_PAIRING'];
            $doublePairing = array(24, 48, 72, 96, 120, 144, 168, 192, 216);
            $incomes = $income / $baseVP;
            $remark = NULL;
            $totalEarned = 0;
            $totalGC = 0;

            for ($i = 1; $i <= $incomes; $i++) {

                $paired_this_day += 1;
                $paired += 1;
                $income = $baseVP;

                if ($paired_this_day > $package_pair_per_day) {
                    $paired_this_day -= 1;
                    $paired -= 1;
                    $remark = "Flushout";
                } else {
                    if (in_array($paired_this_day, $doublePairing)) {
                        if ($paired == 5) {

                            $paired = 5;
                            $remark = "GC";
                            $income = ($baseVP * 2);
                            $totalGC += $income * 2;
                        } else if ($paired > 5) {

                            $remark = "Income";
                            $income = ($baseVP * 2);
                            $totalEarned += $income;
                            $paired = 1;
                        } else {

                            $remark = "Income";
                            $income = ($baseVP * 2);
                            $totalEarned += $income;
                        }
                    } else {
                        if ($paired == 5) {

                            $paired = 5;
                            $remark = "GC";
                            $totalGC += $baseVP;
                        } else if ($paired > 5) {

                            $remark = "Income";
                            $income = $baseVP;
                            $totalEarned += $income;
                            $paired = 1;
                        } else {
                            $remark = "Income";
                            $income = $baseVP;
                            $totalEarned += $income;
                        }
                    }
                }





                $query = "INSERT INTO pairing_bonus_income VALUES (NULL, '{$pairing_id}', '{$account_id}', '{$balance}', '{$new_registered_account_id}', '{$new_registered_account_value_points}', '{$income}', '{$remaining_vp}', '{$heavy_side}', '{$paired}', '{$paired_this_day}', '{$remark}', '{$this->date}')";
                $res = $this->con->query($query);
            }


            $wallet = new Wallet($invited_account_id);
            $response = $wallet->updateWallet($totalEarned, $totalGC);

            if ($response) {
                $this->bonuses($account_id, $invited_account_id, $invited_account_id, $new_registered_account_id, $income);
            }
        } else {
            $remark = "Income";
            $query = "INSERT INTO pairing_bonus_income VALUES (NULL, '{$pairing_id}', '{$account_id}', '{$balance}', '{$new_registered_account_id}', '{$new_registered_account_value_points}', '{$income}', '{$remaining_vp}', '{$invited_account_position}', '{$paired}', '{$paired_this_day}', '{$remark}', '{$this->date}')";
            $res = $this->con->query($query);

            if ($res) {
                $this->bonuses($account_id, $invited_account_id, $invited_account_id, $new_registered_account_id, 0);
            }
        }
    }

    function paired($account_id)
    {
        $query = "SELECT paired FROM pairing_bonus_income WHERE account_id = '{$account_id}' ORDER BY id DESC LIMIT 1";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('paired' => $paired) = $datas;
                return $paired;
            }
        }

        return 0;
    }

    function package_pair_per_day($package_id)
    {
        $query = "SELECT pair_per_day FROM pairing_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('pair_per_day' => $pair_per_day) = $datas;
                return $pair_per_day;
            }
        }
        return 0;
    }

    function transaction($balance, $value_points, $heavy_side, $invited_account_position)
    {

        $obj = new \stdClass();
        $obj->income = 0;
        $obj->remaining_vp = $balance;
        $obj->heavy_side = $heavy_side;

        if ($heavy_side == $invited_account_position) {
            $obj->remaining_vp += $value_points;
            $obj->heavy_side = $heavy_side;
        } else {
            if ($value_points > $balance) {
                $obj->income = $balance;
                $obj->remaining_vp = abs($value_points - $balance);
                $obj->heavy_side = $invited_account_position;
            } else if ($balance > $value_points) {
                $obj->income = $value_points;
                $obj->remainig_vp = abs($balance - $value_points);
                $obj->heavy_side;
            } else if ($balance == $value_points) {
                $obj->income = $value_points;
                $obj->remaining_vp = 0;
                $obj->heavy_side = $invited_account_position;
            }
        }

        return $obj;
    }

    function latest_transaction($account_id)
    {
        $query = "SELECT * FROM pairing_bonus_income WHERE account_id = '{$account_id}' AND created = '{$this->date}' ORDER BY id DESC LIMIT 1";
        $res = $this->con->query($query);

        $obj = new \stdClass();
        $obj->pairing_id = 0;
        $obj->balance = 0;
        $obj->heavy_side = "None";
        $obj->paired = 0;
        $obj->paired_this_day = 0;
        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list(
                    'id' => $obj->pairing_id,
                    'remaining_vp' => $obj->balance,
                    'heavy_side' => $obj->heavy_side,
                    'paired_this_day' => $obj->paired_this_day
                ) = $datas;

                return $obj;
            }
        }
        return $obj;
    }

    function invited_account_reside_position($account_id)
    {
        $query = "SELECT position FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('position' => $position) = $datas;
                return $position;
            }
        }
    }

    function value_points($package_id)
    {
        $query = "SELECT value_points FROM pairing_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res);
                list('value_points' => $value_points) = $datas;
                return $value_points;
            }
        }

        return 0;
    }

    function bonuses($invited_account_id, $new_registered_account_id, $earned)
    {
        $account_id = $this->reference_id($invited_account_id);

        if ($account_id == 0) {
            return;
        }

        $bonuses = new bonuses();
        $res = $bonuses->pairingIndirectDirectQueueBonuses($account_id, $new_registered_account_id, $earned);
    }

    function reference_id($account_id)
    {
        $query = "SELECT upline_id  FROM account WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('upline_id' => $reference_id) = $datas;
                return $reference_id;
            }
        }
        return 0;
    }

    function account_package_id($account_id)
    {
        $query = "SELECT package_id FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('package_id' => $package_id) = $datas;
                return $package_id;
            }
        }

        return 0;
    }

    function validateForPairing($package_id)
    {
        $query = "SELECT id FROM pairing_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);


        if ($res) {
            if (mysqli_num_rows($res)) {
                return true;
            }
        }
        return false;
    }
}
