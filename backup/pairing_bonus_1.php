<?php


    class pairing_bonus{
        protected $con;
        protected $date;


        function __construct(){
            $this->con = connectionString();
            $this->date = date('Y-m-d'); 
        }

        //ang gamit rasa new registered_account_id kay makuha ang value points
        function pairing_income($account_id, $invited_account_id, $new_registered_account_id){



            $loop_account_id = $account_id;
            $loop_invited_account_id = $invited_account_id;

            if($account_id == 0){
                return;
            }
            
            $referral_side = $this->referralReside($account_id, $invited_account_id); //asa naka reside ang gi invite sa nag invite
            $account_id = $this->reference_id($invited_account_id);  //kinsay nag invite nimo
            $pairing_id = ($this->paired_id($account_id))? $this->paired_id($account_id): 0 ; 
            $balance = ($this->remainingVP($pairing_id))? $this->remainingVP($pairing_id): 0 ; //remaining vp sa nag invite nimo
             
            $value_points = ($this->valuePoints($new_registered_account_id))? $this->valuePoints($new_registered_account_id): 0 ;  //pilay equivalent value point sa package nga gipalit sa bagong account or user
            $heavy_side = ($this->heavySide($pairing_id))? $this->heavySide($pairing_id): 'None' ; //kuhaon ang heavy position sa vp
            $paired = ($this->paired($pairing_id))? $this->paired($pairing_id): 0 ; 
            $paired_date = ($paired->created)? $paired->created : date('Y-m-d') ;
            $paired = ($paired->paired)? $paired->paired : 0 ;

            $transaction = $this->transaction($balance, $value_points, $heavy_side, $referral_side, $paired); 
            $income = $transaction->income; 
            $remaining_vp = $transaction->remaining_vp; 
            $heavy_side = $transaction->heavy_side; 

            
            $paired_this_day = $this->paired_this_day($account_id);
            $package_pair_per_day = $this->package_pair_per_day($account_id);
            
            if($income != 0){
                $loop = $income / 250;
                $remark = NULL;
                $totalEarned = 0;
                $totalGC = 0;

                for($i = 1; $i <= $loop; $i++){
                    
                    $paired_this_day += 1;
                    $paired += 1;

                    if($paired_this_day > $package_pair_per_day){
                        $paired_this_day -= 1;
                        $paired -= 1; 
                        $remark = "Flushout";
                    }
                    else{
                        if($paired == 5){
                            $paired = 5;
                            $remark = "GC";
                        }else{
                            $remark = "Income";
                        }
                    }

                    $income = 250;
                    $pairingIncomeQuery = "INSERT INTO pairing_bonus_income VALUES(NULL, '{$pairing_id}', '{$account_id}', '{$balance}', '{$new_registered_account_id}', '{$value_points}', '{$income}', '{$remaining_vp}', '{$heavy_side}', '{$paired}', {$paired_this_day}, '{$remark}', '{$this->date}' )";
                    $res = $this->con->query($pairingIncomeQuery);

                }
                

                $wallet = new Wallet($account_id);
                $response = $wallet->updateWallet($totalEarned, $totalGC);
                 
                if($response){
                    $this->bonuses($loop_account_id, $loop_invited_account_id, $new_registered_account_id, $totalEarned);
                }
            }
            else{
                $remark = "Income";
                $pairingIncomeQuery = "INSERT INTO pairing_bonus_income VALUES(NULL, '{$pairing_id}', '{$account_id}', '{$balance}', '{$new_registered_account_id}', '{$value_points}', '{$income}', '{$remaining_vp}', '{$heavy_side}', '{$paired}', {$paired_this_day}, '{$remark}', '{$this->date}' )";
                $res = $this->con->query($pairingIncomeQuery);
                
                if($res){
                    $this->bonuses($loop_account_id, $loop_invited_account_id, $new_registered_account_id, $income);
                }
            }
        }

        function bonuses($loop_account_id, $loop_invited_account_id, $new_registered_account_id, $income){
            $account_id = $this->reference_id($loop_account_id);
            $invited_account_id = $this->reference_id($loop_invited_account_id);
            $bonuses = new bonuses();
            $bonuses->pairingIndirectDirectQueueBonuses($account_id, $invited_account_id, $new_registered_account_id, $income);
        }

        function paired_this_day($account_id){
            $query = "SELECT paired_this_day FROM pairing_bonus_income WHERE account_id = '{$account_id}' AND created = '{$this->date}' ORDER BY id DESC LIMIT 1";
            $res = $this->con->query($query);

            if($res){
                if(mysqli_num_rows($res)){
                    $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                    list('paired_this_day' => $paired_this_day) = $datas;
                    return $paired_this_day;
                }else{
                    return 0;
                }
            }
        }

        function package_pair_per_day($account_id){
            $query = "SELECT pair_per_day FROM pairing_bonuses INNER JOIN accounts ON pairing_bonuses.package_id = accounts.package_id WHERE accounts.id = '{$account_id}'";
            $res = $this->con->query($query);

            if($res){
                if(mysqli_num_rows($res)){
                    $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                    list('pair_per_day' => $pair_per_day) = $datas;
                    return $pair_per_day;
                }else{
                    return 0;
                }
            }
        }

        //retrieve last_pairing transaction
        function paired_id($account_id){
            $query = "SELECT id FROM pairing_bonus_income WHERE account_id = '{$account_id}' ORDER BY id DESC LIMIT 1";
            $res = $this->con->query($query);
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('id' => $pairing_id) = $datas;
            return $pairing_id;
        }

        //retrieve remaining balance
        function remainingVP($paired_id){
            $query = "SELECT remaining_vp FROM pairing_bonus_income WHERE id = '{$paired_id}'";
            $res = $this->con->query($query);
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('remaining_vp' => $remaining_vp) = $datas;
            return $remaining_vp;
        }

        //equivalent value points sa package
        function valuePoints($account_id){
            $query = "SELECT value_points FROM pairing_bonuses INNER JOIN accounts ON pairing_bonuses.package_id = accounts.package_id WHERE accounts.id = '{$account_id}'";
            $res = $this->con->query($query);
            $datas = mysqli_fetch_array($res);
            list('value_points' => $value_points) = $datas;
            return $value_points;
        }

        //kuhaon asa ibutang ang vp
        function referralReside($account_id, $invited_account_id){
            $query = "SELECT referral_side FROM accounts WHERE id = '{$invited_account_id}' AND reference_id = '{$account_id}'";
            $res = $this->con->query($query);
            $datas = mysqli_fetch_array($res);
            list('referral_side' => $referral_side) = $datas;
            return $referral_side;
        }

        //kuhaon ang heavy side sa last transactions
        function heavySide($pairing_id){
            $query = "SELECT heavy_side FROM pairing_bonus_income WHERE id = '{$pairing_id}'";
            $res = $this->con->query($query);
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('heavy_side' => $heavy_side) = $datas;
            return $heavy_side;
        }

        //pila nay na paired
        function paired($pairing_id){
            $query = "SELECT paired, created FROM pairing_bonus_income WHERE id = '{$pairing_id}'";
            $res = $this->con->query($query);
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            $obj = new \stdClass();
            list('paired' => $paired, 'created' => $created) = $datas;
            $obj->paired = ($paired)? $paired : 0;
            $obj->created = ($created)? $created : date('Y-m-d'); 

            return $obj;
        }

        //retrieve reference_id id
        function reference_id($account_id){
            $query = "SELECT reference_id FROM accounts WHERE id = '{$account_id}'";
            $res = $this->con->query($query);
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('reference_id' => $reference_id) = $datas;
            return $reference_id;
        }

        //remaining balance/balance is associated with heavy side or current heavy side
        function transaction($balance, $value_points, $heavy_side, $referral_side){
            $obj = new \stdClass();
            $obj->income = 0;
            $obj->remaining_vp = $balance;
            $obj->heavy_side = $heavy_side;
            
        
            if($heavy_side == $referral_side)
            {
                $obj->income = 0;
                $obj->remaining_vp = abs($balance + $value_points);
                $obj->heavy_side = $heavy_side;
            }
            else
            {

                if($balance > $value_points)
                {
                    $obj->remaining_vp = abs($balance - $value_points);
                    $obj->income = abs($balance - $obj->remaining_vp);
                    $obj->heavy_side = $heavy_side;

                }
                else if($balance < $value_points)
                {
                    $obj->remaining_vp = abs($value_points - $balance);
                    $obj->income = abs($obj->remaining_vp - $value_points);
                    $obj->heavy_side = $referral_side;
                }
                else
                {
                    $obj->income = $value_points;
                    $obj->heavy_side = $referral_side;
                    $obj->remaining_vp = 0;
                }
            }

        
            return $obj;
        }

        function package_price($account_id){
            $query = "SELECT price FROM packages INNER JOIN accounts ON packages.id = accounts.package_id WHERE accounts.id = '{$account_id}'";
            $res = $this->con->query($query);
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('price' => $price) = $datas;
            return $price;
        }
        
        function totalEarnThisDay($account_id){
            $date = date('Y-m-d');
            $query = "SELECT SUM(income) as `earnings` FROM pairing_bonus_income WHERE account_id = '{$account_id}' AND created = '{$date}'";
            $res = $this->con->query($query);
            $datas = mysqli_fetch_array($res);
            list('earnings' => $earnings) = $datas;;
            return $earnings;
        }
        
    }