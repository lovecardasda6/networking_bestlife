<?php
if (isset($_POST['create_bdo_account'])) {
    $name = $_POST['name'];
    $number = $_POST['number'];

    $res = $accounts->createBankAccount($name, $number, 'BDO');
    if($res){
        header("location: ?page=account&bank=1&action=add&sucess=1");
    }
    else{
        
        header("location: ?page=account&bank=1&action=add&sucess=0");
    }
}

if (isset($_POST['update_bdo_account'])) {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $number = $_POST['number'];

    $res = $accounts->updateBank($id, $name, $number);
    if($res){
        header("location: ?page=account&bank=1&action=update&sucess=1");
    }
    else{
        
        header("location: ?page=account&bank=1&action=update&sucess=0");
    }
}
?>

<ol class="breadcrumb" style="background-color: #00674b;">
    <li class="?page=binarytree" style="color:white">Encashment</li>
    <li class="active" style="color:white">BDO</li>
</ol>

<div class="tiles">
    <div id="title">
        Bank Details - BDO
    </div>
    <hr />

    <?php
    if (!$accounts->fetchBankAccount('BDO')) {
    ?>

        <form method="POST">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="lastname">Account Name : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="name" placeholder="Account Name" required>
                    </div>
                    <div class="form-group">
                        <label for="firstname">Account Number : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="number" placeholder="Account Number" required>
                    </div>
                    <div style="text-align: right;">
                        <input type="submit" value="Create Account" name="submit" class="btn btn-default" />
                    </div>
                </div>
            </div>
        </form>
    <?php
    } else {
        $details = $accounts->fetchBankAccount('BDO');
    ?>
        
        <form method="POST">
            <input type="hidden" name="id" value="<?= $details->id ?>" />
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="lastname">Account Name : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="name" value="<?= $details->account_name ?>" placeholder="Account Name" required>
                    </div>
                    <div class="form-group">
                        <label for="firstname">Account Number : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="number" value="<?= $details->account_number ?>" placeholder="Account Number" required>
                    </div>
                    <div style="text-align: right;">
                        <input type="submit" value="Update Account" name="update" class="btn btn-default" />
                    </div>
                </div>
            </div>
        </form>
    <?php
    }
    ?>
</div>