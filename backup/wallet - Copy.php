<h1 style="font-family:creamCake; font-size: 5em;"> My Journey</h1>
<br />

<style>
    #bronze {
        background: linear-gradient(100deg, #543d28, #fd7a00);
        color: white;
    }

    #silver {
        background: linear-gradient(100deg, #999, #100f0f);
        color: white;
    }

    #gold {
        background: linear-gradient(100deg, #65622b, #e0cf00);
        color: white;
    }

    #platinum {
        background: linear-gradient(100deg, #3f51b5, #000e5a);
        color: white;
    }

    .profile {
        background: #00674b;
        position: inherit;
        top: 0;
        left: 0;
        width: 100%;
        height: 16%;
    }
</style>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="margin-bottom:0px;">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" style="height: 990px; border-radius:6px;">
                    <div class="panel-body profile" style="border-top-left-radius:6px;border-top-right-radius:6px;margin-bottom: 40px;">
                        <div class="row">
                            <center>
                                <br />
                                <br />
                                <img src="./assets/user.png" class="img-rounded" alt="Cinque Terre" style="width:100px;margin-top:18px;background:#fff;border-radius:50px;box-shadow:0 4px 10px 0 rgba(0,0,0,0.2), 0 4px 20px 0 rgba(0,0,0,0.19)">
                            </center>
                            <div style="text-align: center;">
                                <h3><?= ucwords(strtolower($name)) ?></h3>
                                <h4><?= $package ?> Account</h4>
                            </div>
                        </div>
                        
                        
                        <div class="row" style="margin-top:20px;">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align:center; border-right:1px solid; margin-bottom:0.8em;">
                                <a href="?page=transactions"><i class="fa fa-download" aria-hidden="true"></i> Transactions</a>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="text-align:center; border-left:1px solid;">
                                <a href="?page=upgrade"><i class="fa fa-arrow-up" aria-hidden="true"></i> Upgrade</a>
                            </div>
                        </div>
                    </div>



                    <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px;">
                    </div>

                    <div class="panel-body profile" style="height: 80px;">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                            <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                <div id="title" style="padding: 0.7em;background-color:#00674b; color:white">
                                    Accumulated Income
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="panel-body profile" style="height: 70px; border-top:1px solid white;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                                <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                    <div style="padding: 0.7em;background-color:#00674b; color:white">
                                        Direct Referral
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px; height: 80px;">
                            <div style="text-align:right;">
                                <label style="font-size: 1.8em; font-weight: bold;">
                                    <?php
                                    $inc = (!empty($income->directReferralTotalIncome()) ?  $income->directReferralTotalIncome() : 0);
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" . number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="panel-body profile" style="height: 70px; border-top:1px solid white;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                                <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                    <div style="padding: 0.7em;background-color:#00674b; color:white">
                                        Pairing Bonus
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px; height: 160px;">
                            <div style="text-align:right;">
                                <label style="font-size: 2em; font-weight: bold;">
                                    <?php
                                    $inc = (!empty($income->pairingTotalCash()) ?  $income->pairingTotalCash() : 0);
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" . number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                            <hr />
                            <div class="row" style="margin-top: -0.5em;">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin: 0px; text-align:center;">
                                    <label style="font-size:.9em; font-weight: 100;">Left VP</label>
                                    <br />
                                    <?= ($income->pairingBonusLeftVP()) ?  number_format($income->pairingBonusLeftVP()) : '0'; ?>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin: 0px; text-align:center;">
                                    <label style="font-size:.9em; font-weight: 100;">Right VP</label>
                                    <br />
                                    <?= ($income->pairingBonusRightVP()) ?  number_format($income->pairingBonusRightVP()) : '0'; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="panel-body profile" style="height: 70px; border-top:1px solid white;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                                <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                    <div style="padding: 0.7em;background-color:#00674b; color:white">
                                        Indirect Bonus
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px; height: 80px;">
                            <div style="text-align:right;">
                                <label style="font-size: 2em; font-weight: bold;">
                                    <?php
                                    $inc = (!empty($income->indirectBonusTotalIncome()) ?  $income->indirectBonusTotalIncome() : 0);
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" .  number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="panel-body profile" style="height: 70px; border-top:1px solid white;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin: 0.5em 0em 0.1em 0em;">
                                <div class="tiles" style="margin-bottom:3em; padding:0px; margin:0px; border:0px;">
                                    <div style="padding: 0.7em;background-color:#00674b; color:white">
                                        Direct Bonus
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body profile" style="background-color:white; border-top-left-radius:6px;border-top-right-radius:6px; height: 80px;">
                            <div style="text-align:right;">
                                <label style="font-size: 2em; font-weight: bold;">
                                    <?php
                                    $inc = (!empty($income->directBonusTotalIncome()) ?  $income->directBonusTotalIncome() : 0);
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" . number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden;">
                <div class="tiles" style="margin:0px;" id="<?php
                                                            if ($package == "Bronze") :
                                                                echo 'bronze';
                                                            elseif ($package == "Silver") :
                                                                echo 'silver';
                                                            elseif ($package == "Gold") :
                                                                echo 'gold';
                                                            elseif ($package == "Platinum") :
                                                                echo 'platinum';
                                                            else :
                                                                echo 'platinum';
                                                            endif;
                                                            ?>">
                    <div id="title" style=" margin-bottom: 0.5em; text-weight:bold;color:White;">
                        Wallet &nbsp; - &nbsp; <?= $package ?> Account
                    </div>
                    <div style="border-bottom:1px solid #d4cccc;color:white;"></div>
                    <Br />
                    <div style="text-align:center;">
                        <label id="amount">
                            <?php
                            $inc = (!empty($income->walletIncome()) ?  $income->walletIncome() : 0);

                            if ($currency == NULL || $currency == 'PHP') :
                                echo "&#8369;" .  number_format($inc, 2);
                            else :
                                echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                            endif;
                            ?>
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden; margin-top: .3em;">
                <div class="tiles" style="margin:0px; background:linear-gradient(100deg , #00d69c, #00674b);">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label style="font-family:Verdana, Geneva, Tahoma, sans-serif; font-size: 1.5em;">Gift Check</label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center; padding: 0.5em;">
                            <label style="font-size: 3em; color: white;">
                                <?php
                                $inc = (!empty($income->wallet_worth_of_product()) ?  $income->wallet_worth_of_product() : 0);
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .   number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="overflow:hidden;">
                <div class="row" style="padding:0px; margin: 0.2em 0em 0.2em 0em;">
                    <?php if ($income->direct_counter_referrals()['counter'] != 0) : ?>
                        <button type="button" onclick="toogle('directReferral')" style="background-color:#ca8001; color:white; border:0px; border-radius:5px; padding: 8px; margin-top:5px;">
                            Direct Referral
                            <span class="badge badge-light"><?= ($income->direct_counter_referrals()['counter']) ?  number_format($income->direct_counter_referrals()['counter']) : '0'; ?></span>
                        </button>
                    <?php endif;
                    if ($income->pairingCounterCash() != 0) : ?>
                        <button type="button" onclick="toogle('pairingIncomeCash')" style="background-color:#005800; color:white; border:0px; border-radius:5px; padding: 8px; margin-top:5px;">
                            Pairing Income
                            <span class="badge badge-light"><?= ($income->pairingCounterCash()) ?  number_format($income->pairingCounterCash()) : '0'; ?></span>
                        </button>
                    <?php endif;
                    if ($income->pairingCounterGC() != 0) : ?>
                        <button type="button" onclick="toogle('pairingIncomeGC')" class="" style="background-color:#08086b; color:white; color:white; border:0px; border-radius:5px; padding: 8px; margin-top:5px;">
                            Pairing GC
                            <span class="badge badge-light"><?= ($income->pairingCounterGC()) ?  number_format($income->pairingCounterGC()) : '0'; ?></span>
                        </button>
                    <?php endif;
                    if ($income->pairingCounterFlushout() != 0) : ?>
                        <button type="button" onclick="toogle('pairingIncomeFlushout')" class="" style="background-color:#bf0000; color:white; color:white; border:0px; border-radius:5px; padding: 8px; margin-top:5px;">
                            Pairing Flushout
                            <span class="badge badge-light"><?= ($income->pairingCounterFlushout()) ?  number_format($income->pairingCounterFlushout()) : '0'; ?></span>
                        </button>
                    <?php endif;
                    if ($income->indirectBonusIncome()['counter'] != 0) : ?>
                        <button type="button" onclick="toogle('indirectBonus')" class="" style="background-color:#004580; color:white; color:white; border:0px; border-radius:5px; padding: 8px; margin-top:5px;">
                            Indirect Bonus
                            <span class="badge badge-light"><?= ($income->indirectBonusIncome()['counter']) ?  number_format($income->indirectBonusIncome()['counter']) : '0'; ?></span>
                        </button>
                    <?php endif;
                    if ($income->directBonusIncome()['counter'] != 0) : ?>
                        <button type="button" onclick="toogle('directBonus')" class="" style="background-color:#530065; color:white; color:white; border:0px; border-radius:5px; padding: 8px; margin-top:5px;">
                            Direct Bonus
                            <span class="badge badge-light"><?= ($income->directBonusIncome()['counter']) ?  number_format($income->directBonusIncome()['counter']) : '0'; ?></span>
                        </button>
                    <?php endif; ?>




                    <div class="tiles" style="margin-bottom:1em; display:none;" id="directReferral">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Direct Referral
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4" style="border:1px">
                                Cash
                            </div>
                            <div class="col-md-8 col-lg-8" style="text-align:right;">
                                <label style="font-size:1.1em; text-align:right;">
                                    <?php
                                    $inc = ($income->direct_counter_referrals()['total']) ?  $income->direct_counter_referrals()['total'] : 0;
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" .  number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-4 col-lg-4" style="border:1px">
                                Gift Check
                            </div>
                            <div class="col-md-8 col-lg-8" style="text-align:right;">
                                <label style="font-size:1.1em; text-align:right;">
                                    <?php
                                    $inc = ($income->direct_counter_referrals()['product']) ?  $income->direct_counter_referrals()['product'] : 0;
                                    if ($currency == NULL || $currency == 'PHP') :
                                        echo "&#8369;" .  number_format($inc, 2);
                                    else :
                                        echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                    endif;
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none;" id="pairingIncomeCash">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Pairing Bonus - Income Cash
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = ($income->pairingCash()) ? $income->pairingCash() : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none;" id="pairingIncomeGC">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Pairing Bonus - Income GC
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = ($income->pairingGC()) ? $income->pairingGC()  : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none;" id="pairingIncomeFlushout">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Pairing Bonus - Flushout
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = ($income->pairingFlushout()) ? $income->pairingFlushout() : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none;" id="indirectBonus">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Indirect Bonus
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = ($income->indirectBonusIncome()['total']) ? $income->indirectBonusIncome()['total']  : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="tiles" style="margin-bottom:1em; display:none;" id="directBonus">
                        <div style="font-size: 1.2em; font-weight:bold; padding:0px; margin:0px">
                            Direct Bonus
                        </div>
                        <div style="border-bottom:1px solid #dcd0d0; margin:5px 0px;"></div>
                        <div style="text-align:right">
                            <label style="font-size:1.1em; text-align:right;">
                                <?php
                                $inc = ($income->directBonusIncome()['total']) ? $income->directBonusIncome()['total']  : 0;
                                if ($currency == NULL || $currency == 'PHP') :
                                    echo "&#8369;" .  number_format($inc, 2);
                                else :
                                    echo "&#36;" . number_format($exchange_rate->exchange_rate_php_to_usd($inc), 2);
                                endif;
                                ?>
                            </label>
                        </div>
                    </div>

                </div>
            </div>





        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">


        <ul class="nav nav-tabs">
            <li class="active" id="nav_tabs"><a data-toggle="tab" href="#transactions" style="font-size: .9em;">Daily Transactions</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#referral" style="font-size: .9em;">Direct Referral</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#pairing" style="font-size: .9em;">Pairing Bonus</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#indirect" style="font-size: .9em;">Indirect Bonus</a></li>
            <li id="nav_tabs"><a data-toggle="tab" href="#direct" style="font-size: .9em;">Direct Bonus</a></li>
        </ul>

        <div class="tab-content">
            <div id="transactions" class="tab-pane fade in active">
                <?php
                $daily_transactions = $income->daily_transactions();
                if ($daily_transactions != null) :
                    foreach ($daily_transactions as $daily_transaction) :
                ?>
                        <div class="tiles" style="border-radius:0px;">
                            <div class="row">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <?php
                                    $date = date_create(@$daily_transaction['created']);
                                    echo '<b style="font-size:1.2em">' . date_format($date, 'M') . '</b>';
                                    echo "<br/>";
                                    echo date_format($date, 'd');
                                    ?>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-6 col-lg-6">
                                    <b>
                                        <?= $daily_transaction['transaction_type'] ?>
                                    </b>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-2 col-lg-4">
                                    <?php
                                    if ($daily_transaction['transaction_type'] == "DIRECT REFERRAL") {
                                        @$daily = $income->direct_referral($daily_transaction['transaction_id']);
                                        echo 'Incomes - PHP ' . $daily['amount'];
                                        echo "<br/>";
                                        echo 'GC - PHP ' . $daily['gc'];
                                    } else if ($daily_transaction['transaction_type'] == "DIRECT BONUS") {
                                        @$daily = $income->direct($daily_transaction['transaction_id']);
                                        echo 'Income - PHP ' . $daily['amount'];
                                    } else if ($daily_transaction['transaction_type'] == "INDIRECT BONUS") {
                                        @$daily = $income->indirect($daily_transaction['transaction_id']);
                                        echo 'Income - PHP ' . $daily['amount'];
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                <?php
                    endforeach;
                endif;
                ?>
            </div>
            <div id="referral" class="tab-pane fade in active">
                <?= $referrals = $income->direct_referrals() ?>
            </div>
            <div id="pairing" class="tab-pane fade">
                <?= $pairings = $income->pairings() ?>
            </div>
            <div id="indirect" class="tab-pane fade">
                <?= $indirects = $income->indirects() ?>
            </div>
            <div id="direct" class="tab-pane fade">
                <?= $directs = $income->directs() ?>
            </div>
        </div>
    </div>
</div>


<script>
    function toogle(element) {
        var elem = document.getElementById(element).style.display;

        if (element == "directReferral") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "pairingIncomeCash") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "pairingIncomeGC") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "pairingIncomeFlushout") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "indirectBonus") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("directBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        } else if (element == "directBonus") {
            if (elem == 'none') {
                document.getElementById(element).style.display = "block";
                document.getElementById("directReferral").style.display = "none";
                document.getElementById("pairingIncomeCash").style.display = "none";
                document.getElementById("pairingIncomeGC").style.display = "none";
                document.getElementById("pairingIncomeFlushout").style.display = "none";
                document.getElementById("indirectBonus").style.display = "none";
            } else {
                document.getElementById(element).style.display = "none";
            }
        }
    }
</script>