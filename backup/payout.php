<script>
    setInterval(function(){
        fetch("?page=json")
        .then(response => response.text())
        .then(data => console.log(data));
    }, 3000);

</script>

<div class="col-md-12">
    <ol class="breadcrumb" style="background-color: #00674b;">
        <li><a href="?page=dashboard" style="color:white">Dashboard</a></li>
        <li class="active" style="color:white">Payout</li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <form method="POST">
                <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:1.2em; font-weight: 300;">Search : </label>
                    </div>
                    <div class="col-md-7">
                        <input class="form form-control" type="date" name="date" />
                    </div>
                    <div class="col-md-1">
                        <input class="btn btn-default" type="submit" name="search_transactions" value="Search">
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6" style="text-align: right;">
            <a href="?page=pending_transactions">
                <input type="button" class="btn btn-default" value="View pending transaction" />
            </a>
        </div>
    </div>
    <hr />
    <?php if (isset($_GET['success']) && $_GET['success'] == 1) : ?>
        <div style="background-color:#003302; padding: 10px; border-radius:5px; color:white;">
            Cashout successfully confirmed.<br />
        </div>
        <br />
    <?php endif; ?>
    <table class="table table-bordered">
        <thead>
            <tr>
                <td>Acc. ID</td>
                <td>Name</td>
                <td>Wallet</td>
                <td>Cashout </td>
                <td>Trans. Fee(%)</td>
                <td>Deduction</td>
                <td>Net Amount</td>
                <td>Date</td>
                <td>Action</td>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($datas != null) :
                foreach ($datas as $info) :
                    $account_id = $info['account_id'];
                    $cashout_id = $info['cashout_id'];
                    $name = $info['name'];
                    $wallet = $info['wallet'];
                    $cashout_amount = $info['cashout_amount'];
                    $fee = $payout->transaction_fee();
                    $percentage = $fee / 100;
                    $deduction = $cashout_amount * $percentage;
                    $net_amount = $cashout_amount - $deduction;
                    $created = $info['created'];

                    $account_id_hash = password_hash($account_id, PASSWORD_BCRYPT);
                    $cashout_id_hash = password_hash($cashout_id, PASSWORD_BCRYPT);
            ?>
                    <tr>
                        <td><a href='?page=account_information&account_id={$account_id}'><?= $account_id ?></a></td>
                        <td><?= $info['name'] ?></td>
                        <td>PHP <?= number_format($wallet, 2) ?></td>
                        <td>PHP <?= number_format($cashout_amount, 2) ?></td>
                        <td><?= $fee ?> (%)</td>
                        <td>PHP <?= number_format($deduction, 2) ?></td>
                        <td>PHP <?= number_format($net_amount, 2) ?></td>
                        <td><?= $created ?></td>
                        <td>
                            <a target='_blank' href='?page=confirm_payout&transaction_id=<?= $cashout_id_hash ?>&accid=<?= $account_id_hash ?>&account_id=<?= $account_id ?>&id=<?= $cashout_id ?>'>
                                <button class='btn btn-primary' type='button' onclick='display()'>
                                    View
                                </button>
                            </a>
                        </td>
                    </tr>
            <?php endforeach;
            endif; ?>
        </tbody>
    </table>
</div>