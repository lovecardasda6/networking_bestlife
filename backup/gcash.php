<?php
$accounts = new accounts($acc_id);

$error = false;
$createError = false;
$updateError = false;

$success = false;
$createSuccess = false;
$updateSuccess =false;

if (isset($_POST['submit'])) {
    $number = $_POST['number'];

    $res = $accounts->createEWalletAccount($number, 'GCash');
    if($res){
        $success = true;
        $updateSuccess =true;
    }else{
        $error = true;
        $createError = true;
    }
}

if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $number = $_POST['number'];

    $res = $accounts->updateEwallet($id, $number);
    if($res){
        $success = true;
        $updateSuccess =true;
    }else{
        $error = true;
        $updateError = true;
    }
}
?>


<ol class="breadcrumb" style="background-color: #00674b;">
    <li class="?page=binarytree" style="color:white">Encashment</li>
    <li class="active" style="color:white">GCash</li>
</ol>

<div class="tiles" style="border:0px;">>>
    <div id="title">
        EWallet - GCash
    </div>
    <hr />

    <?php
    if (!$accounts->fetchEWalletAccount('GCash')) {
    ?>

        <form method="POST">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstname">EWallet Number : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="number" placeholder="EWallet Number" required>
                    </div>
                    <div style="text-align: right;">
                        <input type="submit" value="Create Account" name="submit" class="btn btn-default" />
                    </div>
                </div>
            </div>
        </form>
    <?php
    } else {
        $details = $accounts->fetchEWalletAccount('GCash');
    ?>
        
        <form method="POST">
            <input type="hidden" name="id" value="<?= $details->id ?>" />
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstname">EWallet Number : <label style="color:red;"> * </label>:</label>
                        <input type="text" class="form-control" name="number" value="<?= $details->ewallet_number ?>" placeholder="EWallet Number" required>
                    </div>
                    <div style="text-align: right;">
                        <input type="submit" value="Update Account" name="update" class="btn btn-default" />
                    </div>
                </div>
            </div>
        </form>
    <?php
    }
    ?>
</div>