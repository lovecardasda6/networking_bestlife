<?php
require_once __DIR__ . '/../config/connections.php';
require_once __DIR__ . '/../functions/login_logout.php';

$loginFailed = false;

if (isset($_POST['login'])) {
    $login = new login_logout();

    $username = trim(htmlspecialchars($_POST['username']));
    $password = trim(htmlspecialchars($_POST['password']));

    $res = $login->login($username, $password);
    if (!$res) {
        $loginFailed = true;
    }
}
?>
<!DOCTYPE html>


<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Best Life Essential</title>
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/style_v2.css" />
    <link rel="stylesheet" type="text/css" href="./assets/font-awesome/css/font-awesome.css" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <script src="./js/jquery.min.js"></script>
    <style>
        .login-box {
            display: flex;
            flex-direction: column;
            box-shadow: none;
            box-sizing: border-box;
            color: rgb(94, 108, 132);
            background: rgb(255, 255, 255);
            border-radius: 3px;
            border-color: transparent;
        }

        .header-brand {
            background: rgb(255, 255, 255);
            margin: auto;
        }

        @media (max-width: 768px) {
            body {
                background: rgb(255, 255, 255) !important;
            }

            #brand {
                width: 100px;
            }
        }

        @media (min-width: 769px) {
            .login-box {
                box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 10px;
                margin: auto;
                /* width: 60%; */
                width: 400px;
                padding: 10px;
            }

            body {
                background: url('./assets/images/networking2.jpg') !important;
            }

            .header-brand {
                background: transparent;
                margin-bottom: 30px;
            }

        }


        .login-failed {
            box-shadow: rgba(9, 30, 66, 0.28) 0px 4px 8px -2px, rgba(9, 30, 66, 0.3) 0px 0px 1px;
            color: rgb(37, 56, 88);
            font-size: 12px;
            line-height: 20px;
            margin-bottom: 24px;
            transform: rotateX(0deg);
            transform-origin: 50% 0px;
            transition-property: visibility, height, margin-bottom, opacity, transform, padding;
            transition-duration: 0s, 0.2s, 0.2s, 0.2s, 0.2s;
            transition-timing-function: ease-in-out;
            border-radius: 3px;
            padding: 16px;
        }

        .login-btn {
            -webkit-box-align: baseline;
            align-items: baseline;
            box-sizing: border-box;
            font-size: inherit !important;
            font-style: normal !important;
            font-weight: 500 !important;
            max-width: 100%;
            cursor: pointer !important;
            vertical-align: middle;
            width: 100%;
            color: rgb(255, 255, 255) !important;
            height: 40px !important;
            line-height: 40px !important;
            text-decoration: none;
            border-radius: 3px !important;
            padding: 0px 8px !important;
            transition: background 0.1s ease-out 0s, box-shadow 0.15s cubic-bezier(0.47, 0.03, 0.49, 1.38) 0s !important;
            outline: none !important;
        }
    </style>
</head>

<body>

    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4" style="padding-right: 0px;padding-left: 0px;">

        <div class="row header-brand">
            <div class=" col-md-2"></div>
            <div class="col-xs-12 col-sm-12 col-md-8">
                <a class="navbar-brand" href="#" style="padding:5px">
                    <img alt="Brand" src="./assets/logo2.png" id="brand">
                </a>
                <a class="navbar-brand" href="javascript:(void)" style="color:black;font-size: 25px;font-weight: 500;padding: 15px 1px;">BestLife Essential</a>
            </div>
            <div class=" col-md-2"></div>
        </div>

        <div class="row">

            <div class="col-sm-12 col-md-12 login-div">
                <div class="panel panel-default login-box">
                    <div class="panel-body">

                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-12">
                                    <?php if ($loginFailed) : ?>
                                        <div class='panel panel-default login-failed' style='text-align:center'>
                                            <h5>
                                                Incorrect username and / or password.
                                                <br>
                                                Do you need help logging in?
                                            </h5>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <br />
                            <form method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-bottom:0px;text-align: center;">
                                            <label for="username" style="font-weight:normal">Log in to continue to:</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: center;">
                                            <label for="username">BestLife Essential</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align:left;border:55px solid;">
                                            <label for="username">Username:</label>
                                            <input type="text" class="form-control" name="username" placeholder="Username" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="password">Password:</label>
                                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-default login-btn" name="login">
                                    Login
                                </button>

                                <hr>
                                <a href="#">Forgot Password</a>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="col-sm-3 col-md-3 col-lg-4"></div>

</body>

</html>