<?php

class bonuses
{
    protected $pairing_bonus;
    protected $indirect_bonus;
    protected $direct_bonus;

    function __construct()
    {
        
        $this->pairing_bonus = new pairing_bonus();
        $this->indirect_bonus = new indirect_bonus();
        $this->direct_bonus = new direct_bonus();
    }
    function pairingIndirectDirectQueueBonuses($account_id, $pairing_account_id, $pairing_invited_account_id, $pairing_new_registered_account_id, $earned)
    {

        $this->indirectDirectQueueBonuses($account_id, $earned);
        $this->pairing_bonus->pairing_income($pairing_account_id, $pairing_invited_account_id, $pairing_new_registered_account_id);
        return true;
    }

    function indirectDirectQueueBonuses($account_id, $earned)
    {

        if ($earned != 0) {
            $this->direct_bonus->direct_income($account_id, $earned);
            $this->indirect_bonus->indirect_income($account_id, $earned);
        }
        return true;
    }
}
