<?php

class indirect_bonus
{

    protected $date;
    protected $con;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    public function indirect_income($account_id, $earned)
    {

        $reference_id = $this->reference_id($account_id);
        $temp_indirect_id = $this->reference_id($reference_id);

        if ($reference_id == 0) {
            return;
        } else {

            for ($level = 1; $level <= 10; $level++) {

                $indirect_package_id = $this->package_id($temp_indirect_id);
                $isValid = $this->validatePackageForIndirectBonus($indirect_package_id);
                if ($isValid) {
                    $percentage =  $this->indirect_percentage($indirect_package_id, $level);
                    $percentage = $percentage / 100;
                    $income = $earned * $percentage;
                    $indirect_bonus_income_id = $this->package_level_id($indirect_package_id, $level);

                    $query = "INSERT INTO indirect_bonus_income VALUES (NULL, '{$temp_indirect_id}', '{$account_id}', '{$indirect_bonus_income_id}', '{$income}', '{$this->date}')";
                    $res = $this->con->query($query);

                    if ($res) {

                        $transaction_id = mysqli_insert_id($this->con);
                        $wallet = new wallet($temp_indirect_id);
                        $response = $wallet->updateWallet($income, 0);

                        if ($response) {

                            $daily_transaction = new daily_transaction();
                            $daily_transaction->indirect_bonus_income($temp_indirect_id, $transaction_id);

                            $bonuses = new bonuses();
                            $bonuses->indirectDirectQueueBonuses($temp_indirect_id, $income);
                        }
                    }
                }

                $temp_indirect_id = $this->reference_id($temp_indirect_id);

                if ($temp_indirect_id == 0) {
                    return;
                }
            }
        }
    }

    public function reference_id($account_id)
    {
        $query = "SELECT reference_id FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);
        if ($res) {
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('reference_id' => $reference_id) = $datas;
            return $reference_id;
        }
    }

    function validatePackageForIndirectBonus($package_id)
    {
        $query = "SELECT id FROM indirect_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);

        if ($res) {
            $num = mysqli_num_rows($res);
            if ($num) {
                return true;
            }
            return false;
        }
    }

    public function package_id($account_id)
    {
        $query = "SELECT package_id FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('package_id' => $package_id) = $datas;
            return $package_id;
        }
    }

    function package_level_id($package_id, $level)
    {
        $query = "SELECT id FROM indirect_bonuses WHERE package_id = '{$package_id}' AND level = '{$level}'";
        $res = $this->con->query($query);

        if ($res) {
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('id' => $id) = $datas;
            return $id;
        }
    }

    function indirect_percentage($package_id, $level)
    {
        $query = "SELECT percent FROM indirect_bonuses WHERE package_id = '{$package_id}' AND level = '{$level}'";
        $res = $this->con->query($query);

        if ($res) {
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('percent' => $percentage) = $datas;

            return $percentage;
        }
    }
    
    function indirect_bonus_income_id($package_id, $level)
    {
        $query = "SELECT id FROM indirect_bonuses WHERE package_id = '{$package_id}' AND level = '{$level}'";
        $res = $this->con->query($query);

        if ($res) {
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('id' => $id) = $datas;

            return $id;
        }
    }
}
