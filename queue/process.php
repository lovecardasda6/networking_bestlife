<?php

require_once __DIR__ . '/../config/Connection.php';
require_once __DIR__ . '/bonuses.php';
require_once __DIR__ . '/daily_transaction.php';
require_once __DIR__ . '/direct_bonus.php';
require_once __DIR__ . '/indirect_bonus.php';
require_once __DIR__ . '/pairing_bonus.php';
require_once __DIR__ . '/wallet.php';

while (true) {
    $con = connectionString();
    $query = "SELECT * FROM processes WHERE status = 'Pending' ORDER BY id ASC";
    $res = $con->query($query);

    if ($res) {
        if (mysqli_num_rows($res)) {
            echo "Pending processes found.\n";
            echo "Processing transactions.\n";
            $timer = 3;
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $process_id = $row['id'];
                $account_id = $row['account_id'];
                $upline_account_id = $row['upline_account_id'];
                $pairing_invited_account_id = $row['pairing_invited_account_id'];
                $pairing_new_registered_account_id = $row['pairing_new_registered_account_id'];
                $direct_referral_income = $row['direct_referral_income'];

                $bonuses = new bonuses();
                $response = $bonuses->pairingIndirectDirectQueueBonuses($account_id, $upline_account_id, $pairing_invited_account_id, $pairing_new_registered_account_id, $direct_referral_income);

                if ($response) {
                    $update = "UPDATE processes SET status = 'Processed' WHERE id = '{$process_id}'";
                    $result = $con->query($update);

                    if ($result) {
                        echo "Transaction no. {$process_id} is complete.\n";
                        sleep($timer);
                        continue;
                    }
                }
                sleep(1);
            }
        } else {
            echo "No pending transactions.\n";
            $timer = 10;
            sleep(1);
        }
    }
    mysqli_close($con);
}
