<?php
class wallet
{
    protected $con;
    protected $account_id;
    protected $date;

    function __construct($account_id)
    {
        $this->con = connectionString();
        $this->account_id = $account_id;
        $this->date = date('Y-m-d H:i:s');
    }



    function updateWallet($amount, $product)
    {
        $wallet = $this->retrieveWallet();
        $balance = (float) $wallet->balance + (float) $amount;
        $product = (float) $wallet->gc + (float) $product;

        $query = "UPDATE wallet SET balance = '{$balance}', gc = '{$product}', updated = '{$this->date}' WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            return true;
        }
        return false;
    }


    function retrieveWallet()
    {
        $query = "SELECT balance, gc FROM wallet WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);
        $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);

        $obj = new \stdClass();
        list('balance' => $balance, 'gc' => $gc) = $datas;
        $obj->balance = $balance;
        $obj->gc = $gc;

        return $obj;
    }
}
