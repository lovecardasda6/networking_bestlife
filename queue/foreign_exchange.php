<?php

require_once __DIR__ . '/../config/connections.php';

$json = file_get_contents("https://v6.exchangerate-api.com/v6/7b0a0bd4b32e2d9c7a0d046c/latest/USD");
$data = json_decode($json);

$con = connectionString();
$query = "SELECT id FROM exchange_rate WHERE currency = 'PHP'";
$res = $con->query($query);
if($res){
    
    $peso_rate_or_usd = $data->conversion_rates->PHP;
    if(mysqli_num_rows($res)){

        $query = "UPDATE exchange_rate SET exchange_rate = '{$peso_rate_or_usd}' WHERE currency = 'PHP'";
        $res = $con->query($query);
        
    }else{
        $query = "INSERT INTO exchange_rate VALUES(NULL, 'PHP', '{$peso_rate_or_usd}')";
        $res = $con->query($query);
        
    }
}
exit;