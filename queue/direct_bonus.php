<?php

class direct_bonus
{
    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    public function direct_income($account_id, $earned)
    {
        $reference_id = $this->fetch_reference_id($account_id);
        if ($reference_id == 0) {
            return;
        }

        $package_id = $this->retrieveReferralPackageType($reference_id);
        $isPackageIncludeForDirectBonus = $this->validatePackageTypeForDirectBonus($package_id);

        if ($isPackageIncludeForDirectBonus) {
            $percentage = $this->receivablePercentage($package_id) / 100;
            $income = $earned * $percentage;

            $response_id = $this->direct_bonus_income_transactions($reference_id, $account_id, $income);
            if ($response_id) {

                $wallet = new wallet($reference_id);
                $response = $wallet->updateWallet($income, 0);
                if ($response) {

                    $daily_transactions = new daily_transaction($reference_id);
                    $daily_transactions->direct_bonus_income($reference_id, $response_id);

                    $bonuses = new bonuses();
                    $bonuses->indirectDirectQueueBonuses($reference_id, $income);
                }
            }
        }
    }

    function fetch_reference_id($account_id)
    {
        $query = "SELECT reference_id FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('reference_id' => $reference_id) = $datas;
                return $reference_id;
            }
        }
    }

    function direct_bonus_income_transactions($reference_id, $account_id, $amount)
    {
        $query = "INSERT INTO direct_bonus_income VALUES (NULL, '{$reference_id}', '{$account_id}', '{$amount}', '{$this->date}')";
        $res = $this->con->query($query);

        if ($res) {
            return mysqli_insert_id($this->con);
        }
        return false;
    }

    function receivablePercentage($package_id)
    {
        $query = "SELECT percentage FROM direct_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);
        if ($res) {
            $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
            list('percentage' => $percentage) = $datas;
            return $percentage;
        }

        return 0;
    }

    function validatePackageTypeForDirectBonus($package_id)
    {
        $validatePackageTypeForDirectBonusQuery = "SELECT percentage FROM direct_bonuses WHERE package_id = '" . $package_id . "'";
        $res = $this->con->query($validatePackageTypeForDirectBonusQuery);
        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('percentage' => $percentage) = $datas;
                return $percentage;
            }
        }
        return false;
    }

    function retrieveReferralPackageType($account_id)
    {
        $retrieveReferralPackageTypeQuery = "SELECT package_id FROM accounts WHERE id = '" . $account_id . "'";
        $result = $this->con->query($retrieveReferralPackageTypeQuery);
        $datas = mysqli_fetch_array($result, MYSQLI_ASSOC);
        list('package_id' => $package_id) = $datas;
        return $package_id;
    }
}
