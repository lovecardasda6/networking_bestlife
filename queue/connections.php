<?php 

function connectionString(){
    $servername = "localhost";
    $port = "3306";
    $username = "root";
    $password = "";
    $database = "build";
    
    // Create connection
    $con = mysqli_connect($servername, $username, $password, $database, $port);

    // Check connection
    if (!$con) {
        die("Connection failed: " . mysqli_connect_error());
        exit();
    }
    return $con;

}
