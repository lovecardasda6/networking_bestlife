<?php

    class daily_transaction{

        protected $con;
        protected $date;
        
        function __construct(){
            $this->con = connectionString();
            $this->date = date('Y-m-d');
        }

        function direct_referral_income($account_id, $transaction_id){
            $query = "INSERT INTO daily_transaction VALUES (NULL, '{$account_id}', 'DIRECT REFERRAL', '{$transaction_id}', '{$this->date}')";
            $res = $this->con->query($query);

            if($res){
                return true;
            }
            return false;
        }

        function direct_bonus_income($account_id, $transaction_id){
            $query = "INSERT INTO daily_transaction VALUES (NULL, '{$account_id}', 'DIRECT BONUS', '{$transaction_id}', '{$this->date}')";
            $res = $this->con->query($query);

            if($res){
                return true;
            }
            return false;
        }
        
        function pairing_bonus_income($account_id, $transaction_id){
            $query = "INSERT INTO daily_transaction VALUES (NULL, '{$account_id}', 'PAIRING BONUS', '{$transaction_id}', '{$this->date}')";
            $res = $this->con->query($query);

            if($res){
                return true;
            }
            return false;
        }

        function indirect_bonus_income($account_id, $transaction_id){
            $query = "INSERT INTO daily_transaction VALUES (NULL, '{$account_id}', 'INDIRECT BONUS', '{$transaction_id}', '{$this->date}')";
            $res = $this->con->query($query);

            if($res){
                return true;
            }
            return false;
        }

        
    }
