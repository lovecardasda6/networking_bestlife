<?php


class pairing_bonus
{
    protected $con;
    protected $date;


    function __construct()
    {
        $this->con  = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function pairing_income($account_id,  $invited_account_id, $new_registered_account_id)
    {

        $account_id = $account_id;
        $invited_account_id = $invited_account_id;
        $new_registered_account_id = $new_registered_account_id;

        $account_package_id = $this->account_package_id($account_id);
        $validateForPairing = $this->validateForPairing($account_package_id);


        if (!$validateForPairing) {

            $this->bonuses($account_id, $invited_account_id, $new_registered_account_id, 0);
            return true;
        }

        $new_registered_account_package_id = $this->account_package_id($new_registered_account_id);
        $new_registered_account_value_points = $this->value_points($new_registered_account_package_id);
        $invited_account_position = $this->invited_account_reside_position($invited_account_id);


        $latest_transaction = $this->latest_transaction($account_id);
        $pairing_id = $latest_transaction->pairing_id;
        $balance = $latest_transaction->balance;
        $heavy_side = $latest_transaction->heavy_side;
        $paired_this_day = $latest_transaction->paired_this_day;

        $transaction = $this->transaction($balance, $new_registered_account_value_points, $heavy_side, $invited_account_position);
        $income = $transaction->income;
        $heavy_side = $transaction->heavy_side;

        $paired = $this->paired($account_id);
        $package_pair_per_day = $this->package_pair_per_day($account_package_id);
        $remark = null;


        $baseVP = $this->fetch_base_value_points();

        if ($income != 0) {
            $doublePairing = array(24, 48, 72, 96, 120, 144, 168, 192, 216);
            $incomes = $income / $baseVP;
            $remark = NULL;
            $totalEarned = 0;
            $totalGC = 0;

            $incomeCash = 0;
            $lastPairedIncomeCash = 0;

            $incomeGC = 0;
            $lastPairedIncomeGC = 0;

            $incomeFlushOut = 0;
            $lastPairedIncomeFlushout = 0;

            $remaining_vp = abs($new_registered_account_value_points - $balance);
            $pairedThisDay = 0;


            for ($i = 1; $i <= $incomes; $i++) {
                $paired_this_day += 1;
                $paired += 1;
                $income = $baseVP;

                if ($paired_this_day > $package_pair_per_day) {
                    $paired_this_day -= 1;
                    $paired -= 1;
                    $remark = "FlushOut";
                } else {
                    if (in_array($paired_this_day, $doublePairing)) {
                        if ($paired == 5) {

                            $paired = 5;
                            $remark = "GC";
                            $income = ($baseVP * 2);
                            $totalGC += $income * 2;
                        } else if ($paired > 5) {

                            $remark = "Income";
                            $income = ($baseVP * 2);
                            $totalEarned += $income;
                            $paired = 1;
                        } else {

                            $remark = "Income";
                            $income = ($baseVP * 2);
                            $totalEarned += $income;
                        }
                    } else {
                        if ($paired == 5) {

                            $paired = 5;
                            $remark = "GC";
                            $totalGC += $baseVP;
                        } else if ($paired > 5) {

                            $remark = "Income";
                            $income = $baseVP;
                            $totalEarned += $income;
                            $paired = 1;
                        } else {
                            $remark = "Income";
                            $income = $baseVP;
                            $totalEarned += $income;
                        }
                    }
                }

                if ($remark == "Income") :
                    $incomeCash += $income;
                    $pairedThisDay = $paired;
                    $lastPairedIncomeCash = $paired_this_day;

                elseif ($remark == "GC") :
                    $incomeGC += $income;
                    $pairedThisDay = $paired;
                    $lastPairedIncomeGC = $paired_this_day;

                elseif ($remark == "FlushOut") :
                    $incomeFlushOut += $income;
                    $pairedThisDay = $paired;
                    $lastPairedIncomeFlushout = $paired_this_day;
                endif;
            }

            if ($lastPairedIncomeCash < $lastPairedIncomeGC) {
                if ($incomeCash != 0) :
                    $this->incomeCash($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeCash, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeCash, $invited_account_position);
                endif;

                if ($incomeGC != 0) :
                    $this->incomeGC($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeGC, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeGC, $invited_account_position);
                endif;

                if ($incomeFlushOut != 0) :
                    $this->incomeFlushOut($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeFlushOut, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeFlushout, $invited_account_position);
                endif;
            }
            else if ($lastPairedIncomeGC < $lastPairedIncomeCash) {

                if ($incomeGC != 0) :
                    $this->incomeGC($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeGC, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeGC, $invited_account_position);
                endif;
                
                if ($incomeCash != 0) :
                    $this->incomeCash($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeCash, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeCash, $invited_account_position);
                endif;

                if ($incomeFlushOut != 0) :
                    $this->incomeFlushOut($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeFlushOut, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeFlushout, $invited_account_position);
                endif;
            }else{

                if ($incomeGC != 0) :
                    $this->incomeGC($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeGC, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeGC, $invited_account_position);
                endif;
                if ($incomeCash != 0) :
                    $this->incomeCash($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeCash, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeCash, $invited_account_position);
                endif;

                if ($incomeFlushOut != 0) :
                    $this->incomeFlushOut($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeFlushOut, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeFlushout, $invited_account_position);
                endif;
            }

            $wallet = new Wallet($account_id);
            $response = $wallet->updateWallet($incomeCash, $incomeGC);

            if ($response) {
                $this->bonuses($account_id, $invited_account_id, $new_registered_account_id, $incomeCash);
                return;
            }
        } else {
            $remaining_vp = abs($new_registered_account_value_points + $balance);
            $query = "INSERT INTO pairing_bonus_income VALUES (NULL, '{$pairing_id}', '{$account_id}', '{$balance}', '{$new_registered_account_id}', '{$new_registered_account_value_points}', '{$income}', '{$remaining_vp}', '{$invited_account_position}', '{$paired}', '{$paired_this_day}', 'Income', '{$this->date}', '{$invited_account_position}')";
            $res = $this->con->query($query);

            if ($res) {
                $this->bonuses($account_id, $invited_account_id, $new_registered_account_id, 0);
                return;
            }
        }
    }

    function incomeCash($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeCash, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeCash, $invited_account_position)
    {

        $query = "INSERT INTO pairing_bonus_income VALUES (NULL, '{$pairing_id}', '{$account_id}', '{$balance}', '{$new_registered_account_id}', '{$new_registered_account_value_points}', '{$incomeCash}', '{$remaining_vp}', '{$heavy_side}', '{$pairedThisDay}', '{$lastPairedIncomeCash}', 'Income', '{$this->date}', '{$invited_account_position}')";
        $res = $this->con->query($query);

        if ($res) :
            $transaction_id = mysqli_insert_id($this->con);
            $daily_transactions = new daily_transaction();
            $daily_transactions->pairing_bonus_income($account_id, $transaction_id);
        endif;
    }

    function incomeGC($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeGC, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeGC, $invited_account_position)
    {

        $query = "INSERT INTO pairing_bonus_income VALUES (NULL, '{$pairing_id}', '{$account_id}', '{$balance}', '{$new_registered_account_id}', '{$new_registered_account_value_points}', '{$incomeGC}', '{$remaining_vp}', '{$heavy_side}', '{$pairedThisDay}', '{$lastPairedIncomeGC}', 'GC', '{$this->date}', '{$invited_account_position}')";
        $res = $this->con->query($query);

        if ($res) :
            $transaction_id = mysqli_insert_id($this->con);
            $daily_transactions = new daily_transaction();
            $daily_transactions->pairing_bonus_income($account_id, $transaction_id);
        endif;
    }

    function incomeFlushOut($pairing_id, $account_id, $balance, $new_registered_account_id, $new_registered_account_value_points, $incomeFlushOut, $remaining_vp, $heavy_side, $pairedThisDay, $lastPairedIncomeFlushout, $invited_account_position)
    {

        $query = "INSERT INTO pairing_bonus_income VALUES (NULL, '{$pairing_id}', '{$account_id}', '{$balance}', '{$new_registered_account_id}', '{$new_registered_account_value_points}', '{$incomeFlushOut}', '{$remaining_vp}', '{$heavy_side}', '{$pairedThisDay}', '{$lastPairedIncomeFlushout}', 'FlushOut', '{$this->date}', '{$invited_account_position}')";
        $res = $this->con->query($query);

        if ($res) :
            $transaction_id = mysqli_insert_id($this->con);
            $daily_transactions = new daily_transaction();
            $daily_transactions->pairing_bonus_income($account_id, $transaction_id);
        endif;
    }

    function paired($account_id)
    {
        $query = "SELECT paired FROM pairing_bonus_income WHERE account_id = '{$account_id}' ORDER BY id DESC LIMIT 1";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('paired' => $paired) = $datas;
                return $paired;
            }
        }

        return 0;
    }

    function package_pair_per_day($package_id)
    {
        $query = "SELECT pair_per_day FROM pairing_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('pair_per_day' => $pair_per_day) = $datas;
                return $pair_per_day;
            }
        }
        return 0;
    }

    function transaction($balance, $value_points, $heavy_side, $invited_account_position)
    {

        $obj = new \stdClass();
        $obj->income = 0;
        $obj->heavy_side = $heavy_side;
        $obj->invited_account_position = $invited_account_position;

        if ($heavy_side == $invited_account_position) {
            $obj->heavy_side = $heavy_side;
        } else {
            if ($value_points > $balance) {
                $obj->income = $balance;
                $obj->heavy_side = $invited_account_position;
            } else if ($balance > $value_points) {
                $obj->income = $value_points;
                $obj->heavy_side = $heavy_side;
            } else if ($balance == $value_points) {
                $obj->income = $value_points;
                $obj->heavy_side = $invited_account_position;
            }
        }

        return $obj;
    }

    function latest_transaction($account_id)
    {
        $query = "SELECT * FROM pairing_bonus_income WHERE account_id = '{$account_id}' ORDER BY id DESC LIMIT 1";
        $res = $this->con->query($query);

        $obj = new \stdClass();
        $obj->pairing_id = 0;
        $obj->balance = 0;
        $obj->heavy_side = "None";
        $obj->paired = 0;
        $obj->paired_this_day = 0;
        $today = date('Y-m-d');


        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list(
                    'id' => $obj->pairing_id,
                    'remaining_vp' => $obj->balance,
                    'heavy_side' => $obj->heavy_side,
                    'paired_this_day' => $obj->paired_this_day,
                    'created' => $created,
                ) = $datas;

                $created = date_create($created);
                $date_format = date_format($created, "Y-m-d");

                if ($today != $date_format) :
                    $obj->paired_this_day = 0;
                endif;

                return $obj;
            }
        }


        return $obj;
    }

    function invited_account_reside_position($account_id)
    {
        $query = "SELECT position FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('position' => $position) = $datas;
                return $position;
            }
        }
    }

    function value_points($package_id)
    {
        $query = "SELECT value_points FROM pairing_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res);
                list('value_points' => $value_points) = $datas;
                return $value_points;
            }
        }

        return 0;
    }

    function bonuses($account_id, $invited_account_id, $new_registered_account_id, $earned)
    {
        $earned_id = $account_id;
        $account_id = $this->reference_id($account_id);
        $invited_account_id = $this->reference_id($invited_account_id);
        if ($account_id == 0) {
            return;
        }

        $bonuses = new bonuses();
        $res = $bonuses->pairingIndirectDirectQueueBonuses($earned_id, $account_id, $invited_account_id, $new_registered_account_id, $earned);
    }

    function reference_id($account_id)
    {
        $query = "SELECT upline_id  FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('upline_id' => $reference_id) = $datas;
                return $reference_id;
            }
        }
        return 0;
    }

    function account_package_id($account_id)
    {
        $query = "SELECT package_id FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('package_id' => $package_id) = $datas;
                return $package_id;
            }
        }

        return 0;
    }

    function validateForPairing($package_id)
    {
        $query = "SELECT id FROM pairing_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);


        if ($res) {
            if (mysqli_num_rows($res)) {
                return true;
            }
        }
        return false;
    }

    function fetch_base_value_points()
    {
        $query = "SELECT value_points FROM pairing_bonuses WHERE status = 'Active'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('value_points' => $value_points) = $datas;
                return $value_points;
            }
        }

        return 250;
    }
}
