<?php
class wallet{
    protected $con;
    protected $account_id;
    protected $date;

    //id sa nag pasulod kay para ma update ang iyang wallet
    function __construct($account_id = null){
        $this->con = connectionString();
        $this->account_id = $account_id;
        $this->date = date('Y-m-d H:i:s');
    }

    

    function updateWallet($amount, $product){
        $wallet = $this->retrieveWallet();
        $balance = floatval($wallet->balance) + floatval($amount);
        $product = floatval($wallet->gc) +floatval($product);

        $query = "UPDATE wallet SET balance = '{$balance}', gc = '{$product}', updated = '{$this->date}' WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if($res){
            return true;    
        }
         return false;
    }
    
    

    function deductWallet($amount, $product){
        $wallet = $this->retrieveWallet();
        $balance = (float) $wallet->balance - (float) $amount;
        $product = (float) $wallet->gc + (float) $product;

        $query = "UPDATE wallet SET balance = '{$balance}', gc = '{$product}', updated = '{$this->date}' WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if($res){
            return true;    
        }
         return false;
    }

    function retrieveWallet(){
        $query = "SELECT balance, gc FROM wallet WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);
        $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);

        $obj = new \stdClass();
        list('balance' => $balance, 'gc' => $gc) = $datas;
        $obj->balance = $balance;
        $obj->gc = $gc;

        return $obj;

    }

    function create_wallet($account_id){
        $query = "INSERT INTO wallet VALUES (NULL, '{$account_id}', '0', '0', '{$this->date}', '{$this->date}')";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_affected_rows($this->con)){
                return true;
            }
        }

        return false;
    }

    function fetch_wallet($account_id){
        $query = "SELECT balance FROM wallet WHERE account_id = '{$account_id}'";
        $res = $this->con->query($query);
        
        if($res):
            if(mysqli_num_rows($res)):
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('balance' => $balance) = $datas;
                return $balance;
            endif;
        endif;
    }

}