<?php

class process{

    protected $con;
    protected $date;

    function __construct(){
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function insertProcess($amount, $product, $account_id, $pairing_account_id, $pairing_invited_account_id, $pairing_new_registered_account_id, $earned){
        
        $query = "INSERT INTO processes VALUES (NULL, '{$account_id}', '{$amount}', '{$product}', '{$pairing_account_id}', '{$pairing_invited_account_id}', '{$pairing_new_registered_account_id}','{$earned}', 'Pending', '{$this->date}')";
        $res = $this->con->query($query);

        if($res){
            return true;
        }
    }
}