<?php
class UpgradeAccountController
{
    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function upgrade_account($account_id, $registration_code)
    {
        $validateRegistrationCode = RegistrationCode::fetch_with_registration_code($registration_code);
        if ($validateRegistrationCode) :
            if ($validateRegistrationCode['status'] == "Unused") :
                $package_id = $validateRegistrationCode['package_id'];
                $registration_code_id  = $validateRegistrationCode['id'];

                
                $response = Account::upgrade_account($account_id, $package_id, $registration_code_id);
                if ($response) :
                    $account_info = Account::fetch($account_id);
                    $referral_id = $account_info['reference_id'];
                    $upline_id = $account_info['upline_id'];

                    $direct_referral_income = DirectReferralIncome::fetch_direct_referral_income_with_package_id($package_id);
                    $income_cash = $direct_referral_income['earnings'];
                    $income_gc = $direct_referral_income['product'];

                    Wallet::add($income_cash, $income_gc, $referral_id);
                    $transaction_id = DirectReferralIncome::store($referral_id, $account_id, $income_cash, $income_gc);
                    DailyTransaction::store($referral_id, 'DIRECT REFERRAL', $transaction_id);
                    RegistrationCode::update_status_with_registration_code_id($registration_code_id);
                    Process::store($referral_id, $income_cash, $income_gc, $upline_id, $account_id, $account_id);

                    header("location: ?page=upgrade&success=1");
                    exit;

                else :
                    return "Account upgrade failed. Please contact the administrator.";
                endif;
            else :
                return "Registration code is already used.";
            endif;
        else :
            return "Invalid registration code. Please try again.";
        endif;
    }

    function update_code_status($registration_code_id)
    {
        $query = "UPDATE registration_codes SET status = 'Used' WHERE id = '{$registration_code_id}'";
        $res = $this->con->query($query);
        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return true;
            }
        }
    }

    function validate_code_and_retrieve_id($code)
    {
        $query = "SELECT id, package_id FROM registration_codes WHERE registration_code = '{$code}' and status = 'Unused'";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('id' => $registration_code_id, 'package_id' => $package_id) = $datas;
                $obj = new \stdClass();
                $obj->registration_code_id = $registration_code_id;
                $obj->package_id = $package_id;
                return $obj;
            endif;
        endif;

        return false;
    }

    function fetch_reference_id($account_id)
    {
        $query = "SELECT reference_id FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res);
                list('reference_id' => $reference_id) = $datas;
                return $reference_id;
            }
        }
    }

    function fetch_upline_id($account_id)
    {
        $query = "SELECT upline_id FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('upline_id' => $upline_id) = $datas;
                return $upline_id;
            endif;
        endif;
    }

    function direct_referral($account_id, $package_id)
    {

        $reference_id = $this->fetch_reference_id($account_id);
        $upline_id = $this->fetch_upline_id($account_id);

        if ($reference_id == 0) {
            return true;;
            exit;
        }

        $direct_referral = new direct_referral($reference_id, $account_id);
        $income = $direct_referral->direct_incomes($package_id);

        if ($income) {
            $amount = $income['earnings'];
            $product = $income['product'];

            $transaction_id = $direct_referral->direct_referral_income($amount, $product);

            $daily_transaction = new daily_transaction();
            $response = $daily_transaction->direct_referral_income($reference_id, $transaction_id);

            if ($response) {

                $wallet = new wallet($reference_id);
                $response = $wallet->updateWallet($amount, $product);
                if ($response) {
                    $process = new process();

                    $process->insertProcess($amount, $product, $reference_id, $upline_id, $account_id, $account_id, $amount);

                    return true;
                }
            }
        }
    }
}
