<?php

class pairing_bonus
{
    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function add_pairing_bonus($obj)
    {
        $package_id = $obj->package_id;
        $value_points = $obj->value_points;
        $pair_per_day = $obj->pair_per_day;

        $query = "INSERT INTO pairing_bonuses VALUES (NULL, '{$package_id}', '{$value_points}', '{$pair_per_day}',  NULL, '{$this->date}')";
        $res = $this->con->query($query);
        if ($res) {
            if(mysqli_affected_rows($this->con)){
                header("location: ?page=pairing_bonus&add=1");
                exit;
            }
        }
        header("location: ?page=pairing_bonus&add=0");
        exit;
    }

    function update_pairing_bonus($obj){
        
        $pairing_id = $obj->pairing_id;
        $package_id = $obj->package_id;
        $value_points = $obj->value_points;
        $pair_per_day = $obj->pair_per_day;

        $query = "UPDATE pairing_bonuses SET `package_id` = '{$package_id}', `value_points`= '{$value_points}', pair_per_day = '{$pair_per_day}' WHERE `id` = '{$pairing_id}'";
        $res = $this->con->query($query);
        if ($res) {
            if(mysqli_affected_rows($this->con)){
                header("location: ?page=pairing_bonus&update=1");
                exit;
            }
        }
        header("location: ?page=pairing_bonus&update=0");
        exit;
    }

    function set_active_vp($pairing_id)
    {
        $query = "UPDATE pairing_bonuses SET status = ''";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                $query = "UPDATE pairing_bonuses SET status = 'Active' WHERE id = '{$pairing_id}'";
                $res = $this->con->query($query);

                if ($res) {
                    if (mysqli_affected_rows($this->con)) {
                        header("location: ?page=pairing_bonus&activate=1");
                        exit;
                    }
                }
            }
        }

        header("location: ?page=pairing_bonus&activate=0");
        exit;
    }
}
