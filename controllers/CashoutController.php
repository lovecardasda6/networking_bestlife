<?php

class CashoutController
{
    private $account_id;

    function __construct($account_id)
    {
        $this->account_id = $account_id;
    }

    function fetch_banks()
    {
        $banks = Bank::fetch_banks($this->account_id);
        return $banks;
    }

    function fetch_remittance()
    {
        $remittance = Remittance::fetch_remittance($this->account_id);
        return $remittance;
    }

    function fetch_ewallet()
    {
        $remittance = EWallet::fetch_ewallet($this->account_id);
        return $remittance;
    }

    function validate_encashment_mode($obj)
    {
        $encashment_mode = base64_decode($obj->encashment_mode);
        $encashment_center = base64_decode($obj->encashment_center);

        $response = Encashment::fetch($encashment_mode, $encashment_center);
        if (!$response) :
            $message = base64_encode("Invalid encashment mode. Please try again.");
            header("Location: ?page=cashout_center&error=1&details={$message}");
            exit;
        endif;
    }

    function cashout($obj)
    {

        $encashment_mode = $obj->encashment_mode;
        $encashment_center = $obj->encashment_center;
        $amount = $obj->amount;
        $amount = str_replace(',', '', $amount);
        $amount = floatval($amount);

        $TransactionFee = TransactionFee::fetch_active();
        $transaction_fee = $TransactionFee['fee'];

        $balance = $this->validate_amount($amount);

        $cashout_id = Cashout::store($this->account_id, $amount, $transaction_fee, $encashment_mode, $encashment_center);

        if ($cashout_id > 0) :
            $response = Wallet::deduct($this->account_id, $amount, 0);

            $balance = floatval($balance) - floatval($amount);

            if ($response) :
                CashoutLog::store($this->account_id, $balance, $amount, $transaction_fee);
                header("Location: ?page=transactions&cashout=1");
                exit;
            endif;
        endif;
    }

    function validate_amount($amount)
    {
        $amount = str_replace(',', '', $amount);
        $amount = floatval($amount);

        if (is_float($amount)) :

            $Wallet = Wallet::fetch($this->account_id);
            $balance = $Wallet['balance'];
            $gc = $Wallet['gc'];
            if (floatval($amount) <= $balance) :
               return $balance;
            endif;
        endif;
        
        header("Location: ?page=cashout_center&cashout=4");
        exit;
    }
}
