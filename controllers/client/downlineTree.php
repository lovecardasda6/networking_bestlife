<?php

class downlineTree{
    function downline($reference_id, $upline_id)
    {

        $downlineTreeQuery = "SELECT accounts.id as `id`, accounts.id as `account_id`, upline_id, CONCAT(lastname, ', ',firstname, ' ', middlename) as `name`, position, packages.package FROM accounts INNER JOIN packages ON packages.id = accounts.package_id WHERE accounts.reference_id = '{$reference_id}' and upline_id = '{$upline_id}' ORDER BY position ASC";
        // $downlineTreeQuery = "SELECT accounts.id as `id`, accounts.id as `account_id`, upline_id, CONCAT(lastname, ', ',firstname, ' ', middlename) as `name`, position, package FROM accounts INNER JOIN packages ON packages.id = accounts.package_id WHERE upline_id = '{$upline_id}' ORDER BY position ASC";

                    //connectionString function from ./functions/connections.php file
                    $res = mysqli_query(connectionString(), $downlineTreeQuery); 
                    $downline = mysqli_fetch_all($res, MYSQLI_ASSOC);
                    if(count($downline) == 1)
                    {
                ?>
                    <ul>
                        <?php if($downline[0]['position'] === "Left"){ ?>
                        <li>
                            <a href="?page=downline&id=<?= $downline[0]['account_id'] ?>">
                            <div class="card">
                            <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                            <p class="title"></p>
                            <p><?= ucwords(strtolower($downline[0]['name'])) ?></p>
                            <p><button class="package-type"><?= ucwords(strtolower($downline[0]['package'])) ?></button></p>
                            </div>
                            </a>
                                <?php $this->downline($reference_id, $downline[0]['account_id']); ?>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default">NONE</a>
                        </li>
                        <?php } else { ?>
                        <li>
                            <a href="#" class="btn btn-default">NONE</a>
                        </li>
                        <li>
                            <a href="?page=downline&id=<?= $downline[0]['account_id'] ?>">
                            <div class="card">
                            <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                            <p class="title"></p>
                            <p><?= ucwords(strtolower($downline[0]['name'])) ?></p>
                            <p><button class="package-type"><?= ucwords(strtolower($downline[0]['package'])) ?></button></p>
                            </div>
                            </a>
                                <?php $this->downline($reference_id, $downline[0]['account_id']); ?>
                        </li>
                        <?php } ?>
                    </ul>
                <?php
                    }
                    else if(count($downline) == 2)
                    {
                ?>
                    <ul>
                        <li>
                            <a href="?page=downline&id=<?= $downline[0]['account_id'] ?>">
                            <div class="card">
                            <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                            <p class="title"></p>
                            <p><?= ucwords(strtolower($downline[0]['name'])) ?></p>
                            <p><button class="package-type"><?= ucwords(strtolower($downline[0]['package'])) ?></button></p>
                            </div>
                            </a>
                                <?php $this->downline($reference_id, $downline[0]['account_id']); ?>
                        </li>
                        <li>
                            <a href="?page=downline&id=<?= $downline[1]['account_id'] ?>">
                            <div class="card">
                            <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                            <p class="title"></p>
                            <p><?= ucwords(strtolower($downline[1]['name'])) ?></p>
                            <p><button class="package-type"><?= ucwords(strtolower($downline[1]['package'])) ?></button></p>
                            </div>
                            </a>
                                <?php $this->downline($reference_id, $downline[1]['account_id']); ?>
                        </li>
                    </ul>
                <?php
                    }
                    else
                    {
                ?>
                    <ul>
                        <li>
                            <a href="#" class="btn btn-default">NONE</a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default">NONE</a>
                        </li>
                    </ul>
                <?php
                    }

    }

}