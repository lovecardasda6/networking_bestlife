<?php

class cashout
{
    protected $con;
    protected $account_id;
    protected $date;

    function __construct($account_id)
    {
        $this->con = connectionString();
        $this->account_id = $account_id;
        $this->date = date('Y-m-d H:i:s');
    }


    function cashout($transaction, $center, $amount)
    {

        $cashout_logs = new cashout_logs();

        $amount = str_replace(',', '', $amount);
        $amount = floatval($amount);
        $this->check_balance($amount);
    

        $query = "INSERT INTO cashout VALUES (NULL, '{$this->account_id}', '{$amount}', '0', '0', '{$transaction}', '{$center}', 'Pending', '', '0', '{$this->date}', '{$this->date}')";
        $res = $this->con->query($query);

        if ($res) {
            $cashout_logs->store_cashout_logs($this->account_id, $amount);

            if (mysqli_affected_rows($this->con)) {
                $this->updateWallet($amount);
                header("location: ?page=transactions&cashout=1");
                exit;
            }
        } else {
            header("location: ?page=cashout_center&cashout=2");
            exit;
        }
    }

    function fetch_encashment_logo($encashment_mode, $encashment_center){
        $query = "SELECT logo FROM encashments WHERE encashment = '{$encashment_mode}' AND encashment_center = '{$encashment_center}'  ORDER BY encashment_center ASC";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('logo' => $logo) = $datas;
                return $logo;
            }
        }
    }

    function check_balance($amount)
    {
        $query = "SELECT balance FROM wallet WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('balance' => $balance) = $datas;
                if (floatval($amount) <= floatval($balance)) {
                    return true;
                }
            }
        }

        header("location: ?page=cashout_center&cashout=4");
        exit;
    }

    function updateWallet($amount)
    {
        $balance = floatval($this->fetch_wallet());
        $balance = floatval($balance) - floatval($amount);

        $query = "UPDATE wallet SET balance = '{$balance}' WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_affected_rows($this->con)){
                return true;
            }
        }
    }

    function fetch_wallet()
    {
        $query = "SELECT balance FROM wallet WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('balance' => $balance) = $datas;
                return $balance;
            }
        }
    }

    function bank_information($center)
    {
        $query = "SELECT * FROM banks WHERE bank_name = '{$center}' AND account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return $datas;
            }
        }

        return null;
    }

    function remittance_information($center)
    {
        $query = "SELECT * FROM remittance WHERE remittance_center = '{$center}' AND account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return $datas;
            }
        }

        return null;
    }

    function ewallet_information($center)
    {
        $query = "SELECT * FROM ewallet WHERE ewallet_center = '{$center}' AND account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return $datas;
            }
        }

        return null;
    }

    function bank_center()
    {
        $query = "SELECT * FROM banks WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
    }

    function remittance_center()
    {
        $query = "SELECT * FROM remittance WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
    }


    function ewallet_centers()
    {
        $query = "SELECT * FROM ewallet WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
    }
}
