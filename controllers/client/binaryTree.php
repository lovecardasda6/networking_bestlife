<?php

class binaryTrees{
    
    function binaryTree($reference_id = null, $upline_id = null)
    {
        $con = connectionString();

        $uplineQuery = "SELECT accounts.id as `id`, accounts.id as `account_id`, upline_id, CONCAT(lastname, ', ',firstname, ' ', middlename) as `name`, position,packages.package FROM accounts INNER JOIN packages ON packages.id = accounts.package_id  WHERE reference_id = '".$reference_id."' and upline_id = '".$upline_id."' ORDER BY position ASC";

            $res = mysqli_query($con, $uplineQuery); 
            $upline = mysqli_fetch_all($res, MYSQLI_ASSOC);
                if(count($upline) == 1)
                {
                ?>
                    <ul>
                        <?php if($upline[0]['position'] === "Left"){ ?>
                        <li>
                            <a href="?page=downline&id=<?= $upline[0]['account_id'] ?>">
                            <div class="card">
                            <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                            <p class="title"></p>
                            <p><?= ucwords(strtolower($upline[0]['name'])) ?></p>
                            <p><button class="package-type"><?= ucwords(strtolower($upline[0]['package'])) ?></button></p>
                            </div>
                            </a>
                                <?php $this->binaryTree($reference_id, $upline[0]['account_id']); ?>
                        </li>
                        <li>
                            <a href="?page=account_registration&ref=<?= $reference_id ?>&upline=<?= $upline_id ?>&position=Right" style="background:white" class="btn btn-default">Add</a>
                        </li>
                        <?php } else { ?>
                        <li>
                            <a href="?page=account_registration&ref=<?= $reference_id ?>&upline=<?= $upline_id ?>&position=Left" style="background:white" class="btn btn-default">Add</a>
                        </li>
                        <li>
                            <a href="?page=downline&id=<?= $upline[0]['account_id'] ?>">
                            <div class="card">
                            <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                            <p class="title"></p>
                            <p><?= ucwords(strtolower($upline[0]['name'])) ?></p>
                            <p><button class="package-type"><?= ucwords(strtolower($upline[0]['package'])) ?></button></p>
                            </div>
                            </a>
                                <?php $this->binaryTree($reference_id, $upline[0]['account_id']); ?>
                        </li>
                        <?php } ?>
                    </ul>
                <?php
                    }
                    else if(count($upline) == 2)
                    {
                ?>
                    <ul>
                        <li>
                            <a href="?page=downline&id=<?= $upline[0]['account_id'] ?>">
                            <div class="card">
                            <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                            <p class="title"></p>
                            <p><?= ucwords(strtolower($upline[0]['name'])) ?></p>
                            <p><button class="package-type"><?= ucwords(strtolower($upline[0]['package'])) ?></button></p>
                            </div>
                            </a>
                                <?php $this->binaryTree($reference_id, $upline[0]['account_id']); ?>
                        </li>
                        <li>
                            <a href="?page=downline&id=<?= $upline[1]['account_id'] ?>">
                            <div class="card">
                            <i class="fa fa-user" aria-hidden="true" style="color:#00674b;font-size:60px"></i>
                            <p class="title"></p>
                            <p><?= ucwords(strtolower($upline[1]['name'])) ?></p>
                            <p><button class="package-type"><?= ucwords(strtolower($upline[1]['package'])) ?></button></p>
                            </div>
                            </a>
                                <?php $this->binaryTree($reference_id, $upline[1]['account_id']); ?>
                        </li>
                    </ul>
                <?php
                    }
                    else
                    {
                ?>
                    <ul>
                        <li>
                            <a href="?page=account_registration&ref=<?= $reference_id ?>&upline=<?= $upline_id ?>&position=Left" style="background:white" class="btn btn-default">Add</a>
                        </li>
                        <li>
                            <a href="?page=account_registration&ref=<?= $reference_id ?>&upline=<?= $upline_id ?>&position=Right" style="background:white" class="btn btn-default">Add</a>
                        </li>
                    </ul>
                <?php
                    }

    }
}