<?php

class direct_referral
{

    protected $account_id;
    protected $invite_account_id;
    protected $con;
    protected $date;

    function __construct($account_id, $invite_account_id)
    {
        $this->account_id = $account_id;
        $this->invite_account_id = $invite_account_id;
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    public function direct_referral_income($amount, $product)
    {

        $query = "INSERT INTO direct_referral_income VALUES (NULL, '{$this->account_id}', '{$this->invite_account_id}', '{$amount}', '{$product}', '{$this->date}')";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                $transaction_id = mysqli_insert_id($this->con);
                return $transaction_id;
            }
        }
    }

    function direct_incomes($package_id){
        $query = "SELECT earnings, product FROM direct_referral_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);

        if($res):
            if(mysqli_num_rows($res)):
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                $income = list('earnings' => $earnings, 'product' => $product) = $datas;
                return $income;
            endif;
        endif;
    }
}
