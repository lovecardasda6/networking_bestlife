<?php

class accounts
{
    protected $con;
    protected $account_id;
    protected $date;

    function __construct($account_id)
    {
        $this->con = connectionString();
        $this->account_id = $account_id;
        $this->date = date('Y-m-d H:i:s');
    }

    function account_informations()
    {
        $query = "SELECT accounts.id as account_id, users.id as `user_id`, `lastname`, `firstname`, `middlename`, `contact`, `gender`, `address`, `date_of_birth`, `place_of_birth`, `reference_id`, `package_id`, `registration_code_id`, `upline_id`, `position`, `username`, `email`, `password`, `token`, `last_login` FROM `accounts` INNER JOIN users ON users.account_id = accounts.id WHERE accounts.id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
    }

    function fetch_account($account_id)
    {
        $query = "SELECT accounts.id as `id`, CONCAT(lastname, ', ',firstname, ' ', middlename) as `name` FROM accounts WHERE id = '{$account_id}'";
        $res = $this->con->query($query);
        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('name' => $name) = $datas;
                return $name;
            }
        }
    }

    function account_currency($currency)
    {
        $query = "SELECT id FROM account_currency WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $query = "UPDATE account_currency SET currency = '{$currency}' WHERE account_id = '{$this->account_id}'";
                $res = $this->con->query($query);
            } else {
                $query = "INSERT INTO account_currency VALUES (NULL, '{$this->account_id}', '{$currency}')";
                $res = $this->con->query($query);
            }
        }
    }

    function fetch_account_currency()
    {
        $query = "SELECT currency FROM account_currency WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('currency' => $currency) = $datas;
                return $currency;
            }
        }

        return null;
    }

    function update_account_information($obj)
    {
        $contact = $obj->contact;
        $gender = $obj->gender;
        $address = $obj->address;
        $date_of_birth = $obj->date_of_birth;
        $place_of_birth = $obj->place_of_birth;

        $query = "UPDATE accounts SET contact = '{$contact}', gender = '{$gender}', address = '{$address}', date_of_birth = '{$date_of_birth}', place_of_birth = '{$place_of_birth}' WHERE id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                header("Location: ?page=account&success=1");
                exit;
            }
        }

        return false;
    }

    function updateEmail($email)
    {

        $email = trim(mysqli_escape_string($this->con, $email));
        $query = "UPDATE users SET email = '{$email}' WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return true;
            }
        }

        return false;
    }

    function validatePassword($current)
    {
        $query = "SELECT password FROM users WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list("password" => $password) = $datas;

                $res = password_verify($current, $password);

                if ($res) {
                    return true;
                }
            }
        }

        return false;
    }

    function updatePassword($password)
    {
        $password = password_hash($password, PASSWORD_BCRYPT);
        $query = "UPDATE users SET password = '{$password}' WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return true;
            }
        }

        return false;
    }


    function fetch_encashment($encashment_mode)
    {
        $query = "SELECT encashment_center FROM encashments WHERE encashment = '{$encashment_mode}' ORDER BY encashment_center ASC";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
    }
    
    ///////////////////////////////////////////////BANK///////////////////////////////////////////////
    function fetch_bank_accounts($bank)
    {
        $query = "SELECT * FROM banks WHERE account_id = '{$this->account_id}' AND bank_name = '{$bank}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
        return false;
    }

    function store_bank_account($name, $number, $bank)
    {
        $query = "INSERT INTO banks VALUES (NULL, '{$this->account_id}', '{$name}', '{$number}', '{$bank}', '{$this->date}')";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                header("location: ?page=account&action=add&account=bank&success=1");
                exit;
            }
        }

        return false;
    }

    function update_bank_account($name, $number, $bank_id)
    {
        $query = "UPDATE banks SET account_name = '{$name}', account_number = '{$number}' WHERE account_id = '{$this->account_id}' AND id = '{$bank_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                header("location: ?page=account&action=update&account=bank&success=1");
                exit;
            }
        }

        return false;
    }
    ///////////////////////////////////////////////BANK///////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////REMITTANCE////////////////////////////////////////////
    function fetch_remittance_accounts($remittance_center)
    {
        $query = "SELECT * FROM remittance WHERE account_id = '{$this->account_id}' AND remittance_center = '{$remittance_center}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
        return false;
    }

    function store_remittance_account($name, $number, $remittance)
    {
        $query = "INSERT INTO remittance VALUES (NULL, '{$this->account_id}', '{$name}', '{$number}', '{$remittance}', '{$this->date}')";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                header("location: ?page=account&action=add&account=remittance&success=1");
                exit;
            }
        }

        return false;
    }

    function update_remittance_account($name, $number, $remittance_id)
    {
        $query = "UPDATE remittance SET receivers_name = '{$name}', contact_number = '{$number}' WHERE account_id = '{$this->account_id}' AND id = '{$remittance_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                header("location: ?page=account&action=update&account=remittance&success=1");
                exit;
            }
        }

        return false;
    }
    ////////////////////////////////////////////REMITTANCE////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////EWALLET//////////////////////////////////////////////
    function fetch_ewallet_accounts($ewallet_center)
    {
        $query = "SELECT * FROM ewallet WHERE account_id = '{$this->account_id}' AND ewallet_center = '{$ewallet_center}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
        return false;
    }

    function store_ewallet_account($number, $ewallet_center)
    {
        $query = "INSERT INTO ewallet VALUES (NULL, '{$this->account_id}', '{$number}', '{$ewallet_center}', '{$this->date}')";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                header("location: ?page=account&action=add&account=ewallet&success=1");
                exit;
            }
        }

        return false;
    }

    function update_ewallet_account($number, $ewallet_id)
    {
        $query = "UPDATE ewallet SET ewallet_number = '{$number}' WHERE account_id = '{$this->account_id}' AND id = '{$ewallet_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                header("location: ?page=account&action=update&account=ewallet&success=1");
                exit;
            }
        }

        return false;
    }
    /////////////////////////////////////////////EWALLET//////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////

    function fetchDocumentFiles()
    {
        $query = "SELECT * FROM documents WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return $datas;
            }
        }

        return false;
    }

    function uploadDocuments($obj)
    {
        $document_type = $obj->document_type;
        $image_files = $obj->file_image;
        $tmp_image = $obj->temp_image;

        $explode = explode(".", $image_files);

        $random_file_name = bin2hex(random_bytes(64));
        $new_file = $random_file_name . '.' . end($explode);
        $target_directory = "./assets/documents/" . $new_file;

        $uploaded = $this->fetchDocumentFiles();

        if ($uploaded) {
            $query = "UPDATE documents SET `type` = '{$document_type}', `path` = '{$target_directory}' WHERE account_id = '{$this->account_id}'";
            $res = $this->con->query($query);

            if ($res) {
                if (mysqli_affected_rows($this->con)) {
                    $res = move_uploaded_file($obj->temp_image, $target_directory);
                    if ($res) {
                        return true;
                    }
                }
            }

            echo "failed";
            exit;
        } else {
            $query = "INSERT INTO documents VALUES (NULL, '{$this->account_id}', '{$document_type}', '{$target_directory}', 'Pending', '{$this->date}')";
            $res = $this->con->query($query);

            if ($res) {
                if (mysqli_affected_rows($this->con)) {
                    $res = move_uploaded_file($obj->temp_image, $target_directory);
                    if ($res) {
                        return true;
                    }
                }
            }
            echo "failed";
            exit;
        }

        return false;
    }

    function fetch_currency()
    {

        $query = "SELECT currency FROM account_currency WHERE account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('currency' => $currency) = $datas;
                return $currency;
            }
        }

        return null;
    }
}
