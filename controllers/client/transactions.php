<?php
class transactions{
    protected $con;
    protected $account_id;
    
    function __construct($account_id)
    {
        $this->con = connectionString();
        $this->account_id = $account_id;
    }

    function transactions(){
        $query = "SELECT * FROM cashout WHERE account_id = '{$this->account_id}' ORDER BY id DESC";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_num_rows($res)){
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }

        return false;
    }

    function fetch_logo($encashment_center){
        $query = "SELECT logo FROM encashments WHERE encashment_center = '{$encashment_center}'";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_num_rows($res)){
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('logo' => $logo) = $datas;
                return $logo;
            }
        }

        return null;
    }
    
}