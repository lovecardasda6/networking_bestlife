<?php

class transaction_fee{
    protected $con;

    function __construct()
    {
        $this->con = connectionString();
    }

    function transaction_fee(){
        $query = "SELECT fee FROM transaction_fee WHERE status = 'Active'";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_num_rows($res)){
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('fee' => $fee) = $datas;
                return $fee;
            }
        }

        return 0;
    }
}