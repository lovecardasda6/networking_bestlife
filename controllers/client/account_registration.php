<?php

class account_registration
{

    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function validate_registration_position($obj){
        
        $reference_id = $obj->reference_id;
        $upline_id = $obj->upline_id;
        $position = $obj->position;

        $query = "SELECT id FROM accounts WHERE reference_id = '{$reference_id}' AND upline_id = '{$upline_id}' AND position = '{$position}'";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_num_rows($res)){
                return true;
            }
        }

        return false;

    }

    function account_registrations($obj)
    {

        $lastname = $obj->lastname;
        $firstname = $obj->firstname;
        $middlename = $obj->middlename;
        $contact = $obj->contact;
        $gender = $obj->gender;
        $address = $obj->address;
        $date_of_birth = ($obj->date_of_birth)?  $obj->date_of_birth : $this->date;
        $place_of_birth = $obj->place_of_birth;

        $reference_id = $obj->reference_id;
        $upline_id = $obj->upline_id;
        $position = $obj->position;
        $registration_code = $obj->registration_code;

        $registration_code_id = $this->validate_registration_code($registration_code);
        $package_id = $this->fetch_registration_code_package_id($registration_code);

        $query = "INSERT INTO accounts VALUES (NULL, '{$lastname}',  '{$firstname}',  '{$middlename}',  '{$contact}',  '{$gender}',  '{$address}',  '{$date_of_birth}',  '{$place_of_birth}',  '{$reference_id}',  '{$package_id}',  '{$registration_code_id}',  '{$upline_id}',  '{$position}',  '{$this->date}')";
        $res  = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return mysqli_insert_id($this->con);
            }
        }

        return false;
    }

    function create_wallet($account_id)
    {
        $query = "INSERT INTO wallet VALUES (NULL, '{$account_id}', '0', '0', '{$this->date}', '{$this->date}')";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return true;
            };
        }
        return false;
    }

    function create_users($account_id, $obj)
    {

        $account_id = $account_id;
        $email_address = $obj->email_address;
        $username = $obj->username;
        $password = $obj->password;
        $password = password_hash($password, PASSWORD_BCRYPT);
        $query = "INSERT INTO users VALUES (NULL, '{$account_id}', '{$username}', '{$email_address}', '{$password}', '', '{$this->date}', '{$this->date}')";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return true;
            }
        }

        return false;
    }

    function fetch_registration_code_package_id($registration_code_id)
    {
        $query = "SELECT package_id FROM registration_codes WHERE id = '{$registration_code_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('package_id' => $package_id) = $datas;
                return $package_id;
            }
        }
    }

    function validate_registration_code($registration_code)
    {
        $query = "SELECT id FROM registration_codes WHERE registration_code = '{$registration_code}' AND status = 'Unused'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('id' => $id) = $datas;
                return $id;
            }
        }
        return false;
    }

    function referral_bonus($account_id, $obj)
    {
        $wallet = new wallet($account_id);
        $income = $obj->earning;
        $product = $obj->product;
        $res = $wallet->updateWallet($income, $product);
        if ($res) {
            return true;
        }

        return false;
    }

    function direct_referral($registration_code_id)
    {
        $package_id = $this->fetch_registration_code_package_id($registration_code_id);
        $query = "SELECT * FROM direct_referral_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                $referral_bonus = list('earnings' => $earnings, 'product' => $product) = $datas;
                return $referral_bonus;
            }
        }

        return 0;
    }

    function validate_username($username)
    {
        $query = "SELECT id FROM users WHERE username = '{$username}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                return true;
            }
        }

        return false;
    }

    function maximum_users($obj)
    {
        $lastname = $obj->lastname;
        $firstname = $obj->firstname;
        $middlename = $obj->middlename;

        $query = "SELECT id FROM accounts WHERE lastname = '{$lastname}' AND firstname = '{$firstname}' AND middlename = '{$middlename}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res) <= 15) {
                return true;
            }
        }

        return false;
    }

    function process(){

    }
}
