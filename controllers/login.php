<?php

@session_start();

class login
{

    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function login($username, $password)
    {
        $username = $username;
        $password = $password;

        $query = "SELECT id, account_id, password FROM users WHERE username = '{$username}'";
        $res = $this->con->query($query);
        if ($res) :
            if (mysqli_num_rows($res)) :
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('id' => $user_id, 'account_id' => $account_id, 'password' => $hash_password) = $datas;

                $auth = password_verify($password, $hash_password);

                if ($auth) :
                    $token = bin2hex(random_bytes(12));

                    $query = "UPDATE users SET token = '{$token}', last_login = '{$this->date}' WHERE id = '{$user_id}' AND account_id = '{$account_id}'";
                    $res = $this->con->query($query);

                    if ($res) :
                        if (mysqli_affected_rows($this->con)) :
                            $_SESSION['user_id'] = $user_id;
                            $_SESSION['token'] = $token;
                            $_SESSION['account_id'] = $account_id;
                            header("location: index.php?page=home");
                            exit;
                        endif;
                    endif;
                endif;
            endif;
        endif;

        return "Incorrect username and / or password.";
    }

    function admin_login($username, $password, $roles)
    {
        $username = trim(mysqli_escape_string($this->con, $username));
        $password = trim(mysqli_escape_string($this->con, $password));

        $query = "SELECT * FROM system_users WHERE username = '{$username}' AND roles = '{$roles}' AND status = 'Active'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list(
                    'id' => $admin_id,
                    'username' => $username,
                    'password' => $hash_password,
                    'lastname' => $lastname,
                    'firstname' => $firstname,
                    'middlename' => $middlename,
                    'roles' => $roles
                ) = $datas;

                $token = bin2hex(random_bytes(12));
                $validate_password = password_verify($password, $hash_password);
                if ($validate_password) {

                    $_SESSION['admin_token'] = $token;
                    $_SESSION['admin_id'] = $admin_id;
                    $_SESSION['admin_name'] = $lastname . ', ' . $firstname . ' ' . $middlename;

                    $response = $this->update_admin_login($admin_id, $token, $roles);

                    if ($response) {
                        if ($roles == 'Administrator') {
                            header("Location: index.php?page=dashboard");
                            exit;
                        } else {
                            header("Location: index.php?page=payout");
                            exit;
                        }
                    }
                }
            }
        }
        header("Location: login.php?error=1");
        exit;
    }

    function update_admin_login($id, $token, $roles)
    {
        $query = "UPDATE system_users SET last_login = '{$this->date}', token = '{$token}' WHERE id = '{$id}' AND roles = '{$roles}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return true;
            }
        }
        return false;
    }
}
