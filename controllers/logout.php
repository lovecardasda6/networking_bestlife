<?php

class logout
{
    protected $con;

    function __construct()
    {
        $this->con = connectionString();
    }

    function logout()
    {
        $user_id = $_SESSION['user_id'];
        $query = "UPDATE users SET token = '' WHERE id = '{$user_id}'";
        $res = $this->con->query($query);
        if ($res) {
            if(mysqli_affected_rows($this->con)){
                unset($_SESSION['user_id']);
                unset($_SESSION['token']);
                unset($_SESSION['account_id']);
            }
        }
        header("location: login.php");
        exit;
    }
}
