<?php

class EncashmentController
{

    public function store($obj)
    {

        $encashment_mode = $obj->encashment_mode;
        $encashment_center = $obj->encashment_center;
        $name_logo = $obj->name_logo;
        $temp_logo = $obj->temp_logo;

        $explode = explode(".", $name_logo);
        $random_file_name = bin2hex(random_bytes(16));
        $logo = $random_file_name . '.' . end($explode);
        $target_directory = __DIR__ . "/../public/assets/logo/" . $logo;
        $response = Encashment::store($encashment_mode, $encashment_center, $logo);

        if ($response) {
            $res = move_uploaded_file($temp_logo, $target_directory);

            if ($res) {
                header("Location: ?page=encashment&add=1");
                exit;
            }
        }

        return false;
    }

    public function update($obj)
    {

        $encashment_id = $obj->encashment_id;
        $current_logo = $obj->logo;
        $encashment_mode = $obj->encashment_mode;
        $encashment_center = $obj->encashment_center;
        $name_logo = $obj->name_logo;
        $temp_logo = $obj->temp_logo;

        if ($name_logo != null || !empty($name_logo)) :

            $explode = explode(".", $name_logo);
            $random_file_name = bin2hex(random_bytes(16));
            $logo = $random_file_name . '.' . end($explode);
            $target_directory = __DIR__ . "/../public/assets/logo/" . $logo;
            $response = Encashment::update($encashment_id, $encashment_mode, $encashment_center, $logo);

            if ($response) :
                $res = move_uploaded_file($temp_logo, $target_directory);
                if ($res) :
                    $filename = __DIR__."/../public/assets/logo/".$current_logo;
                    unlink($filename);
                    header("Location: ?page=encashment&update=1");
                    exit;
                endif;
            endif;
        else :

            $response = Encashment::update($encashment_id, $encashment_mode, $encashment_center, $current_logo);
            if ($response) :
                header("Location: ?page=encashment&update=1");
                exit;
            endif;
        endif;

        return false;
    }

    public function remove($encashment_id)
    {
        $encashment_id = $encashment_id;
        $encashment = Encashment::fetch_by_id($encashment_id);
        $encashment_mode = $encashment['encashment'];
        $encashment_center  = $encashment['encashment_center'];
        $logo  = $encashment['logo'];

        $validate = Cashout::fetch_with_encashment($encashment_mode, $encashment_center);

        
        if($validate):
            return false;
        endif;


        $response = Encashment::remove(($encashment_id));

        if ($response) :

            if ($encashment_mode == "Bank") :
                Bank::remove_by_center($encashment_center);
            elseif ($encashment_mode == "Remittance") :
                Remittance::remove_by_center($encashment_center);
            elseif ($encashment_mode == "EWallet") :
                EWallet::remove_by_center($encashment_center);
            endif;

            $filename = __DIR__."/../public/assets/logo/".$logo;
            unlink($filename);
            header("Location: ?page=encashment&remove=1");
            exit;
        endif;

        return false;
    }
}
