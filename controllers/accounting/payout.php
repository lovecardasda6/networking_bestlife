<?php
class payout
{

    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function transaction()
    {

        $validateDate = $this->retrieveTransactions();


        if (!$validateDate) {
            return;
        }
        $dateTime = $validateDate . " 23:59:59";
        $query = "SELECT accounts.id as `account_id`, cashout.id as `cashout_id`, CONCAT(lastname, ', ', firstname, ' ', middlename) as `name`,  balance as `wallet`, cashout.amount as `cashout_amount`, cashout.encashment_mode, cashout.created FROM accounts INNER JOIN cashout ON accounts.id = cashout.account_id INNER JOIN wallet ON wallet.account_id = cashout.account_id WHERE status = 'Pending' AND cashout.created <= '{$dateTime}'";
        $res = $this->con->query($query);
        if ($res) {
            if (mysqli_num_rows($res)) {

                while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                    $account_id = $row['account_id'];
                    $cashout_id = $row['cashout_id'];
                    $name = $row['name'];
                    $wallet = $row['wallet'];
                    $cashout_amount = $row['cashout_amount'];
                    $transaction_fee = $this->transaction_fee();
                    $percent = $transaction_fee / 100;
                    $deduction = ($cashout_amount * $percent);
                    $recievable_amount = ($cashout_amount - $deduction);
                    $date = $row['created'];


                    $cashout_id_hash = password_hash($cashout_id, PASSWORD_BCRYPT);
                    $account_id_hash = password_hash($account_id, PASSWORD_BCRYPT);

                    echo "
                    <tr>
                        <td><a href='?page=account_information&account_id={$account_id}'>{$account_id}</a></td>
                        <td>{$name}</td>
                        <td>PHP {$wallet}</td>
                        <td>PHP {$cashout_amount}</td>
                        <td>{$transaction_fee} (%)</td>
                        <td>PHP {$deduction}</td>
                        <td>PHP {$recievable_amount}</td>
                        <td>" . date('M d,Y h:i:s a', strtotime($date)) . "</td>
                        <td>
                            <a target='_blank' href='?page=confirm_payout&transaction_id={$cashout_id_hash}&accid={$account_id_hash}&account_id={$account_id}&id={$cashout_id}'>
                                <button class='btn btn-primary' type='button' onclick='display()'>
                                    View
                                </button>
                            </a>
                        </td>
                    </tr>
                    ";
                }
            }
        }
    }

    function transaction_search($date)
    {
        $dateTime = $date . " 23:59:59";
        $query = "SELECT accounts.id as `account_id`, cashout.id as `cashout_id`, CONCAT(lastname, ', ', firstname, ' ', middlename) as `name`,  balance as `wallet`, cashout.amount as `cashout_amount`, fee, receivable_amount, cashout.encashment_mode, cashout.created FROM accounts INNER JOIN cashout ON accounts.id = cashout.account_id INNER JOIN wallet ON wallet.account_id = cashout.account_id WHERE status = 'Pending' AND cashout.created <= '{$dateTime}'";
        $res = $this->con->query($query);
        if ($res) :
            if (mysqli_num_rows($res)) :
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            endif;
        endif;
    }

    function pending_transactions()
    {
        $query = "SELECT accounts.id as `account_id`, cashout.id as `cashout_id`, CONCAT(lastname, ', ', firstname, ' ', middlename) as `name`,  balance as `wallet`, cashout.amount as `cashout_amount`, cashout.encashment_mode, cashout.created FROM accounts INNER JOIN cashout ON accounts.id = cashout.account_id INNER JOIN wallet ON wallet.account_id = cashout.account_id WHERE status = 'Pending' 
        ORDER BY cashout.id ASC";
        $res = $this->con->query($query);
        if ($res) {
            if (mysqli_num_rows($res)) {

                while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                    $account_id = $row['account_id'];
                    $cashout_id = $row['cashout_id'];
                    $name = $row['name'];
                    $wallet = $row['wallet'];
                    $cashout_amount = $row['cashout_amount'];
                    $transaction_fee = $this->transaction_fee();
                    $percent = $transaction_fee / 100;
                    $deduction = ($cashout_amount * $percent);
                    $recievable_amount = ($cashout_amount - $deduction);
                    $date = $row['created'];


                    $cashout_id_hash = password_hash($cashout_id, PASSWORD_BCRYPT);
                    $account_id_hash = password_hash($account_id, PASSWORD_BCRYPT);

                    echo "
                    <tr>
                        <td><a href='?page=account_information&account_id={$account_id}'>{$account_id}</a></td>
                        <td>{$name}</td>
                        <td>PHP {$wallet}</td>
                        <td>PHP {$cashout_amount}</td>
                        <td>{$transaction_fee} (%)</td>
                        <td>PHP {$deduction}</td>
                        <td>PHP {$recievable_amount}</td>
                        <td>" . date('M d,Y h:i:s a', strtotime($date)) . "</td>
                        <td>Pending</td>
                    </tr>
                    ";
                }
            }
        }
    }

    private function retrieveTransactions()
    {
        $payout_time = $this->payout_time();

        $date = date('Y-m-d');
        $today = date_create($date);
        date_sub($today, date_interval_create_from_date_string("1 days"));
        $yester_day = date_format($today, 'l');
        $yesterday = date_format($today, 'Y-m-d');

        if ($payout_time == $yester_day) {
            return $yesterday;
        }
        return false;
    }

    private function payout_time()
    {
        $query = "SELECT day FROM payout_time WHERE status = 'Active'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('day' => $day) = $datas;
                return $day;
            }
        }

        return false;
    }

    public function transaction_fee()
    {
        $query = "SELECT fee FROM transaction_fee WHERE status = 'Active'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('fee' => $fee) = $datas;
                return $fee;
            } else {
                return 0;
            }
        }
    }
}
