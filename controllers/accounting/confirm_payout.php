<?php

class confirm_payout
{

    protected $con;
    protected $date;
    protected $transaction_id;
    protected $account_id;

    function __construct($transaction_id, $account_id)
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
        $this->transaction_id = $transaction_id;
        $this->account_id = $account_id;
    }

    function account_informations()
    {
        $query = "SELECT lastname, firstname, middlename, contact, package FROM accounts INNER JOIN packages ON packages.id = accounts.package_id WHERE accounts.id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res);
                $details = list('lastname' => $lastname, 'firstname' => $firstname, 'middlename' => $middlename, 'contact' => $contact) = $datas;
                return $details;
            }
        }
    }

    function payout_details()
    {
        $query = "SELECT amount, created, status FROM cashout WHERE id = '{$this->transaction_id}' AND account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res);
                list('amount' => $amount, 'created' => $created, 'status' => $status) = $datas;
                $obj = new \stdClass();
                $obj->cashout_amount = $amount;
                $obj->transaction_fee = $this->transaction_fee();
                $obj->percent = $obj->transaction_fee / 100;
                $obj->deduction = ($obj->cashout_amount * $obj->percent);
                $obj->receivable_amount = ($obj->cashout_amount - $obj->deduction);

                $date = date_create($created);
                $format = date_format($date, 'Y-m-d');
                $obj->date = $format;
                $obj->status = $status;
                return $obj;
            }
        }
    }

    function transaction_fee()
    {
        $query = "SELECT fee FROM transaction_fee WHERE status = 'Active'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('fee' => $fee) = $datas;
                return $fee;
            } else {
                return 0;
            }
        }
    }

    function encashment_mode()
    {
        $query = "SELECT encashment_mode, encashment_center FROM cashout WHERE id = '{$this->transaction_id}' AND account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                $encashment = list('encashment_mode' => $encashment_mode, 'encashment_center' => $encashment_center) = $datas;
                return $encashment;
            }
        }
    }

    function bankDetails()
    {
        $encashment = $this->encashment_mode();
        $query = "SELECT * FROM banks WHERE account_id = '{$this->account_id}' AND bank_name = '{$encashment['encashment_center']}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                $details = list('id' => $id, 'account_name' => $account_name, 'account_number' => $account_number, 'bank_name' => $bank_name) = $datas;
                return $details;
            }
        }
    }

    function remittanceDetails()
    {
        $encashment = $this->encashment_mode();
        $query = "SELECT * FROM remittance WHERE account_id = '{$this->account_id}' AND remittance_center = '{$encashment['encashment_center']}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                $details = list('id' => $id, 'receivers_name' => $receivers_name, 'contact_number' => $contact_number, 'remittance_center' => $remittance_center) = $datas;
                return $details;
            }
        }
    }

    function ewalletDetails()
    {
        $encashment = $this->encashment_mode();
        $query = "SELECT * FROM ewallet WHERE account_id = '{$this->account_id}' AND ewallet_center = '{$encashment['encashment_center']}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                $details = list('id' => $id, 'ewallet_number' => $ewallet_number, 'ewallet_center' => $ewallet_center) = $datas;
                return $details;
            }
        }
    }

    function bankEncashment()
    {
        $bankDetails = $this->bankDetails();
        return $bankDetails;
        exit;
        echo "
            <div class='row'>
                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Encashment</label>
                        <input type='text' class='form-control' value='Bank' readonly>
                    </div>
                </div>
                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Cashout Center</label>
                        <input type='text' class='form-control' value='{$bankDetails['bank_name']}' readonly>
                    </div>
                </div>
                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Account name:</label>
                        <input type='text' class='form-control' value='{$bankDetails['account_name']}' readonly>
                    </div>
                </div>
                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Account Number:</label>
                        <input type='text' class='form-control' value='{$bankDetails['account_number']}' readonly>
                    </div>
                </div>
            </div>
        ";
    }

    function remittanceEncashment()
    {
        $remittanceDetails = $this->remittanceDetails();
        return $remittanceDetails;
        exit;
        echo "
            <div class='row'>
                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Encashment</label>
                        <input type='text' class='form-control' value='Remittance' readonly>
                    </div>
                </div>
                
                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Cashout Center</label>
                        <input type='text' class='form-control' value='{$remittanceDetails['remittance_center']}' readonly>
                    </div>
                </div>

                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Receiver's name:</label>
                        <input type='text' class='form-control' value='{$remittanceDetails['receivers_name']}' readonly>
                    </div>
                </div>
                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Contact No.:</label>
                        <input type='text' class='form-control' value='{$remittanceDetails['contact_number']}' readonly>
                    </div>
                </div>
            </div>
        ";
    }

    function eWalletEncashment()
    {
        $ewalletDetails = $this->ewalletDetails();
        return $ewalletDetails;
        exit;
        echo "
            <div class='row'>
                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Cashout Center</label>
                        <input type='text' class='form-control' value='EWallet' readonly>
                    </div>
                </div>

                <div class='col-md-12'>
                    <div class='form-group'>
                        <label for='lastname'>Wallet Number:</label>
                        <input type='text' class='form-control' value='{$ewalletDetails['ewallet_number']}' readonly>
                    </div>
                </div>
            </div>
        ";
    }

    function last_transaction_details()
    {
        $query = "SELECT created FROM cashout WHERE account_id = '{$this->account_id}' AND status = 'Confirmed' ORDER BY id DESC LIMIT 1";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                $date = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('created' => $created) = $date;
                return $created;
            endif;
        endif;


        return null;
    }

    function latest_transaction_details()
    {
        $query = "SELECT created FROM cashout WHERE account_id = '{$this->account_id}' AND status = 'Pending' ORDER BY id DESC LIMIT 1";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                $date = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('created' => $created) = $date;
                return $created;
            endif;
        endif;

        return null;
    }

    function direct_referral_income_details()
    {
        $last_transaction_date_timestamp = $this->last_transaction_details();
        $latest_transaction_date_timestamp = $this->latest_transaction_details();

        $directReferralQuery = null;
        if ($last_transaction_date_timestamp == null) :
            $directReferralQuery = "SELECT * FROM direct_referral_income WHERE account_id = {$this->account_id} AND created <= '{$latest_transaction_date_timestamp}'";
        else :
            $directReferralQuery = "SELECT * FROM direct_referral_income WHERE account_id = '{$this->account_id}' AND created >= '{$last_transaction_date_timestamp}' AND created <= '{$latest_transaction_date_timestamp}'";
        endif;


        $directReferralRes = $this->con->query($directReferralQuery);
        if ($directReferralRes) :
            if (mysqli_num_rows($directReferralRes)) :
                $obj = new \stdClass();
                $obj->amount = 0;
                $obj->product = 0;

                while ($rows = mysqli_fetch_array($directReferralRes, MYSQLI_ASSOC)) {
                    $obj->amount += $rows['amount'];
                    $obj->product += $rows['gc'];
                }

                return $obj;
            endif;
        endif;
    }

    function pairing_income_details()
    {
        $last_transaction_date_timestamp = $this->last_transaction_details();
        $latest_transaction_date_timestamp = $this->latest_transaction_details();

        $pairingBonusQuery = null;
        if ($last_transaction_date_timestamp == null) :
            $pairingBonusQuery = "SELECT * FROM pairing_bonus_income WHERE account_id = '{$this->account_id}' AND created <= '{$latest_transaction_date_timestamp}'";
        else :
            $pairingBonusQuery = "SELECT * FROM pairing_bonus_income WHERE account_id = '{$this->account_id}' AND created >= '{$last_transaction_date_timestamp}' AND created <= '{$latest_transaction_date_timestamp}'";
        endif;


        $pairingBonusRes = $this->con->query($pairingBonusQuery);

        $obj = new \stdClass();
        $obj->income_cash = 0;
        $obj->income_gc = 0;
        $obj->flushout = 0;
        $obj->left_vp = 0;
        $obj->right_vp = 0;

        $temp_invited_id_left = 0;
        $temp_vp_left = 0;

        $temp_invited_id_right = 0;
        $temp_vp_right = 0;
        if ($pairingBonusRes) :
            if (mysqli_num_rows($pairingBonusRes)) :


                while ($rows = mysqli_fetch_array($pairingBonusRes, MYSQLI_ASSOC)) {

                    if ($rows['remark'] == "Income") :
                        $obj->income_cash += $rows['income'];
                    elseif ($rows['remark'] == "GC") :
                        $obj->income_gc += $rows['income'];
                    elseif ($rows['remark'] == "FlushOut") :
                        $obj->flushout += $rows['income'];
                    endif;

                    // if ($rows['positioned'] == 'Left') :
                    //     if ($temp_invited_id_left != $rows['invited_account_id']) :
                    //         $temp_invited_id_left = $rows['invited_account_id'];
                    //         $temp_vp_left += $rows['value_points'];
                    //         $obj->left_vp += $rows['value_points'];
                    //     endif;
                    // endif;
                    // if ($rows['positioned'] == 'Right') :
                    //     if ($temp_invited_id_right != $rows['invited_account_id'] || $temp_vp_right != $rows['value_points']) :
                    //         $temp_invited_id_right = $rows['invited_account_id'];
                    //         $temp_vp_right += $rows['value_points'];
                    //         $obj->right_vp += $rows['value_points'];
                    //     endif;
                    // endif;
                }
                return $obj;
            endif;
        endif;
    }

    function indirect_income_details()
    {
        $last_transaction_date_timestamp = $this->last_transaction_details();
        $latest_transaction_date_timestamp = $this->latest_transaction_details();

        $last_transaction_create_date = date_create($last_transaction_date_timestamp);
        $latest_transaction_create_date = date_create($latest_transaction_date_timestamp);

        $last_transaction_date = date_format($last_transaction_create_date, "Y-m-d");
        $latest_transaction_date = date_format($latest_transaction_create_date, "Y-m-d");

        $indirectBonusQuery = null;
        if ($last_transaction_date_timestamp == null) :
            $indirectBonusQuery = "SELECT * FROM indirect_bonus_income WHERE account_id = '{$this->account_id}' AND created <= '{$latest_transaction_date_timestamp}'";
        else :
            $indirectBonusQuery = "SELECT * FROM indirect_bonus_income WHERE account_id = '{$this->account_id}' AND created >= '{$last_transaction_date}' AND created <= '{$latest_transaction_date}'";
        endif;


        $indirectReferralRes = $this->con->query($indirectBonusQuery);
        if ($indirectReferralRes) :
            if (mysqli_num_rows($indirectReferralRes)) :
                $obj = new \stdClass();
                $obj->amount = 0;

                while ($rows = mysqli_fetch_array($indirectReferralRes, MYSQLI_ASSOC)) {
                    $obj->amount += $rows['amount'];
                }

                return $obj;
            endif;
        endif;
    }

    function direct_income_details()
    {
        $last_transaction_date_timestamp = $this->last_transaction_details();
        $latest_transaction_date_timestamp = $this->latest_transaction_details();

        $directBonusQuery = null;
        if ($last_transaction_date_timestamp == null) :
            $directBonusQuery = "SELECT * FROM direct_bonus_income WHERE account_id = '{$this->account_id}' AND created <= '{$latest_transaction_date_timestamp}'";
        else :
            $directBonusQuery = "SELECT * FROM direct_bonus_income WHERE account_id = '{$this->account_id}' AND created >= '{$latest_transaction_date_timestamp}' AND created <= '{$latest_transaction_date_timestamp}'";
        endif;


        $directReferralRes = $this->con->query($directBonusQuery);
        if ($directReferralRes) :
            if (mysqli_num_rows($directReferralRes)) :
                $obj = new \stdClass();
                $obj->amount = 0;

                while ($rows = mysqli_fetch_array($directReferralRes, MYSQLI_ASSOC)) {
                    $obj->amount += $rows['amount'];
                }

                return $obj;
            endif;
        endif;
    }

    function confirm_payout($remark, $created_on)
    {

        $confirm_user_id = isset($_SESSION['admin_id']) ? $_SESSION['admin_id'] : '';
        $payout_details = $this->payout_details();
        $transaction_fee = $this->transaction_fee();
        $receivable_amount = $payout_details->receivable_amount;
        $cashout_amount = $payout_details->cashout_amount;
        $remark = mysqli_real_escape_string($this->con, $remark);

        $query = "UPDATE cashout SET fee = '{$transaction_fee}', receivable_amount = '{$receivable_amount}', status = 'Confirmed', remark = '{$remark}', confirm_id = '{$confirm_user_id}', updated = '{$this->date}' WHERE id = '{$this->transaction_id}' AND account_id = '{$this->account_id}'";
        $res = $this->con->query($query);

        if ($res) {
            header("location: ?page=payout&success=1&date={$created_on}");
            exit;
        }
    }

    public function pairingBonusLeftVP()
    {
        $query = "SELECT value_points, invited_account_id  FROM pairing_bonus_income WHERE account_id = '{$this->account_id}' AND positioned = 'Left'";
        $res = mysqli_query($this->con, $query);

        $temp_value_points = 0;
        $temp_invited_account_id  = null;
        $temp_vp = 0;
        if ($res) {
            while ($rows = mysqli_fetch_array($res)) {
                if ($temp_invited_account_id  != $rows['invited_account_id'] || $temp_vp != $rows['value_points']) {
                    $temp_invited_account_id  = $rows['invited_account_id'];
                    $temp_vp = $rows['value_points'];
                    $temp_value_points += $rows['value_points'];
                }
            }
        }
        return $temp_value_points;
    }

    public function pairingBonusRightVP()
    {
        $query = "SELECT value_points, invited_account_id FROM pairing_bonus_income WHERE account_id = '{$this->account_id}' AND positioned = 'Right'";
        $res = mysqli_query($this->con, $query);

        $temp_value_points = 0;
        $temp_invited_account_id = null;
        $temp_vp = 0;
        if ($res) {
            while ($rows = mysqli_fetch_array($res)) {
                if ($temp_invited_account_id != $rows['invited_account_id']  || $temp_vp != $rows['value_points']) {
                    $temp_invited_account_id = $rows['invited_account_id'];
                    $temp_vp = $rows['value_points'];
                    $temp_value_points += $rows['value_points'];
                }
            }
        }
        return $temp_value_points;
    }
}
