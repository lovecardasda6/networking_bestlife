<?php

class reports
{

    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function reports()
    {
        $query = "SELECT * FROM cashout INNER JOIN accounts ON accounts.id = cashout.account_id WHERE status = 'Confirmed' ORDER BY cashout.created DESC";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }

        return false;
    }

    function fetch_latest_transaction()
    {
        $date = date_create($this->fetch_latest_date_transaction());
        $date = date_format($date, "Y-m-d");
        $created = $date . " 23:59:59";
        $query = "SELECT * FROM cashout INNER JOIN accounts ON accounts.id = cashout.account_id WHERE status = 'Confirmed' AND cashout.created <= '{$created}' ORDER BY cashout.id";
        $res = $this->connect->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
    }

    function fetch_by_transaction($date)
    {
        $dates = $date;
        $date = date_create($date);
        $date = date_format($date, "Y-m-d");
        $from = $date . " 00:00:00";
        $to = $date . " 23:59:59";

        $query = "SELECT * FROM cashout INNER JOIN accounts ON accounts.id = cashout.account_id WHERE status = 'Confirmed' AND cashout.updated >= '{$from}' AND cashout.updated <= '{$to}' ORDER BY cashout.id ASC";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                $obj = new \stdClass();
                $obj->datas = $datas;
                $obj->date = $dates;
                return $obj;
            }
        }
    }

    function fetch_reports_transaction($from, $to)
    {

        $from = mysqli_escape_string($this->con, trim($from));
        $from = date_create($from);
        $from = date_format($from, "Y-m-d");
        $from = $from . " 00:00:00";

        $to = mysqli_escape_string($this->con, trim($to));
        $to = date_create($to);
        $to = date_format($to, "Y-m-d");
        $to = $to . " 23:59:59";

        $query = "SELECT * FROM cashout INNER JOIN accounts ON accounts.id = cashout.account_id WHERE status = 'Confirmed' AND cashout.created >= '{$from}' AND cashout.created <= '{$to}' ORDER BY cashout.id";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
    }

    function fetch_approved_by($admin_id)
    {
        $query = "SELECT * FROM system_users WHERE id = '{$admin_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return  $datas;
            }
        }
        return false;
    }

    function export_report_to_excels()
    {
        $query = "SELECT accounts.id as `account_id`, lastname, firstname, middlename, contact, amount, fee , receivable_amount, encashment_mode , encashment_center, cashout.updated as `created` FROM accounts INNER JOIN cashout ON accounts.id = cashout.account_id WHERE status = 'Confirmed' ORDER BY cashout.id";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);


                $timestamp = time();
                $filename = 'Export_excel_report_' . $this->date . '.xls';

                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename=\"$filename\"");
                $isPrintHeader = false;
                echo "Transactions\n";
                echo "Date : {$this->date}\n";

                $arr = array();
                foreach ($datas as $data) {
                    $array = array();
                    array_push($array, $data['account_id']);
                    array_push($array, $data['lastname'] . ', ' . $data['firstname'] . ' ' . $data['middlename']);
                    array_push($array, $data['contact']);
                    array_push($array, $data['amount']);
                    array_push($array, $data['fee']);
                    array_push($array, $data['receivable_amount']);
                    array_push($array, $data['encashment_mode']);
                    array_push($array, $data['encashment_center']);

                    if ($data['encashment_mode'] == "Bank") {
                        $encs = $this->fetch_bank_encashment($data['account_id'], $data['encashment_center']);
                        foreach ($encs as $enc) {
                            array_push($array, $enc);
                        }
                        array_push($array, "-");
                        array_push($array, "-");
                        array_push($array, "-");
                    } else if ($data['encashment_mode'] == "Remittance") {
                        $encs = $this->fetch_remitance_encashment($data['account_id'], $data['encashment_center']);

                        array_push($array, "-");
                        array_push($array, "-");
                        foreach ($encs as $enc) {
                            array_push($array, $enc);
                        }
                        array_push($array, "-");
                    } else if ($data['encashment_mode'] == "EWallet") {
                        $encs = $this->fetch_ewallet_encashment($data['account_id'], $data['encashment_center']);

                        array_push($array, "-");
                        array_push($array, "-");
                        array_push($array, "-");
                        array_push($array, "-");
                        foreach ($encs as $enc) {
                            array_push($array, $enc);
                        }
                    }

                    array_push($array, $data['created']);
                    array_push($arr, $array);
                }

                $headers = [
                    "Account ID",
                    "Fullname",
                    "Contact No.",
                    "Gross",
                    "Transaction Fee (%)",
                    "Net Amount",
                    "Encashment",
                    "Encashment Center",
                    "Account Name",
                    "Account Number",
                    "Receiver's Name",
                    "Contact Number",
                    "Ewallet Number",
                    "Approved On",
                ];

                echo implode("\t", array_values($headers)) . "\n";

                foreach ($arr as $excel) {
                    echo implode("\t", array_values($excel)) . "\n";
                }
            }
        }
    }

    function export_to_excel($date)
    {
        $dates = $date;
        $date = mysqli_escape_string($this->con, trim($date));
        $date = date_create($date);
        $date = date_format($date, "Y-m-d");
        $from = $date . " 00:00:00";
        $to = $date . " 23:59:59";
        $query = "SELECT accounts.id as `account_id`, lastname, firstname, middlename, contact, amount, fee , receivable_amount, encashment_mode , encashment_center, cashout.updated as `created` FROM accounts INNER JOIN cashout ON accounts.id = cashout.account_id WHERE status = 'Confirmed' AND cashout.created >= '{$from}' AND cashout.created <= '{$to}' ORDER BY cashout.id";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);


                $timestamp = time();
                $filename = 'Export_excel_report_' . $date . '.xls';

                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename=\"$filename\"");
                $isPrintHeader = false;
                echo "Transactions\n";
                echo "Date : {$dates}\n";

                $arr = array();
                foreach ($datas as $data) {
                    $array = array();
                    array_push($array, $data['account_id']);
                    array_push($array, $data['lastname'] . ', ' . $data['firstname'] . ' ' . $data['middlename']);
                    array_push($array, $data['contact']);
                    array_push($array, $data['amount']);
                    array_push($array, $data['fee']);
                    array_push($array, $data['receivable_amount']);
                    array_push($array, $data['encashment_mode']);
                    array_push($array, $data['encashment_center']);

                    if ($data['encashment_mode'] == "Bank") {
                        $encs = $this->fetch_bank_encashment($data['account_id'], $data['encashment_center']);
                        foreach ($encs as $enc) {
                            array_push($array, $enc);
                        }
                        array_push($array, NULL);
                        array_push($array, NULL);
                        array_push($array, NULL);
                    } else if ($data['encashment_mode'] == "Remittance") {
                        $encs = $this->fetch_remitance_encashment($data['account_id'], $data['encashment_center']);

                        array_push($array, NULL);
                        array_push($array, NULL);
                        foreach ($encs as $enc) {
                            array_push($array, $enc);
                        }
                        array_push($array, NULL);
                    } else if ($data['encashment_mode'] == "EWallet") {
                        $encs = $this->fetch_ewallet_encashment($data['account_id'], $data['encashment_center']);
                        array_push($array, NULL);
                        array_push($array, NULL);
                        array_push($array, NULL);
                        array_push($array, NULL);

                        foreach ($encs as $enc) {
                            array_push($array, $enc);
                        }
                    }

                    array_push($array, $data['created']);
                    array_push($arr, $array);
                }

                $headers = [
                    "Account ID",
                    "Fullname",
                    "Contact No.",
                    "Gross",
                    "Transaction Fee (%)",
                    "Net Amount",
                    "Encashment",
                    "Encashment Center",
                    "Account Name",
                    "Account Number",
                    "Receiver's Name",
                    "Contact Number",
                    "Ewallet Number",
                    "Approved On",
                ];

                echo implode("\t", array_values($headers)) . "\n";

                foreach ($arr as $excel) {
                    echo implode("\t", array_values($excel)) . "\n";
                }
            }
        }
    }

    function fetch_bank_encashment($account_id, $center)
    {
        $query = "SELECT account_name as `Account Name`, account_number as `Account Number` FROM banks WHERE account_id = '{$account_id}' AND bank_name = '{$center}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);

                return $datas;
            }
        }
    }

    function fetch_remitance_encashment($account_id, $center)
    {

        $query = "SELECT receivers_name as `Receiver's Name`, contact_number as `Contact Number` FROM remittance WHERE account_id = '{$account_id}' AND remittance_center = '{$center}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);

                return $datas;
            }
        }
    }

    function fetch_ewallet_encashment($account_id, $center)
    {

        $query = "SELECT  ewallet_number as `EWallet Number` FROM ewallet WHERE account_id = '{$account_id}' AND ewallet_center = '{$center}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);

                return $datas;
            }
        }
    }

    private function fetch_latest_date_transaction()
    {
        $query = "SELECT created FROM cashout WHERE status = 'Confirmed' ORDER BY id DESC LIMIT 1";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_assoc($res);
                list('created' => $created) = $datas;
                return $created;
            }
        }
    }
}
