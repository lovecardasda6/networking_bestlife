<?php

class income_logs
{

    protected $con;
    protected $date;
    protected $connect;
    protected $account_id;
    function __construct($account_id)
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
        $this->connect = db::connect();
        $this->account_id = $account_id;
    }

    function fetch_latest_transaction(){
        $query = "SELECT created FROM cashout WHERE account_id = '$this->account_id' AND status = 'Pending' ORDER by id DESC LIMIT 1";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('created' => $date) = $datas;
                return $date;
            }
        }

        return false;
    }

    function fetch_last_transaction()
    {
        $query = "SELECT created FROM cashout WHERE account_id = '$this->account_id' AND status = 'Confirmed' ORDER by id DESC LIMIT 1";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('created' => $date) = $datas;
                return $date;
            }
        }

        return false;
    }

    function fetch_referral_logs()
    {
        $latest_transaction = $this->fetch_latest_transaction();
        $last_transactions = $this->fetch_last_transaction();
        $query = null;
        if ($last_transactions) {

            $query = "SELECT CONCAT(lastname, ', ',firstname, ' ', middlename) as `name`, package, amount, direct_bonus_income.created  FROM accounts INNER JOIN packages ON packages.id = accounts.package_id INNER JOIN direct_bonus_income ON accounts.id = direct_bonus_income.invited_account_id WHERE direct_bonus_income.account_id = '{$this->account_id}' AND direct_bonus_income.created >= '{$last_transactions}' AND direct_bonus_income.created <= '{$latest_transaction}'";
        } else {


            $query = "SELECT CONCAT(lastname, ', ',firstname, ' ', middlename) as `name`, package, amount, direct_bonus_income.created  FROM accounts INNER JOIN packages ON packages.id = accounts.package_id INNER JOIN direct_bonus_income ON accounts.id = direct_bonus_income.invited_account_id WHERE direct_bonus_income.account_id = '{$this->account_id}' AND direct_bonus_income.created <= '{$latest_transaction}'";
        }
        $res = mysqli_query($this->con, $query);

        while ($rows = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
?>
            <tr id="<?php
                    switch ($rows['package']) {
                        case "Bronze":
                            echo "bronze";
                            break;

                        case "Silver":
                            echo "silver";
                            break;

                        case "Gold":
                            echo "gold";
                            break;

                        case "Platinum":
                            echo "platinum";
                            break;
                    }
                    ?>">
                <td><?= $rows["name"] ?></td>
                <td><?= $rows["package"] ?></td>
                <td>PHP <?= $rows["amount"] ?></td>
                <td><?= $rows["created"] ?></td>
            </tr>
<?php
        }
    }
}
