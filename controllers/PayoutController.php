<?php

class PayoutController
{

    private $con;

    public function __construct()
    {
        $this->con = connectionString();
    }

    function fetch($cashout_id, $account_id)
    {

        $cashout = Cashout::fetch($cashout_id, $account_id);
        return $cashout;
    }

    function confirm($obj)
    {
        $cashout_id = $obj->cashout_id;
        $account_id = $obj->account_id;
        $confirm_by_id = isset($_SESSION['admin_id']) ? $_SESSION['admin_id'] : '';
        $remark = $obj->remark;
        $created_on = $obj->created_on;

        $response = Cashout::confirm_cashout($cashout_id, $account_id, $confirm_by_id, $remark);

        if ($response) :
            header("location: ?page=payout&success=1&date={$created_on}");
        endif;
        return false;
    }

    function all_by_date($date)
    {

        $payouts = Cashout::all_by_date($date);
        return $payouts;
    }

    function export_transactions($date)
    {
        header("Content-Type: application/text");
        header("Content-Disposition: attachment; filename=payout_transactions-{$date}.txt");
        $array = array();
        $transactions = Cashout::export_transactions_with_encashment_details($date);

        foreach ($transactions as $transaction) :
            $arr = array();
            $cashout_id = $transaction['cashout_id'];
            $account_id = $transaction['account_id'];


            $encashment_mode = $transaction['encashment_mode'];
            $encashment_center = $transaction['encashment_center'];

            $arr['cashout_id']  = $cashout_id;
            $arr['account_id'] = $account_id;
            $arr['lastname'] = $transaction['lastname'];
            $arr['firstname']  = $transaction['firstname'];
            $arr['middlename'] = $transaction['middlename'];
            $arr['gross_income']  = $transaction['balance'] + $transaction['amount'];
            $arr['wallet_balance'] = $transaction['balance'];
            $arr['cashout_amount']  = $transaction['amount'];
            $arr['fee'] = $transaction['fee'];
            $arr['receivable_amount']  = $transaction['receivable_amount'];
            $arr['status']  = $transaction['status'];
            $arr['approved_by']  = $transaction['confirm_id'];
            $arr['remark']  = $transaction['remark'];
            $arr['cashout_created']  = $transaction['cashout_created'];
            $arr['encashment_mode']  = $encashment_mode;
            $arr['encashment_center']  = $encashment_center;

            if ($encashment_mode == "Bank") :
                $details = Bank::fetch($account_id, $encashment_center);
                $arr['account_name'] = $details['account_name'];
                $arr['account_number']  = $details['account_number'];

            elseif ($encashment_mode == "Remittance") :
                $details = Remittance::fetch($account_id, $encashment_center);
                $arr['receivers_name'] = $details['receivers_name'];
                $arr['contact_number'] = $details['contact_number'];

            elseif ($encashment_mode == "EWallet") :
                $details = EWallet::fetch($account_id, $encashment_center);
                $arr['ewallet_number'] = $details['ewallet_number'];

            endif;

            $direct_referral_income_details = $this->direct_referral_income_details($account_id);
            $pairing_income_details = $this->pairing_income_details($account_id);
            $pairing_bonus_left = $this->pairingBonusLeftVP($account_id);
            $pairing_bonus_right = $this->pairingBonusRightVP($account_id);

            $indirect_income_details = $this->indirect_income_details($account_id);
            $direct_income_details = $this->direct_income_details($account_id);

            $arr['direct_referral'] = $direct_referral_income_details;
            $arr['pairing_income_details'] = $pairing_income_details;
            $arr['pairing_bonus_left'] = $pairing_bonus_left;
            $arr['pairing_bonus_right'] = $pairing_bonus_right;
            $arr['indirect_income_details'] = $indirect_income_details;
            $arr['direct_income_details'] = $direct_income_details;

            array_push($array, $arr);
        endforeach;

        print_r($array);
        exit;
    }

    function last_transaction_details($account_id)
    {
        $query = "SELECT created FROM cashout WHERE account_id = '{$account_id}' AND status = 'Confirmed' ORDER BY id DESC LIMIT 1";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                $date = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('created' => $created) = $date;
                return $created;
            endif;
        endif;


        return null;
    }

    function latest_transaction_details($account_id)
    {
        $query = "SELECT created FROM cashout WHERE account_id = '{$account_id}' AND status = 'Pending' ORDER BY id DESC LIMIT 1";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                $date = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('created' => $created) = $date;
                return $created;
            endif;
        endif;

        return null;
    }

    function direct_referral_income_details($account_id)
    {
        $last_transaction_date_timestamp = $this->last_transaction_details($account_id);
        $latest_transaction_date_timestamp = $this->latest_transaction_details($account_id);

        $directReferralQuery = null;
        if ($last_transaction_date_timestamp == null) :
            $directReferralQuery = "SELECT * FROM direct_referral_income WHERE account_id = {$account_id} AND created <= '{$latest_transaction_date_timestamp}'";
        else :
            $directReferralQuery = "SELECT * FROM direct_referral_income WHERE account_id = '{$account_id}' AND created >= '{$last_transaction_date_timestamp}' AND created <= '{$latest_transaction_date_timestamp}'";
        endif;


        $directReferralRes = $this->con->query($directReferralQuery);
        if ($directReferralRes) :
            if (mysqli_num_rows($directReferralRes)) :
                $obj = new \stdClass();
                $obj->amount = 0;
                $obj->product = 0;

                while ($rows = mysqli_fetch_array($directReferralRes, MYSQLI_ASSOC)) {
                    $obj->amount += $rows['amount'];
                    $obj->product += $rows['gc'];
                }

                return $obj;
            endif;
        endif;
    }

    function pairing_income_details($account_id)
    {
        $last_transaction_date_timestamp = $this->last_transaction_details($account_id);
        $latest_transaction_date_timestamp = $this->latest_transaction_details($account_id);

        echo $latest_transaction_date_timestamp . "<br>";

        $pairingBonusQuery = null;
        if ($last_transaction_date_timestamp == null) :
            $pairingBonusQuery = "SELECT * FROM pairing_bonus_income WHERE account_id = '{$account_id}' AND created <= '{$latest_transaction_date_timestamp}'";
        else :
            $pairingBonusQuery = "SELECT * FROM pairing_bonus_income WHERE account_id = '{$account_id}' AND created >= '{$last_transaction_date_timestamp}' AND created <= '{$latest_transaction_date_timestamp}'";
        endif;


        $pairingBonusRes = $this->con->query($pairingBonusQuery);

        $obj = new \stdClass();
        $obj->income_cash = 0;
        $obj->income_gc = 0;
        $obj->flushout = 0;

        if ($pairingBonusRes) :
            if (mysqli_num_rows($pairingBonusRes)) :
                while ($rows = mysqli_fetch_array($pairingBonusRes, MYSQLI_ASSOC)) :
                    if ($rows['remark'] == "Income") :
                        $obj->income_cash += $rows['income'];
                    elseif ($rows['remark'] == "GC") :
                        $obj->income_gc += $rows['income'];
                    elseif ($rows['remark'] == "FlushOut") :
                        $obj->flushout += $rows['income'];
                    endif;
                endwhile;

                return $obj;
            endif;
        endif;
    }

    function indirect_income_details($account_id)
    {
        $last_transaction_date_timestamp = $this->last_transaction_details($account_id);
        $latest_transaction_date_timestamp = $this->latest_transaction_details($account_id);

        $last_transaction_create_date = date_create($last_transaction_date_timestamp);
        $latest_transaction_create_date = date_create($latest_transaction_date_timestamp);

        $last_transaction_date = date_format($last_transaction_create_date, "Y-m-d");
        $latest_transaction_date = date_format($latest_transaction_create_date, "Y-m-d");

        $indirectBonusQuery = null;
        if ($last_transaction_date_timestamp == null) :
            $indirectBonusQuery = "SELECT * FROM indirect_bonus_income WHERE account_id = '{$account_id}' AND created <= '{$latest_transaction_date_timestamp}'";
        else :
            $indirectBonusQuery = "SELECT * FROM indirect_bonus_income WHERE account_id = '{$account_id}' AND created >= '{$last_transaction_date}' AND created <= '{$latest_transaction_date}'";
        endif;


        $indirectReferralRes = $this->con->query($indirectBonusQuery);
        if ($indirectReferralRes) :
            if (mysqli_num_rows($indirectReferralRes)) :
                $obj = new \stdClass();
                $obj->amount = 0;

                while ($rows = mysqli_fetch_array($indirectReferralRes, MYSQLI_ASSOC)) {
                    $obj->amount += $rows['amount'];
                }

                return $obj;
            endif;
        endif;

        return 0;
    }

    function direct_income_details($account_id)
    {
        $last_transaction_date_timestamp = $this->last_transaction_details($account_id);
        $latest_transaction_date_timestamp = $this->latest_transaction_details($account_id);

        $directBonusQuery = null;
        if ($last_transaction_date_timestamp == null) :
            $directBonusQuery = "SELECT * FROM direct_bonus_income WHERE account_id = '{$account_id}' AND created <= '{$latest_transaction_date_timestamp}'";
        else :
            $directBonusQuery = "SELECT * FROM direct_bonus_income WHERE account_id = '{$account_id}' AND created >= '{$latest_transaction_date_timestamp}' AND created <= '{$latest_transaction_date_timestamp}'";
        endif;


        $directReferralRes = $this->con->query($directBonusQuery);
        if ($directReferralRes) :
            if (mysqli_num_rows($directReferralRes)) :
                $obj = new \stdClass();
                $obj->amount = 0;

                while ($rows = mysqli_fetch_array($directReferralRes, MYSQLI_ASSOC)) {
                    $obj->amount += $rows['amount'];
                }

                return $obj;
            endif;
        endif;

        return 0;
    }

    public function pairingBonusLeftVP($account_id)
    {
        $query = "SELECT value_points, invited_account_id  FROM pairing_bonus_income WHERE account_id = '{$account_id}' AND positioned = 'Left'";
        $res = mysqli_query($this->con, $query);

        $temp_value_points = 0;
        $temp_invited_account_id  = null;
        $temp_vp = 0;
        if ($res) {
            while ($rows = mysqli_fetch_array($res)) {
                if ($temp_invited_account_id  != $rows['invited_account_id'] || $temp_vp != $rows['value_points']) {
                    $temp_invited_account_id  = $rows['invited_account_id'];
                    $temp_vp = $rows['value_points'];
                    $temp_value_points += $rows['value_points'];
                }
            }
        }
        return $temp_value_points;
    }

    public function pairingBonusRightVP($account_id)
    {
        $query = "SELECT value_points, invited_account_id FROM pairing_bonus_income WHERE account_id = '{$account_id}' AND positioned = 'Right'";
        $res = mysqli_query($this->con, $query);

        $temp_value_points = 0;
        $temp_invited_account_id = null;
        $temp_vp = 0;
        if ($res) {
            while ($rows = mysqli_fetch_array($res)) {
                if ($temp_invited_account_id != $rows['invited_account_id']  || $temp_vp != $rows['value_points']) {
                    $temp_invited_account_id = $rows['invited_account_id'];
                    $temp_vp = $rows['value_points'];
                    $temp_value_points += $rows['value_points'];
                }
            }
        }
        return $temp_value_points;
    }
}
