<?php
class exchange_rate
{
    protected $con;
    protected $date;

    public function __construct()
    {
        $this->con = connectionString();
        $this->today = date('Y-m-d');
    }

    function exchange_rate_php_to_usd($amount)
    {

        $rate = null;

        $query = "SELECT exchange_rate FROM exchange_rate WHERE currency = 'PHP'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('exchange_rate' => $exchange_rate) = $datas;
                $rate = $exchange_rate;
            }
        }

        if ($rate != null) {
            $exchange_rate = $amount / $rate;
            return $exchange_rate;
        }

        return $amount;
    }

    function rate_to_usd()
    {
        $query = "SELECT exchange_rate FROM exchange_rate WHERE currency = 'PHP'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('exchange_rate' => $exchange_rate) = $datas;
                return $exchange_rate;
            }
        }
    }
}
