<?php

class registrationCodes
{
    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function update_status_with_id($registration_code_id)
    {
        $query = "UPDATE registration_codes SET status = 'Used' WHERE id = '{$registration_code_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return true;
            }
        }

        return false;
    }

    function update_status_with_registration_code($registration_code)
    {
        $query = "UPDATE registration_codes SET status = 'Used' WHERE registration_code = '{$registration_code}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return true;
            }
        }

        return false;
    }

    function update_status_with_package_id($package_id)
    {
        $query = "UPDATE registration_codes SET status = 'Used' WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                return true;
            }
        }

        return false;
    }

    function generate_registration_codes($package_id, $type, $quantity)
    {
        for ($i = 1; $i <= $quantity; $i++) {
            $code  = bin2hex(random_bytes(12));
            $code = strtoupper($code);
            $code = substr($code, 0, 16);
            $query = "INSERT INTO registration_codes VALUES (NULL, '{$package_id}', '{$code}', '{$type}', 'Unused', NULL)";
            $res = $this->con->query($query);
        }
    }
}
