<?php

class auth
{
    static function validate()
    {
        $con = connectionString();

        $user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
        $token = isset($_SESSION['token']) ? $_SESSION['token'] : null;
        $account_id = isset($_SESSION['account_id']) ? $_SESSION['account_id'] : null;

        $query = "SELECT id FROM users WHERE token = '{$token}' AND id = '{$user_id}' AND account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                return true;
            } else {
                header("location: login.php");
                exit;
            }
        } else {
            header("location: login.php");
            exit;
        }
    }

    static function check_wallet()
    {
        $con = connectionString();

        $account_id = $_SESSION['account_id'];

        $query = "SELECT id FROM wallet WHERE account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (!mysqli_num_rows($res)) {
                Wallet::store($account_id);
            }
        }
    }

    static function admin_auth($roles)
    {

        $con = connectionString();

        $token =  isset($_SESSION['admin_token']) ? $_SESSION['admin_token'] : NULL;
        $user_id = isset($_SESSION['admin_id']) ? $_SESSION['admin_id'] : NULL;

        $query = "SELECT id FROM system_users WHERE token = '$token' AND id = '{$user_id}' AND roles = '{$roles}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                return true;
            } else {

                header("Location: login.php?error=1");
                exit;
            }
        } else {
            header("location: login.php");
            exit;
        }
    }
}
