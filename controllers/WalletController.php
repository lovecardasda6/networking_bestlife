<?php
class WalletController
{
    protected $account_id;
    protected $date;
    protected $today;

    public function __construct($account_id)
    {
        $this->date = date('Y-m-d H:i:s');
        $this->account_id = $account_id;
        $this->today = date('Y-m-d');
    }

    public function fetch_wallet()
    {
        $wallet = Wallet::fetch($this->account_id);
        return $wallet;
    }

    public function daily_transactions()
    {
        $daily_transactions = DailyTransaction::fetch($this->account_id);
        return $daily_transactions;
    }

    public function fetch_direct_referral_income()
    {
        $direct_referral_incomes = DirectReferralIncome::fetch($this->account_id);

        $current_direct_referral_earned = 0;
        $total_current_direct_referral_earned_cash = 0;
        $total_current_direct_referral_earned_gc = 0;

        $total_direct_referral_earned_cash = 0;
        $total_direct_referral_earned_gc = 0;


        if ($direct_referral_incomes != null) :
            foreach ($direct_referral_incomes as $income) :

                $created = date_create($income['created']);
                $date_format = date_format($created, "Y-m-d");

                if ($this->today == $date_format) :
                    $current_direct_referral_earned += 1;
                    $total_current_direct_referral_earned_cash += $income['amount'];
                    $total_current_direct_referral_earned_gc += $income['gc'];
                endif;

                $total_direct_referral_earned_cash += $income['amount'];
                $total_direct_referral_earned_gc += $income['gc'];
            endforeach;
        endif;

        $array = array();
        $array['direct_referral_incomes'] = $direct_referral_incomes;
        $array['current_direct_referral_earned'] = $current_direct_referral_earned;
        $array['total_current_direct_referral_earned_cash'] = $total_current_direct_referral_earned_cash;
        $array['total_current_direct_referral_earned_gc'] = $total_current_direct_referral_earned_gc;
        $array['total_direct_referral_earned_cash'] = $total_direct_referral_earned_cash;
        $array['total_direct_referral_earned_gc'] = $total_direct_referral_earned_gc;


        return $array;
    }

    public function fetch_pairing_bonus_income()
    {
        $pairing_income = PairingBonusIncome::fetch($this->account_id);

        $current_cash_counter = 0;
        $current_gc_counter = 0;
        $current_flushout_counter = 0;

        $current_cash = 0;
        $current_gc = 0;
        $current_flushout = 0;

        $total_pairing_cash = 0;
        $total_pairing_gc = 0;
        $total_pairing_flushout = 0;

        $left_vp = 0;
        $right_vp = 0;


        $temp_invited_account_id_left = null;
        $temp_value_points_left = null;

        $temp_invited_account_id_right = null;
        $temp_value_points_right = null;

        if ($pairing_income != null) :
            foreach ($pairing_income as $income) :

                $created = date_create($income['created']);
                $date_format = date_format($created, "Y-m-d");

                if ($this->today == $date_format) :
                    if ($income['remark'] == "Income" && $income['income'] != 0) :
                        $current_cash_counter += 1;
                        $current_cash += $income['income'];

                    elseif ($income['remark'] == "GC" && $income['income'] != 0) :
                        $current_gc_counter += 1;
                        $current_gc += $income['income'];

                    elseif ($income['remark'] == "FlushOut" && $income['income'] != 0) :
                        $current_flushout_counter += 1;
                        $current_flushout += $income['income'];
                    endif;
                endif;

                if ($income['remark'] == "Income" && $income['income'] != 0) :
                    $total_pairing_cash += $income['income'];

                elseif ($income['remark'] == "GC" && $income['income'] != 0) :
                    $total_pairing_gc += $income['income'];

                elseif ($income['remark'] == "FlushOut" && $income['income'] != 0) :
                    $total_pairing_flushout += $income['income'];
                endif;
                

                if ($income['positioned'] == "Left" && ($temp_invited_account_id_left != $income['invited_account_id'] || $temp_value_points_left != $income['value_points'])) :
                    $left_vp += $income['value_points']; 
                    $temp_invited_account_id_left = $income['invited_account_id'];
                    $temp_value_points_left = $income['value_points'];
                endif;

                if ($income['positioned'] == "Right" && ($temp_invited_account_id_right != $income['invited_account_id'] || $temp_value_points_right != $income['value_points'])) :
                    $right_vp += $income['value_points'];
                    $temp_invited_account_id_right = $income['invited_account_id'];
                    $temp_value_points_right = $income['value_points'];
                endif;

            endforeach;
        endif;

        $array = array();
        $array['pairing_logs'] = $pairing_income;

        $array['current_cash_counter'] = $current_cash_counter;
        $array['current_gc_counter'] = $current_gc_counter;
        $array['current_flushout_counter'] = $current_flushout_counter;

        $array['current_cash'] = $current_cash;
        $array['current_gc'] = $current_gc;
        $array['current_flushout'] = $current_flushout;

        $array['total_pairing_cash'] = $total_pairing_cash;
        $array['total_pairing_gc'] = $total_pairing_gc;
        $array['total_pairing_flushout'] = $total_pairing_flushout;

        $array['left_vp'] = $left_vp;
        $array['right_vp'] = $right_vp;

        return $array;
    }

    public function fetch_indirect_bonus_income()
    {
        $indirect_income = IndirectBonusIncome::fetch($this->account_id);

        $current_cash_counter = 0;
        $current_cash = 0;

        $total_indirect_cash = 0;

        if ($indirect_income != null) :
            foreach ($indirect_income as $income) :

                $created = date_create($income['created']);
                $date_format = date_format($created, "Y-m-d");

                if ($this->today == $date_format) :
                    $current_cash_counter += 1;
                    $current_cash += $income['amount'];
                endif;

                $total_indirect_cash += $income['amount'];
            endforeach;
        endif;

        $array = array();
        $array['indirect_income'] = $indirect_income;
        $array['current_cash_counter'] = $current_cash_counter;
        $array['current_cash'] = $current_cash;

        $array['total_indirect_cash'] = $total_indirect_cash;


        return $array;
    }

    public function fetch_direct_bonus_income()
    {
        $direct_income = DirectBonusIncome::fetch($this->account_id);

        $current_cash_counter = 0;
        $current_cash = 0;

        $total_direct_cash = 0;

        if ($direct_income != null) :
            foreach ($direct_income as $income) :

                $created = date_create($income['created']);
                $date_format = date_format($created, "Y-m-d");

                if ($this->today == $date_format) :
                    $current_cash_counter += 1;
                    $current_cash += $income['amount'];
                endif;

                $total_direct_cash += $income['amount'];
            endforeach;
        endif;

        $array = array();
        $array['direct_income'] = $direct_income;

        $array['current_cash_counter'] = $current_cash_counter;
        $array['current_cash'] = $current_cash;

        $array['total_direct_cash'] = $total_direct_cash;


        return $array;
    }
}
