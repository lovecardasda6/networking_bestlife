<?php

class system_user
{
    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function create_system_users($obj)
    {

        $username = $obj->username;
        $password = $obj->password;
        $password = password_hash($password, PASSWORD_BCRYPT);

        $lastname = $obj->lastname;
        $firstname =  $obj->firstname;
        $middlename = $obj->middlename;
        $roles = $obj->roles;


        $query = "INSERT INTO system_users VALUES(NULL, '{$username}', '{$password}', '{$lastname}', '{$firstname}', '{$middlename}', '{$roles}', '', '{$this->date}', '{$this->date}', 'Active')";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_affected_rows($this->con)) :
                if ($roles == 'Accounting') {
                    header("location: ?page=create_accounting_account&success=1");
                    exit;
                } else {
                    header("location: ?page=create_administrator_account&success=1");
                    exit;
                }
            endif;
        endif;


        return false;
    }

    function validate_username($input, $role)
    {
        $query = "SELECT id FROM system_users WHERE username = '{$input}' AND roles = '{$role}'";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                return true;
            endif;
        endif;

        return false;
    }

    function system_users($role)
    {
        $query = "SELECT * FROM system_users WHERE roles = '{$role}' AND status = 'Active'";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            endif;
        endif;

        return false;
    }

    function disable_system_users($id)
    {
        $query = "UPDATE system_users SET status = 'Inactive' WHERE id = '{$id}'";
        $res = $this->con->query($query);

        if ($res) :
            if (mysqli_affected_rows($res)) :
                header("location: ?page=accounting&action=disable&success=1");
                exit;
            endif;
        endif;

        return false;
    }
}
