<?php

class pairing_level{
    protected $con;
    protected $date;


    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function fetch_package_id($pairing_level_id){
        $query = "SELECT package_id FROM indirect_level WHERE id = '{$pairing_level_id}'";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_num_rows($res)){
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('package_id' => $package_id) = $datas;
                return $package_id;
            }
        }
        return false;
    }

    function remove_pairing_level($pairing_level_id){
        
        $package_id = $this->fetch_package_id($pairing_level_id);

        $query = "DELETE FROM indirect_level WHERE id = '{$pairing_level_id}'";
        $res = $this->con->query($query);
        if($res){
            return $this->remove_indirect_bonus_level($package_id);
        }

        return false;
    }

    function remove_indirect_bonus_level($package_id){
        $query = "DELETE FROM indirect_bonuses WHERE package_id = '{$package_id}'";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_affected_rows($this->con)){
                return true;
            }
        }
        return false;
    }
}   