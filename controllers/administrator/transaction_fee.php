<?php

class transaction_fee
{
    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function transaction_fees()
    {
        $query = "SELECT * FROM transaction_fee ORDER by fee ASC";
        $res = $this->con->query($query);

        if ($res) {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $id = $row['id'];
                $fee = $row['fee'];
                $status = ($row['status'] == "Active") ? "Active" : "";
                $transactionStatus = ($status == "Active") ? "Active" : "<a href='?page=transaction_fee&active_transaction=true&id={$id}'>Activate</a>";

                echo "
                
                    <tr>
                        <td>{$fee}</td>
                        <td>{$status}</td>
                        <td>
                            {$transactionStatus}
                            &nbsp; | &nbsp; 
                            <a href='?page=transaction_fee&action=modify&id={$id}&fee={$fee}&status={$status}'>Modify</a>
                        </td>
                    </tr>
                ";
            }
        }
    }

    function active_transaction_fee($id)
    {
        $query = "UPDATE transaction_fee SET status = 'Inactive'";
        $res = $this->con->query($query);
        if ($res) {
            $query = "UPDATE transaction_fee SET status = 'Active' WHERE id = '{$id}'";
            $res = $this->con->query($query);
            if ($res) {
                return true;
            }
        }
    }

    function save_transaction_fee($fee)
    {
        $query = "INSERT INTO transaction_fee VALUES (NULL, '{$fee}', 'Inactive', '{$this->date}', '{$this->date}')";
        $res = $this->con->query($query);
        if ($res) {
            return true;
        }
    }

    function update_transaction_fee($id, $fee, $status)
    {
        $query = "UPDATE transaction_fee SET fee = '{$fee}', status = '{$status}', updated = '{$this->date}' WHERE id = '{$id}'";
        $res = $this->con->query($query);
        if ($res) {
            return true;
        }
    }
}
