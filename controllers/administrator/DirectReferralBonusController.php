<?php

class DirectReferralBonusController
{
    public static function store_direct_referral($obj)
    {
        $package_id = $obj->package_id;
        $amount = $obj->amount;
        $product = $obj->product;

        $direct_referral_bonus_id = DirectReferralBonus::store($package_id, $amount, $product);
        if($direct_referral_bonus_id){
            header("Location: ?page=referral_bonus&action=add&success=1");
            exit;
        }
        return "Cannot add referral bonus and/or package name is already used.";
    }

    public function update_direct_referral($obj){
        
        $direct_referral_bonus_id = $obj->direct_referral_bonus_id;
        $package_id = $obj->package_id;
        $amount = $obj->amount;
        $product = $obj->product;

        $response = DirectReferralBonus::update($direct_referral_bonus_id, $package_id, $amount, $product);
        if($response){
            header("Location: ?page=referral_bonus&action=add&success=1");
            exit;
        }
        return "Cannot add referral bonus and/or package name is already used.";
    }
}
