<?php

class notifications{
    
    protected $con;
    protected $date;


    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function notification(){
        $query = "SELECT * FROM documents INNER JOIN accounts ON accounts.id = documents.account_id WHERE status = 'Pending'";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_num_rows($res)){
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }

        return null;
    }

    
    static function fetch_notifications(){
        $query = "SELECT id FROM documents WHERE status = 'Pending'";
        $res = connectionString()->query($query);

        if($res){
            if(mysqli_num_rows($res)){
                return mysqli_num_rows($res);
            }
        }

        return null;
    }

    static function fetch_documents($account_id){
        $con = connectionString();
        $account_id = trim($account_id);
        $account_id = mysqli_escape_string($con, $account_id);
        $query = "SELECT * FROM documents WHERE account_id = '{$account_id}'";
        $res = connectionString()->query($query);

        if($res){
            if(mysqli_num_rows($res)){
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                return $datas;
            }
        }

        return null;
    }
}