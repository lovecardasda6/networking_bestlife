<?php
class PackageController
{
    function store_package($obj)
    {
        $package = $obj->package;
        $price = $obj->price;

        $package_id = Package::store($package, $price);
        if ($package_id > 0) :
            header("Location: ?page=package&action=add&success=1");
            exit;
        endif;

        return "Invalid package name and/or already used. Please try again";
    }

    function update_package($obj)
    {
        $package_id = $obj->package_id;
        $package = $obj->package;
        $price = $obj->price;

        $response = Package::update($package_id, $package, $price);
        if ($response) :
            header("Location: ?page=package&action=update&success=1");
            exit;
        endif;

        return "Package cannot be updated. Please try again.";
    }

    function remove_package($package_id)
    {
        $package_id = $package_id;

        $response = Package::remove($package_id);
        if ($response) :
            header("Location: ?page=package&action=remove&success=1");
            exit;
        endif;

        return "Package cannot be remove. Already used. Please try again.";
    }
}
