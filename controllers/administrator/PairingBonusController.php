<?php
class PairingBonusController
{
    function store_pairing($obj)
    {
        $package_id = $obj->package_id;
        $value_points = $obj->value_points;
        $pair_per_day = $obj->pair_per_day;

        $pairing_id = PairingBonus::store($package_id, $value_points, $pair_per_day);
        if ($package_id > 0) :
            header("Location: ?page=pairing_bonus&action=add&success=1");
            exit;
        endif;

        return "Invalid package name and/or already used. Please try again";
    }

    function update_pairing($obj)
    {
        $pairing_id = $obj->pairing_id;
        $package_id = $obj->package_id;
        $value_points = $obj->value_points;
        $pair_per_day = $obj->pair_per_day;

        $response = PairingBonus::update($pairing_id, $package_id, $value_points, $pair_per_day);
        if ($response) :
            header("Location: ?page=pairing_bonus&action=update&success=1");
            exit;
        endif;

        return "Invalid package name and/or already used. Please try again";
    }

    function activate_value_points($obj)
    {
        $pairing_id = $obj->pairing_id;
        $package_id = $obj->package_id;

        PairingBonus::inactive();
        $response = PairingBonus::activate($pairing_id, $package_id);
        if ($response) :
            header("Location: ?page=pairing_bonus&action=activate&success=1");
            exit;
        else :
            return "Cannot update current selected value points. Please try again. Error code 2";
        endif;

        return "Cannot update current selected value points. Please try again";
    }
}
