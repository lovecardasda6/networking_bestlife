<?php
class search{
    protected $con;
    protected $date ;
    protected $search;

    function __construct($input)
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
        $this->search = $input;
    }

    function accounts(){
        $query = "SELECT * FROM accounts WHERE lastname LIKE '%{$this->search}%' OR firstname LIKE '%{$this->search}%' OR middlename LIKE '%{$this->search}%'";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_num_rows($res)){
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                return $datas;
            }
        }
        return false;
    }
}