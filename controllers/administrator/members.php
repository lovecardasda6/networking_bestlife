<?php

class leaders
{
    protected $con;
    protected $date;
    protected $registration_codes;
    protected $packages;
    protected $wallets;
    protected $users;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
        $this->registration_codes = new registration_codes();
        $this->packages = new packages();
        $this->wallets = new wallet();
    }

    function create_account($obj)
    {
        $logs = new \stdClass();
        
        $lastname = trim(mysqli_escape_string($this->con, $obj->lastname));
        $firstname = trim(mysqli_escape_string($this->con, $obj->firstname));
        $middlename = trim(mysqli_escape_string($this->con, $obj->middlename));
        $contact_no = trim(mysqli_escape_string($this->con, $obj->contact_no));
        $registration_code = trim(mysqli_escape_string($this->con, $obj->registration_code));
        $registration_id = $this->registration_codes->registration_code_id($registration_code);

        if ($registration_id) :

            $users = new \stdClass();
            $users->username = trim(mysqli_escape_string($this->con, $obj->username));
            $users->email = trim(mysqli_escape_string($this->con, $obj->email));
            $users->password = trim(mysqli_escape_string($this->con, $obj->password));

            if ($this->validate_unused_username($users->username)) :

                if ($this->registration_codes->validate_unsused_code($registration_id)) :
                    $package_id = $this->registration_codes->package_id($registration_id);
                    $query = "INSERT INTO accounts VALUES(NULL, '{$lastname}', '{$firstname}', '{$middlename}', '{$contact_no}', '', '', '', '', '0', '{$package_id}', '{$registration_id}', '0', 'None', '{$this->date}')";
                    $res = $this->con->query($query);

                    if ($res) :
                        if (mysqli_affected_rows($this->con)) :
                            $this->registration_codes->update_code_status($registration_id);

                            $users->account_id = mysqli_insert_id($this->con);
                            $res = $this->create_user($users);
                            header("location: ?page=create_members_account&success=1");
                            exit;
                        endif;
                    endif;
                else :
                    $logs->isUsed = true;
                endif;
            else :
                $logs->usernameTaken = true;
            endif;
        else :
            $logs->invalidCode = true;
        endif;

        return $logs;
    }

    function create_user($obj)
    {
        var_dump($obj);
        
        $account_id = $obj->account_id;
        $username = $obj->username;
        $email = $obj->email;
        $password = $obj->password;
        $password = password_hash($password, PASSWORD_BCRYPT);

        $query = "INSERT INTO users VALUES (NULL, '{$account_id}', '{$username}', '{$email}', '{$password}', '', '{$this->date}', '{$this->date}')";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_affected_rows($this->con)) {
                $wallets = new wallets();
                $wallets->create_wallet($account_id);
                return true;
            }
        }

        return false;
    }

    function validate_unused_username($username)
    {
        $query = "SELECT id FROM users WHERE username = '{$username}'";
        $res = $this->con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                return false;
            }
        }

        return true;
    }
}
