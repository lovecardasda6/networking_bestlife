<?php

class documents
{
    protected $con;
    protected $date;

    function __construct()
    {
        $this->con = connectionString();
        $this->date = date('Y-m-d H:i:s');
    }

    function confirm_document($document_id){
        $query = "UPDATE documents SET `status` = 'Confirmed' WHERE id = '{$document_id}'";
        $res = $this->con->query($query);

        if($res){
            if(mysqli_affected_rows($this->con)){
                return true;
            }
        }

        return false;
    }
}
