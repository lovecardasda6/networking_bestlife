<?php

class AccountRegistrationController
{

    function store_accounts($obj)
    {
        $lastname = $obj->lastname;
        $firstname = $obj->firstname;
        $middlename = $obj->middlename;
        $contact_no = $obj->contact_no;
        $gender = $obj->gender;
        $address = $obj->address;
        $date_of_birth = $obj->date_of_birth;
        $place_of_birth = $obj->place_of_birth;
        $referral_id = $obj->reference_id;
        $upline_id = $obj->upline_id;
        $position = $obj->position;
        $registration_code = $obj->registration_code;
        $username = $obj->username;
        $email_address = $obj->email_address;
        $password = $obj->password;

        $isUsernameTaken = User::fetch_user_with_username($username);

        if ($isUsernameTaken) :
            return "Username is already taken and/or used.";
        else :
            $validateRegistrationCode = RegistrationCode::fetch_with_registration_code($registration_code);
            if ($validateRegistrationCode) :
                if ($validateRegistrationCode['status'] == "Unused") :
                    $package_id = $validateRegistrationCode['package_id'];
                    $registration_code_id = $validateRegistrationCode['id'];

                    $account_per_person_hit = Account::max_account_per_person($lastname, $firstname, $middlename);
                    if ($account_per_person_hit) {
                        return "15 Accounts per person is already reach.";
                    }

                    $account_id = Account::store($lastname, $firstname, $middlename, $contact_no, $gender, $address, $date_of_birth, $place_of_birth, $referral_id, $package_id, $registration_code_id, $upline_id, $position);
                    if ($account_id > 0) :
                        $user_id = User::store($account_id, $username, $email_address, $password);

                        if ($user_id) :
                            $wallet_id = Wallet::store($account_id);

                            if ($wallet_id > 0) :
                                $direct_referral_income = DirectReferralIncome::fetch_direct_referral_income_with_package_id($package_id);
                                $income_amount = $direct_referral_income['earnings'];
                                $income_gc = $direct_referral_income['product'];
                                $direct_referral_id = DirectReferralIncome::store($referral_id, $account_id, $income_amount, $income_gc);
                                DailyTransaction::store($referral_id, 'DIRECT REFERRAL', $direct_referral_id);

                                if ($direct_referral_id) :
                                    $process_id = Process::store($referral_id, $income_amount, $income_gc, $upline_id, $account_id, $account_id);

                                    if ($process_id) :
                                        $response = Wallet::add($income_amount, $income_gc, $referral_id);

                                        if ($response) :
                                            RegistrationCode::update_status_with_registration_code_id($registration_code_id);
                                            header("Location: ?page=binarytree&action=add&success=1");
                                            exit;
                                        else :
                                            Account::undo($account_id);
                                            return "Account registration failed. Please contact the administrator.";
                                        endif;
                                    else :
                                        Account::undo($account_id);
                                        return "Account registration failed. Please contact the administrator.";
                                    endif;
                                else :
                                    Account::undo($account_id);
                                    return "Account registration failed. Please contact the administrator.";
                                endif;
                            else :
                                Account::undo($account_id);
                                return "Account registration failed. Please contact the administrator.";
                            endif;
                        else :
                            Account::undo($account_id);
                            return "Account registration failed. Please contact the administrator.";
                        endif;
                    else :
                        return "Account registration failed. Please contact the administrator.";
                    endif;
                else :
                    return "Registration code is already used.";
                endif;
            else :
                return "Invalid registrationon code. Please try again.";
            endif;
        endif;
    }

    function validation($obj){
        $reference_id = $obj->reference_id;
        $upline_id = $obj->upline_id;
        $position = $obj->position;
        $response = Account::validation($reference_id, $upline_id, $position);
        return $response;
    }
}
