<?php

class AccountController
{
    public function store_member($obj)
    {

        $lastname = $obj->lastname;
        $firstname = $obj->firstname;
        $middlename = $obj->middlename;
        $registration_code = $obj->registration_code;

        $username = $obj->username;
        $email = $obj->email;
        $password = $obj->password;

        $validateUsername = User::fetch_user_with_username($username);

        if ($validateUsername) :
            return "Username is already taken.";
        endif;

        $validateRegistrationCode = RegistrationCode::fetch_with_registration_code($registration_code);

        if ($validateRegistrationCode) :
            if ($validateRegistrationCode['status'] == "Unused") :
                $package_id = $validateRegistrationCode['package_id'];
                $registration_code_id = $validateRegistrationCode['id'];
                $account_id = Account::store($lastname, $firstname, $middlename, 0, 0, 0, 0, 0, 0, $package_id, $registration_code_id, 0, 0);

                if ($account_id > 0) :
                    $user_id = User::store($account_id, $username, $email, $password);
                    if ($user_id > 0) :
                        $wallet_id = Wallet::store($account_id);
                        if ($wallet_id > 0) :
                            $response = RegistrationCode::update_status_with_registration_code_id($registration_code_id);
                            if ($response) :
                                header("Location: ?page=create_members_account&action=add&success=1");
                                exit;
                            else :
                                return "Account registration failed. Please contact the administrator. Error code 4";
                            endif;
                        else :
                            return "Account registration failed. Please contact the administrator. Error code 3";
                        endif;
                    else :
                        return "Account registration failed. Please contact the administrator. Error code 2";
                    endif;
                else :
                    return "Account registration failed. Please contact the administrator. Error code 1";
                endif;
            else :
                return "Registration code is already used.";
            endif;
        else :
            return "Invalid registration code and/or not found.";
        endif;
    }

    function update_account($obj)
    {
        $lastname = $obj->lastname;
        $firstname = $obj->firstname;
        $middlename = $obj->middlename;
        $account_id =  $obj->account_id;

        $response = Account::update_account_information($account_id, $lastname, $firstname, $middlename);

        if ($response) :
            $message = base64_encode("Personal Information successfully save.");
            header("Location: ?page=view_members_account&account_id={$account_id}&success=1&details={$message}");
            exit;
        endif;

        return "An error occurred, unable to update account information. Please try again later.";
    }

    function confirm_document($account_id, $document_id)
    {
        $account_id = $account_id;

        $response = Document::confirm_document($account_id, $document_id);
        if ($response) :
            $message = base64_encode("Document successfully confirmed.");
            header("Location: ?page=view_members_account&account_id={$account_id}&success=1&details={$message}");
            exit;
        endif;

        return "An error occurred, unable to confirm account document. Please try again later.";
    }

    function reset_password($obj)
    {
        $user_id = $obj->user_id;
        $account_id = $obj->account_id;

        $password = random_int(1111111, 9999999);

        $response = User::change_password($user_id, $account_id, $password);

        if ($response) :
            $message = base64_encode("Password reset successfully. \n New password generated - {$password}");
            header("Location: ?page=view_members_account&account_id={$account_id}&success=1&details={$message}");
            exit;
        endif;
    }

    function update_basic_information($obj)
    {
        $account_id = $obj->account_id;
        $contact = $obj->contact;
        $gender = $obj->gender;
        $address = $obj->address;
        $date_of_birth = $obj->date_of_birth;
        $place_of_birth = $obj->place_of_birth;
        $currency = $obj->currency;

        Account::update_basic_information($account_id, $contact, $gender, $address, $date_of_birth, $place_of_birth);

        $response = AccountCurrency::fetch($account_id);
        if ($response) :
            AccountCurrency::update($account_id, $currency);

            $message = base64_encode("Account successfully save.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        else :
            AccountCurrency::store($account_id, $currency);

            base64_encode("Account successfully save.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        endif;

        return "Error occurred. Please try again later.";
    }

    function update_user_email($obj)
    {
        $user_id = $obj->user_id;
        $account_id = $obj->account_id;
        $email = $obj->email;

        $response = User::update_email_address($user_id, $account_id, $email);
        if ($response) :
            $message = base64_encode("Email address successfully change.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        endif;
        return "An error occurred. Please try again.";
    }

    function change_password($obj)
    {
        $user_id = $obj->user_id;
        $account_id = $obj->account_id;
        $password = $obj->password;

        $response = User::change_password($user_id, $account_id, $password);
        if ($response) :
            $message = base64_encode("Password successfully change.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        endif;
        return "An error occurred. Please try again.";
    }

    function store_bank($obj)
    {
        $account_id = $obj->account_id;
        $name = $obj->name;
        $number = $obj->number;
        $encashment_center = $obj->encashment_center;

        $response = Bank::store($account_id, $name, $number, $encashment_center);
        if ($response) :
            $message = base64_encode("Encashment account successfully added.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        endif;
        return "An error occurred. Please try again.";
    }

    function store_remittance($obj)
    {
        $account_id = $obj->account_id;
        $name = $obj->name;
        $number = $obj->number;
        $encashment_center = $obj->encashment_center;

        $response = Remittance::store($account_id, $name, $number, $encashment_center);
        if ($response) :
            $message = base64_encode("Encashment account successfully added.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        endif;
        return "An error occurred. Please try again.";
    }

    function store_ewallet($obj)
    {
        $account_id = $obj->account_id;
        $number = $obj->number;
        $encashment_center = $obj->encashment_center;

        $response = EWallet::store($account_id, $number, $encashment_center);
        if ($response) :
            $message = base64_encode("Encashment account successfully added.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        endif;
        return "An error occurred. Please try again.";
    }

    function update_bank($obj)
    {
        $bank_id = $obj->bank_id;
        $account_id = $obj->account_id;
        $name = $obj->name;
        $number = $obj->number;

        $response = Bank::update($bank_id, $account_id, $name, $number);
        if ($response) :
            $message = base64_encode("Encashment account successfully save.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        endif;
        return "An error occurred. Please try again.";
    }

    function update_remittance($obj)
    {
        $remittance_id = $obj->remittance_id;
        $account_id = $obj->account_id;
        $name = $obj->name;
        $number = $obj->number;

        $response = Remittance::update($remittance_id, $account_id, $name, $number);
        if ($response) :
            $message = base64_encode("Encashment account successfully save.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        endif;
        return "An error occurred. Please try again.";
    }

    function update_ewallet($obj)
    {
        $ewallet_id = $obj->ewallet_id;
        $account_id = $obj->account_id;
        $number = $obj->number;

        $response = EWallet::update($ewallet_id, $account_id, $number);
        if ($response) :
            $message = base64_encode("Encashment account successfully save.");
            header("Location: ?page=account&success=1&details={$message}");
            exit;
        endif;
        return "An error occurred. Please try again.";
    }

    function upload_document($obj)
    {
        $account_id = $obj->account_id;
        $document_type = $obj->document_type;
        $image_files = $obj->file_image;
        $tmp_image = $obj->temp_image;

        $explode = explode(".", $image_files);

        $random_file_name = bin2hex(random_bytes(64));
        $file_name = $random_file_name . '.' . end($explode);
        $target_directory = "./assets/documents/" . $file_name;

        $response = Document::store($account_id, $document_type, $file_name);

        if ($response) :
            $res = move_uploaded_file($tmp_image, $target_directory);
            if ($res) {
                $message = base64_encode("Document successfully uploaded. Waiting for confirmation.");
                header("Location: ?page=account&success=1&details={$message}");
                exit;
            }
        endif;
        return "An error occurred. Please try again.";
    }

    function fetch_banks($account_id, $encashment_center)
    {
        $response = Bank::fetch_banks($account_id, $encashment_center);
        return $response;
    }

    function fetch_remittance($account_id, $encashment_center)
    {
        $response = Remittance::fetch_remittance($account_id, $encashment_center);
        return $response;
    }

    function fetch_ewallet($account_id, $encashment_center)
    {
        $response = EWallet::fetch_ewallet($account_id, $encashment_center);
        return $response;
    }
}
