<?php

class AnnouncementController
{
    function fetch()
    {
        $date = date('Y-m-d');

        $response = Announcement::fetch($date);

        if ($response) :
            return $response;
        endif;

        return false;
    }

    function store($obj)
    {
        $subject = $obj->subject;
        $content = $obj->content;
        $signed_by = $obj->signed_by;
        $position = $obj->position;
        $from = $obj->from;
        $until = $obj->until;

        $response = Announcement::store($subject, $content, $signed_by, $position, $from, $until);

        if ($response) :
            header("Location: ?page=announcement&add=1");
            exit;
        endif;

        return false;
    }
}
