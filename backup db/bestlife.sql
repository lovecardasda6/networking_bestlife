-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2020 at 02:15 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bestlife`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `gender` enum('Male','Female','','') NOT NULL,
  `address` varchar(300) NOT NULL,
  `date_of_birth` date NOT NULL,
  `place_of_birth` varchar(200) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `registration_code_id` int(11) NOT NULL,
  `upline_id` int(11) NOT NULL,
  `position` enum('None','Left','Right') NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `lastname`, `firstname`, `middlename`, `contact`, `gender`, `address`, `date_of_birth`, `place_of_birth`, `reference_id`, `package_id`, `registration_code_id`, `upline_id`, `position`, `created`) VALUES
(1, 'DEL ROSARIO', 'ROY', 'MONTE MAYOR', '09999999998', 'Female', 'MAYACABAC DAUIS BOHOL', '2000-02-20', 'POBLACION UBAY BOHOL', 0, 4, 1, 0, '', '2020-07-23'),
(2, '1', '1', '1', '', 'Male', 'MAYACABAC DAUIS BOHOL', '0000-00-00', 'MAYACABAC DAUIS BOHOL', 1, 3, 2, 1, 'Left', '2020-07-23'),
(3, '2', '2', '2', '2', 'Female', 'SAN ANTONIO LILA BOHOL', '2010-11-11', 'POBLACION UBAY BOHOL', 1, 3, 3, 1, 'Right', '2020-07-23'),
(4, '21', '21', '21', '', 'Male', 'SAN ANTONIO LILA BOHOL', '1997-03-26', 'SAN ANTONIO LILA BOHOLS', 3, 3, 4, 3, 'Left', '2020-07-23'),
(5, '22', '22', '22', '22', 'Female', 'MAYACABAC DAUIS BOHOL', '1991-11-11', 'MAYACABAC DAUIS BOHOL', 3, 3, 5, 3, 'Right', '2020-07-23'),
(6, '221', '221', '221', '221', 'Male', '', '0000-00-00', '', 5, 3, 6, 5, 'Left', '2020-07-23'),
(7, '222', '222', '222', '', 'Male', '', '0000-00-00', '', 5, 4, 8, 5, 'Right', '2020-07-23');

-- --------------------------------------------------------

--
-- Table structure for table `account_currency`
--

CREATE TABLE `account_currency` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `currency` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account_currency`
--

INSERT INTO `account_currency` (`id`, `account_id`, `currency`) VALUES
(1, 1, 'PHP');

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `token` varchar(200) NOT NULL,
  `role` enum('Administrator','Accounting','','') NOT NULL,
  `remark` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `signed_by` varchar(200) NOT NULL,
  `position` varchar(50) NOT NULL,
  `from` date NOT NULL,
  `end` date NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `subject`, `content`, `signed_by`, `position`, `from`, `end`, `created`) VALUES
(1, 'GOOD NEWS,', 'Where going to update system to make cashout more easier and faster.', 'Mondragon, Tiffany Pearl', 'CEO', '2020-07-23', '2020-07-24', '2020-07-23');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `account_id`, `account_name`, `account_number`, `bank_name`, `created`) VALUES
(1, 1, 'Paul James Miculob Cartajena', '4162970000296129', 'Banko De Oro', '2020-07-23 02:52:26'),
(2, 1, 'Paul James M. Cartajena', '5895590036641261', 'First Consolidated Bank', '2020-07-23 02:52:32');

-- --------------------------------------------------------

--
-- Table structure for table `cashout`
--

CREATE TABLE `cashout` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `fee` int(11) NOT NULL,
  `receivable_amount` decimal(13,2) NOT NULL,
  `encashment_type` enum('Bank','Remittance','EWallet') NOT NULL,
  `encashment_center` varchar(50) NOT NULL,
  `status` enum('Pending','Confirmed','','') NOT NULL,
  `remark` text NOT NULL,
  `confirm_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cashout_logs`
--

CREATE TABLE `cashout_logs` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_transaction`
--

CREATE TABLE `daily_transaction` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `transaction_type` enum('DIRECT REFERRAL','PAIRING BONUS','INDIRECT BONUS','DIRECT BONUS','') NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daily_transaction`
--

INSERT INTO `daily_transaction` (`id`, `account_id`, `transaction_type`, `transaction_id`, `created`) VALUES
(1, 1, 'DIRECT REFERRAL', 1, '2020-07-23 03:00:51'),
(2, 1, 'DIRECT REFERRAL', 2, '2020-07-23 03:02:36'),
(3, 1, 'PAIRING BONUS', 2, '2020-07-22 16:00:00'),
(4, 1, 'PAIRING BONUS', 3, '2020-07-22 16:00:00'),
(5, 3, 'DIRECT REFERRAL', 3, '2020-07-23 03:15:22'),
(6, 1, 'DIRECT BONUS', 1, '2020-07-22 16:00:00'),
(7, 3, 'DIRECT REFERRAL', 4, '2020-07-23 03:19:06'),
(8, 1, 'DIRECT BONUS', 2, '2020-07-22 16:00:00'),
(9, 3, 'PAIRING BONUS', 6, '2020-07-22 16:00:00'),
(10, 3, 'PAIRING BONUS', 7, '2020-07-22 16:00:00'),
(11, 1, 'DIRECT BONUS', 3, '2020-07-22 16:00:00'),
(12, 5, 'DIRECT REFERRAL', 5, '2020-07-23 03:21:30'),
(13, 1, 'INDIRECT BONUS', 1, '2020-07-22 16:00:00'),
(14, 5, 'DIRECT REFERRAL', 6, '2020-07-23 03:26:09'),
(15, 1, 'INDIRECT BONUS', 2, '2020-07-22 16:00:00'),
(16, 5, 'PAIRING BONUS', 12, '2020-07-22 16:00:00'),
(17, 5, 'PAIRING BONUS', 13, '2020-07-22 16:00:00'),
(18, 1, 'INDIRECT BONUS', 3, '2020-07-22 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonuses`
--

CREATE TABLE `direct_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `percentage` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_bonuses`
--

INSERT INTO `direct_bonuses` (`id`, `package_id`, `percentage`, `created`) VALUES
(1, 4, '15.00', '2020-07-23 02:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonus_income`
--

CREATE TABLE `direct_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_bonus_income`
--

INSERT INTO `direct_bonus_income` (`id`, `account_id`, `invited_account_id`, `amount`, `created`) VALUES
(1, 1, 3, '67.50', '2020-07-23 03:15:27'),
(2, 1, 3, '67.50', '2020-07-23 03:19:06'),
(3, 1, 3, '187.50', '2020-07-23 03:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `direct_referral_bonuses`
--

CREATE TABLE `direct_referral_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `earnings` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_referral_bonuses`
--

INSERT INTO `direct_referral_bonuses` (`id`, `package_id`, `earnings`, `product`, `created`) VALUES
(1, 1, 75, 125, '2020-07-23 02:37:10'),
(2, 2, 125, 125, '2020-07-23 02:37:16'),
(3, 3, 450, 450, '2020-07-23 02:37:27'),
(4, 4, 1250, 1250, '2020-07-23 02:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `direct_referral_income`
--

CREATE TABLE `direct_referral_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `gc` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_referral_income`
--

INSERT INTO `direct_referral_income` (`id`, `account_id`, `invited_account_id`, `amount`, `gc`, `created`) VALUES
(1, 1, 2, '450.00', '450.00', '2020-07-23 03:00:51'),
(2, 1, 3, '450.00', '450.00', '2020-07-23 03:02:36'),
(3, 3, 4, '450.00', '450.00', '2020-07-23 03:15:22'),
(4, 3, 5, '450.00', '450.00', '2020-07-23 03:19:06'),
(5, 5, 6, '450.00', '450.00', '2020-07-23 03:21:30'),
(6, 5, 7, '1250.00', '1250.00', '2020-07-23 03:26:09');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `path` varchar(200) NOT NULL,
  `status` enum('Pending','Confirmed','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `account_id`, `type`, `path`, `status`, `created`) VALUES
(1, 1, 'Any', 'f61f6813b768b9f7271522578acdfa7cf660b2bd3aac623e87551a3dd45c573e105ac38f41e91d6760e6b97c2b7288b071100a93c81e265941741a09a55aff6d.png', 'Confirmed', '2020-07-23 08:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `encashments`
--

CREATE TABLE `encashments` (
  `id` int(11) NOT NULL,
  `encashment` enum('Bank','Remittance','EWallet','') NOT NULL,
  `encashment_center` varchar(200) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `encashments`
--

INSERT INTO `encashments` (`id`, `encashment`, `encashment_center`, `logo`, `created`) VALUES
(1, 'Bank', 'Banko De Oro', 'aaba101e40c01abf929c147e1f55d395.png', '2020-07-23 02:42:44'),
(2, 'Bank', 'First Consolidated Bank', '3cb8a15f35118f2af38a860e8db11fb6.png', '2020-07-23 02:42:52'),
(3, 'Remittance', 'Palawan Express', '653d3a9566f37cd4c98174f2bead406f.jpg', '2020-07-23 02:43:07'),
(4, 'Remittance', 'Cebuana Lhuillier', '164b496524a1cb1fda05f42c2d79ef10.png', '2020-07-23 02:43:19'),
(5, 'EWallet', 'GCash', '80e66aa4a891eb2034ccf1bb13d78dff.png', '2020-07-23 02:43:28');

-- --------------------------------------------------------

--
-- Table structure for table `ewallet`
--

CREATE TABLE `ewallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `ewallet_number` varchar(200) NOT NULL,
  `ewallet_center` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ewallet`
--

INSERT INTO `ewallet` (`id`, `account_id`, `ewallet_number`, `ewallet_center`, `created`) VALUES
(1, 1, '09099575101', 'GCash', '2020-07-23 02:54:14');

-- --------------------------------------------------------

--
-- Table structure for table `exchange_rate`
--

CREATE TABLE `exchange_rate` (
  `id` int(11) NOT NULL,
  `currency` varchar(12) NOT NULL,
  `exchange_rate` decimal(13,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonuses`
--

CREATE TABLE `indirect_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_bonuses`
--

INSERT INTO `indirect_bonuses` (`id`, `package_id`, `level`, `percent`, `created`) VALUES
(1, 3, 1, '10.00', '2020-07-23 02:38:20'),
(2, 3, 2, '75.00', '2020-07-23 02:38:20'),
(3, 3, 3, '5.00', '2020-07-23 02:38:20'),
(4, 3, 4, '2.50', '2020-07-23 02:38:20'),
(5, 3, 5, '1.50', '2020-07-23 02:38:20'),
(6, 3, 6, '1.50', '2020-07-23 02:38:20'),
(7, 3, 7, '1.50', '2020-07-23 02:38:20'),
(8, 3, 8, '1.50', '2020-07-23 02:38:20'),
(9, 3, 9, '1.50', '2020-07-23 02:38:20'),
(10, 3, 10, '1.50', '2020-07-23 02:38:20'),
(11, 4, 1, '10.00', '2020-07-23 02:38:22'),
(12, 4, 2, '7.50', '2020-07-23 02:38:22'),
(13, 4, 3, '5.00', '2020-07-23 02:38:22'),
(14, 4, 4, '2.50', '2020-07-23 02:38:22'),
(15, 4, 5, '1.50', '2020-07-23 02:38:22'),
(16, 4, 6, '1.50', '2020-07-23 02:38:22'),
(17, 4, 7, '1.50', '2020-07-23 02:38:22'),
(18, 4, 8, '1.50', '2020-07-23 02:38:22'),
(19, 4, 9, '1.50', '2020-07-23 02:38:22'),
(20, 4, 10, '1.50', '2020-07-23 02:38:22');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonus_income`
--

CREATE TABLE `indirect_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `downline_id` int(11) NOT NULL,
  `indirect_bonuses_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_bonus_income`
--

INSERT INTO `indirect_bonus_income` (`id`, `account_id`, `downline_id`, `indirect_bonuses_id`, `amount`, `created`) VALUES
(1, 1, 5, 11, '45.00', '2020-07-23 03:21:31'),
(2, 1, 5, 11, '125.00', '2020-07-23 03:26:10'),
(3, 1, 5, 11, '125.00', '2020-07-23 03:26:10');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_level`
--

CREATE TABLE `indirect_level` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_level`
--

INSERT INTO `indirect_level` (`id`, `package_id`, `level`, `created`) VALUES
(1, 3, 10, '2020-07-23 02:38:20'),
(2, 4, 10, '2020-07-23 02:38:22');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `message` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package`, `price`, `created`) VALUES
(1, 'Bronze', 750, '2020-07-23 02:36:29'),
(2, 'Silver', 1500, '2020-07-23 02:36:51'),
(3, 'Gold', 4500, '2020-07-23 02:36:55'),
(4, 'Platinum', 12500, '2020-07-23 02:37:05');

-- --------------------------------------------------------

--
-- Table structure for table `pair`
--

CREATE TABLE `pair` (
  `id` int(11) NOT NULL,
  `multiplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonuses`
--

CREATE TABLE `pairing_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `pair_per_day` int(11) NOT NULL,
  `status` enum('Active','Inactive','','') DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pairing_bonuses`
--

INSERT INTO `pairing_bonuses` (`id`, `package_id`, `value_points`, `pair_per_day`, `status`, `created`) VALUES
(1, 1, 250, 3, 'Active', '2020-07-23 02:37:49'),
(2, 2, 500, 6, '', '2020-07-23 02:38:01'),
(3, 3, 1500, 18, '', '2020-07-23 02:38:08'),
(4, 4, 3500, 50, '', '2020-07-23 02:38:14');

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonus_income`
--

CREATE TABLE `pairing_bonus_income` (
  `id` int(11) NOT NULL,
  `pairing_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `income` decimal(11,2) NOT NULL,
  `remaining_vp` int(11) NOT NULL,
  `heavy_side` enum('None','Left','Right','') NOT NULL,
  `paired` int(11) NOT NULL,
  `paired_this_day` int(11) NOT NULL,
  `remark` enum('Income','GC','FlushOut','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `positioned` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pairing_bonus_income`
--

INSERT INTO `pairing_bonus_income` (`id`, `pairing_id`, `account_id`, `balance`, `invited_account_id`, `value_points`, `income`, `remaining_vp`, `heavy_side`, `paired`, `paired_this_day`, `remark`, `created`, `positioned`) VALUES
(1, 0, 1, 0, 2, 1500, '0.00', 1500, 'Left', 0, 0, 'Income', '2020-07-23 03:00:51', 'Left'),
(2, 1, 1, 1500, 3, 1500, '250.00', 0, 'Right', 1, 5, 'GC', '2020-07-23 03:02:37', 'Right'),
(3, 1, 1, 1500, 3, 1500, '1250.00', 0, 'Right', 1, 6, 'Income', '2020-07-23 03:02:37', 'Right'),
(4, 0, 3, 0, 4, 1500, '0.00', 1500, 'Left', 0, 0, 'Income', '2020-07-23 03:15:27', 'Left'),
(5, 3, 1, 0, 4, 1500, '0.00', 1500, 'Right', 1, 6, 'Income', '2020-07-23 03:15:27', 'Right'),
(6, 4, 3, 1500, 5, 1500, '250.00', 0, 'Right', 1, 5, 'GC', '2020-07-23 03:19:06', 'Right'),
(7, 4, 3, 1500, 5, 1500, '1250.00', 0, 'Right', 1, 6, 'Income', '2020-07-23 03:19:06', 'Right'),
(8, 5, 1, 1500, 5, 1500, '0.00', 3000, 'Right', 1, 6, 'Income', '2020-07-23 03:19:06', 'Right'),
(9, 0, 5, 0, 6, 1500, '0.00', 1500, 'Left', 0, 0, 'Income', '2020-07-23 03:21:31', 'Left'),
(10, 7, 3, 0, 6, 1500, '0.00', 1500, 'Right', 1, 6, 'Income', '2020-07-23 03:21:31', 'Right'),
(11, 8, 1, 3000, 6, 1500, '0.00', 4500, 'Right', 1, 6, 'Income', '2020-07-23 03:21:31', 'Right'),
(12, 9, 5, 1500, 7, 3500, '250.00', 2000, 'Right', 1, 5, 'GC', '2020-07-23 03:26:10', 'Right'),
(13, 9, 5, 1500, 7, 3500, '1250.00', 2000, 'Right', 1, 6, 'Income', '2020-07-23 03:26:10', 'Right'),
(14, 10, 3, 1500, 7, 3500, '0.00', 5000, 'Right', 1, 6, 'Income', '2020-07-23 03:26:10', 'Right'),
(15, 11, 1, 4500, 7, 3500, '0.00', 8000, 'Right', 1, 6, 'Income', '2020-07-23 03:26:10', 'Right');

-- --------------------------------------------------------

--
-- Table structure for table `payout_time`
--

CREATE TABLE `payout_time` (
  `id` int(11) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Active','Inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE `processes` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `direct_referral_income` decimal(13,2) NOT NULL,
  `direct_referral_gc` decimal(13,2) NOT NULL,
  `upline_account_id` int(11) NOT NULL,
  `pairing_invited_account_id` int(11) NOT NULL,
  `pairing_new_registered_account_id` int(11) NOT NULL,
  `status` enum('Pending','Processed','Ongoing','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `processes`
--

INSERT INTO `processes` (`id`, `account_id`, `direct_referral_income`, `direct_referral_gc`, `upline_account_id`, `pairing_invited_account_id`, `pairing_new_registered_account_id`, `status`, `created`) VALUES
(1, 1, '450.00', '450.00', 1, 2, 2, 'Processed', '2020-07-23 03:00:51'),
(2, 1, '450.00', '450.00', 1, 3, 3, 'Processed', '2020-07-23 03:02:36'),
(3, 3, '450.00', '450.00', 3, 4, 4, 'Processed', '2020-07-23 03:15:22'),
(4, 3, '450.00', '450.00', 3, 5, 5, 'Processed', '2020-07-23 03:19:06'),
(5, 5, '450.00', '450.00', 5, 6, 6, 'Processed', '2020-07-23 03:21:30'),
(6, 5, '1250.00', '1250.00', 5, 7, 7, 'Processed', '2020-07-23 03:26:09');

-- --------------------------------------------------------

--
-- Table structure for table `registration_codes`
--

CREATE TABLE `registration_codes` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `registration_code` varchar(30) NOT NULL,
  `type` enum('Paid','Unpaid','','') NOT NULL,
  `status` enum('Unused','Used','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registration_codes`
--

INSERT INTO `registration_codes` (`id`, `package_id`, `registration_code`, `type`, `status`, `created`, `updated`) VALUES
(1, 4, 'EAC00E480827763F', 'Unpaid', 'Used', '2020-07-23 08:39:50', NULL),
(2, 3, 'CE6106E334F0FD3D', 'Paid', 'Used', '2020-07-23 09:00:08', NULL),
(3, 3, 'B26BA381D30ACF4A', 'Paid', 'Used', '2020-07-23 09:00:08', NULL),
(4, 3, '47339E42C0E2CF86', 'Paid', 'Used', '2020-07-23 09:14:22', NULL),
(5, 3, 'BB1A14C9FE48B3E1', 'Paid', 'Used', '2020-07-23 09:14:22', NULL),
(6, 3, '71F179B63C9D21D3', 'Paid', 'Used', '2020-07-23 09:20:37', NULL),
(7, 3, '99EA91791E96B905', 'Paid', 'Unused', '2020-07-23 09:21:07', NULL),
(8, 4, '9A1AE8EB5CD26BB9', 'Paid', 'Used', '2020-07-23 09:25:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `remittance`
--

CREATE TABLE `remittance` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `receivers_name` varchar(200) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `remittance_center` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `remittance`
--

INSERT INTO `remittance` (`id`, `account_id`, `receivers_name`, `contact_number`, `remittance_center`, `created`) VALUES
(1, 1, 'Paul James Miculob Cartajena', '09099575101', 'Cebuana Lhuillier', '2020-07-23 02:53:02'),
(2, 1, 'Paul James M. Cartajena', '091037884311', 'Palawan Express', '2020-07-23 02:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `system_users`
--

CREATE TABLE `system_users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `roles` enum('Administrator','Accounting','','') NOT NULL,
  `token` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Active','Inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `system_users`
--

INSERT INTO `system_users` (`id`, `username`, `password`, `lastname`, `firstname`, `middlename`, `roles`, `token`, `created`, `last_login`, `status`) VALUES
(1, 'admin', '$2y$10$gnGfMqdY59oI5QJRnlsyD.Fz5mwDbvUj9l/lhiPjyUhttdS05HAK2', 'CARTAJENA', 'PAUL JAMES', 'MICULOB', 'Administrator', '6c7eb085b4835a0d430307d8', '2020-07-07 03:19:22', '2020-07-23 17:47:56', 'Active'),
(5, 'admin', '$2y$10$A7hbxZiz6fWtS/jQxvQXbeJgoIAzsiWGMWSICfmYW/xRP5we.7.Si', 'MONDRAGON', 'CECILION', '', 'Accounting', '4b5204d52db4be1cfb2fab73', '2020-07-11 19:16:34', '2020-07-19 20:06:08', 'Active'),
(6, '2312', '$2y$10$fGKCkSMeZjgfsxwcUtYNP.f9H8AjZIZSqi3jowavPFG8pH0dxeASe', 'RAGNAROK', 'ROY', 'cahanap', 'Administrator', '', '2020-07-20 21:16:16', '2020-07-20 21:16:16', 'Active'),
(7, 'admin123', '$2y$10$WJoMSfmGYMAkVWkdiVTTVuraKpMTunTcPH3SjOptRR/gr73QN9z66', 'RAGNAROK', 'ROY', 'cahanap', 'Administrator', '', '2020-07-20 21:17:34', '2020-07-20 21:17:34', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_fee`
--

CREATE TABLE `transaction_fee` (
  `id` int(11) NOT NULL,
  `fee` decimal(13,2) NOT NULL,
  `status` enum('Active','Inactive','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction_fee`
--

INSERT INTO `transaction_fee` (`id`, `fee`, `status`, `created`, `updated`) VALUES
(1, '10.00', 'Active', '2020-07-23 02:42:15', '2020-07-23 02:42:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(300) NOT NULL,
  `token` varchar(200) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `account_id`, `username`, `email`, `password`, `token`, `last_login`, `created`) VALUES
(1, 1, 'admin', 'cardasda@gmail.com', '$2y$10$rfkFIs.N8Rz567LpoZqtuu//uNunwFz2UPlH9g7ltsyYGHiizzJIe', '', '2020-07-23 17:47:40', '2020-07-23 02:41:08'),
(2, 2, '1', '', '$2y$10$rgFTjUUxvS9YUEkl6IJZnuaPaAgbGVLfRVEfckTb.OkLibZ/QJkAG', '3fa6f0d272a596b903622857', '2020-07-23 18:12:57', '2020-07-23 03:00:51'),
(3, 3, '2', 'miculob.cartajena@gmail.com', '$2y$10$cOOVgSiQaZ6VJbPYPtzG2uNqFgibpY.ot8zjWYvRPlhyd5eRo38cW', '841decb02c707f0728469b29', '2020-07-23 03:22:43', '2020-07-23 03:02:36'),
(4, 4, '21', 'selenapearl@gmail.com', '$2y$10$D7RiidFZ6VBiVY3HeNWTz..Rkgce/6KzQ8akDmYYyaLktGZrLmL2y', '', '2020-07-23 03:15:36', '2020-07-23 03:15:22'),
(5, 5, '22', '', '$2y$10$ylMhidLFu7w6T9ht8nS7VelmWLIw.RxQ2vXzW4APzjUAKljTg4v8e', 'b63bb8f2e49b785e51479c85', '2020-07-23 03:22:54', '2020-07-23 03:19:06'),
(6, 6, '221', '', '$2y$10$mrJb/PSBoGiNg19x10fRiuTsG7KcvZnPDED3Gx.CmaIbM5G0sU9Xm', '', '2020-07-23 03:21:30', '2020-07-23 03:21:30'),
(7, 7, '222', '', '$2y$10$HTDlTgHNIdLV5tQcwJlbBud/sn8wC3dmB9C/LbJYgwwugyXS7nRcm', '', '2020-07-23 03:26:09', '2020-07-23 03:26:09');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` decimal(13,2) NOT NULL,
  `gc` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `account_id`, `balance`, `gc`, `created`, `updated`) VALUES
(1, 1, '2767.50', '1150.00', '2020-07-23', '2020-07-23 03:26:10'),
(2, 2, '0.00', '0.00', '2020-07-23', '2020-07-23 03:00:51'),
(3, 3, '2150.00', '1150.00', '2020-07-23', '2020-07-23 03:19:06'),
(4, 4, '0.00', '0.00', '2020-07-23', '2020-07-23 03:15:22'),
(5, 5, '2950.00', '1950.00', '2020-07-23', '2020-07-23 03:26:10'),
(6, 6, '0.00', '0.00', '2020-07-23', '2020-07-23 03:21:30'),
(7, 7, '0.00', '0.00', '2020-07-23', '2020-07-23 03:26:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activation_code` (`registration_code_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `upline_id` (`upline_id`),
  ADD KEY `upline_id_2` (`upline_id`);

--
-- Indexes for table `account_currency`
--
ALTER TABLE `account_currency`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `cashout`
--
ALTER TABLE `cashout`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `confirm_id` (`confirm_id`);

--
-- Indexes for table `cashout_logs`
--
ALTER TABLE `cashout_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `direct_referral_bonuses`
--
ALTER TABLE `direct_referral_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `encashments`
--
ALTER TABLE `encashments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `encashment_center` (`encashment_center`);

--
-- Indexes for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `exchange_rate`
--
ALTER TABLE `exchange_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `downline_id` (`downline_id`),
  ADD KEY `indirect_bonuses_id` (`indirect_bonuses_id`);

--
-- Indexes for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package` (`package`);

--
-- Indexes for table `pair`
--
ALTER TABLE `pair`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id_4` (`invited_account_id`);

--
-- Indexes for table `payout_time`
--
ALTER TABLE `payout_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processes`
--
ALTER TABLE `processes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration_codes`
--
ALTER TABLE `registration_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `remittance`
--
ALTER TABLE `remittance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `system_users`
--
ALTER TABLE `system_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `account_currency`
--
ALTER TABLE `account_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cashout`
--
ALTER TABLE `cashout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cashout_logs`
--
ALTER TABLE `cashout_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `direct_referral_bonuses`
--
ALTER TABLE `direct_referral_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `encashments`
--
ALTER TABLE `encashments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ewallet`
--
ALTER TABLE `ewallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exchange_rate`
--
ALTER TABLE `exchange_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `indirect_level`
--
ALTER TABLE `indirect_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pair`
--
ALTER TABLE `pair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `payout_time`
--
ALTER TABLE `payout_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `processes`
--
ALTER TABLE `processes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `registration_codes`
--
ALTER TABLE `registration_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `remittance`
--
ALTER TABLE `remittance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_users`
--
ALTER TABLE `system_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`),
  ADD CONSTRAINT `accounts_ibfk_2` FOREIGN KEY (`registration_code_id`) REFERENCES `registration_codes` (`id`);

--
-- Constraints for table `account_currency`
--
ALTER TABLE `account_currency`
  ADD CONSTRAINT `account_currency_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cashout`
--
ALTER TABLE `cashout`
  ADD CONSTRAINT `cashout_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cashout_logs`
--
ALTER TABLE `cashout_logs`
  ADD CONSTRAINT `cashout_logs_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD CONSTRAINT `daily_transaction_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD CONSTRAINT `direct_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD CONSTRAINT `direct_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_referral_bonuses`
--
ALTER TABLE `direct_referral_bonuses`
  ADD CONSTRAINT `direct_referral_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD CONSTRAINT `direct_referral_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_referral_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD CONSTRAINT `ewallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD CONSTRAINT `indirect_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD CONSTRAINT `indirect_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_2` FOREIGN KEY (`downline_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_3` FOREIGN KEY (`indirect_bonuses_id`) REFERENCES `indirect_bonuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD CONSTRAINT `indirect_level_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD CONSTRAINT `pairing_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD CONSTRAINT `pairing_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pairing_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `registration_codes`
--
ALTER TABLE `registration_codes`
  ADD CONSTRAINT `registration_codes_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `remittance`
--
ALTER TABLE `remittance`
  ADD CONSTRAINT `remittance_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
