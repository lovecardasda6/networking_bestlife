-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2020 at 12:48 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `build`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code_id` int(11) NOT NULL,
  `upline_id` int(11) NOT NULL,
  `referral_side` enum('None','Left','Right','') NOT NULL,
  `position` enum('None','Left','Right') NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `lastname`, `firstname`, `middlename`, `contact`, `reference_id`, `package_id`, `activation_code_id`, `upline_id`, `referral_side`, `position`, `created`) VALUES
(1, 'CARTAJENA', 'PAUL JAMES', 'MICULOB', '09999999999', 0, 10, 229, 0, 'None', 'None', '0000-00-00'),
(647, '1', '1', '1', '1', 1, 13, 217, 1, 'Left', 'Left', '2020-07-05'),
(648, '1', '1', '1', '1', 1, 13, 218, 1, 'Right', 'Right', '2020-07-05'),
(649, '1', '1', '1', '1', 1, 13, 219, 647, 'Left', 'Left', '2020-07-05'),
(650, '1', '1', '1', '1', 1, 13, 220, 647, 'Left', 'Right', '2020-07-05'),
(651, '1', '1', '1', '1', 1, 8, 241, 649, 'Left', 'Left', '2020-07-05'),
(652, '1', '1', '1', '1', 1, 8, 240, 649, 'Left', 'Right', '2020-07-05'),
(653, '1', '1', '1', '1', 1, 13, 221, 648, 'Right', 'Left', '2020-07-05'),
(654, '1', '1', '1', '1', 1, 13, 222, 648, 'Right', 'Right', '2020-07-05'),
(655, '1', '1', '1', '1', 1, 13, 223, 653, 'Right', 'Left', '2020-07-05'),
(656, '1', '1', '1', '1', 1, 13, 224, 654, 'Right', 'Right', '2020-07-05'),
(657, 'LAGURA', 'TIFFANY PEARL', 'CAHANAP', '09999999999', 0, 13, 229, 0, 'None', 'None', '0000-00-00'),
(659, '2', '2', '2', '2', 657, 10, 230, 657, 'Left', 'Left', '2020-07-05'),
(660, '2', '2', '2', '2', 657, 8, 242, 657, 'Right', 'Right', '2020-07-05'),
(661, '2', '2', '2', '2', 657, 8, 243, 660, 'Right', 'Right', '2020-07-05'),
(662, '3', '3', '3', '3', 660, 10, 233, 660, 'Left', 'Left', '2020-07-05'),
(663, 'TEST', 'TSET', 'SET', '1', 647, 13, 225, 647, 'Left', 'Left', '2020-07-05'),
(664, '213', '3123', '214', '2134', 647, 13, 226, 647, 'Right', 'Right', '2020-07-05'),
(665, 'TSET', '123', '1321', '321', 664, 13, 227, 664, 'Left', 'Left', '2020-07-05');

-- --------------------------------------------------------

--
-- Table structure for table `activation_codes`
--

CREATE TABLE `activation_codes` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code` text NOT NULL,
  `type` enum('Paid','Unpaid','','') NOT NULL,
  `status` enum('Unused','Used','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activation_codes`
--

INSERT INTO `activation_codes` (`id`, `package_id`, `activation_code`, `type`, `status`, `created`) VALUES
(181, 13, '5986DC905E400AC6', 'Paid', 'Used', '2020-07-04 04:03:37'),
(217, 13, 'ACA36838AA4A8D6C', 'Paid', 'Used', '2020-07-05 09:54:54'),
(218, 13, 'E0516575C47A80A3', 'Paid', 'Used', '2020-07-05 09:54:54'),
(219, 13, '0EF3549D85C9E081', 'Paid', 'Used', '2020-07-05 09:54:54'),
(220, 13, '6E5A0105C169BEBD', 'Paid', 'Used', '2020-07-05 09:54:54'),
(221, 13, 'D48DD4CB399EB4CB', 'Paid', 'Used', '2020-07-05 09:54:54'),
(222, 13, '86520350E714C79A', 'Paid', 'Used', '2020-07-05 09:54:54'),
(223, 13, '0E5D2EEA82AC349D', 'Paid', 'Used', '2020-07-05 09:54:54'),
(224, 13, '552B5C275E613DB5', 'Paid', 'Used', '2020-07-05 09:54:54'),
(225, 13, '8E776C82CA2E7C6F', 'Paid', 'Used', '2020-07-05 09:54:54'),
(226, 13, '71793CBE0CFB16D8', 'Paid', 'Used', '2020-07-05 09:54:54'),
(227, 13, '05704363A267A50B', 'Paid', 'Used', '2020-07-05 09:54:54'),
(228, 13, '1E8D083812D89FE6', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(229, 10, '8CD021846FA901A0', 'Paid', 'Used', '2020-07-05 11:05:58'),
(230, 10, '84A83C1EA22DB622', 'Paid', 'Used', '2020-07-05 11:05:58'),
(231, 13, 'E3BAA4D1D09D9E4A', 'Paid', 'Unused', '2020-07-06 11:07:12'),
(232, 13, '86B96883B42358C6', 'Paid', 'Unused', '2020-07-06 11:07:12'),
(233, 10, 'B5E819B6BADCDEC1', 'Paid', 'Used', '2020-07-06 11:07:16'),
(234, 10, '3204CD0B5578FC08', 'Paid', 'Unused', '2020-07-06 11:07:16'),
(235, 10, '90500107FD8E8352', 'Paid', 'Unused', '2020-07-06 11:24:33'),
(237, 9, '685E6531F60722BE', 'Paid', 'Unused', '2020-07-07 11:25:47'),
(238, 9, 'E95785D5EBDFC52B', 'Paid', 'Unused', '2020-07-07 11:25:47'),
(240, 8, '70979FEE8FDD64A1', 'Paid', 'Used', '2020-07-05 01:18:02'),
(241, 8, 'C431FF0BCD8C04C0', 'Paid', 'Used', '2020-07-05 01:18:02'),
(242, 8, 'F2B812FA33A29730', 'Paid', 'Used', '2020-07-05 12:26:01'),
(243, 8, '9FEA2AD3171B47A7', 'Paid', 'Used', '2020-07-05 12:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `token` varchar(200) NOT NULL,
  `role` enum('Administrator','Accounting','','') NOT NULL,
  `remark` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(25) NOT NULL,
  `bank_name` enum('BDO','FCB') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `account_id`, `account_name`, `account_number`, `bank_name`, `created`) VALUES
(1, 1, 'PAUL JAMES M. CARTAJENA', '0033004301324032', 'BDO', '2020-07-03 03:03:06');

-- --------------------------------------------------------

--
-- Table structure for table `cashout`
--

CREATE TABLE `cashout` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `fee` int(11) NOT NULL,
  `receivable_amount` decimal(13,2) NOT NULL,
  `encashment_type` enum('Bank','Remmittance','EWallet') NOT NULL,
  `encashment_center` enum('BDO','FCB','Palawan Pawnshop','Cebuana Lhuillier','GCash','') NOT NULL,
  `status` enum('Pending','Confirmed','','') NOT NULL,
  `remark` text NOT NULL,
  `confirm_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cashout`
--

INSERT INTO `cashout` (`id`, `account_id`, `amount`, `fee`, `receivable_amount`, `encashment_type`, `encashment_center`, `status`, `remark`, `confirm_id`, `created`, `updated`) VALUES
(3, 1, '1000.00', 10, '900.00', 'Remmittance', 'Palawan Pawnshop', 'Pending', '', 1, '2020-07-01 04:01:35', '2020-06-30 19:23:48');

-- --------------------------------------------------------

--
-- Table structure for table `daily_transaction`
--

CREATE TABLE `daily_transaction` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `transaction_type` enum('DIRECT REFERRAL','PAIRING BONUS','INDIRECT BONUS','DIRECT BONUS','') NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daily_transaction`
--

INSERT INTO `daily_transaction` (`id`, `account_id`, `transaction_type`, `transaction_id`, `created`) VALUES
(373, 1, 'DIRECT REFERRAL', 314, '2020-07-03 16:00:00'),
(374, 1, 'DIRECT REFERRAL', 315, '2020-07-04 16:00:00'),
(375, 1, 'DIRECT REFERRAL', 316, '2020-07-04 16:00:00'),
(376, 1, 'DIRECT REFERRAL', 317, '2020-07-04 16:00:00'),
(377, 1, 'DIRECT REFERRAL', 318, '2020-07-04 16:00:00'),
(378, 1, 'DIRECT REFERRAL', 319, '2020-07-04 16:00:00'),
(379, 1, 'DIRECT REFERRAL', 320, '2020-07-04 16:00:00'),
(380, 1, 'DIRECT REFERRAL', 321, '2020-07-04 16:00:00'),
(381, 1, 'DIRECT REFERRAL', 322, '2020-07-04 16:00:00'),
(383, 1, 'DIRECT REFERRAL', 324, '2020-07-04 16:00:00'),
(384, 1, 'DIRECT REFERRAL', 325, '2020-07-04 16:00:00'),
(385, 1, 'DIRECT REFERRAL', 326, '2020-07-04 16:00:00'),
(386, 1, 'DIRECT REFERRAL', 327, '2020-07-04 16:00:00'),
(387, 1, 'DIRECT REFERRAL', 328, '2020-07-04 16:00:00'),
(388, 1, 'DIRECT REFERRAL', 329, '2020-07-04 16:00:00'),
(389, 1, 'DIRECT REFERRAL', 330, '2020-07-04 16:00:00'),
(390, 1, 'DIRECT REFERRAL', 331, '2020-07-04 16:00:00'),
(391, 1, 'DIRECT REFERRAL', 332, '2020-07-04 16:00:00'),
(392, 1, 'DIRECT REFERRAL', 333, '2020-07-04 16:00:00'),
(393, 1, 'DIRECT REFERRAL', 334, '2020-07-04 16:00:00'),
(394, 1, 'DIRECT REFERRAL', 335, '2020-07-04 16:00:00'),
(395, 1, 'DIRECT REFERRAL', 336, '2020-07-04 16:00:00'),
(396, 1, 'DIRECT REFERRAL', 337, '2020-07-04 16:00:00'),
(397, 1, 'DIRECT REFERRAL', 338, '2020-07-04 16:00:00'),
(398, 1, 'DIRECT REFERRAL', 339, '2020-07-04 16:00:00'),
(399, 1, 'DIRECT REFERRAL', 340, '2020-07-04 16:00:00'),
(400, 1, 'DIRECT REFERRAL', 341, '2020-07-04 16:00:00'),
(401, 1, 'DIRECT REFERRAL', 342, '2020-07-04 16:00:00'),
(402, 1, 'DIRECT REFERRAL', 343, '2020-07-04 16:00:00'),
(403, 1, 'DIRECT REFERRAL', 344, '2020-07-04 16:00:00'),
(404, 1, 'DIRECT REFERRAL', 345, '2020-07-04 16:00:00'),
(405, 1, 'DIRECT REFERRAL', 346, '2020-07-04 16:00:00'),
(406, 1, 'DIRECT REFERRAL', 347, '2020-07-04 16:00:00'),
(407, 1, 'DIRECT REFERRAL', 348, '2020-07-04 16:00:00'),
(408, 1, 'DIRECT REFERRAL', 349, '2020-07-04 16:00:00'),
(409, 1, 'DIRECT REFERRAL', 350, '2020-07-04 16:00:00'),
(410, 1, 'DIRECT REFERRAL', 351, '2020-07-04 16:00:00'),
(411, 1, 'DIRECT REFERRAL', 352, '2020-07-04 16:00:00'),
(412, 1, 'DIRECT REFERRAL', 353, '2020-07-04 16:00:00'),
(413, 1, 'DIRECT REFERRAL', 354, '2020-07-04 16:00:00'),
(414, 1, 'DIRECT REFERRAL', 355, '2020-07-04 16:00:00'),
(415, 1, 'DIRECT REFERRAL', 356, '2020-07-04 16:00:00'),
(416, 1, 'DIRECT REFERRAL', 357, '2020-07-04 16:00:00'),
(417, 1, 'DIRECT REFERRAL', 358, '2020-07-04 16:00:00'),
(418, 1, 'DIRECT REFERRAL', 359, '2020-07-04 16:00:00'),
(419, 1, 'DIRECT REFERRAL', 360, '2020-07-04 16:00:00'),
(420, 1, 'DIRECT REFERRAL', 361, '2020-07-04 16:00:00'),
(421, 1, 'DIRECT REFERRAL', 362, '2020-07-04 16:00:00'),
(422, 1, 'DIRECT REFERRAL', 363, '2020-07-04 16:00:00'),
(423, 1, 'DIRECT REFERRAL', 364, '2020-07-04 16:00:00'),
(424, 1, 'DIRECT REFERRAL', 365, '2020-07-04 16:00:00'),
(425, 1, 'DIRECT REFERRAL', 366, '2020-07-04 16:00:00'),
(426, 1, 'DIRECT REFERRAL', 367, '2020-07-04 16:00:00'),
(427, 1, 'DIRECT REFERRAL', 368, '2020-07-04 16:00:00'),
(428, 1, 'DIRECT REFERRAL', 369, '2020-07-04 16:00:00'),
(429, 1, 'DIRECT REFERRAL', 370, '2020-07-04 16:00:00'),
(430, 1, 'DIRECT REFERRAL', 371, '2020-07-04 16:00:00'),
(431, 1, 'DIRECT REFERRAL', 372, '2020-07-04 16:00:00'),
(432, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(433, 1, 'DIRECT REFERRAL', 373, '2020-07-04 16:00:00'),
(434, 1, 'DIRECT REFERRAL', 374, '2020-07-04 16:00:00'),
(435, 1, 'DIRECT BONUS', 75, '2020-07-04 16:00:00'),
(436, 1, 'DIRECT REFERRAL', 375, '2020-07-04 16:00:00'),
(437, 1, 'DIRECT REFERRAL', 376, '2020-07-04 16:00:00'),
(438, 1, 'DIRECT REFERRAL', 377, '2020-07-04 16:00:00'),
(439, 1, 'DIRECT REFERRAL', 378, '2020-07-04 16:00:00'),
(440, 1, 'DIRECT REFERRAL', 379, '2020-07-04 16:00:00'),
(441, 1, 'DIRECT REFERRAL', 380, '2020-07-04 16:00:00'),
(442, 1, 'DIRECT REFERRAL', 381, '2020-07-04 16:00:00'),
(443, 1, 'DIRECT REFERRAL', 382, '2020-07-04 16:00:00'),
(444, 1, 'DIRECT REFERRAL', 383, '2020-07-04 16:00:00'),
(445, 1, 'DIRECT REFERRAL', 384, '2020-07-04 16:00:00'),
(446, 1, 'DIRECT REFERRAL', 385, '2020-07-04 16:00:00'),
(447, 1, 'DIRECT REFERRAL', 386, '2020-07-04 16:00:00'),
(448, 1, 'DIRECT REFERRAL', 387, '2020-07-04 16:00:00'),
(449, 1, 'DIRECT REFERRAL', 388, '2020-07-04 16:00:00'),
(450, 1, 'DIRECT REFERRAL', 389, '2020-07-04 16:00:00'),
(451, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(452, 1, 'DIRECT BONUS', 75, '2020-07-04 16:00:00'),
(453, 1, 'DIRECT REFERRAL', 390, '2020-07-04 16:00:00'),
(454, 1, 'DIRECT REFERRAL', 391, '2020-07-04 16:00:00'),
(455, 1, 'DIRECT REFERRAL', 392, '2020-07-04 16:00:00'),
(456, 1, 'DIRECT REFERRAL', 393, '2020-07-04 16:00:00'),
(457, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(458, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(459, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(460, 1, 'DIRECT BONUS', 75, '2020-07-04 16:00:00'),
(461, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(462, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(463, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(464, 1, 'DIRECT BONUS', 75, '2020-07-04 16:00:00'),
(465, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(466, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(467, 657, 'DIRECT REFERRAL', 394, '2020-07-04 16:00:00'),
(468, 657, 'DIRECT REFERRAL', 395, '2020-07-04 16:00:00'),
(469, 657, 'DIRECT REFERRAL', 396, '2020-07-04 16:00:00'),
(470, 657, 'DIRECT REFERRAL', 397, '2020-07-04 16:00:00'),
(471, 660, 'DIRECT REFERRAL', 398, '2020-07-04 16:00:00'),
(472, 647, 'DIRECT REFERRAL', 399, '2020-07-04 16:00:00'),
(473, 1, 'DIRECT BONUS', 11, '2020-07-04 16:00:00'),
(474, 647, 'DIRECT REFERRAL', 400, '2020-07-04 16:00:00'),
(475, 1, 'DIRECT BONUS', 11, '2020-07-04 16:00:00'),
(476, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00'),
(477, 664, 'DIRECT REFERRAL', 401, '2020-07-04 16:00:00'),
(478, 1, 'INDIRECT BONUS', 12, '2020-07-04 16:00:00'),
(479, 1, 'DIRECT BONUS', 38, '2020-07-04 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonuses`
--

CREATE TABLE `direct_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_bonuses`
--

INSERT INTO `direct_bonuses` (`id`, `package_id`, `percentage`, `created`) VALUES
(13, 10, 15, '2020-06-26 08:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonus_income`
--

CREATE TABLE `direct_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_bonus_income`
--

INSERT INTO `direct_bonus_income` (`id`, `account_id`, `invited_account_id`, `amount`, `created`) VALUES
(31, 1, 647, '37.50', '2020-07-05'),
(32, 1, 649, '75.00', '2020-07-05'),
(33, 1, 648, '37.50', '2020-07-05'),
(34, 1, 648, '37.50', '2020-07-05'),
(35, 1, 647, '10.50', '2020-07-05'),
(36, 1, 647, '10.50', '2020-07-05'),
(37, 1, 647, '37.50', '2020-07-05'),
(38, 1, 647, '37.50', '2020-07-05');

-- --------------------------------------------------------

--
-- Table structure for table `direct_referral_income`
--

CREATE TABLE `direct_referral_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_referral_income`
--

INSERT INTO `direct_referral_income` (`id`, `account_id`, `invited_account_id`, `amount`, `worth_of_product`, `created`) VALUES
(384, 1, 647, '70.00', '125.00', '2020-07-05'),
(385, 1, 648, '70.00', '125.00', '2020-07-05'),
(386, 1, 649, '70.00', '125.00', '2020-07-05'),
(387, 1, 650, '70.00', '125.00', '2020-07-05'),
(388, 1, 651, '500.00', '250.00', '2020-07-05'),
(389, 1, 652, '500.00', '250.00', '2020-07-05'),
(390, 1, 653, '70.00', '125.00', '2020-07-05'),
(391, 1, 654, '70.00', '125.00', '2020-07-05'),
(392, 1, 655, '70.00', '125.00', '2020-07-05'),
(393, 1, 656, '70.00', '125.00', '2020-07-05'),
(395, 657, 659, '3500.00', '1000.00', '2020-07-05'),
(396, 657, 660, '500.00', '250.00', '2020-07-05'),
(397, 657, 661, '500.00', '250.00', '2020-07-05'),
(398, 660, 662, '3500.00', '1000.00', '2020-07-05'),
(399, 647, 663, '70.00', '125.00', '2020-07-05'),
(400, 647, 664, '70.00', '125.00', '2020-07-05'),
(401, 664, 665, '70.00', '125.00', '2020-07-05');

-- --------------------------------------------------------

--
-- Table structure for table `ewallet`
--

CREATE TABLE `ewallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `ewallet_number` varchar(200) NOT NULL,
  `ewallet_center` enum('GCash') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ewallet`
--

INSERT INTO `ewallet` (`id`, `account_id`, `ewallet_number`, `ewallet_center`, `created`) VALUES
(1, 1, '09099575101', 'GCash', '2020-07-03 05:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonuses`
--

CREATE TABLE `indirect_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_bonuses`
--

INSERT INTO `indirect_bonuses` (`id`, `package_id`, `level`, `percent`, `created`) VALUES
(54, 9, 1, '10.00', '2020-07-01 20:39:08'),
(55, 9, 2, '7.50', '2020-07-01 20:39:08'),
(56, 9, 3, '5.00', '2020-07-01 20:39:08'),
(57, 9, 4, '2.50', '2020-07-01 20:39:08'),
(58, 9, 5, '1.00', '2020-07-01 20:39:08'),
(59, 9, 6, '1.00', '2020-07-01 20:39:08'),
(60, 9, 7, '1.00', '2020-07-01 20:39:08'),
(61, 9, 8, '1.00', '2020-07-01 20:39:08'),
(62, 9, 9, '1.00', '2020-07-01 20:39:08'),
(63, 9, 10, '1.00', '2020-07-01 20:39:08'),
(64, 10, 1, '10.00', '2020-07-01 20:39:12'),
(65, 10, 2, '7.50', '2020-07-01 20:39:12'),
(66, 10, 3, '5.00', '2020-07-01 20:39:12'),
(67, 10, 4, '2.50', '2020-07-01 20:39:12'),
(68, 10, 5, '1.00', '2020-07-01 20:39:12'),
(69, 10, 6, '1.00', '2020-07-01 20:39:12'),
(70, 10, 7, '1.00', '2020-07-01 20:39:12'),
(71, 10, 8, '1.00', '2020-07-01 20:39:12'),
(72, 10, 9, '1.00', '2020-07-01 20:39:12'),
(73, 10, 10, '1.00', '2020-07-01 20:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonus_income`
--

CREATE TABLE `indirect_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `downline_id` int(11) NOT NULL,
  `indirect_bonuses_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_bonus_income`
--

INSERT INTO `indirect_bonus_income` (`id`, `account_id`, `downline_id`, `indirect_bonuses_id`, `amount`, `created`) VALUES
(12, 1, 664, 64, '7.00', '2020-07-05');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_level`
--

CREATE TABLE `indirect_level` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_level`
--

INSERT INTO `indirect_level` (`id`, `package_id`, `level`, `created`) VALUES
(10, 9, 10, '2020-07-02 02:39:08'),
(11, 10, 10, '2020-07-02 02:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `message` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package`, `price`, `created`) VALUES
(8, 'Silver', 1000, '2020-06-20 11:27:06'),
(9, 'Gold', 4500, '2020-06-20 11:27:11'),
(10, 'Platinum', 12500, '2020-06-20 11:27:18'),
(13, 'Bronze', 750, '2020-06-20 11:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `pair`
--

CREATE TABLE `pair` (
  `id` int(11) NOT NULL,
  `multiplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pair`
--

INSERT INTO `pair` (`id`, `multiplier`) VALUES
(1, 250);

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonuses`
--

CREATE TABLE `pairing_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `pair_per_day` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pairing_bonuses`
--

INSERT INTO `pairing_bonuses` (`id`, `package_id`, `value_points`, `pair_per_day`, `created`) VALUES
(1, 13, 250, 3, '2020-06-21 09:57:29'),
(2, 8, 500, 6, '2020-06-21 09:57:29'),
(5, 9, 1500, 18, '2020-06-21 10:00:27'),
(7, 10, 3500, 50, '2020-06-21 10:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonus_income`
--

CREATE TABLE `pairing_bonus_income` (
  `id` int(11) NOT NULL,
  `pairing_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `income` decimal(11,2) NOT NULL,
  `remaining_vp` int(11) NOT NULL,
  `heavy_side` enum('None','Left','Right','') NOT NULL,
  `paired` int(11) NOT NULL,
  `paired_this_day` int(11) NOT NULL,
  `remark` enum('Income','GC','FlushOut','') NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp(),
  `positioned` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pairing_bonus_income`
--

INSERT INTO `pairing_bonus_income` (`id`, `pairing_id`, `account_id`, `balance`, `invited_account_id`, `value_points`, `income`, `remaining_vp`, `heavy_side`, `paired`, `paired_this_day`, `remark`, `created`, `positioned`) VALUES
(143933, 0, 1, 0, 647, 250, '0.00', 250, 'Left', 0, 0, 'Income', '2020-07-05', 'Left'),
(143934, 143933, 1, 250, 648, 250, '250.00', 0, 'Right', 1, 1, 'Income', '2020-07-05', 'Right'),
(143935, 0, 647, 0, 649, 250, '0.00', 250, 'Left', 0, 0, 'Income', '2020-07-05', 'Left'),
(143936, 143934, 1, 0, 649, 250, '0.00', 250, 'Left', 1, 1, 'Income', '2020-07-05', 'Left'),
(143937, 143935, 647, 250, 650, 250, '250.00', 0, 'Right', 1, 1, 'Income', '2020-07-05', 'Right'),
(143938, 143936, 1, 250, 650, 250, '0.00', 500, 'Left', 1, 1, 'Income', '2020-07-05', 'Left'),
(143939, 0, 649, 0, 651, 500, '0.00', 500, 'Left', 0, 0, 'Income', '2020-07-05', 'Left'),
(143940, 143937, 647, 0, 651, 500, '0.00', 500, 'Left', 1, 1, 'Income', '2020-07-05', 'Left'),
(143941, 143938, 1, 500, 651, 500, '0.00', 1000, 'Left', 1, 1, 'Income', '2020-07-05', 'Left'),
(143942, 143939, 649, 500, 652, 500, '250.00', 0, 'Right', 1, 1, 'Income', '2020-07-05', 'Right'),
(143943, 143939, 649, 500, 652, 500, '250.00', 0, 'Right', 2, 2, 'Income', '2020-07-05', 'Right'),
(143944, 143940, 647, 500, 652, 500, '0.00', 1000, 'Left', 1, 1, 'Income', '2020-07-05', 'Left'),
(143945, 143941, 1, 1000, 652, 500, '0.00', 1500, 'Left', 1, 1, 'Income', '2020-07-05', 'Left'),
(143946, 0, 648, 0, 653, 250, '0.00', 250, 'Left', 0, 0, 'Income', '2020-07-05', 'Left'),
(143947, 143945, 1, 1500, 653, 250, '250.00', 1500, 'Left', 2, 2, 'Income', '2020-07-05', 'Right'),
(143948, 143946, 648, 250, 654, 250, '250.00', 0, 'Right', 1, 1, 'Income', '2020-07-05', 'Right'),
(143949, 143947, 1, 1500, 654, 250, '250.00', 1500, 'Left', 3, 3, 'Income', '2020-07-05', 'Right'),
(143950, 0, 653, 0, 655, 250, '0.00', 250, 'Left', 0, 0, 'Income', '2020-07-05', 'Left'),
(143951, 143948, 648, 0, 655, 250, '0.00', 250, 'Left', 1, 1, 'Income', '2020-07-05', 'Left'),
(143952, 143949, 1, 1500, 655, 250, '250.00', 1500, 'Left', 4, 4, 'Income', '2020-07-05', 'Right'),
(143953, 0, 654, 0, 656, 250, '0.00', 250, 'Right', 0, 0, 'Income', '2020-07-05', 'Right'),
(143954, 143951, 648, 250, 656, 250, '250.00', 0, 'Right', 2, 2, 'Income', '2020-07-05', 'Right'),
(143955, 143952, 1, 1500, 656, 250, '250.00', 1500, 'Left', 5, 5, 'GC', '2020-07-05', 'Right'),
(143957, 0, 657, 0, 659, 3500, '0.00', 3500, 'Left', 0, 0, 'Income', '2020-07-05', 'Left'),
(143958, 143957, 657, 3500, 660, 500, '250.00', 3500, 'Left', 1, 1, 'Income', '2020-07-05', 'Right'),
(143959, 143957, 657, 3500, 660, 500, '250.00', 3500, 'Left', 2, 2, 'Income', '2020-07-05', 'Right'),
(143960, 0, 660, 0, 661, 500, '0.00', 500, 'Right', 0, 0, 'Income', '2020-07-05', 'Right'),
(143961, 143959, 657, 3500, 661, 500, '250.00', 3500, 'Left', 3, 3, 'Income', '2020-07-05', 'Right'),
(143962, 143959, 657, 3500, 661, 500, '250.00', 3500, 'Left', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143963, 143960, 660, 500, 662, 3500, '250.00', 3000, 'Left', 1, 1, 'Income', '2020-07-05', 'Left'),
(143964, 143960, 660, 500, 662, 3500, '250.00', 3000, 'Left', 2, 2, 'Income', '2020-07-05', 'Left'),
(143965, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143966, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143967, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143968, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143969, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143970, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143971, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143972, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143973, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143974, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143975, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143976, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143977, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143978, 143962, 657, 3500, 662, 3500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-05', 'Right'),
(143979, 143944, 647, 1000, 663, 250, '0.00', 1250, 'Left', 1, 1, 'Income', '2020-07-05', 'Left'),
(143980, 143955, 1, 1500, 663, 250, '0.00', 1750, 'Left', 5, 5, 'Income', '2020-07-05', 'Left'),
(143981, 143979, 647, 1250, 664, 250, '250.00', 1250, 'Left', 2, 2, 'Income', '2020-07-05', 'Right'),
(143982, 143980, 1, 1750, 664, 250, '0.00', 2000, 'Left', 5, 5, 'Income', '2020-07-05', 'Left'),
(143983, 0, 664, 0, 665, 250, '0.00', 250, 'Left', 0, 0, 'Income', '2020-07-05', 'Left'),
(143984, 143981, 647, 1250, 665, 250, '250.00', 1250, 'Left', 3, 3, 'Income', '2020-07-05', 'Right'),
(143985, 143982, 1, 2000, 665, 250, '0.00', 2250, 'Left', 5, 5, 'Income', '2020-07-05', 'Left');

-- --------------------------------------------------------

--
-- Table structure for table `payout_time`
--

CREATE TABLE `payout_time` (
  `id` int(11) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Active','Inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payout_time`
--

INSERT INTO `payout_time` (`id`, `day`, `created`, `status`) VALUES
(1, 'Monday', '2020-06-21 23:56:48', 'Active'),
(2, 'Tuesday', '2020-06-21 23:56:48', 'Inactive'),
(3, 'Wednesday', '2020-06-21 23:56:48', 'Inactive'),
(4, 'Thursday', '2020-06-21 23:56:48', 'Inactive'),
(5, 'Friday', '2020-06-21 23:56:48', 'Inactive'),
(6, 'Saturday', '2020-06-21 23:56:48', 'Inactive'),
(7, 'Sunday', '2020-06-21 23:56:48', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE `processes` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `direct_referral_income` decimal(13,2) NOT NULL,
  `direct_referral_gc` decimal(13,2) NOT NULL,
  `pairing_account_id` int(11) NOT NULL,
  `pairing_invited_account_id` int(11) NOT NULL,
  `pairing_new_registered_account_id` int(11) NOT NULL,
  `earned` decimal(13,2) NOT NULL,
  `status` enum('Pending','Processed','Ongoing','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `processes`
--

INSERT INTO `processes` (`id`, `account_id`, `direct_referral_income`, `direct_referral_gc`, `pairing_account_id`, `pairing_invited_account_id`, `pairing_new_registered_account_id`, `earned`, `status`, `created`) VALUES
(79, 1, '70.00', '125.00', 1, 647, 647, '70.00', 'Processed', '2020-07-05 04:44:07'),
(80, 1, '70.00', '125.00', 1, 648, 648, '70.00', 'Processed', '2020-07-05 04:44:30'),
(81, 1, '70.00', '125.00', 647, 649, 649, '70.00', 'Processed', '2020-07-05 04:44:48'),
(82, 1, '70.00', '125.00', 647, 650, 650, '70.00', 'Processed', '2020-07-05 04:45:01'),
(83, 1, '500.00', '250.00', 649, 651, 651, '500.00', 'Processed', '2020-07-05 04:45:26'),
(84, 1, '500.00', '250.00', 649, 652, 652, '500.00', 'Processed', '2020-07-05 04:45:45'),
(85, 1, '70.00', '125.00', 648, 653, 653, '70.00', 'Processed', '2020-07-05 04:51:16'),
(86, 1, '70.00', '125.00', 648, 654, 654, '70.00', 'Processed', '2020-07-05 04:51:31'),
(87, 1, '70.00', '125.00', 653, 655, 655, '70.00', 'Processed', '2020-07-05 04:51:44'),
(88, 1, '70.00', '125.00', 654, 656, 656, '70.00', 'Processed', '2020-07-05 04:52:02'),
(89, 657, '3500.00', '1000.00', 657, 658, 658, '3500.00', 'Processed', '2020-07-05 06:23:02'),
(90, 657, '3500.00', '1000.00', 657, 659, 659, '3500.00', 'Processed', '2020-07-05 06:25:26'),
(91, 657, '500.00', '250.00', 657, 660, 660, '500.00', 'Processed', '2020-07-05 06:26:20'),
(92, 657, '500.00', '250.00', 660, 661, 661, '500.00', 'Processed', '2020-07-05 06:27:40'),
(93, 660, '3500.00', '1000.00', 660, 662, 662, '3500.00', 'Processed', '2020-07-05 06:29:38'),
(94, 647, '70.00', '125.00', 647, 663, 663, '70.00', 'Processed', '2020-07-05 06:39:22'),
(95, 647, '70.00', '125.00', 647, 664, 664, '70.00', 'Processed', '2020-07-05 06:40:51'),
(96, 664, '70.00', '125.00', 664, 665, 665, '70.00', 'Processed', '2020-07-05 06:43:16');

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `earnings` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `referrals`
--

INSERT INTO `referrals` (`id`, `package_id`, `earnings`, `product`, `created`) VALUES
(2, 8, 500, 250, '2020-06-20 13:42:11'),
(5, 9, 1000, 500, '2020-06-20 13:55:15'),
(6, 10, 3500, 1000, '2020-06-20 13:55:27'),
(7, 13, 70, 125, '2020-06-21 09:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `remmittance`
--

CREATE TABLE `remmittance` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `receivers_name` varchar(200) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `remmittance_center` enum('Palawan Pawnshop','Cebuana Lhuillier','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `remmittance`
--

INSERT INTO `remmittance` (`id`, `account_id`, `receivers_name`, `contact_number`, `remmittance_center`, `created`) VALUES
(1, 1, 'PAUL JAMES M. CARTAJENA', '09099575101', 'Palawan Pawnshop', '2020-07-03 05:15:55');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_fee`
--

CREATE TABLE `transaction_fee` (
  `id` int(11) NOT NULL,
  `fee` int(11) NOT NULL,
  `status` enum('Active','Inactive','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction_fee`
--

INSERT INTO `transaction_fee` (`id`, `fee`, `status`, `created`, `updated`) VALUES
(1, 10, 'Active', '2020-07-03 02:37:36', '2020-07-02 20:55:07'),
(2, 15, 'Inactive', '2020-07-02 20:52:56', '2020-07-02 20:57:52'),
(3, 20, 'Inactive', '2020-07-02 20:58:36', '2020-07-02 20:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(300) NOT NULL,
  `token` varchar(200) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `account_id`, `username`, `email`, `password`, `token`, `last_login`, `created`) VALUES
(459, 1, 'admin', '', '$2y$10$AO72iG3fKhoXtS6mZkF4BezQfbNm3J3.qu2ND4kSoWXOkAjS3nzqu', 'ab609eabc624a0e47d73e4c5', '2020-07-05 06:43:34', '2020-07-04 05:37:29'),
(611, 647, '1', '', '$2y$10$UdyVH6XlJcqrnJu4zO7Fm.aYBFQ1M9sLzw8Z1NEB82B2SitFCiUcG', '', '2020-07-05 06:38:23', '2020-07-05 04:44:07'),
(612, 648, '6', '', '$2y$10$BqnAs.cQ8bw9HEhE7i3pju/KXG8irmu24mpQtLFcMECkjn4yg2yZe', '', '2020-07-05 04:44:30', '2020-07-05 04:44:30'),
(613, 649, '2', '', '$2y$10$PmXOviZpG9DvTHLT63HoP.3QvCFTvqEVOisY2AbCKKro9wut0ILwG', 'f7e820515682bf51c8418a7e', '2020-07-05 04:48:41', '2020-07-05 04:44:48'),
(614, 650, '3', '', '$2y$10$L9QwOsf0.WyO6f.x5PanveJltqAbrmMuVac.eG3QpOCFlABrM0wkq', '', '2020-07-05 04:45:01', '2020-07-05 04:45:01'),
(615, 651, '4', '', '$2y$10$VlZCM9nQLiqPpoI/7bDAyuByKnUcNIA.1laW51uRDkmyiGaEdZcOG', '', '2020-07-05 04:45:26', '2020-07-05 04:45:26'),
(616, 652, '5', '', '$2y$10$qUT4UmKsAqcl43cUr0JdC.hYOoGT.SzITCUbEZkOwyEBechGw0vNy', '', '2020-07-05 04:45:45', '2020-07-05 04:45:45'),
(617, 653, '7', '', '$2y$10$acUG6Jja9PLiMktFZkoA7OBr7S79zZViUCNuvqVlHm7IFSJSAuf9G', '', '2020-07-05 04:51:16', '2020-07-05 04:51:16'),
(618, 654, '8', '', '$2y$10$b92KTLFBGRk1mNRnHKqeW.OUSHX.HnqJY1MZfIAJXGReT9zr447fm', '', '2020-07-05 04:51:30', '2020-07-05 04:51:30'),
(619, 655, '9', '', '$2y$10$KH16aolyUDtvttpcY7/3auTRxkSQlwmrmj.JtPtQRlo7qjVBJSVUG', '', '2020-07-05 04:51:44', '2020-07-05 04:51:44'),
(620, 656, '10', '', '$2y$10$6v4CjPNwZ7CNmXgAO.QPvuq3infF9PBkWtxxdtM4F/sg.56LBliam', '', '2020-07-05 04:52:02', '2020-07-05 04:52:02'),
(621, 657, 'admin12', '', '$2y$10$AO72iG3fKhoXtS6mZkF4BezQfbNm3J3.qu2ND4kSoWXOkAjS3nzqu', '', '2020-07-05 06:31:49', '2020-07-04 05:37:29'),
(623, 659, '21', '', '$2y$10$16CxPKVvLzkHlpa4HvQifet3y0M5SRk5vzkTw5HbnK95oFtDY6PKS', '', '2020-07-05 06:27:20', '2020-07-05 06:25:26'),
(624, 660, '22', '', '$2y$10$1Xfk49CC39HFkxivL22IuO4KSU8mBfMlhbzpfGGsK5xTZEyBa1CA6', '', '2020-07-05 06:28:24', '2020-07-05 06:26:20'),
(625, 661, '23', '', '$2y$10$dWb0wJyTWuHC7/jxUijWGOb9i53QKvzLX7AZmh8TwoJ05GXqij5QW', '', '2020-07-05 06:27:39', '2020-07-05 06:27:39'),
(626, 662, '31', '', '$2y$10$b9PcBLyBQHxN81dx6ACqzucFQ3qRe.HRZyzoiL/lFG8G3I7Q0fSfS', '', '2020-07-05 06:29:38', '2020-07-05 06:29:38'),
(627, 663, 'test1', '', '$2y$10$gYBbunn8WH.LQKNiZ8s9RuL3DLKhJQSscm7Aa6hUf1eSUdBHHi.Vu', '', '2020-07-05 06:39:22', '2020-07-05 06:39:22'),
(628, 664, 'test12', '', '$2y$10$COybttmKL8IJIe.zAhoo1.cuAVaw/Bmrug4JADS2me9AghBsVj6Gu', '', '2020-07-05 06:42:42', '2020-07-05 06:40:51'),
(629, 665, 'tset22', '', '$2y$10$SQPij8gvSobJkDdywCKDXusuNidDswOSwfZ5sPh6MwG0mYEGFv7B.', '', '2020-07-05 06:43:16', '2020-07-05 06:43:16');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `accumulated_income` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `account_id`, `balance`, `worth_of_product`, `accumulated_income`, `created`, `updated`) VALUES
(121, 1, '2850.50', '1750.00', '2850.50', '2020-07-02', '2020-07-05 06:43:26'),
(403, 647, '890.00', '250.00', '890.00', '2020-07-05', '2020-07-05 06:43:26'),
(404, 648, '500.00', '0.00', '500.00', '2020-07-05', '2020-07-05 05:12:24'),
(405, 649, '500.00', '0.00', '500.00', '2020-07-05', '2020-07-05 05:12:11'),
(406, 650, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 04:45:01'),
(407, 651, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 04:45:26'),
(408, 652, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 04:45:45'),
(409, 653, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 04:51:16'),
(410, 654, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 04:51:31'),
(411, 655, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 04:51:44'),
(412, 656, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 04:52:02'),
(414, 657, '5250.00', '1500.00', '5250.00', '2020-07-05', '2020-07-05 06:29:46'),
(415, 659, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 06:25:26'),
(416, 660, '4000.00', '1000.00', '4000.00', '2020-07-05', '2020-07-05 06:29:46'),
(417, 661, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 06:27:40'),
(418, 662, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 06:29:38'),
(419, 663, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 06:39:22'),
(420, 664, '70.00', '125.00', '70.00', '2020-07-05', '2020-07-05 06:43:16'),
(421, 665, '0.00', '0.00', '0.00', '2020-07-05', '2020-07-05 06:43:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activation_code` (`activation_code_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `upline_id` (`upline_id`),
  ADD KEY `upline_id_2` (`upline_id`);

--
-- Indexes for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `cashout`
--
ALTER TABLE `cashout`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `confirm_id` (`confirm_id`);

--
-- Indexes for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `downline_id` (`downline_id`),
  ADD KEY `indirect_bonuses_id` (`indirect_bonuses_id`);

--
-- Indexes for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pair`
--
ALTER TABLE `pair`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id_4` (`invited_account_id`);

--
-- Indexes for table `payout_time`
--
ALTER TABLE `payout_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processes`
--
ALTER TABLE `processes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `remmittance`
--
ALTER TABLE `remmittance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=666;

--
-- AUTO_INCREMENT for table `activation_codes`
--
ALTER TABLE `activation_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=244;

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cashout`
--
ALTER TABLE `cashout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=480;

--
-- AUTO_INCREMENT for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=402;

--
-- AUTO_INCREMENT for table `ewallet`
--
ALTER TABLE `ewallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `indirect_level`
--
ALTER TABLE `indirect_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pair`
--
ALTER TABLE `pair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143986;

--
-- AUTO_INCREMENT for table `payout_time`
--
ALTER TABLE `payout_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `processes`
--
ALTER TABLE `processes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `remmittance`
--
ALTER TABLE `remmittance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=630;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=422;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`),
  ADD CONSTRAINT `accounts_ibfk_2` FOREIGN KEY (`activation_code_id`) REFERENCES `activation_codes` (`id`);

--
-- Constraints for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD CONSTRAINT `activation_codes_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cashout`
--
ALTER TABLE `cashout`
  ADD CONSTRAINT `cashout_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD CONSTRAINT `daily_transaction_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD CONSTRAINT `direct_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD CONSTRAINT `direct_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD CONSTRAINT `direct_referral_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_referral_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD CONSTRAINT `ewallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD CONSTRAINT `indirect_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD CONSTRAINT `indirect_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_2` FOREIGN KEY (`downline_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_3` FOREIGN KEY (`indirect_bonuses_id`) REFERENCES `indirect_bonuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD CONSTRAINT `indirect_level_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD CONSTRAINT `pairing_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD CONSTRAINT `pairing_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pairing_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `referrals`
--
ALTER TABLE `referrals`
  ADD CONSTRAINT `referrals_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `remmittance`
--
ALTER TABLE `remmittance`
  ADD CONSTRAINT `remmittance_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
