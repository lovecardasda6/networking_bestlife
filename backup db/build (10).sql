-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2020 at 06:31 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `build`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `gender` enum('Male','Female','','') NOT NULL,
  `address` varchar(300) NOT NULL,
  `date_of_birth` date NOT NULL,
  `place_of_birth` varchar(200) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code_id` int(11) NOT NULL,
  `upline_id` int(11) NOT NULL,
  `position` enum('None','Left','Right') NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `lastname`, `firstname`, `middlename`, `contact`, `gender`, `address`, `date_of_birth`, `place_of_birth`, `reference_id`, `package_id`, `activation_code_id`, `upline_id`, `position`, `created`) VALUES
(1, 'MONTESORES', 'MEI', 'DEL SANTOS', '09099571020', 'Female', 'POBLACION PASIG CITY MANILA', '1990-07-12', 'POBLACION PASIG CITY MANILA', 0, 5, 1, 0, 'None', '2020-07-12'),
(5, '2', '2', '', '2', 'Male', '', '0000-00-00', '577B-9356-BA74-D760', 1, 5, 2, 1, 'Left', '2020-07-12'),
(6, '3', '3', '', '3', 'Male', '', '0000-00-00', '', 1, 5, 3, 1, 'Right', '2020-07-12'),
(7, '4', '4', '', '4', 'Male', '', '0000-00-00', '', 1, 5, 4, 5, 'Left', '2020-07-12'),
(8, '5', '5', '', '5', 'Male', '', '0000-00-00', '', 1, 2, 9, 5, 'Right', '2020-07-12'),
(9, '6', '6', '', '6', 'Male', '', '0000-00-00', '', 1, 4, 5, 6, 'Right', '2020-07-12'),
(10, '7', '7', '', '7', 'Male', '', '0000-00-00', '', 9, 2, 10, 9, 'Left', '2020-07-12'),
(11, '8', '8', '', '8', 'Male', '', '0000-00-00', '', 9, 2, 11, 9, 'Right', '2020-07-12'),
(12, '9', '9', '9', '9', 'Male', '', '0000-00-00', '', 11, 4, 6, 11, 'Left', '2020-07-12'),
(13, '10', '10', '10', '10', 'Male', '', '0000-00-00', '', 11, 4, 7, 11, 'Right', '2020-07-12');

-- --------------------------------------------------------

--
-- Table structure for table `activation_codes`
--

CREATE TABLE `activation_codes` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code` text NOT NULL,
  `type` enum('Paid','Unpaid','','') NOT NULL,
  `status` enum('Unused','Used','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activation_codes`
--

INSERT INTO `activation_codes` (`id`, `package_id`, `activation_code`, `type`, `status`, `created`) VALUES
(1, 5, '3556241EFB903FF3', 'Unpaid', 'Used', '2020-07-12 01:38:15'),
(2, 5, '577B9356BA74D760', 'Paid', 'Used', '2020-07-12 02:59:29'),
(3, 5, '97359491BBD94D3C', 'Paid', 'Used', '2020-07-12 02:59:29'),
(4, 5, '4112865BB6C837F1', 'Paid', 'Used', '2020-07-12 02:59:29'),
(5, 4, '151E3DBE6B0771E8', 'Paid', 'Used', '2020-07-12 02:59:39'),
(6, 4, '990F5BC99F862393', 'Paid', 'Used', '2020-07-12 02:59:39'),
(7, 4, '27A98F2F19B2A8C6', 'Paid', 'Used', '2020-07-12 02:59:39'),
(8, 2, '6A053F55259D82BA', 'Paid', 'Used', '2020-07-12 02:59:54'),
(9, 2, '939AA283E0844E44', 'Paid', 'Used', '2020-07-12 02:59:54'),
(10, 2, 'C7E94465BCF8A656', 'Paid', 'Used', '2020-07-12 02:59:54'),
(11, 2, '2A3819A38691C6CD', 'Paid', 'Used', '2020-07-12 02:59:54');

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `token` varchar(200) NOT NULL,
  `role` enum('Administrator','Accounting','','') NOT NULL,
  `remark` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `bank_name` enum('BDO','FCB') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `account_id`, `account_name`, `account_number`, `bank_name`, `created`) VALUES
(1, 1, 'MONTESORES, MEI DEL SANTOS', '0056100156011001', 'BDO', '2020-07-11 19:52:30'),
(2, 1, 'MONTESORES, MEI', '1001500520020001', 'FCB', '2020-07-11 19:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `cashout`
--

CREATE TABLE `cashout` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `fee` int(11) NOT NULL,
  `receivable_amount` decimal(13,2) NOT NULL,
  `encashment_type` enum('Bank','Remittance','EWallet') NOT NULL,
  `encashment_center` enum('BDO','FCB','Palawan Pawnshop','Cebuana Lhuillier','GCash','') NOT NULL,
  `status` enum('Pending','Confirmed','','') NOT NULL,
  `remark` text NOT NULL,
  `confirm_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cashout`
--

INSERT INTO `cashout` (`id`, `account_id`, `amount`, `fee`, `receivable_amount`, `encashment_type`, `encashment_center`, `status`, `remark`, `confirm_id`, `created`, `updated`) VALUES
(1, 1, '18272.50', 0, '0.00', 'Bank', 'BDO', 'Pending', '', 0, '2020-07-11 22:27:03', '2020-07-11 22:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `daily_transaction`
--

CREATE TABLE `daily_transaction` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `transaction_type` enum('DIRECT REFERRAL','PAIRING BONUS','INDIRECT BONUS','DIRECT BONUS','') NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daily_transaction`
--

INSERT INTO `daily_transaction` (`id`, `account_id`, `transaction_type`, `transaction_id`, `created`) VALUES
(2, 1, 'DIRECT REFERRAL', 2, '2020-07-11 16:00:00'),
(3, 1, 'DIRECT REFERRAL', 3, '2020-07-11 16:00:00'),
(4, 1, 'DIRECT REFERRAL', 4, '2020-07-11 16:00:00'),
(5, 1, 'DIRECT REFERRAL', 5, '2020-07-11 16:00:00'),
(6, 1, 'PAIRING BONUS', 2, '2020-07-11 16:00:00'),
(7, 1, 'PAIRING BONUS', 3, '2020-07-11 16:00:00'),
(8, 1, 'PAIRING BONUS', 4, '2020-07-11 16:00:00'),
(9, 1, 'PAIRING BONUS', 5, '2020-07-11 16:00:00'),
(10, 1, 'PAIRING BONUS', 6, '2020-07-11 16:00:00'),
(11, 1, 'PAIRING BONUS', 7, '2020-07-11 16:00:00'),
(12, 1, 'PAIRING BONUS', 8, '2020-07-11 16:00:00'),
(13, 1, 'PAIRING BONUS', 9, '2020-07-11 16:00:00'),
(14, 1, 'PAIRING BONUS', 10, '2020-07-11 16:00:00'),
(15, 1, 'PAIRING BONUS', 11, '2020-07-11 16:00:00'),
(16, 1, 'PAIRING BONUS', 12, '2020-07-11 16:00:00'),
(17, 1, 'PAIRING BONUS', 13, '2020-07-11 16:00:00'),
(18, 1, 'PAIRING BONUS', 14, '2020-07-11 16:00:00'),
(19, 1, 'PAIRING BONUS', 15, '2020-07-11 16:00:00'),
(20, 5, 'PAIRING BONUS', 18, '2020-07-11 16:00:00'),
(21, 1, 'DIRECT BONUS', 1, '2020-07-11 16:00:00'),
(22, 1, 'DIRECT REFERRAL', 6, '2020-07-11 16:00:00'),
(23, 1, 'PAIRING BONUS', 21, '2020-07-11 16:00:00'),
(24, 1, 'PAIRING BONUS', 22, '2020-07-11 16:00:00'),
(25, 9, 'DIRECT REFERRAL', 7, '2020-07-11 16:00:00'),
(26, 1, 'DIRECT BONUS', 2, '2020-07-11 16:00:00'),
(27, 9, 'DIRECT REFERRAL', 8, '2020-07-11 16:00:00'),
(28, 1, 'DIRECT BONUS', 3, '2020-07-11 16:00:00'),
(29, 9, 'PAIRING BONUS', 26, '2020-07-11 16:00:00'),
(30, 1, 'DIRECT BONUS', 4, '2020-07-11 16:00:00'),
(31, 11, 'DIRECT REFERRAL', 9, '2020-07-11 16:00:00'),
(32, 1, 'INDIRECT BONUS', 1, '2020-07-11 16:00:00'),
(33, 11, 'DIRECT REFERRAL', 10, '2020-07-11 16:00:00'),
(34, 1, 'INDIRECT BONUS', 2, '2020-07-11 16:00:00'),
(35, 11, 'PAIRING BONUS', 33, '2020-07-11 16:00:00'),
(36, 11, 'PAIRING BONUS', 34, '2020-07-11 16:00:00'),
(37, 11, 'PAIRING BONUS', 35, '2020-07-11 16:00:00'),
(38, 11, 'PAIRING BONUS', 36, '2020-07-11 16:00:00'),
(39, 11, 'PAIRING BONUS', 37, '2020-07-11 16:00:00'),
(40, 11, 'PAIRING BONUS', 38, '2020-07-11 16:00:00'),
(41, 1, 'INDIRECT BONUS', 3, '2020-07-11 16:00:00'),
(42, 1, 'DIRECT REFERRAL', 11, '2020-07-11 16:00:00'),
(43, 1, 'PAIRING BONUS', 43, '2020-07-11 16:00:00'),
(44, 1, 'PAIRING BONUS', 44, '2020-07-11 16:00:00'),
(45, 1, 'PAIRING BONUS', 45, '2020-07-11 16:00:00'),
(46, 1, 'PAIRING BONUS', 46, '2020-07-11 16:00:00'),
(47, 1, 'PAIRING BONUS', 47, '2020-07-11 16:00:00'),
(48, 1, 'PAIRING BONUS', 48, '2020-07-11 16:00:00'),
(49, 1, 'PAIRING BONUS', 49, '2020-07-11 16:00:00'),
(50, 1, 'PAIRING BONUS', 50, '2020-07-11 16:00:00'),
(51, 1, 'PAIRING BONUS', 51, '2020-07-11 16:00:00'),
(52, 1, 'PAIRING BONUS', 52, '2020-07-11 16:00:00'),
(53, 1, 'PAIRING BONUS', 53, '2020-07-11 16:00:00'),
(54, 1, 'PAIRING BONUS', 54, '2020-07-11 16:00:00'),
(55, 1, 'PAIRING BONUS', 55, '2020-07-11 16:00:00'),
(56, 1, 'PAIRING BONUS', 56, '2020-07-11 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonuses`
--

CREATE TABLE `direct_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_bonuses`
--

INSERT INTO `direct_bonuses` (`id`, `package_id`, `percentage`, `created`) VALUES
(1, 5, 15, '2020-07-11 19:36:50');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonus_income`
--

CREATE TABLE `direct_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_bonus_income`
--

INSERT INTO `direct_bonus_income` (`id`, `account_id`, `invited_account_id`, `amount`, `created`) VALUES
(1, 1, 5, '37.50', '2020-07-12'),
(2, 1, 9, '11.25', '2020-07-12'),
(3, 1, 9, '11.25', '2020-07-12'),
(4, 1, 9, '37.50', '2020-07-12');

-- --------------------------------------------------------

--
-- Table structure for table `direct_referral_income`
--

CREATE TABLE `direct_referral_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_referral_income`
--

INSERT INTO `direct_referral_income` (`id`, `account_id`, `invited_account_id`, `amount`, `worth_of_product`, `created`) VALUES
(2, 1, 5, '3500.00', '1000.00', '2020-07-12'),
(3, 1, 6, '3500.00', '1000.00', '2020-07-12'),
(4, 1, 7, '75.00', '125.00', '2020-07-12'),
(5, 1, 8, '75.00', '125.00', '2020-07-12'),
(6, 1, 9, '1000.00', '500.00', '2020-07-12'),
(7, 9, 10, '75.00', '125.00', '2020-07-12'),
(8, 9, 11, '75.00', '125.00', '2020-07-12'),
(9, 11, 12, '1000.00', '500.00', '2020-07-12'),
(10, 11, 13, '1000.00', '500.00', '2020-07-12'),
(11, 1, 7, '3500.00', '1000.00', '2020-07-12');

-- --------------------------------------------------------

--
-- Table structure for table `ewallet`
--

CREATE TABLE `ewallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `ewallet_number` varchar(200) NOT NULL,
  `ewallet_center` enum('GCash') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ewallet`
--

INSERT INTO `ewallet` (`id`, `account_id`, `ewallet_number`, `ewallet_center`, `created`) VALUES
(1, 1, '09102626101', 'GCash', '2020-07-11 19:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonuses`
--

CREATE TABLE `indirect_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_bonuses`
--

INSERT INTO `indirect_bonuses` (`id`, `package_id`, `level`, `percent`, `created`) VALUES
(1, 4, 1, '10.00', '2020-07-11 19:35:27'),
(2, 4, 2, '7.50', '2020-07-11 19:35:27'),
(3, 4, 3, '5.00', '2020-07-11 19:35:27'),
(4, 4, 4, '2.50', '2020-07-11 19:35:27'),
(5, 4, 5, '1.00', '2020-07-11 19:35:27'),
(6, 4, 6, '1.00', '2020-07-11 19:35:27'),
(7, 4, 7, '1.00', '2020-07-11 19:35:27'),
(8, 4, 8, '1.00', '2020-07-11 19:35:27'),
(9, 4, 9, '1.00', '2020-07-11 19:35:27'),
(10, 4, 10, '15.00', '2020-07-11 19:35:27'),
(11, 5, 1, '10.00', '2020-07-11 19:35:30'),
(12, 5, 2, '7.50', '2020-07-11 19:35:30'),
(13, 5, 3, '5.00', '2020-07-11 19:35:30'),
(14, 5, 4, '2.50', '2020-07-11 19:35:30'),
(15, 5, 5, '1.00', '2020-07-11 19:35:30'),
(16, 5, 6, '1.00', '2020-07-11 19:35:30'),
(17, 5, 7, '1.00', '2020-07-11 19:35:30'),
(18, 5, 8, '1.00', '2020-07-11 19:35:30'),
(19, 5, 9, '1.00', '2020-07-11 19:35:30'),
(20, 5, 10, '1.00', '2020-07-11 19:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonus_income`
--

CREATE TABLE `indirect_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `downline_id` int(11) NOT NULL,
  `indirect_bonuses_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_bonus_income`
--

INSERT INTO `indirect_bonus_income` (`id`, `account_id`, `downline_id`, `indirect_bonuses_id`, `amount`, `created`) VALUES
(1, 1, 11, 11, '100.00', '2020-07-12'),
(2, 1, 11, 11, '100.00', '2020-07-12'),
(3, 1, 11, 11, '75.00', '2020-07-12');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_level`
--

CREATE TABLE `indirect_level` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_level`
--

INSERT INTO `indirect_level` (`id`, `package_id`, `level`, `created`) VALUES
(1, 4, 10, '2020-07-11 19:35:27'),
(2, 5, 10, '2020-07-11 19:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `message` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package`, `price`, `created`) VALUES
(2, 'Bronze', 750, '2020-07-11 19:27:23'),
(3, 'Silver', 1000, '2020-07-11 19:27:28'),
(4, 'Gold', 4500, '2020-07-11 19:27:33'),
(5, 'Platinum', 12500, '2020-07-11 19:27:38');

-- --------------------------------------------------------

--
-- Table structure for table `pair`
--

CREATE TABLE `pair` (
  `id` int(11) NOT NULL,
  `multiplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonuses`
--

CREATE TABLE `pairing_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `pair_per_day` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pairing_bonuses`
--

INSERT INTO `pairing_bonuses` (`id`, `package_id`, `value_points`, `pair_per_day`, `created`) VALUES
(1, 2, 250, 3, '2020-07-11 19:34:26'),
(2, 3, 500, 6, '2020-07-11 19:34:34'),
(3, 4, 1500, 18, '2020-07-11 19:35:02'),
(4, 5, 3500, 50, '2020-07-11 19:35:20');

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonus_income`
--

CREATE TABLE `pairing_bonus_income` (
  `id` int(11) NOT NULL,
  `pairing_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `income` decimal(11,2) NOT NULL,
  `remaining_vp` int(11) NOT NULL,
  `heavy_side` enum('None','Left','Right','') NOT NULL,
  `paired` int(11) NOT NULL,
  `paired_this_day` int(11) NOT NULL,
  `remark` enum('Income','GC','FlushOut','') NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp(),
  `positioned` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pairing_bonus_income`
--

INSERT INTO `pairing_bonus_income` (`id`, `pairing_id`, `account_id`, `balance`, `invited_account_id`, `value_points`, `income`, `remaining_vp`, `heavy_side`, `paired`, `paired_this_day`, `remark`, `created`, `positioned`) VALUES
(1, 0, 1, 0, 5, 3500, '0.00', 3500, 'Left', 0, 0, 'Income', '2020-07-12', 'Left'),
(2, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 1, 1, 'Income', '2020-07-12', 'Right'),
(3, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 2, 2, 'Income', '2020-07-12', 'Right'),
(4, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 3, 3, 'Income', '2020-07-12', 'Right'),
(5, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 4, 4, 'Income', '2020-07-12', 'Right'),
(6, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 5, 5, 'GC', '2020-07-12', 'Right'),
(7, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 1, 6, 'Income', '2020-07-12', 'Right'),
(8, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 2, 7, 'Income', '2020-07-12', 'Right'),
(9, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 3, 8, 'Income', '2020-07-12', 'Right'),
(10, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 4, 9, 'Income', '2020-07-12', 'Right'),
(11, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 5, 10, 'GC', '2020-07-12', 'Right'),
(12, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 1, 11, 'Income', '2020-07-12', 'Right'),
(13, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 2, 12, 'Income', '2020-07-12', 'Right'),
(14, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 3, 13, 'Income', '2020-07-12', 'Right'),
(15, 1, 1, 3500, 6, 3500, '250.00', 0, 'Right', 4, 14, 'Income', '2020-07-12', 'Right'),
(16, 0, 5, 0, 7, 250, '0.00', 250, 'Left', 0, 0, 'Income', '2020-07-12', 'Left'),
(17, 15, 1, 0, 7, 250, '0.00', 250, 'Left', 4, 14, 'Income', '2020-07-12', 'Left'),
(18, 16, 5, 250, 8, 250, '250.00', 0, 'Right', 1, 1, 'Income', '2020-07-12', 'Right'),
(19, 17, 1, 250, 8, 250, '0.00', 500, 'Left', 4, 14, 'Income', '2020-07-12', 'Left'),
(20, 0, 6, 0, 9, 1500, '0.00', 1500, 'Right', 0, 0, 'Income', '2020-07-12', 'Right'),
(21, 19, 1, 500, 9, 1500, '250.00', 1000, 'Right', 5, 15, 'GC', '2020-07-12', 'Right'),
(22, 19, 1, 500, 9, 1500, '250.00', 1000, 'Right', 1, 16, 'Income', '2020-07-12', 'Right'),
(23, 0, 9, 0, 10, 250, '0.00', 250, 'Left', 0, 0, 'Income', '2020-07-12', 'Left'),
(24, 20, 6, 1500, 10, 250, '0.00', 1750, 'Right', 0, 0, 'Income', '2020-07-12', 'Right'),
(25, 22, 1, 1000, 10, 250, '0.00', 1250, 'Right', 1, 16, 'Income', '2020-07-12', 'Right'),
(26, 23, 9, 250, 11, 250, '250.00', 0, 'Right', 1, 1, 'Income', '2020-07-12', 'Right'),
(27, 24, 6, 1750, 11, 250, '0.00', 2000, 'Right', 0, 0, 'Income', '2020-07-12', 'Right'),
(28, 25, 1, 1250, 11, 250, '0.00', 1500, 'Right', 1, 16, 'Income', '2020-07-12', 'Right'),
(29, 0, 11, 0, 12, 1500, '0.00', 1500, 'Left', 0, 0, 'Income', '2020-07-12', 'Left'),
(30, 26, 9, 0, 12, 1500, '0.00', 1500, 'Right', 1, 1, 'Income', '2020-07-12', 'Right'),
(31, 27, 6, 2000, 12, 1500, '0.00', 3500, 'Right', 0, 0, 'Income', '2020-07-12', 'Right'),
(32, 28, 1, 1500, 12, 1500, '0.00', 3000, 'Right', 1, 16, 'Income', '2020-07-12', 'Right'),
(33, 29, 11, 1500, 13, 1500, '250.00', 0, 'Right', 1, 1, 'Income', '2020-07-12', 'Right'),
(34, 29, 11, 1500, 13, 1500, '250.00', 0, 'Right', 2, 2, 'Income', '2020-07-12', 'Right'),
(35, 29, 11, 1500, 13, 1500, '250.00', 0, 'Right', 3, 3, 'Income', '2020-07-12', 'Right'),
(36, 29, 11, 1500, 13, 1500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-12', 'Right'),
(37, 29, 11, 1500, 13, 1500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-12', 'Right'),
(38, 29, 11, 1500, 13, 1500, '250.00', 0, 'Right', 3, 3, 'FlushOut', '2020-07-12', 'Right'),
(39, 30, 9, 1500, 13, 1500, '0.00', 3000, 'Right', 1, 1, 'Income', '2020-07-12', 'Right'),
(40, 31, 6, 3500, 13, 1500, '0.00', 5000, 'Right', 0, 0, 'Income', '2020-07-12', 'Right'),
(41, 32, 1, 3000, 13, 1500, '0.00', 4500, 'Right', 1, 16, 'Income', '2020-07-12', 'Right'),
(42, 18, 5, 0, 7, 3500, '0.00', 3500, 'Left', 1, 1, 'Income', '2020-07-12', 'Left'),
(43, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 2, 17, 'Income', '2020-07-12', 'Left'),
(44, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 3, 18, 'Income', '2020-07-12', 'Left'),
(45, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 4, 19, 'Income', '2020-07-12', 'Left'),
(46, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 5, 20, 'GC', '2020-07-12', 'Left'),
(47, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 1, 21, 'Income', '2020-07-12', 'Left'),
(48, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 2, 22, 'Income', '2020-07-12', 'Left'),
(49, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 3, 23, 'Income', '2020-07-12', 'Left'),
(50, 41, 1, 4500, 7, 3500, '500.00', 4500, 'Right', 4, 24, 'Income', '2020-07-12', 'Left'),
(51, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 5, 25, 'GC', '2020-07-12', 'Left'),
(52, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 1, 26, 'Income', '2020-07-12', 'Left'),
(53, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 2, 27, 'Income', '2020-07-12', 'Left'),
(54, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 3, 28, 'Income', '2020-07-12', 'Left'),
(55, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 4, 29, 'Income', '2020-07-12', 'Left'),
(56, 41, 1, 4500, 7, 3500, '250.00', 4500, 'Right', 5, 30, 'GC', '2020-07-12', 'Left');

-- --------------------------------------------------------

--
-- Table structure for table `payout_time`
--

CREATE TABLE `payout_time` (
  `id` int(11) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Active','Inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payout_time`
--

INSERT INTO `payout_time` (`id`, `day`, `created`, `status`) VALUES
(1, 'Monday', '2020-07-12 01:37:08', 'Active'),
(3, 'Tuesday', '2020-07-12 01:37:29', 'Inactive'),
(4, 'Wednesday', '2020-07-12 01:37:29', 'Inactive'),
(5, 'Thursday', '2020-07-12 01:37:29', 'Inactive'),
(6, 'Friday', '2020-07-12 01:37:40', 'Inactive'),
(7, 'Saturday', '2020-07-12 01:37:40', 'Inactive'),
(8, 'Sunday', '2020-07-12 01:37:40', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE `processes` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `direct_referral_income` decimal(13,2) NOT NULL,
  `direct_referral_gc` decimal(13,2) NOT NULL,
  `pairing_account_id` int(11) NOT NULL,
  `pairing_invited_account_id` int(11) NOT NULL,
  `pairing_new_registered_account_id` int(11) NOT NULL,
  `earned` decimal(13,2) NOT NULL,
  `status` enum('Pending','Processed','Ongoing','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `processes`
--

INSERT INTO `processes` (`id`, `account_id`, `direct_referral_income`, `direct_referral_gc`, `pairing_account_id`, `pairing_invited_account_id`, `pairing_new_registered_account_id`, `earned`, `status`, `created`) VALUES
(2, 1, '3500.00', '1000.00', 1, 5, 5, '3500.00', 'Processed', '2020-07-11 21:17:55'),
(3, 1, '3500.00', '1000.00', 1, 6, 6, '3500.00', 'Processed', '2020-07-11 21:20:10'),
(4, 1, '75.00', '125.00', 5, 7, 7, '75.00', 'Processed', '2020-07-11 21:21:27'),
(5, 1, '75.00', '125.00', 5, 8, 8, '75.00', 'Processed', '2020-07-11 21:22:04'),
(6, 1, '1000.00', '500.00', 6, 9, 9, '1000.00', 'Processed', '2020-07-11 21:26:19'),
(7, 9, '75.00', '125.00', 9, 10, 10, '75.00', 'Processed', '2020-07-11 22:04:27'),
(8, 9, '75.00', '125.00', 9, 11, 11, '75.00', 'Processed', '2020-07-11 22:13:03'),
(9, 11, '1000.00', '500.00', 11, 12, 12, '1000.00', 'Processed', '2020-07-11 22:15:34'),
(10, 11, '1000.00', '500.00', 11, 13, 13, '1000.00', 'Processed', '2020-07-11 22:18:23'),
(11, 1, '3500.00', '1000.00', 5, 7, 7, '3500.00', 'Processed', '2020-07-11 22:21:20');

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `earnings` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `referrals`
--

INSERT INTO `referrals` (`id`, `package_id`, `earnings`, `product`, `created`) VALUES
(1, 2, 75, 125, '2020-07-11 19:27:55'),
(2, 3, 500, 250, '2020-07-11 19:28:07'),
(3, 4, 1000, 500, '2020-07-11 19:34:02'),
(4, 5, 3500, 1000, '2020-07-11 19:34:09');

-- --------------------------------------------------------

--
-- Table structure for table `remittance`
--

CREATE TABLE `remittance` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `receivers_name` varchar(200) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `remittance_center` enum('Palawan Pawnshop','Cebuana Lhuillier','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `remittance`
--

INSERT INTO `remittance` (`id`, `account_id`, `receivers_name`, `contact_number`, `remittance_center`, `created`) VALUES
(1, 1, 'MONTESORES, MEI DEL SANTOS', '090995715101', 'Palawan Pawnshop', '2020-07-11 19:54:11'),
(2, 1, 'MONTESORES, MEI', '09102626101', 'Cebuana Lhuillier', '2020-07-11 19:54:28');

-- --------------------------------------------------------

--
-- Table structure for table `system_users`
--

CREATE TABLE `system_users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `roles` enum('Administrator','Accounting','','') NOT NULL,
  `token` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Active','Inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `system_users`
--

INSERT INTO `system_users` (`id`, `username`, `password`, `lastname`, `firstname`, `middlename`, `roles`, `token`, `created`, `last_login`, `status`) VALUES
(1, 'admin', '$2y$10$gnGfMqdY59oI5QJRnlsyD.Fz5mwDbvUj9l/lhiPjyUhttdS05HAK2', 'CARTAJENA', 'PAUL JAMES', 'MICULOB', 'Administrator', '5ef3747334a6da74485bb458', '2020-07-07 03:19:22', '2020-07-11 22:12:51', 'Active'),
(2, 'accounting', '$2y$10$2rRfx.S8gfIP0vvWLaY5vurFLJ9lqDWsY2sAneyTFTl95byZN8XsO', 'CARTAJENA', 'PAUL JAMES', 'MICULOB', 'Accounting', '769587f0bcad8d7137197577', '2020-07-07 03:19:22', '2020-07-07 00:41:25', 'Active'),
(5, 'admin', '$2y$10$A7hbxZiz6fWtS/jQxvQXbeJgoIAzsiWGMWSICfmYW/xRP5we.7.Si', 'MONDRAGON', 'CECILION', '', 'Accounting', '', '2020-07-11 19:16:34', '2020-07-11 19:16:34', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_fee`
--

CREATE TABLE `transaction_fee` (
  `id` int(11) NOT NULL,
  `fee` int(11) NOT NULL,
  `status` enum('Active','Inactive','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction_fee`
--

INSERT INTO `transaction_fee` (`id`, `fee`, `status`, `created`, `updated`) VALUES
(1, 10, 'Active', '2020-07-11 22:26:54', '2020-07-11 22:26:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(300) NOT NULL,
  `token` varchar(200) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `account_id`, `username`, `email`, `password`, `token`, `last_login`, `created`) VALUES
(1, 1, 'admin', 'mei.mondragon@gmail.com', '$2y$10$Yb1wNFpIy/BtkZVe93qm3eGP8hs9OsFlniM1ZPbbOoRFQj10o6Y86', '', '2020-07-11 20:58:26', '2020-07-11 19:46:07'),
(5, 5, '2', '', '$2y$10$y12NrraA99pbCcyVETYFye2jlT3jCPwr/Y74Tj9zjnGMJt1NdRrUG', '', '2020-07-11 22:30:35', '2020-07-11 21:17:55'),
(6, 6, '3', '', '$2y$10$wnf1ETinf.y4rpvpKfffDeo6SCqnOSuGXe8oqjHKsjNjVNjYnIjpC', '', '2020-07-11 21:20:10', '2020-07-11 21:20:10'),
(7, 7, '4', '', '$2y$10$d3YbjD/N4OyL4oV2OzOBiuEYYiMAK2iurvmGIOy4WV43nK34JLm92', 'd5675b4ae2723ae5a2212c05', '2020-07-11 22:20:01', '2020-07-11 21:21:27'),
(8, 8, '5', '', '$2y$10$VCNObyXwM7evByqgSMBQeOnTbnl9stQ0tTTMshLpl09VbDxE2oPdO', '', '2020-07-11 21:22:04', '2020-07-11 21:22:04'),
(9, 9, '6', '', '$2y$10$YekUNNHwzQTpS/B0vyoaJOTt.s4iumMo3lupjzVd7y5GXQKmtuKw.', '', '2020-07-11 22:03:17', '2020-07-11 21:26:19'),
(10, 10, '7', '', '$2y$10$YIp283yyQeRcxBsWDGSGpeEvqGxKliCabHl5iCu/bk9KJAjFh4udS', '', '2020-07-11 22:04:27', '2020-07-11 22:04:27'),
(11, 11, '8', '', '$2y$10$MyHc63M1zaBbo5/.px4nx.oDPyzOpyf.n38rnAco6NP8OptKMaENS', 'c723d91f4160fa5c7673561b', '2020-07-11 22:14:42', '2020-07-11 22:13:03'),
(12, 12, '9', '', '$2y$10$epA1vyMM3H2uKDMMHl5DJOGMVZ3UHmLXVfMnTKeBwjBPVVcDyimcC', '', '2020-07-11 22:15:34', '2020-07-11 22:15:34'),
(13, 13, '10', '', '$2y$10$VROOZNw0ae6XzjJb6a3mneqMGedBWj9WFy6m5ZZFKg4UKXkyJ2eMO', '', '2020-07-11 22:18:23', '2020-07-11 22:18:23');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `account_id`, `balance`, `worth_of_product`, `created`, `updated`) VALUES
(1, 1, '0.00', '5250.00', '2020-07-12', '2020-07-11 22:21:36'),
(5, 5, '250.00', '0.00', '2020-07-12', '2020-07-11 21:22:50'),
(6, 6, '0.00', '0.00', '2020-07-12', '2020-07-11 21:20:10'),
(7, 7, '0.00', '0.00', '2020-07-12', '2020-07-11 21:21:27'),
(8, 8, '0.00', '0.00', '2020-07-12', '2020-07-11 21:22:04'),
(9, 9, '400.00', '250.00', '2020-07-12', '2020-07-11 22:13:41'),
(10, 10, '0.00', '0.00', '2020-07-12', '2020-07-11 22:04:27'),
(11, 11, '2750.00', '1000.00', '2020-07-12', '2020-07-11 22:18:27'),
(12, 12, '0.00', '0.00', '2020-07-12', '2020-07-11 22:15:34'),
(13, 13, '0.00', '0.00', '2020-07-12', '2020-07-11 22:18:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activation_code` (`activation_code_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `upline_id` (`upline_id`),
  ADD KEY `upline_id_2` (`upline_id`);

--
-- Indexes for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `cashout`
--
ALTER TABLE `cashout`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `confirm_id` (`confirm_id`);

--
-- Indexes for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `downline_id` (`downline_id`),
  ADD KEY `indirect_bonuses_id` (`indirect_bonuses_id`);

--
-- Indexes for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pair`
--
ALTER TABLE `pair`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id_4` (`invited_account_id`);

--
-- Indexes for table `payout_time`
--
ALTER TABLE `payout_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processes`
--
ALTER TABLE `processes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `remittance`
--
ALTER TABLE `remittance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `system_users`
--
ALTER TABLE `system_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `activation_codes`
--
ALTER TABLE `activation_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cashout`
--
ALTER TABLE `cashout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ewallet`
--
ALTER TABLE `ewallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `indirect_level`
--
ALTER TABLE `indirect_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pair`
--
ALTER TABLE `pair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `payout_time`
--
ALTER TABLE `payout_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `processes`
--
ALTER TABLE `processes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `remittance`
--
ALTER TABLE `remittance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_users`
--
ALTER TABLE `system_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`),
  ADD CONSTRAINT `accounts_ibfk_2` FOREIGN KEY (`activation_code_id`) REFERENCES `activation_codes` (`id`);

--
-- Constraints for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD CONSTRAINT `activation_codes_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cashout`
--
ALTER TABLE `cashout`
  ADD CONSTRAINT `cashout_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD CONSTRAINT `daily_transaction_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD CONSTRAINT `direct_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD CONSTRAINT `direct_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD CONSTRAINT `direct_referral_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_referral_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD CONSTRAINT `ewallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD CONSTRAINT `indirect_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD CONSTRAINT `indirect_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_2` FOREIGN KEY (`downline_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_3` FOREIGN KEY (`indirect_bonuses_id`) REFERENCES `indirect_bonuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD CONSTRAINT `indirect_level_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD CONSTRAINT `pairing_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD CONSTRAINT `pairing_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pairing_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `referrals`
--
ALTER TABLE `referrals`
  ADD CONSTRAINT `referrals_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `remittance`
--
ALTER TABLE `remittance`
  ADD CONSTRAINT `remittance_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
