-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2020 at 06:57 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `build`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `gender` enum('Male','Female','','') NOT NULL,
  `address` varchar(300) NOT NULL,
  `date_of_birth` date NOT NULL,
  `place_of_birth` varchar(200) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code_id` int(11) NOT NULL,
  `upline_id` int(11) NOT NULL,
  `position` enum('None','Left','Right') NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `activation_codes`
--

CREATE TABLE `activation_codes` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code` text NOT NULL,
  `type` enum('Paid','Unpaid','','') NOT NULL,
  `status` enum('Unused','Used','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `token` varchar(200) NOT NULL,
  `role` enum('Administrator','Accounting','','') NOT NULL,
  `remark` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `bank_name` enum('BDO','FCB') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cashout`
--

CREATE TABLE `cashout` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `fee` int(11) NOT NULL,
  `receivable_amount` decimal(13,2) NOT NULL,
  `encashment_type` enum('Bank','Remittance','EWallet') NOT NULL,
  `encashment_center` enum('BDO','FCB','Palawan Pawnshop','Cebuana Lhuillier','GCash','') NOT NULL,
  `status` enum('Pending','Confirmed','','') NOT NULL,
  `remark` text NOT NULL,
  `confirm_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_transaction`
--

CREATE TABLE `daily_transaction` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `transaction_type` enum('DIRECT REFERRAL','PAIRING BONUS','INDIRECT BONUS','DIRECT BONUS','') NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonuses`
--

CREATE TABLE `direct_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `percentage` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonus_income`
--

CREATE TABLE `direct_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `direct_referral_income`
--

CREATE TABLE `direct_referral_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ewallet`
--

CREATE TABLE `ewallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `ewallet_number` varchar(200) NOT NULL,
  `ewallet_center` enum('GCash') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonuses`
--

CREATE TABLE `indirect_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonus_income`
--

CREATE TABLE `indirect_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `downline_id` int(11) NOT NULL,
  `indirect_bonuses_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indirect_level`
--

CREATE TABLE `indirect_level` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `message` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pair`
--

CREATE TABLE `pair` (
  `id` int(11) NOT NULL,
  `multiplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonuses`
--

CREATE TABLE `pairing_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `pair_per_day` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonus_income`
--

CREATE TABLE `pairing_bonus_income` (
  `id` int(11) NOT NULL,
  `pairing_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `income` decimal(11,2) NOT NULL,
  `remaining_vp` int(11) NOT NULL,
  `heavy_side` enum('None','Left','Right','') NOT NULL,
  `paired` int(11) NOT NULL,
  `paired_this_day` int(11) NOT NULL,
  `remark` enum('Income','GC','FlushOut','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `positioned` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payout_time`
--

CREATE TABLE `payout_time` (
  `id` int(11) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Active','Inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payout_time`
--

INSERT INTO `payout_time` (`id`, `day`, `created`, `status`) VALUES
(1, 'Monday', '2020-07-12 01:37:08', 'Active'),
(3, 'Tuesday', '2020-07-12 01:37:29', 'Inactive'),
(4, 'Wednesday', '2020-07-12 01:37:29', 'Inactive'),
(5, 'Thursday', '2020-07-12 01:37:29', 'Inactive'),
(6, 'Friday', '2020-07-12 01:37:40', 'Inactive'),
(7, 'Saturday', '2020-07-12 01:37:40', 'Inactive'),
(8, 'Sunday', '2020-07-12 01:37:40', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE `processes` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `direct_referral_income` decimal(13,2) NOT NULL,
  `direct_referral_gc` decimal(13,2) NOT NULL,
  `pairing_account_id` int(11) NOT NULL,
  `pairing_invited_account_id` int(11) NOT NULL,
  `pairing_new_registered_account_id` int(11) NOT NULL,
  `earned` decimal(13,2) NOT NULL,
  `status` enum('Pending','Processed','Ongoing','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `earnings` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `remittance`
--

CREATE TABLE `remittance` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `receivers_name` varchar(200) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `remittance_center` enum('Palawan Pawnshop','Cebuana Lhuillier','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `system_users`
--

CREATE TABLE `system_users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `roles` enum('Administrator','Accounting','','') NOT NULL,
  `token` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Active','Inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `system_users`
--

INSERT INTO `system_users` (`id`, `username`, `password`, `lastname`, `firstname`, `middlename`, `roles`, `token`, `created`, `last_login`, `status`) VALUES
(1, 'admin', '$2y$10$gnGfMqdY59oI5QJRnlsyD.Fz5mwDbvUj9l/lhiPjyUhttdS05HAK2', 'CARTAJENA', 'PAUL JAMES', 'MICULOB', 'Administrator', '337e169514581fe9d4ed8124', '2020-07-07 03:19:22', '2020-07-15 20:08:50', 'Active'),
(2, 'accounting', '$2y$10$2rRfx.S8gfIP0vvWLaY5vurFLJ9lqDWsY2sAneyTFTl95byZN8XsO', 'CARTAJENA', 'PAUL JAMES', 'MICULOB', 'Accounting', '769587f0bcad8d7137197577', '2020-07-07 03:19:22', '2020-07-07 00:41:25', 'Active'),
(5, 'admin', '$2y$10$A7hbxZiz6fWtS/jQxvQXbeJgoIAzsiWGMWSICfmYW/xRP5we.7.Si', 'MONDRAGON', 'CECILION', '', 'Accounting', '', '2020-07-11 19:16:34', '2020-07-11 19:16:34', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_fee`
--

CREATE TABLE `transaction_fee` (
  `id` int(11) NOT NULL,
  `fee` decimal(13,2) NOT NULL,
  `status` enum('Active','Inactive','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(300) NOT NULL,
  `token` varchar(200) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `created` date NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activation_code` (`activation_code_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `upline_id` (`upline_id`),
  ADD KEY `upline_id_2` (`upline_id`);

--
-- Indexes for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `cashout`
--
ALTER TABLE `cashout`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `confirm_id` (`confirm_id`);

--
-- Indexes for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `downline_id` (`downline_id`),
  ADD KEY `indirect_bonuses_id` (`indirect_bonuses_id`);

--
-- Indexes for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pair`
--
ALTER TABLE `pair`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id_4` (`invited_account_id`);

--
-- Indexes for table `payout_time`
--
ALTER TABLE `payout_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processes`
--
ALTER TABLE `processes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `package_id_2` (`package_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `remittance`
--
ALTER TABLE `remittance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `system_users`
--
ALTER TABLE `system_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `activation_codes`
--
ALTER TABLE `activation_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cashout`
--
ALTER TABLE `cashout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ewallet`
--
ALTER TABLE `ewallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indirect_level`
--
ALTER TABLE `indirect_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pair`
--
ALTER TABLE `pair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payout_time`
--
ALTER TABLE `payout_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `processes`
--
ALTER TABLE `processes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `remittance`
--
ALTER TABLE `remittance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_users`
--
ALTER TABLE `system_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`),
  ADD CONSTRAINT `accounts_ibfk_2` FOREIGN KEY (`activation_code_id`) REFERENCES `activation_codes` (`id`);

--
-- Constraints for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD CONSTRAINT `activation_codes_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cashout`
--
ALTER TABLE `cashout`
  ADD CONSTRAINT `cashout_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD CONSTRAINT `daily_transaction_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD CONSTRAINT `direct_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD CONSTRAINT `direct_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD CONSTRAINT `direct_referral_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_referral_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD CONSTRAINT `ewallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD CONSTRAINT `indirect_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD CONSTRAINT `indirect_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_2` FOREIGN KEY (`downline_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_3` FOREIGN KEY (`indirect_bonuses_id`) REFERENCES `indirect_bonuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD CONSTRAINT `indirect_level_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD CONSTRAINT `pairing_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD CONSTRAINT `pairing_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pairing_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `referrals`
--
ALTER TABLE `referrals`
  ADD CONSTRAINT `referrals_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `remittance`
--
ALTER TABLE `remittance`
  ADD CONSTRAINT `remittance_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
