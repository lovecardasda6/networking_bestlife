-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2020 at 03:08 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `build`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code_id` int(11) NOT NULL,
  `upline_id` int(11) NOT NULL,
  `referral_side` enum('None','Left','Right','') NOT NULL,
  `position` enum('None','Left','Right') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `lastname`, `firstname`, `middlename`, `contact`, `reference_id`, `package_id`, `activation_code_id`, `upline_id`, `referral_side`, `position`, `created`) VALUES
(1, 'CARTAJENA', 'PAUL JAMES', 'MICULOB', '09999999999', 0, 8, 140, 0, 'None', 'None', '0000-00-00'),
(416, 'RAGNAROK', 'ROY', 'CAHANAP', '09099575101', 1, 8, 141, 1, 'Left', 'Left', '2020-07-03'),
(417, 'LAGURA', 'TIFFANY PEARL', 'CAHANAP', '09123456789', 1, 8, 142, 1, 'Right', 'Right', '2020-07-03'),
(418, 'CAHANAP', 'ESTRELLA', 'CAHANAP', '09315620101', 1, 8, 142, 416, 'Left', 'Left', '2020-07-03'),
(419, 'MONDRAGON', 'CARD', 'MONTE', '09123456789', 1, 8, 142, 417, 'Right', 'Left', '2020-07-03'),
(420, 'LAGURA', 'SELENA PEARL', 'CAHANAP', '09315620101', 1, 8, 142, 416, 'Left', 'Right', '2020-07-03'),
(421, 'LAGURA', 'RIKO', 'CAHANAP', '09123456789', 1, 8, 142, 417, 'Right', 'Right', '2020-07-03');

-- --------------------------------------------------------

--
-- Table structure for table `activation_codes`
--

CREATE TABLE `activation_codes` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code` text NOT NULL,
  `type` enum('Paid','Unpaid','','') NOT NULL,
  `status` enum('Unused','Used','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activation_codes`
--

INSERT INTO `activation_codes` (`id`, `package_id`, `activation_code`, `type`, `status`, `created`) VALUES
(48, 10, '20694d4e1acc075082c1da4b', 'Paid', 'Used', '2020-06-21 00:10:53'),
(130, 13, 'c5594be988a2b41bde9e85bf', 'Paid', 'Used', '2020-06-28 09:58:55'),
(131, 13, 'f3033259f1ac0df58a77d7eb', 'Paid', 'Used', '2020-06-28 10:20:25'),
(132, 13, '4e9a9626884217225b272162', 'Paid', 'Used', '2020-06-28 10:20:25'),
(133, 13, '8cdd0750fb632b6d038662bf', 'Paid', 'Used', '2020-06-28 10:20:25'),
(134, 13, '6307a0e52d7d5e91c0297557', 'Paid', 'Used', '2020-06-29 02:45:21'),
(135, 13, '595b626acc0c735dc4559266', 'Paid', 'Used', '2020-06-29 02:45:21'),
(136, 13, 'e7c00885c85189bf2c89d61c', 'Paid', 'Used', '2020-06-29 02:45:21'),
(137, 13, '1df851c78b83cd801a576007', 'Paid', 'Used', '2020-06-29 02:45:24'),
(138, 13, '934beaf0a1b0daa2e2d3d8b5', 'Paid', 'Unused', '2020-06-29 02:45:24'),
(139, 13, '2f0c4f996174fd1bc93c431a', 'Paid', 'Unused', '2020-06-29 02:45:24'),
(140, 8, '2af55d7a9ddd55c5532576a4', 'Paid', 'Used', '2020-06-29 02:45:31'),
(141, 8, 'b76c0762e344588d730d3403', 'Paid', 'Unused', '2020-06-29 02:45:31'),
(142, 8, '80405be3e15e8885a971c867', 'Paid', 'Unused', '2020-06-29 02:45:31'),
(143, 9, '43a4ab8d492a43e0df735c44', 'Paid', 'Used', '2020-06-29 02:45:35'),
(144, 9, 'e45074632fef207ddda0357a', 'Paid', 'Used', '2020-06-29 02:45:35'),
(145, 9, '169a4537479624b333b9f52b', 'Paid', 'Unused', '2020-06-29 02:45:35'),
(146, 10, '7215ff025bac02c34ae9c854', 'Paid', 'Used', '2020-06-29 02:45:38'),
(147, 10, 'a53abca9067c5b29b0e9c45f', 'Paid', 'Unused', '2020-06-29 02:45:38'),
(148, 10, '9414333b086c8cb0d6022c9f', 'Paid', 'Unused', '2020-06-29 02:45:38'),
(149, 13, '756916770a7d966d9fe9ff2c', 'Paid', 'Unused', '2020-07-02 12:53:48'),
(150, 13, 'dbdb909e56ce3a17ade70124', 'Paid', 'Unused', '2020-07-03 00:23:45'),
(151, 13, '588880465be47b72a58a2ac1', 'Paid', 'Unused', '2020-07-03 00:23:45'),
(152, 13, '062e71723b881ffe35bae549', 'Paid', 'Unused', '2020-07-03 00:23:45');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(25) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_transaction`
--

CREATE TABLE `daily_transaction` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `transaction_type` enum('DIRECT REFERRAL','PAIRING BONUS','INDIRECT BONUS','DIRECT BONUS','') NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daily_transaction`
--

INSERT INTO `daily_transaction` (`id`, `account_id`, `transaction_type`, `transaction_id`, `created`) VALUES
(204, 1, 'DIRECT REFERRAL', 151, '2020-07-02 16:00:00'),
(205, 1, 'DIRECT REFERRAL', 152, '2020-07-02 16:00:00'),
(206, 1, 'PAIRING BONUS', 141257, '2020-07-02 16:00:00'),
(207, 1, 'PAIRING BONUS', 141258, '2020-07-02 16:00:00'),
(208, 1, 'DIRECT REFERRAL', 153, '2020-07-02 16:00:00'),
(209, 1, 'DIRECT REFERRAL', 154, '2020-07-02 16:00:00'),
(210, 1, 'PAIRING BONUS', 141260, '2020-07-02 16:00:00'),
(211, 1, 'PAIRING BONUS', 141261, '2020-07-02 16:00:00'),
(212, 1, 'DIRECT REFERRAL', 155, '2020-07-02 16:00:00'),
(213, 1, 'DIRECT REFERRAL', 156, '2020-07-02 16:00:00'),
(214, 1, 'PAIRING BONUS', 141263, '2020-07-02 16:00:00'),
(215, 1, 'PAIRING BONUS', 141264, '2020-07-02 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonuses`
--

CREATE TABLE `direct_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_bonuses`
--

INSERT INTO `direct_bonuses` (`id`, `package_id`, `percentage`, `created`) VALUES
(12, 9, 15, '2020-06-26 08:14:58'),
(13, 10, 15, '2020-06-26 08:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonus_income`
--

CREATE TABLE `direct_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `direct_referral_income`
--

CREATE TABLE `direct_referral_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_referral_income`
--

INSERT INTO `direct_referral_income` (`id`, `account_id`, `invited_account_id`, `amount`, `worth_of_product`, `created`) VALUES
(151, 1, 416, '500.00', '250.00', '2020-07-03'),
(152, 1, 417, '500.00', '250.00', '2020-07-03'),
(153, 1, 418, '500.00', '250.00', '2020-07-03'),
(154, 1, 419, '500.00', '250.00', '2020-07-03'),
(155, 1, 420, '500.00', '250.00', '2020-07-03'),
(156, 1, 421, '500.00', '250.00', '2020-07-03');

-- --------------------------------------------------------

--
-- Table structure for table `ewallet`
--

CREATE TABLE `ewallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `ewallet_number` varchar(200) NOT NULL,
  `ewallet_center` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonuses`
--

CREATE TABLE `indirect_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_bonuses`
--

INSERT INTO `indirect_bonuses` (`id`, `package_id`, `level`, `percent`, `created`) VALUES
(54, 9, 1, '10.00', '2020-07-01 20:39:08'),
(55, 9, 2, '7.50', '2020-07-01 20:39:08'),
(56, 9, 3, '5.00', '2020-07-01 20:39:08'),
(57, 9, 4, '2.50', '2020-07-01 20:39:08'),
(58, 9, 5, '1.00', '2020-07-01 20:39:08'),
(59, 9, 6, '1.00', '2020-07-01 20:39:08'),
(60, 9, 7, '1.00', '2020-07-01 20:39:08'),
(61, 9, 8, '1.00', '2020-07-01 20:39:08'),
(62, 9, 9, '1.00', '2020-07-01 20:39:08'),
(63, 9, 10, '1.00', '2020-07-01 20:39:08'),
(64, 10, 1, '10.00', '2020-07-01 20:39:12'),
(65, 10, 2, '7.50', '2020-07-01 20:39:12'),
(66, 10, 3, '5.00', '2020-07-01 20:39:12'),
(67, 10, 4, '2.50', '2020-07-01 20:39:12'),
(68, 10, 5, '1.00', '2020-07-01 20:39:12'),
(69, 10, 6, '1.00', '2020-07-01 20:39:12'),
(70, 10, 7, '1.00', '2020-07-01 20:39:12'),
(71, 10, 8, '1.00', '2020-07-01 20:39:12'),
(72, 10, 9, '1.00', '2020-07-01 20:39:12'),
(73, 10, 10, '1.00', '2020-07-01 20:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonus_income`
--

CREATE TABLE `indirect_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `downline_id` int(11) NOT NULL,
  `indirect_bonuses_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indirect_level`
--

CREATE TABLE `indirect_level` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_level`
--

INSERT INTO `indirect_level` (`id`, `package_id`, `level`, `created`) VALUES
(10, 9, 10, '2020-07-02 02:39:08'),
(11, 10, 10, '2020-07-02 02:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `message` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package`, `price`, `created`) VALUES
(8, 'Silver', 1000, '2020-06-20 11:27:06'),
(9, 'Gold', 4500, '2020-06-20 11:27:11'),
(10, 'Platinum', 12500, '2020-06-20 11:27:18'),
(13, 'Bronze', 750, '2020-06-20 11:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `pair`
--

CREATE TABLE `pair` (
  `id` int(11) NOT NULL,
  `multiplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pair`
--

INSERT INTO `pair` (`id`, `multiplier`) VALUES
(1, 250);

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonuses`
--

CREATE TABLE `pairing_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pairing_bonuses`
--

INSERT INTO `pairing_bonuses` (`id`, `package_id`, `value_points`, `created`) VALUES
(1, 13, 250, '2020-06-21 09:57:29'),
(2, 8, 500, '2020-06-21 09:57:29'),
(5, 9, 1500, '2020-06-21 10:00:27'),
(7, 10, 3500, '2020-06-21 10:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonus_income`
--

CREATE TABLE `pairing_bonus_income` (
  `id` int(11) NOT NULL,
  `pairing_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `income` decimal(11,2) NOT NULL,
  `remaining_vp` int(11) NOT NULL,
  `heavy_side` enum('None','Left','Right','') NOT NULL,
  `paired` int(11) NOT NULL,
  `remark` enum('Income','GC','FlushOut','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pairing_bonus_income`
--

INSERT INTO `pairing_bonus_income` (`id`, `pairing_id`, `account_id`, `balance`, `invited_account_id`, `value_points`, `income`, `remaining_vp`, `heavy_side`, `paired`, `remark`, `created`) VALUES
(141256, 0, 1, 0, 416, 500, '0.00', 500, 'Left', 0, 'Income', '2020-07-03'),
(141257, 141256, 1, 500, 417, 500, '250.00', 0, 'Right', 1, 'Income', '2020-07-03'),
(141258, 141256, 1, 500, 417, 500, '250.00', 0, 'Right', 2, 'Income', '2020-07-03'),
(141259, 141258, 1, 0, 418, 500, '0.00', 500, 'Left', 2, 'Income', '2020-07-03'),
(141260, 141259, 1, 500, 419, 500, '250.00', 0, 'Right', 3, 'Income', '2020-07-03'),
(141261, 141259, 1, 500, 419, 500, '250.00', 0, 'Right', 4, 'Income', '2020-07-03'),
(141262, 141261, 1, 0, 420, 500, '0.00', 500, 'Left', 4, 'Income', '2020-07-03'),
(141263, 141262, 1, 500, 421, 500, '250.00', 0, 'Right', 5, 'GC', '2020-07-03'),
(141264, 141262, 1, 500, 421, 500, '250.00', 0, 'Right', 0, 'FlushOut', '2020-07-03');

-- --------------------------------------------------------

--
-- Table structure for table `payout_time`
--

CREATE TABLE `payout_time` (
  `id` int(11) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Active','Inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payout_time`
--

INSERT INTO `payout_time` (`id`, `day`, `created`, `status`) VALUES
(1, 'Monday', '2020-06-21 23:56:48', 'Active'),
(2, 'Tuesday', '2020-06-21 23:56:48', 'Inactive'),
(3, 'Wednesday', '2020-06-21 23:56:48', 'Inactive'),
(4, 'Thursday', '2020-06-21 23:56:48', 'Inactive'),
(5, 'Friday', '2020-06-21 23:56:48', 'Inactive'),
(6, 'Saturday', '2020-06-21 23:56:48', 'Inactive'),
(7, 'Sunday', '2020-06-21 23:56:48', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE `processes` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `direct_referral_income` decimal(13,2) NOT NULL,
  `direct_referral_gc` decimal(13,2) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `new_registered_account_id` int(11) NOT NULL,
  `earned` decimal(13,2) NOT NULL,
  `status` enum('Pending','Processed','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `processes`
--

INSERT INTO `processes` (`id`, `account_id`, `direct_referral_income`, `direct_referral_gc`, `invited_account_id`, `new_registered_account_id`, `earned`, `status`, `created`) VALUES
(3, 1, '500.00', '250.00', 416, 416, '500.00', 'Processed', '2020-07-02 18:24:32'),
(4, 1, '500.00', '250.00', 417, 417, '500.00', 'Processed', '2020-07-02 18:38:32'),
(5, 1, '500.00', '250.00', 418, 418, '500.00', 'Processed', '2020-07-02 18:41:06'),
(6, 1, '500.00', '250.00', 419, 419, '500.00', 'Processed', '2020-07-02 18:45:53'),
(7, 1, '500.00', '250.00', 420, 420, '500.00', 'Processed', '2020-07-02 18:49:42'),
(8, 1, '500.00', '250.00', 421, 421, '500.00', 'Processed', '2020-07-02 18:50:21');

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `earnings` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `referrals`
--

INSERT INTO `referrals` (`id`, `package_id`, `earnings`, `product`, `created`) VALUES
(2, 8, 500, 250, '2020-06-20 13:42:11'),
(5, 9, 1000, 500, '2020-06-20 13:55:15'),
(6, 10, 3500, 1000, '2020-06-20 13:55:27'),
(7, 13, 70, 125, '2020-06-21 09:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `remmittance`
--

CREATE TABLE `remmittance` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `receivers_name` varchar(200) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `remmittance_center` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `account_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `account_id`, `created`) VALUES
(378, 'ragnarok@gmail.com', '123', 416, '2020-07-02 18:24:32'),
(379, 'tiffany.pearl@gmail.com', '123', 417, '2020-07-02 18:38:32'),
(380, 'estrella@gmail.com', '123', 418, '2020-07-02 18:41:06'),
(381, 'monte@gmail.com', '123', 419, '2020-07-02 18:45:53'),
(382, 'selenapearl.lagura@gmail.com', '123', 420, '2020-07-02 18:49:42'),
(383, 'riko@gmail.com', '123', 421, '2020-07-02 18:50:21');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `accumulated_income` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `account_id`, `balance`, `worth_of_product`, `accumulated_income`, `created`, `updated`) VALUES
(121, 1, '4000.00', '1750.00', '4000.00', '2020-07-02', '2020-07-02 18:50:31'),
(171, 416, '0.00', '0.00', '0.00', '2020-07-03', '2020-07-02 18:24:32'),
(172, 417, '0.00', '0.00', '0.00', '2020-07-03', '2020-07-02 18:38:32'),
(173, 418, '0.00', '0.00', '0.00', '2020-07-03', '2020-07-02 18:41:06'),
(174, 419, '0.00', '0.00', '0.00', '2020-07-03', '2020-07-02 18:45:53'),
(175, 420, '0.00', '0.00', '0.00', '2020-07-03', '2020-07-02 18:49:42'),
(176, 421, '0.00', '0.00', '0.00', '2020-07-03', '2020-07-02 18:50:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activation_code` (`activation_code_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `upline_id` (`upline_id`),
  ADD KEY `upline_id_2` (`upline_id`);

--
-- Indexes for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `downline_id` (`downline_id`),
  ADD KEY `indirect_bonuses_id` (`indirect_bonuses_id`);

--
-- Indexes for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pair`
--
ALTER TABLE `pair`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id_4` (`invited_account_id`);

--
-- Indexes for table `payout_time`
--
ALTER TABLE `payout_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processes`
--
ALTER TABLE `processes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `remmittance`
--
ALTER TABLE `remmittance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=422;

--
-- AUTO_INCREMENT for table `activation_codes`
--
ALTER TABLE `activation_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `ewallet`
--
ALTER TABLE `ewallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `indirect_level`
--
ALTER TABLE `indirect_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pair`
--
ALTER TABLE `pair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141265;

--
-- AUTO_INCREMENT for table `payout_time`
--
ALTER TABLE `payout_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `processes`
--
ALTER TABLE `processes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `remmittance`
--
ALTER TABLE `remmittance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=384;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`),
  ADD CONSTRAINT `accounts_ibfk_2` FOREIGN KEY (`activation_code_id`) REFERENCES `activation_codes` (`id`);

--
-- Constraints for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD CONSTRAINT `activation_codes_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD CONSTRAINT `daily_transaction_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD CONSTRAINT `direct_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD CONSTRAINT `direct_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD CONSTRAINT `direct_referral_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_referral_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD CONSTRAINT `ewallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD CONSTRAINT `indirect_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD CONSTRAINT `indirect_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_2` FOREIGN KEY (`downline_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_3` FOREIGN KEY (`indirect_bonuses_id`) REFERENCES `indirect_bonuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD CONSTRAINT `indirect_level_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD CONSTRAINT `pairing_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD CONSTRAINT `pairing_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pairing_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `referrals`
--
ALTER TABLE `referrals`
  ADD CONSTRAINT `referrals_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `remmittance`
--
ALTER TABLE `remmittance`
  ADD CONSTRAINT `remmittance_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
