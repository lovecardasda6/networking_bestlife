-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2020 at 03:39 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `build`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `reference_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code_id` int(11) NOT NULL,
  `upline_id` int(11) NOT NULL,
  `referral_side` enum('None','Left','Right','') NOT NULL,
  `position` enum('None','Left','Right') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `lastname`, `firstname`, `middlename`, `contact`, `reference_id`, `package_id`, `activation_code_id`, `upline_id`, `referral_side`, `position`, `created`) VALUES
(1, 'CARTAJENA', 'PAUL JAMES', 'MICULOB', '09999999999', 0, 13, 181, 0, 'None', 'None', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `activation_codes`
--

CREATE TABLE `activation_codes` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activation_code` text NOT NULL,
  `type` enum('Paid','Unpaid','','') NOT NULL,
  `status` enum('Unused','Used','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activation_codes`
--

INSERT INTO `activation_codes` (`id`, `package_id`, `activation_code`, `type`, `status`, `created`) VALUES
(181, 13, '5986DC905E400AC6', 'Paid', 'Used', '2020-07-04 04:03:37'),
(217, 13, 'ACA36838AA4A8D6C', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(218, 13, 'E0516575C47A80A3', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(219, 13, '0EF3549D85C9E081', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(220, 13, '6E5A0105C169BEBD', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(221, 13, 'D48DD4CB399EB4CB', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(222, 13, '86520350E714C79A', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(223, 13, '0E5D2EEA82AC349D', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(224, 13, '552B5C275E613DB5', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(225, 13, '8E776C82CA2E7C6F', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(226, 13, '71793CBE0CFB16D8', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(227, 13, '05704363A267A50B', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(228, 13, '1E8D083812D89FE6', 'Paid', 'Unused', '2020-07-05 09:54:54'),
(229, 10, '8CD021846FA901A0', 'Paid', 'Unused', '2020-07-05 11:05:58'),
(230, 10, '84A83C1EA22DB622', 'Paid', 'Unused', '2020-07-05 11:05:58'),
(231, 13, 'E3BAA4D1D09D9E4A', 'Paid', 'Unused', '2020-07-06 11:07:12'),
(232, 13, '86B96883B42358C6', 'Paid', 'Unused', '2020-07-06 11:07:12'),
(233, 10, 'B5E819B6BADCDEC1', 'Paid', 'Unused', '2020-07-06 11:07:16'),
(234, 10, '3204CD0B5578FC08', 'Paid', 'Unused', '2020-07-06 11:07:16'),
(235, 10, '90500107FD8E8352', 'Paid', 'Unused', '2020-07-06 11:24:33'),
(236, 10, 'CE1C7B22906C4945', 'Paid', 'Unused', '2020-07-06 11:24:33'),
(237, 9, '685E6531F60722BE', 'Paid', 'Unused', '2020-07-07 11:25:47'),
(238, 9, 'E95785D5EBDFC52B', 'Paid', 'Unused', '2020-07-07 11:25:47'),
(239, 10, '29B400D0166B8345', 'Paid', 'Unused', '2020-07-07 11:25:50');

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `token` varchar(200) NOT NULL,
  `role` enum('Administrator','Accounting','','') NOT NULL,
  `remark` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(25) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `account_id`, `account_name`, `account_number`, `bank_name`, `created`) VALUES
(1, 1, 'PAUL JAMES M. CARTAJENA', '0033004301324032', 'BDO', '2020-07-03 03:03:06');

-- --------------------------------------------------------

--
-- Table structure for table `cashout`
--

CREATE TABLE `cashout` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `fee` int(11) NOT NULL,
  `receivable_amount` decimal(13,2) NOT NULL,
  `encashment_type` enum('Bank','Remmittance','EWallet') NOT NULL,
  `encashment_center` enum('BDO','FCB','Palawan Pawnshop','Cebuana Lhuillier','GCash','') NOT NULL,
  `status` enum('Pending','Confirmed','','') NOT NULL,
  `remark` text NOT NULL,
  `confirm_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cashout`
--

INSERT INTO `cashout` (`id`, `account_id`, `amount`, `fee`, `receivable_amount`, `encashment_type`, `encashment_center`, `status`, `remark`, `confirm_id`, `created`, `updated`) VALUES
(3, 1, '1000.00', 10, '900.00', 'Remmittance', 'Palawan Pawnshop', 'Pending', 'CONFIRMED', 1, '2020-07-03 07:22:05', '2020-07-03 01:22:37');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `config_name` varchar(100) NOT NULL,
  `config_value` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_transaction`
--

CREATE TABLE `daily_transaction` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `transaction_type` enum('DIRECT REFERRAL','PAIRING BONUS','INDIRECT BONUS','DIRECT BONUS','') NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daily_transaction`
--

INSERT INTO `daily_transaction` (`id`, `account_id`, `transaction_type`, `transaction_id`, `created`) VALUES
(318, 1, 'DIRECT REFERRAL', 259, '2020-07-04 16:00:00'),
(319, 1, 'DIRECT REFERRAL', 260, '2020-07-04 16:00:00'),
(320, 1, 'DIRECT REFERRAL', 261, '2020-07-04 16:00:00'),
(321, 1, 'DIRECT REFERRAL', 262, '2020-07-04 16:00:00'),
(322, 1, 'DIRECT REFERRAL', 263, '2020-07-04 16:00:00'),
(323, 1, 'DIRECT REFERRAL', 264, '2020-07-04 16:00:00'),
(324, 1, 'DIRECT REFERRAL', 265, '2020-07-04 16:00:00'),
(325, 1, 'DIRECT REFERRAL', 266, '2020-07-04 16:00:00'),
(326, 1, 'DIRECT REFERRAL', 267, '2020-07-04 16:00:00'),
(327, 1, 'DIRECT REFERRAL', 268, '2020-07-04 16:00:00'),
(328, 1, 'DIRECT REFERRAL', 269, '2020-07-04 16:00:00'),
(329, 1, 'DIRECT REFERRAL', 270, '2020-07-04 16:00:00'),
(330, 1, 'DIRECT REFERRAL', 271, '2020-07-04 16:00:00'),
(331, 1, 'DIRECT REFERRAL', 272, '2020-07-04 16:00:00'),
(332, 1, 'DIRECT REFERRAL', 273, '2020-07-04 16:00:00'),
(333, 1, 'DIRECT REFERRAL', 274, '2020-07-04 16:00:00'),
(334, 1, 'DIRECT REFERRAL', 275, '2020-07-04 16:00:00'),
(335, 1, 'DIRECT REFERRAL', 276, '2020-07-04 16:00:00'),
(336, 1, 'DIRECT REFERRAL', 277, '2020-07-04 16:00:00'),
(337, 1, 'DIRECT REFERRAL', 278, '2020-07-04 16:00:00'),
(338, 1, 'DIRECT REFERRAL', 279, '2020-07-04 16:00:00'),
(339, 1, 'DIRECT REFERRAL', 280, '2020-07-04 16:00:00'),
(340, 1, 'DIRECT REFERRAL', 281, '2020-07-04 16:00:00'),
(341, 1, 'DIRECT REFERRAL', 282, '2020-07-04 16:00:00'),
(342, 1, 'DIRECT REFERRAL', 283, '2020-07-04 16:00:00'),
(343, 1, 'DIRECT REFERRAL', 284, '2020-07-04 16:00:00'),
(344, 1, 'DIRECT REFERRAL', 285, '2020-07-04 16:00:00'),
(345, 1, 'DIRECT REFERRAL', 286, '2020-07-04 16:00:00'),
(346, 1, 'DIRECT REFERRAL', 287, '2020-07-04 16:00:00'),
(347, 1, 'DIRECT REFERRAL', 288, '2020-07-04 16:00:00'),
(348, 1, 'DIRECT REFERRAL', 289, '2020-07-04 16:00:00'),
(349, 1, 'DIRECT REFERRAL', 290, '2020-07-04 16:00:00'),
(350, 1, 'DIRECT REFERRAL', 291, '2020-07-04 16:00:00'),
(351, 1, 'DIRECT REFERRAL', 292, '2020-07-05 16:00:00'),
(352, 1, 'DIRECT REFERRAL', 293, '2020-07-05 16:00:00'),
(353, 1, 'DIRECT REFERRAL', 294, '2020-07-05 16:00:00'),
(354, 1, 'DIRECT REFERRAL', 295, '2020-07-05 16:00:00'),
(355, 1, 'DIRECT REFERRAL', 296, '2020-07-05 16:00:00'),
(356, 1, 'DIRECT REFERRAL', 297, '2020-07-05 16:00:00'),
(357, 1, 'DIRECT REFERRAL', 298, '2020-07-05 16:00:00'),
(358, 1, 'DIRECT REFERRAL', 299, '2020-07-05 16:00:00'),
(359, 1, 'DIRECT REFERRAL', 300, '2020-07-05 16:00:00'),
(360, 1, 'DIRECT REFERRAL', 301, '2020-07-05 16:00:00'),
(361, 1, 'DIRECT REFERRAL', 302, '2020-07-05 16:00:00'),
(362, 1, 'DIRECT REFERRAL', 303, '2020-07-05 16:00:00'),
(363, 1, 'DIRECT REFERRAL', 304, '2020-07-06 16:00:00'),
(364, 1, 'DIRECT REFERRAL', 305, '2020-07-06 16:00:00'),
(365, 1, 'DIRECT REFERRAL', 306, '2020-07-03 16:00:00'),
(366, 1, 'DIRECT REFERRAL', 307, '2020-07-03 16:00:00'),
(367, 1, 'DIRECT REFERRAL', 308, '2020-07-03 16:00:00'),
(368, 1, 'DIRECT REFERRAL', 309, '2020-07-03 16:00:00'),
(369, 1, 'DIRECT REFERRAL', 310, '2020-07-03 16:00:00'),
(370, 1, 'DIRECT REFERRAL', 311, '2020-07-03 16:00:00'),
(371, 1, 'DIRECT REFERRAL', 312, '2020-07-03 16:00:00'),
(372, 1, 'DIRECT REFERRAL', 313, '2020-07-03 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonuses`
--

CREATE TABLE `direct_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `percentage` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_bonuses`
--

INSERT INTO `direct_bonuses` (`id`, `package_id`, `percentage`, `created`) VALUES
(13, 10, 15, '2020-06-26 08:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `direct_bonus_income`
--

CREATE TABLE `direct_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `direct_referral_income`
--

CREATE TABLE `direct_referral_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ewallet`
--

CREATE TABLE `ewallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `ewallet_number` varchar(200) NOT NULL,
  `ewallet_center` enum('GCash','','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ewallet`
--

INSERT INTO `ewallet` (`id`, `account_id`, `ewallet_number`, `ewallet_center`, `created`) VALUES
(1, 1, '09099575101', 'GCash', '2020-07-03 05:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonuses`
--

CREATE TABLE `indirect_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_bonuses`
--

INSERT INTO `indirect_bonuses` (`id`, `package_id`, `level`, `percent`, `created`) VALUES
(54, 9, 1, '10.00', '2020-07-01 20:39:08'),
(55, 9, 2, '7.50', '2020-07-01 20:39:08'),
(56, 9, 3, '5.00', '2020-07-01 20:39:08'),
(57, 9, 4, '2.50', '2020-07-01 20:39:08'),
(58, 9, 5, '1.00', '2020-07-01 20:39:08'),
(59, 9, 6, '1.00', '2020-07-01 20:39:08'),
(60, 9, 7, '1.00', '2020-07-01 20:39:08'),
(61, 9, 8, '1.00', '2020-07-01 20:39:08'),
(62, 9, 9, '1.00', '2020-07-01 20:39:08'),
(63, 9, 10, '1.00', '2020-07-01 20:39:08'),
(64, 10, 1, '10.00', '2020-07-01 20:39:12'),
(65, 10, 2, '7.50', '2020-07-01 20:39:12'),
(66, 10, 3, '5.00', '2020-07-01 20:39:12'),
(67, 10, 4, '2.50', '2020-07-01 20:39:12'),
(68, 10, 5, '1.00', '2020-07-01 20:39:12'),
(69, 10, 6, '1.00', '2020-07-01 20:39:12'),
(70, 10, 7, '1.00', '2020-07-01 20:39:12'),
(71, 10, 8, '1.00', '2020-07-01 20:39:12'),
(72, 10, 9, '1.00', '2020-07-01 20:39:12'),
(73, 10, 10, '1.00', '2020-07-01 20:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `indirect_bonus_income`
--

CREATE TABLE `indirect_bonus_income` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `downline_id` int(11) NOT NULL,
  `indirect_bonuses_id` int(11) NOT NULL,
  `amount` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `indirect_level`
--

CREATE TABLE `indirect_level` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `indirect_level`
--

INSERT INTO `indirect_level` (`id`, `package_id`, `level`, `created`) VALUES
(10, 9, 10, '2020-07-02 02:39:08'),
(11, 10, 10, '2020-07-02 02:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `message` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package`, `price`, `created`) VALUES
(8, 'Silver', 1000, '2020-06-20 11:27:06'),
(9, 'Gold', 4500, '2020-06-20 11:27:11'),
(10, 'Platinum', 12500, '2020-06-20 11:27:18'),
(13, 'Bronze', 750, '2020-06-20 11:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `pair`
--

CREATE TABLE `pair` (
  `id` int(11) NOT NULL,
  `multiplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pair`
--

INSERT INTO `pair` (`id`, `multiplier`) VALUES
(1, 250);

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonuses`
--

CREATE TABLE `pairing_bonuses` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `pair_per_day` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pairing_bonuses`
--

INSERT INTO `pairing_bonuses` (`id`, `package_id`, `value_points`, `pair_per_day`, `created`) VALUES
(1, 13, 250, 3, '2020-06-21 09:57:29'),
(2, 8, 500, 6, '2020-06-21 09:57:29'),
(5, 9, 1500, 18, '2020-06-21 10:00:27'),
(7, 10, 3500, 50, '2020-06-21 10:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `pairing_bonus_income`
--

CREATE TABLE `pairing_bonus_income` (
  `id` int(11) NOT NULL,
  `pairing_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `value_points` int(11) NOT NULL,
  `income` decimal(11,2) NOT NULL,
  `remaining_vp` int(11) NOT NULL,
  `heavy_side` enum('None','Left','Right','') NOT NULL,
  `paired` int(11) NOT NULL,
  `paired_this_day` int(11) NOT NULL,
  `remark` enum('Income','GC','FlushOut','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payout_time`
--

CREATE TABLE `payout_time` (
  `id` int(11) NOT NULL,
  `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('Active','Inactive','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payout_time`
--

INSERT INTO `payout_time` (`id`, `day`, `created`, `status`) VALUES
(1, 'Monday', '2020-06-21 23:56:48', 'Active'),
(2, 'Tuesday', '2020-06-21 23:56:48', 'Inactive'),
(3, 'Wednesday', '2020-06-21 23:56:48', 'Inactive'),
(4, 'Thursday', '2020-06-21 23:56:48', 'Inactive'),
(5, 'Friday', '2020-06-21 23:56:48', 'Inactive'),
(6, 'Saturday', '2020-06-21 23:56:48', 'Inactive'),
(7, 'Sunday', '2020-06-21 23:56:48', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE `processes` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `direct_referral_income` decimal(13,2) NOT NULL,
  `direct_referral_gc` decimal(13,2) NOT NULL,
  `invited_account_id` int(11) NOT NULL,
  `new_registered_account_id` int(11) NOT NULL,
  `earned` decimal(13,2) NOT NULL,
  `status` enum('Pending','Processed','Ongoing','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `processes`
--

INSERT INTO `processes` (`id`, `account_id`, `direct_referral_income`, `direct_referral_gc`, `invited_account_id`, `new_registered_account_id`, `earned`, `status`, `created`) VALUES
(11, 1, '70.00', '125.00', 570, 570, '70.00', 'Pending', '2020-07-04 07:08:17'),
(12, 1, '70.00', '125.00', 571, 571, '70.00', 'Pending', '2020-07-04 07:08:37'),
(13, 1, '70.00', '125.00', 572, 572, '70.00', 'Pending', '2020-07-04 07:08:55'),
(14, 1, '70.00', '125.00', 573, 573, '70.00', 'Pending', '2020-07-04 07:09:11'),
(15, 1, '70.00', '125.00', 574, 574, '70.00', 'Pending', '2020-07-04 07:09:28'),
(16, 1, '70.00', '125.00', 575, 575, '70.00', 'Pending', '2020-07-04 07:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `earnings` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `referrals`
--

INSERT INTO `referrals` (`id`, `package_id`, `earnings`, `product`, `created`) VALUES
(2, 8, 500, 250, '2020-06-20 13:42:11'),
(5, 9, 1000, 500, '2020-06-20 13:55:15'),
(6, 10, 3500, 1000, '2020-06-20 13:55:27'),
(7, 13, 70, 125, '2020-06-21 09:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `remmittance`
--

CREATE TABLE `remmittance` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `receivers_name` varchar(200) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `remmittance_center` enum('Palawan Pawnshop','Cebuana Lhuillier','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `remmittance`
--

INSERT INTO `remmittance` (`id`, `account_id`, `receivers_name`, `contact_number`, `remmittance_center`, `created`) VALUES
(1, 1, 'PAUL JAMES M. CARTAJENA', '09099575101', 'Palawan Pawnshop', '2020-07-03 05:15:55');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_fee`
--

CREATE TABLE `transaction_fee` (
  `id` int(11) NOT NULL,
  `fee` int(11) NOT NULL,
  `status` enum('Active','Inactive','','') NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction_fee`
--

INSERT INTO `transaction_fee` (`id`, `fee`, `status`, `created`, `updated`) VALUES
(1, 10, 'Active', '2020-07-03 02:37:36', '2020-07-02 20:55:07'),
(2, 15, 'Inactive', '2020-07-02 20:52:56', '2020-07-02 20:57:52'),
(3, 20, 'Inactive', '2020-07-02 20:58:36', '2020-07-02 20:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(300) NOT NULL,
  `token` varchar(200) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `account_id`, `username`, `email`, `password`, `token`, `last_login`, `created`) VALUES
(459, 1, 'admin', '', '$2y$10$AO72iG3fKhoXtS6mZkF4BezQfbNm3J3.qu2ND4kSoWXOkAjS3nzqu', '992127fb829a8873b4f1aab7', '2020-07-04 07:06:52', '2020-07-04 05:37:29');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE `wallet` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `balance` decimal(13,2) NOT NULL,
  `worth_of_product` decimal(13,2) NOT NULL,
  `accumulated_income` decimal(13,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wallet`
--

INSERT INTO `wallet` (`id`, `account_id`, `balance`, `worth_of_product`, `accumulated_income`, `created`, `updated`) VALUES
(121, 1, '420.00', '750.00', '420.00', '2020-07-02', '2020-07-04 07:09:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activation_code` (`activation_code_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `upline_id` (`upline_id`),
  ADD KEY `upline_id_2` (`upline_id`);

--
-- Indexes for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `cashout`
--
ALTER TABLE `cashout`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `confirm_id` (`confirm_id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id` (`invited_account_id`);

--
-- Indexes for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `downline_id` (`downline_id`),
  ADD KEY `indirect_bonuses_id` (`indirect_bonuses_id`);

--
-- Indexes for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pair`
--
ALTER TABLE `pair`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `invited_account_id_4` (`invited_account_id`);

--
-- Indexes for table `payout_time`
--
ALTER TABLE `payout_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processes`
--
ALTER TABLE `processes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `remmittance`
--
ALTER TABLE `remmittance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=576;

--
-- AUTO_INCREMENT for table `activation_codes`
--
ALTER TABLE `activation_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cashout`
--
ALTER TABLE `cashout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=373;

--
-- AUTO_INCREMENT for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=314;

--
-- AUTO_INCREMENT for table `ewallet`
--
ALTER TABLE `ewallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `indirect_level`
--
ALTER TABLE `indirect_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pair`
--
ALTER TABLE `pair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143692;

--
-- AUTO_INCREMENT for table `payout_time`
--
ALTER TABLE `payout_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `processes`
--
ALTER TABLE `processes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `remmittance`
--
ALTER TABLE `remmittance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaction_fee`
--
ALTER TABLE `transaction_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=541;

--
-- AUTO_INCREMENT for table `wallet`
--
ALTER TABLE `wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=333;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`),
  ADD CONSTRAINT `accounts_ibfk_2` FOREIGN KEY (`activation_code_id`) REFERENCES `activation_codes` (`id`);

--
-- Constraints for table `activation_codes`
--
ALTER TABLE `activation_codes`
  ADD CONSTRAINT `activation_codes_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cashout`
--
ALTER TABLE `cashout`
  ADD CONSTRAINT `cashout_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `daily_transaction`
--
ALTER TABLE `daily_transaction`
  ADD CONSTRAINT `daily_transaction_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_bonuses`
--
ALTER TABLE `direct_bonuses`
  ADD CONSTRAINT `direct_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `direct_bonus_income`
--
ALTER TABLE `direct_bonus_income`
  ADD CONSTRAINT `direct_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `direct_referral_income`
--
ALTER TABLE `direct_referral_income`
  ADD CONSTRAINT `direct_referral_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direct_referral_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD CONSTRAINT `ewallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonuses`
--
ALTER TABLE `indirect_bonuses`
  ADD CONSTRAINT `indirect_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_bonus_income`
--
ALTER TABLE `indirect_bonus_income`
  ADD CONSTRAINT `indirect_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_2` FOREIGN KEY (`downline_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indirect_bonus_income_ibfk_3` FOREIGN KEY (`indirect_bonuses_id`) REFERENCES `indirect_bonuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `indirect_level`
--
ALTER TABLE `indirect_level`
  ADD CONSTRAINT `indirect_level_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pairing_bonuses`
--
ALTER TABLE `pairing_bonuses`
  ADD CONSTRAINT `pairing_bonuses_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `pairing_bonus_income`
--
ALTER TABLE `pairing_bonus_income`
  ADD CONSTRAINT `pairing_bonus_income_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pairing_bonus_income_ibfk_2` FOREIGN KEY (`invited_account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `referrals`
--
ALTER TABLE `referrals`
  ADD CONSTRAINT `referrals_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `remmittance`
--
ALTER TABLE `remmittance`
  ADD CONSTRAINT `remmittance_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
