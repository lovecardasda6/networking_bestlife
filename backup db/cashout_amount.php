<h1 style="font-family:creamCake; font-size: 5em;">Cashout Amount</h1>
<br />


<?php
$wallet = $WalletController->fetch_wallet();
$balance = $wallet['balance'];
$rate = number_format($exchange_rate->exchange_rate_php_to_usd($balance), 2);
?>

<form method="POST" action="?page=cashout">
    <input type="hidden" name="encashment_mode" value="<?= $_GET['encashment_mode'] ?>">
    <input type="hidden" name="encashment_center" value="<?= $_GET['encashment_center'] ?>">
    <div class="row">
        <div class="tiles">
            <br />
            <div style="text-align:center;">
                <label style="font-size:1.5em;">Amount (<?= ($currency == NULL || $currency  == 'PHP') ? "PHP" : "USD" ?>)</label>
                <input type="number" id="cashoutAmount" onkeyup="conversion_rate()" step="any" placeholder="0.00" name="amount" value="<?= $balance ?>" class="cashout" max="<?= $balance ?>" />
                <div style="text-align: center;
                    font-size: 1.5em;" id="conversion">
                    ( &#36; <?= $rate ?> )
                </div>
                <hr>
                <label>Wallet</label>
                <div style="font-size: 2.5em;">
                    PHP <?= $balance ?>
                </div>
                <div style="text-align: center;
                    font-size: 1.5em;">
                    ( &#36; <?= $rate ?> )
                </div>
                <br />
            </div>

            <div style="text-align:right;">
                <a href="?page=cashout_center">
                    <input type="button" class="btn btn-primary" name="bank" value="Cancel" />
                </a> &nbsp;
                <input type="submit" class="btn btn-default" name="confirmAmount" value="Preview" style="float:right;" />
            </div>
        </div>
    </div>
</form>


<style>
    .cashout {
        border: none;
        outline: none;
        width: 100%;
        text-align: center;
        font-size: 5em;
        font-weight: 1px;
    }

    input[type="number"]::-webkit-inner-spin-button,
    input[type="number"]::-webkit-outer-spin-button {
        -moz-appearance: none;
        -webkit-appearance: none;
        appearance: none;
        margin: 0;
    }
</style>

<script>
    function conversion_rate() {
        var amount = document.getElementById('cashoutAmount').value;

        var usd_rate = <?= $exchange_rate->rate_to_usd() ?>;
        var conversion = amount / usd_rate;

        document.getElementById('conversion').innerHTML = "( &#36;" + parseFloat(conversion.toFixed(2)) + " )";
    }
</script>