<?php

$con = connectionString();
switch ($request) {
    case "dashboard":
        // require_once __DIR__ . "/../../accounting/payout.php";
        break;

    case "payout":
        $PayoutController = new PayoutController();
        $payouts = null;
        $search_date = null;

        if (isset($_POST['search_transactions'])) :
            $date = Sanitize::clean($_POST['date']);
            $search_date = $date;

            $payouts = $PayoutController->all_by_date($date);
        endif;

        if(isset($_POST['export_transactions'])):
            $date = Sanitize::clean($_POST['date']);

            $PayoutController->export_transactions($date);
        endif;

        if (isset($_GET['date'])) :
            $date = Sanitize::clean($_GET['date']);
            $date = date_create($date);
            $date = date_format($date, "Y-m-d");
            
            $payouts = $PayoutController->all_by_date($date);
        endif;

        break;
    case "confirm_payout":
        $PayoutController = new PayoutController();

        $cashout_id = isset($_GET['cashout_id']) ? base64_decode($_GET['cashout_id']) : '';
        $account_id = isset($_GET['account_id']) ? base64_decode($_GET['account_id']) : '';

        
        $confirm_payout = new confirm_payout($cashout_id, $account_id);
        $cashout = $PayoutController->fetch($cashout_id, $account_id);


        $error = false;
        if (isset($_POST['confirm_transaction'])) :
            $obj = new \stdClass();
            $obj->cashout_id = Sanitize::clean($cashout_id);
            $obj->account_id = Sanitize::clean($account_id);
            $obj->created_on = Sanitize::clean($_POST['created_on']);
            $obj->remark = Sanitize::clean($_POST['remarks']);

            $response = $PayoutController->confirm($obj);

            if(!$response):
                $error = true;
            endif;
        endif;
        break;

    case "report":
        $report = new reports();
        $date = null;
        $reports = null;
        if (isset($_POST['search'])) :
            $obj = new \stdClass();
            $obj->date = Sanitize::clean($_POST['date']);
            $transaction = $report->fetch_by_transaction($obj->date);
            if ($transaction != null) :
                $reports = $transaction->datas;
                $date = $transaction->date;
            endif;
        endif;


        if (isset($_POST['export_to_excel'])) :
            $date = $_POST['date'];
            $export = $report->export_to_excel($date);
            var_dump($export);
            exit;
        endif;
        break;

    case "reports":
        $report = new reports();
        $reports = $report->reports();

        if (isset($_POST['search_reports'])) {
            $obj = new \stdClass();
            $obj->from = $_POST['from'];
            $obj->to = $_POST['to'];
            $reports = $report->fetch_reports_transaction($obj->from, $obj->to);
        }

        if (isset($_POST['export_to_excel'])) {
            $date = $_POST['date'];
            $export = $report->export_report_to_excels();
            var_dump($export);
            exit;
        }
        break;
    case "view_report":
        $confirm_payout = null;
        $transaction_id = $_GET['id'];
        $account_id = $_GET['account_id'];
        $wallet = new wallet();

        $confirm_payout = new confirm_payout($transaction_id, $account_id);

        $account_details = $confirm_payout->account_informations();
        $payout_details = $confirm_payout->payout_details();
        break;

    case "registration_code":
        if (isset($_POST['generate'])) {
            $package_id = Sanitize::clean($_POST['package']);
            $type = Sanitize::clean($_POST['type']);
            $quantity = Sanitize::clean($_POST['quantity']);

            $registration_code = new registrationCodes();
            $registration_code->generate_registration_codes($package_id, $type, $quantity);
        }
        break;

    case "unpaid_registration_codes":
        if (isset($_POST['generate'])) {
            $package_id = Sanitize::clean($_POST['package']);
            $type = Sanitize::clean($_POST['type']);
            $quantity = Sanitize::clean($_POST['quantity']);

            $registration_code = new registrationCodes();
            $registration_code->generate_registration_codes($package_id, $type, $quantity);
        }
        break;

    case "transaction_fee":
        $transaction_fee = new transaction_fee();
        if (isset($_POST['add'])) {
            $fee = trim($_POST['fee']);
            $response = $transaction_fee->save_transaction_fee($fee);
            if ($response) {
                header("location: ?page=transaction_fee&success=1");
            }
        }

        if (isset($_POST['update'])) {
            $transaction_fee_id = $_POST['id'];
            $fee = $_POST['fee'];
            $status = $_POST['status'];
            $response = $transaction_fee->update_transaction_fee($transaction_fee_id, $fee, $status);
            if ($response) {
                header("location: ?page=transaction_fee&success=2");
            }
        }

        if (isset($_GET['active_transaction']) && $_GET['active_transaction'] == "true") {
            $id = $_GET['id'];
            $response = $transaction_fee->active_transaction_fee($id);
            if ($response) {
                header("location: ?page=transaction_fee&success=3");
            }
        }
        break;

    case "json":
        // $payout = new payout();
        // $date = file_get_contents('php://input');
        // if (isset($date)) :
        //     $datas = $payout->transaction_search($date);
        //     echo json_encode($datas);
        // endif;
        // echo json_decode($json);
        // exit;
        break;
    default:
        header("location: ?page=payout");
        break;
}
