<?php
$AccountCurrency = AccountCurrency::fetch($acc_id);
$ExchangeRate = ExchangeRate::fetch_php_to_usd();
$php_to_usd_rate = $ExchangeRate['exchange_rate'];

$currency = $AccountCurrency ? $AccountCurrency['currency'] : NULL;

switch ($request):
    case "home":
        $AnnouncementController = new AnnouncementController();
        break;
    case "wallet":
        $income = new WalletController($acc_id);
        $WalletController = new WalletController($acc_id);
        break;
    case "account":
        $AccountController = new AccountController();
        $error = false;
        $message = null;

        if (isset($_POST['update_basic_information'])) {
            $obj = new \stdClass();

            $obj->account_id = Sanitize::clean($acc_id);
            $obj->contact = Sanitize::clean($_POST['contact']);
            $obj->gender = Sanitize::clean(Sanitize::clean($_POST['gender']));
            $obj->address = strtoupper(Sanitize::clean($_POST['address']));
            $obj->date_of_birth = Sanitize::clean($_POST['date_of_birth']);
            $obj->place_of_birth = strtoupper(Sanitize::clean($_POST['place_of_birth']));
            $obj->currency = Sanitize::clean($_POST['currency']);

            $response = $AccountController->update_basic_information($obj);

            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }

        if (isset($_POST['submit_email'])) {
            $obj = new \stdClass();
            $obj->user_id = Sanitize::clean($_POST['user_id']);
            $obj->account_id = Sanitize::clean($acc_id);
            $obj->email = Sanitize::clean($_POST['email']);

            $response = $AccountController->update_user_email($obj);

            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }

        if (isset($_POST['submit_password'])) {
            $obj = new \stdClass();
            $obj->user_id = Sanitize::clean($_POST['user_id']);
            $obj->account_id = Sanitize::clean($acc_id);
            $obj->current = Sanitize::clean($_POST['current']);
            $obj->password = Sanitize::clean($_POST['password']);
            $obj->confirm = Sanitize::clean($_POST['confirm']);
            $csrf_token = $_POST['csrf_token'];

            if (!CsrfToken::validate_csrf_token($csrf_token)) :
                header("Location: ?page=account&token=0");
                exit;
            endif;

            $user = User::fetch($obj->user_id, $obj->account_id);
            $hash_password = $user['password'];
            $validate = password_verify($obj->current, $hash_password);

            if ($validate) :
                if ($obj->password == $obj->confirm) :
                    if (strlen($obj->password) >= 8) :
                        $response = $AccountController->change_password($obj);
                        if ($response) :
                            $error = true;
                            $message = $response;
                        else :
                            $error = true;
                            $message = $response;
                        endif;
                    else :
                        $error = true;
                        $message = "Password must be at least consist of eight(8) characters. Please try again.";
                    endif;
                else :
                    $error = true;
                    $message = "Mismatch password. Please try again.";
                endif;
            else :
                $error = true;
                $message = "Invalid current password. Please try again.";
            endif;
        }

        if (isset($_POST['add_bank_center'])) {
            $obj = new \stdClass();

            $obj->account_id = Sanitize::clean($acc_id);
            $obj->encashment_center = Sanitize::clean($_POST['encashment_center']);
            $obj->name = Sanitize::clean($_POST['name']);
            $obj->number = Sanitize::clean($_POST['number']);

            $response = $AccountController->store_bank($obj);

            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }

        if (isset($_POST['update_bank_center'])) {
            $obj = new \stdClass();

            $obj->account_id = Sanitize::clean($acc_id);
            $obj->bank_id = Sanitize::clean($_POST['bank_id']);
            $obj->name = Sanitize::clean($_POST['name']);
            $obj->number = Sanitize::clean($_POST['number']);

            $response = $AccountController->update_bank($obj);

            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }

        if (isset($_POST['add_remittance_center'])) {
            $obj = new \stdClass();

            $obj->account_id = Sanitize::clean($acc_id);
            $obj->encashment_center = Sanitize::clean($_POST['encashment_center']);
            $obj->name = Sanitize::clean($_POST['name']);
            $obj->number = Sanitize::clean($_POST['number']);

            $response = $AccountController->store_remittance($obj);

            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }

        if (isset($_POST['update_remittance_center'])) {
            $obj = new \stdClass();

            $obj->account_id = Sanitize::clean($acc_id);
            $obj->remittance_id = Sanitize::clean($_POST['remittance_id']);
            $obj->name = Sanitize::clean($_POST['name']);
            $obj->number = Sanitize::clean($_POST['number']);

            $response = $AccountController->update_remittance($obj);

            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }

        if (isset($_POST['add_ewallet_center'])) {
            $obj = new \stdClass();

            $obj->account_id = Sanitize::clean($acc_id);
            $obj->encashment_center = Sanitize::clean($_POST['encashment_center']);
            $obj->number = Sanitize::clean($_POST['number']);


            $response = $AccountController->store_ewallet($obj);

            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }

        if (isset($_POST['update_ewallet_center'])) {
            $obj = new \stdClass();

            $obj->account_id = Sanitize::clean($acc_id);
            $obj->ewallet_id = Sanitize::clean($_POST['ewallet_id']);
            $obj->number = Sanitize::clean($_POST['number']);

            $response = $AccountController->update_ewallet($obj);

            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }

        if (isset($_POST['upload_document'])) {
            $obj = new \stdClass();
            $obj->account_id = Sanitize::clean($acc_id);
            $obj->document_type = Sanitize::clean($_POST['document_type']);
            $obj->file_image = $_FILES['file_image']['name'];
            $obj->temp_image = $_FILES['file_image']['tmp_name'];

            $response = $AccountController->upload_document($obj);

            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }


        $Account = Account::fetch_account($acc_id);
        $Referral = Account::fetch($Account['reference_id']);

        $EncashmentModeBank = Encashment::fetch_by_encashment_mode("Bank");
        $EncashmentModeRemittance = Encashment::fetch_by_encashment_mode("Remittance");
        $EncashmentModeEWallet = Encashment::fetch_by_encashment_mode("EWallet");


        break;

    case "cashout_center":
        $CashoutController = new CashoutController($acc_id);
        $banks = $CashoutController->fetch_banks();
        $remittances = $CashoutController->fetch_remittance();
        $ewallets = $CashoutController->fetch_ewallet();


        if (isset($_POST['continue_transaction'])) {
            $encashment_mode = base64_encode(Sanitize::clean($_POST['transaction']));
            $encashment_center = base64_encode(Sanitize::clean($_POST['center']));

            header("Location: ?page=cashout_amount&encashment_mode={$encashment_mode}&encashment_center={$encashment_center}&token={$csrf_token}");
            exit;
        }
        break;

    case "cashout_amount":
        $CashoutController = new CashoutController($acc_id);
        $WalletController = new WalletController($acc_id);

        if (!isset($_GET['encashment_mode']) || !isset($_GET['encashment_center']) || !isset($_GET['token'])) :
            header("Location: ?page=wallet");
            exit;
        else :
            $obj = new \stdClass();
            $obj->encashment_mode = $_GET['encashment_mode'];
            $obj->encashment_center = $_GET['encashment_center'];

            $CashoutController->validate_encashment_mode($obj);
        endif;

        $wallet = $WalletController->fetch_wallet();
        $balance = $wallet['balance'];

        break;

    case "cashout":
        $CashoutController = new CashoutController($acc_id);

        $account_id = $acc_id;
        $transaction = null;
        $center = null;
        $amount = null;
        if (isset($_POST['confirmAmount'])) :
            $transaction =  base64_decode(Sanitize::clean($_POST['encashment_mode']));
            $center =  base64_decode(Sanitize::clean($_POST['encashment_center']));
            $amount =  trim(Sanitize::clean($_POST['amount']));
            $amount = str_replace(',', '', $amount);
            $amount = floatval($amount);

            if ($currency == NULL || $currency == 'PHP') :
                if ($amount < 100) :
                    header("location: ?page=cashout_center&cashout=3");
                    exit;
                else :
                    $CashoutController->validate_amount($amount);
                endif;
            else :
                $amount = $amount * $php_to_usd_rate;
                if ($amount < 100) :
                    header("location: ?page=cashout_center&cashout=3");
                    exit;
                else :
                    $CashoutController->validate_amount($amount);
                endif;
            endif;
        endif;


        if (isset($_POST['confirm_transaction'])) :
            $obj = new \stdClass();
            $obj->encashment_mode =  Sanitize::clean($_POST['transaction']);
            $obj->encashment_center =  Sanitize::clean($_POST['center']);
            $obj->amount =  Sanitize::clean($_POST['amount']);
            $csrf_token = $_POST['csrf_token'];

            if (!CsrfToken::validate_csrf_token($csrf_token)) :
                header("Location: ?page=cashout_center&cashout=2");
                exit;
            endif;

            $response = $CashoutController->cashout($obj);
        endif;

        if ($transaction == null || $center == null || $amount == null) :
            header("Location: ?page=wallet");
            exit;
        endif;

        $transaction_fee = new transaction_fee();
        $fee = $transaction_fee->transaction_fee();
        break;

    case "transactions":
        $TransactionController = new TransactionController();
        $transactions = $TransactionController->transactions($acc_id);
        break;

    case "account_registration":
        $AccountRegistrationController = new AccountRegistrationController();
        $logs = false;
        $error = false;
        $success = false;
        $message = null;

        if (!isset($_GET['ref']) || !isset($_GET['upline']) || !isset($_GET['position']) || $_GET['ref'] == null || $_GET['upline'] == null || $_GET['position'] == null) :
            header('location: ?page=binarytree');
            exit;
        else :
            $obj = new \stdClass();
            $obj->reference_id = Sanitize::clean($_GET['ref']);
            $obj->upline_id = Sanitize::clean($_GET['upline']);
            $obj->position = Sanitize::clean($_GET['position']);

            $res = $AccountRegistrationController->validation($obj);
            if ($res) {
                header('location: ?page=binarytree&invalid=1');
                exit;
            }
        endif;

        if (isset($_POST['register_account'])) :
            $csrf_token = $_POST['csrf_token'];

            if (!CsrfToken::validate_csrf_token($csrf_token)) :
                header("Location: ?page=binarytree&token=0");
                exit;
            endif;

            $obj = new \stdClass();
            $obj->lastname = strtoupper(Sanitize::clean($_POST['lastname']));
            $obj->firstname = strtoupper(Sanitize::clean($_POST['firstname']));
            $obj->middlename = strtoupper(Sanitize::clean($_POST['middlename']));
            $obj->contact_no = Sanitize::clean($_POST['contact_no']);
            $obj->gender = Sanitize::clean($_POST['gender']);
            $obj->address = strtoupper(Sanitize::clean($_POST['address']));
            $obj->date_of_birth = Sanitize::clean($_POST['date_of_birth']);
            $obj->place_of_birth = strtoupper(Sanitize::clean($_POST['place_of_birth']));

            $obj->reference_id = Sanitize::clean($_POST['reference_id']);
            $obj->upline_id = Sanitize::clean($_POST['upline_id']);
            $obj->position = Sanitize::clean($_POST['position']);

            $registration_code = Sanitize::clean($_POST['registration_code']);
            $registration_code_str_replace = str_replace('-', NULL, $registration_code);
            $obj->registration_code = trim($registration_code_str_replace);


            $obj->username = Sanitize::clean($_POST['username']);
            $obj->email_address = Sanitize::clean($_POST['email_address']);
            $obj->password = Sanitize::clean($_POST['password']);
            $obj->confirm_password = Sanitize::clean($_POST['confirm_password']);



            if (strlen($obj->password) >= 8) :
                if ($obj->password == $obj->confirm_password) :
                    $response = $AccountRegistrationController->store_accounts($obj);
                    if ($response) :
                        $message = $response;
                    endif;
                else :
                    $message = "Password do not match. Please retype the password.";
                endif;
            else :
                $message = "Password must be at least 8 character long. Please try again.";
            endif;

            $error = true;
            $logs = $obj;
        endif;
        break;

    case "upgrade":
        $upgradeAccountController = new UpgradeAccountController();
        $error = false;
        $message = null;
        $logs = null;

        if (isset($_POST['upgrade'])) :

            $account_id = Sanitize::clean($acc_id);
            $registration_code = Sanitize::clean($_POST['registration_code']);
            $registration_code_str_replace = str_replace('-', '', $registration_code);
            $registration_code = trim($registration_code_str_replace);
            $csrf_token = $_POST['csrf_token'];

            if (!CsrfToken::validate_csrf_token($csrf_token)) :
                header("Location: ?page=upgrade&token=0");
                exit;
            endif;

            $response = $upgradeAccountController->upgrade_account($account_id, $registration_code);
            if ($response) :
                $message = $response;
            endif;

            $error = true;
            $logs = $_POST['registration_code'];
        endif;

        break;

endswitch;
