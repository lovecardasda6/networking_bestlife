<?php
$con = connectionString();
$date = date('Y-m-d H:i:s');
$pairing_bonus = new pairing_bonus();
$system_user = new system_user();

switch ($request):
    case "package":
        $PackageController = new PackageController();
        $error = false;
        $message = null;

        if (isset($_POST['submit'])) {
            $obj = new \stdClass();
            $obj->package = ucfirst(Sanitize::clean($_POST['package']));
            $obj->price = Sanitize::clean($_POST['price']);

            $response = $PackageController->store_package($obj);
            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }

        if (isset($_POST['update'])) {
                                          $obj = new \stdClass();
            $obj->package_id = Sanitize::clean($_POST['package_id']);
            $obj->package = ucfirst(Sanitize::clean($_POST['package']));
            $obj->price = Sanitize::clean($_POST['price']);

            $response = $PackageController->update_package($obj);
            if ($response) :
                $message = $response;
                $error = true;
            endif;
        }
        break;

    case "referral_bonus":
        $error = false;
        $message = null;
        $DirectReferralBonusController = new DirectReferralBonusController();
        if (isset($_POST['submit'])) {
            $obj = new \stdClass();
            $obj->package_id = Sanitize::clean($_POST['package']);
            $obj->amount = Sanitize::clean($_POST['earnings']);
            $obj->product = Sanitize::clean($_POST['product']);

            $response = $DirectReferralBonusController->store_direct_referral($obj);
            if ($response) {
                $message = $response;
            }

            $error = true;
        }

        if (isset($_POST['update'])) {
            $obj = new \stdClass();
            $obj->direct_referral_bonus_id = Sanitize::clean($_POST['id']);
            $obj->package_id = Sanitize::clean($_POST['package']);
            $obj->amount = Sanitize::clean($_POST['earnings']);
            $obj->product = Sanitize::clean($_POST['product']);

            $response = $DirectReferralBonusController->update_direct_referral($obj);
            if ($response) {
                $message = $response;
            }

            $error = true;
        }
        break;

    case "pairing_bonus":
        $PairingBonusController = new PairingBonusController();
        $error = false;
        $message = null;

        if (isset($_POST['submit'])) {
            $obj = new \stdClass();
            $obj->package_id = Sanitize::clean($_POST['package_id']);
            $obj->value_points = Sanitize::clean($_POST['value_points']);
            $obj->pair_per_day = Sanitize::clean($_POST['pair_per_day']);

            $response = $PairingBonusController->store_pairing($obj);
            if ($response) :
                $message = $response;
            endif;

            $error = true;
        }

        if (isset($_POST['update'])) {
            $obj = new \stdClass();
            $obj->pairing_id = Sanitize::clean($_POST['pairing_id']);
            $obj->package_id = Sanitize::clean($_POST['package_id']);
            $obj->value_points = Sanitize::clean($_POST['value_points']);
            $obj->pair_per_day = Sanitize::clean($_POST['pair_per_day']);

            $response = $PairingBonusController->update_pairing($obj);
            if ($response) :
                $message = $response;
            endif;

            $error = true;
        }

        if (isset($_GET['pairing_id']) && $_GET['action'] === "active") {
            $obj = new \stdClass();
            $obj->pairing_id = Sanitize::clean($_GET['pairing_id']);
            $obj->package_id = Sanitize::clean($_GET['package_id']);


            $response = $PairingBonusController->activate_value_points($obj);
            if ($response) :
                $message = $response;
            endif;

            $error = true;
        }

        break;

    case "indirect_bonus":
        if (isset($_POST['submit'])) {
            $package = $_POST['package'];
            $earnings = $_POST['earnings'];

            $query = "INSERT INTO direct_bonuses VALUES (NULL, '" . $package . "', '" . $earnings . "', NULL)";
            $res = mysqli_query($con, $query);
            if ($res) {
                header("location: ?page=indirect_bonus&add=1");
            }
        }

        if (isset($_POST['update'])) {
            $id = $_POST['id'];
            $package = $_POST['package'];
            $level = $_POST['level'];
            $percent = $_POST['percent'];

            $query = "UPDATE indirect_bonuses SET `percent` = '" . $percent . "' WHERE `id` = '" . $id . "'";

            $res = mysqli_query($con, $query);
            if ($res) {
                header("location: ?page=indirect_bonus&update=1&package={$package}");
            }
        }

        if (isset($_GET['id']) && $_GET['action'] === "remove") {
            $query = "DELETE FROM direct_bonuses WHERE id = '" . $_GET['id'] . "'";
            $res = mysqli_query($con, $query);
            if ($res) {
                header("location: ?page=indirect_bonus&remove=1&package={$package}");
            }
        }
        break;

    case "indirect_level":
        if (isset($_POST['submit'])) {
            $package = $_POST['package'];
            $level = $_POST['level'];
            $query = "INSERT INTO indirect_level VALUES (NULL, '{$package}', '$level', '{$date}')";
            $res = mysqli_query($con, $query);
            if ($res) {
                for ($i = 1; $i <= $level; $i++) {
                    $query = "INSERT INTO indirect_bonuses VALUES(NULL, '{$package}', '{$i}', '15', '{$date}')";
                    $con->query($query);
                }
                header("location: ?page=indirect_level&add=1&package={$package}");
            }
        }

        if (isset($_POST['update'])) {
            $id = $_POST['id'];
            $package_id = $_POST['package'];
            $level = $_POST['level'];
            $query = "UPDATE indirect_level SET `package_id` = '$package_id', `level`= '{$level}' WHERE `id` = '{$id}'";
            $res = mysqli_query($con, $query);
            if ($res) {
                $query = "UPDATE indirect_bonuses SET package_id = '{$package_id}' WHERE package_id = '{$id}'";
                $res = $con->query($query);
                header("location: ?page=indirect_level&update=1");
            }
        }

        if (isset($_GET['action']) && $_GET['action'] == "remove") :
            $pairing_level_id = Sanitize::clean($_GET['id']);
            $pairing_level = new pairing_level();
            $response = $pairing_level->remove_pairing_level($pairing_level_id);

            if ($response) {
                header("location: ?page=indirect_level&remove=1");
            }

        endif;
        break;

    case "direct_bonus":
        if (isset($_POST['submit'])) {
            $package = $_POST['package'];
            $percentage = $_POST['percentage'];
            $query = "INSERT INTO direct_bonuses VALUES (NULL, '" . $package . "', '" . $percentage . "', '" . date('Y-m-d H:i:s') . "')";
            $res = mysqli_query($con, $query);
            if ($res) {
                header("location: ?page=direct_bonus&add=1");
            }
        }

        if (isset($_POST['update'])) {
            $id = $_POST['id'];
            $package = $_POST['package'];
            $percentage = $_POST['percentage'];
            $query = "UPDATE direct_bonuses SET `package_id` = '" . $package . "', `percentage`= '" . $percentage . "' WHERE `id` = '" . $id . "'";
            $res = mysqli_query($con, $query);
            if ($res) {
                header("location: ?page=direct_bonus&update=1");
            }
        }

        if (isset($_GET['id']) && $_GET['action'] === "remove") {
            $query = "DELETE FROM direct_bonuses WHERE id = '" . $_GET['id'] . "'";
            $res = mysqli_query($con, $query);
            if ($res) {
                header("location: ?page=direct_bonus&remove=1");
            }
        }

        break;

    case "transaction_fee":
        $transaction_fee = new transaction_fee();
        if (isset($_POST['add'])) {
            $fee = trim($_POST['fee']);
            $response = $transaction_fee->save_transaction_fee($fee);
            if ($response) {
                header("location: ?page=transaction_fee&success=1");
            }
        }

        if (isset($_POST['update'])) {
            $transaction_fee_id = $_POST['id'];
            $fee = $_POST['fee'];
            $status = $_POST['status'];
            $response = $transaction_fee->update_transaction_fee($transaction_fee_id, $fee, $status);
            if ($response) {
                header("location: ?page=transaction_fee&success=2");
            }
        }

        if (isset($_GET['active_transaction']) && $_GET['active_transaction'] == "true") {
            $id = $_GET['id'];
            $response = $transaction_fee->active_transaction_fee($id);
            if ($response) {
                header("location: ?page=transaction_fee&success=3");
            }
        }
        break;

    case "registration_code":
        if (isset($_POST['generate'])) {
            $obj = new \stdClass();
            $obj->package_id = Sanitize::clean($_POST['package']);
            $obj->type = Sanitize::clean($_POST['type']);
            $obj->quantity = Sanitize::clean($_POST['quantity']);

            $RegistrationCodeController = new RegistrationCodeController();
            $RegistrationCodeController->store($obj);
        }
        break;

    case "unpaid_registration_codes":
        if (isset($_POST['generate'])) {
            $obj = new \stdClass();
            $obj->package_id = Sanitize::clean($_POST['package']);
            $obj->type = Sanitize::clean($_POST['type']);
            $obj->quantity = Sanitize::clean($_POST['quantity']);

            $RegistrationCodeController = new RegistrationCodeController();
            $RegistrationCodeController->store($obj);
        }
        break;

    case "payout_time":
        if (isset($_GET['action']) && $_GET['action'] === "activate") {
            $id = $_GET['id'];
            $query = "UPDATE `payout_time` SET `status` = 'Inactive'";
            $res = mysqli_query($con, $query);
            if ($res) {
                $query = "UPDATE `payout_time` SET `status` = 'Active' WHERE `id` = '" . $id . "'";
                $res = mysqli_query($con, $query);
                if ($res) {
                    header("location: ?page=payout_time&update=1");
                }
            }
        }
        break;

    case "members":
        if (isset($_POST['search'])) :
            $search = trim(htmlspecialchars($_POST['input']));
            $accounts = new search($search);
        endif;
        break;

    case "view_members_account":
        $AccountController = new AccountController();
        $error = false;
        $message = null;
        $logs = null;

        if (isset($_POST['reset_password'])) :
            $obj = new \stdClass();
            $obj->user_id = Sanitize::clean($_POST['user_id']);
            $obj->account_id = Sanitize::clean($_POST['account_id']);

            $response = $AccountController->reset_password($obj);
            if ($response) :
                $error = true;
                $message = null;
            endif;
        endif;

        if (isset($_POST['update_personal_information'])) :
            $obj = new \stdClass();
            $obj->lastname = Sanitize::clean($_POST['lastname']);
            $obj->firstname = Sanitize::clean($_POST['firstname']);
            $obj->middlename = Sanitize::clean($_POST['middlename']);
            $obj->account_id = Sanitize::clean($_POST['account_id']);

            $response = $AccountController->update_account($obj);
            if ($response) :
                $error = true;
                $message = $response;
            endif;

            $logs = $obj;
        endif;

        if (isset($_POST['confirm_document'])) :
            $account_id = Sanitize::clean($_POST['account_id']);
            $document_id = Sanitize::clean($_POST['document_id']);

            $response = $AccountController->confirm_document($account_id, $document_id);
            if ($response) :
                $error = true;
                $message = "An error occurred, unable to confirm account document. Please try again later.";
            endif;
        endif;

        break;

    case "create_members_account":
        $AccountController = new AccountController();

        $failed = false;
        $log = null;
        $message = null;

        if (isset($_POST['submit'])) {
            $obj = new \stdClass();
            $obj->lastname = strtoupper(Sanitize::clean($_POST['lastname']));
            $obj->firstname = strtoupper(Sanitize::clean($_POST['firstname']));
            $obj->middlename = strtoupper(Sanitize::clean($_POST['middlename']));

            $obj->registration_code = Sanitize::clean($_POST['registration_code']);
            $registration_code_str_replace = str_replace('-', '', $obj->registration_code);
            $obj->registration_code = trim($registration_code_str_replace);

            $obj->username = Sanitize::clean($_POST['username']);
            $obj->email = Sanitize::clean($_POST['email']);
            $obj->password = Sanitize::clean($_POST['password']);
            $confirm_password = Sanitize::clean($_POST['confirm_password']);
            if ($obj->password == $confirm_password) :
                $response = $AccountController->store_member($obj);
                $message = $response;
            else :
                $message = "Password mismatch. Please retype the password again.";
            endif;


            $failed = true;
            $log = $obj;
        }
        break;

    case "accounting":
        break;

    case "create_accounting_account":
        $failedRegistration = false;
        $failedLogs = null;
        $message = null;
        if (isset($_POST['submit'])) :
            $obj = new \stdClass();

            $obj->lastname = Sanitize::clean($_POST['lastname']);
            $obj->firstname = Sanitize::clean($_POST['firstname']);
            $obj->middlename = Sanitize::clean($_POST['middlename']);
            $obj->username = Sanitize::clean($_POST['username']);
            $obj->password = Sanitize::clean($_POST['password']);
            $obj->confirm_password = Sanitize::clean($_POST['confirm_password']);
            $obj->roles = 'Accounting';

            if ($obj->confirm_password == $obj->password) :

                $response = $system_user->validate_username($obj->username, $obj->roles);
                if (!$response) :
                    $res = $system_user->create_system_users($obj);

                    if (!$res) :
                        $failedRegistration = true;
                        $failedLogs = $obj;
                    endif;
                else :
                    $failedRegistration = true;
                    $failedLogs = $obj;
                    $message = "Username is already taken. Please choose another username.";
                endif;
            else :
                $failedRegistration = true;
                $failedLogs = $obj;
                $message = "Password do not match. Please try again.";
            endif;

        endif;
        break;
    case "create_administrator_account":
        $failedRegistration = false;
        $failedLogs = null;
        $message = null;
        if (isset($_POST['submit'])) :
            $obj = new \stdClass();

            $obj->lastname = Sanitize::clean($_POST['lastname']);
            $obj->firstname = Sanitize::clean($_POST['firstname']);
            $obj->middlename = Sanitize::clean($_POST['middlename']);
            $obj->username = Sanitize::clean($_POST['username']);
            $obj->password = Sanitize::clean($_POST['password']);
            $obj->confirm_password = Sanitize::clean($_POST['confirm_password']);
            $obj->roles = 'Administrator';

            if ($obj->confirm_password == $obj->password) :

                $response = $system_user->validate_username($obj->username, $obj->roles);
                if (!$response) :
                    $res = $system_user->create_system_users($obj);

                    if (!$res) :
                        $failedRegistration = true;
                        $failedLogs = $obj;
                    endif;
                else :
                    $failedRegistration = true;
                    $failedLogs = $obj;
                    $message = "Username is already taken. Please choose another username.";
                endif;
            else :
                $failedRegistration = true;
                $failedLogs = $obj;
                $message = "Password do not match. Please try again.";
            endif;

        endif;
        break;
    case "notifications":
        $notifications = new notifications();
        break;
    case "announcement":
        $AnnouncementController = new AnnouncementController();

        $error = false;
        $message = null;
        $logs = null;

        if (isset($_POST['save_announcement'])) :

            $obj = new \stdClass();
            $obj->subject = Sanitize::clean($_POST['subject']);
            $obj->content = Sanitize::clean($_POST['content']);
            $obj->signed_by = Sanitize::clean($_POST['signed_by']);
            $obj->position = Sanitize::clean($_POST['position']);
            $obj->from = Sanitize::clean($_POST['from']);
            $obj->until = Sanitize::clean($_POST['until']);

            $AnnouncementController->store($obj);

            $error = true;
            $message = "Error occurred. Please try again.";
            $logs = $obj;
            
        endif;
        break;
    case "encashment":
        $EncashmentController = new EncashmentController();
        $encashments = Encashment::all();
        $error = false;
        $message = null;
        $logs = null;



        if (isset($_POST['add'])) :

            $obj = new \stdClass();
            $obj->encashment_mode = Sanitize::clean($_POST['encashment']);
            $obj->encashment_center = Sanitize::clean($_POST['center']);
            $obj->name_logo = $_FILES['logo']['name'];
            $obj->temp_logo = $_FILES['logo']['tmp_name'];

            $EncashmentController->store($obj);

            $error = true;
            $message = "Invalid Encashment mode and/or center. Please try again later.";
            $logs = $obj;
        endif;

        if (isset($_POST['update'])) :

            $obj = new \stdClass();
            $obj->encashment_id = Sanitize::clean($_POST['encashment_id']);
            $obj->logo = Sanitize::clean($_POST['logo']);
            $obj->encashment_mode = Sanitize::clean($_POST['encashment']);
            $obj->encashment_center = Sanitize::clean($_POST['center']);
            $obj->name_logo = $_FILES['logo']['name'];
            $obj->temp_logo = $_FILES['logo']['tmp_name'];

            $EncashmentController->update($obj);

            $error = true;
            $message = "Invalid Encashment mode and/or center. Please try again later.";
            $logs = $obj;
        endif;

        if(isset($_GET['encashment_id']) && isset($_GET['action']) && $_GET['action'] == "remove"):
            $encashment_id = Sanitize::clean($_GET['encashment_id']);

            $EncashmentController->remove($encashment_id);

            $error = true;
            $message = "Invalid Encashment mode/center and/or pending transaction is still on going. Please try again later.";
        endif;

        break;
    default:
        header("Location: ?page=package");
        break;
endswitch;
