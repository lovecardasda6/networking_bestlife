<?php
class Sanitize{
     static function clean($string){
        $trim = trim($string);
        $sanitize = htmlspecialchars($trim);
        return $sanitize;
    }
}