<?php
class CsrfToken
{
    static function generate_csrf_token()
    {
        $token = random_bytes(12);
        $token = bin2hex($token);
        
        $_SESSION['csrf_token'] = $token;
        return $token;
    }

    static function validate_csrf_token($token)
    {
        if ($token == $_SESSION['csrf_token']) :
            unset($_SESSION['csrf_token']);
            return true;
        else :
            return false;
        endif;
    }
}
