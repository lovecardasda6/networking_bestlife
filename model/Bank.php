<?php

class Bank
{
    public static function fetch($account_id, $encashment_center)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM banks WHERE account_id = '{$account_id}' AND bank_name = '{$encashment_center}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }


    public static function fetch_banks($account_id, $encashment_center)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM banks WHERE account_id = '{$account_id}' AND bank_name = '{$encashment_center}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($account_id, $name, $number, $encashment_center)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO banks VALUES (NULL, '{$account_id}', '{$name}', '{$number}', '{$encashment_center}', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $bank_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $bank_id;
            }
        }
        return false;
    }

    public static function update($bank_id, $account_id, $name, $number)
    {
        $con = Connection::ConnectionString();
        $query = "UPDATE banks SET account_name = '{$name}', account_number = '{$number}' WHERE account_id = '{$account_id}' AND id = '{$bank_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }

        return false;
    }

    public static function remove_by_center($encashment_center)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "DELETE FROM banks WHERE bank_name = '{$encashment_center}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
}
