<?php
class TransactionFee
{

    public static function fetch_active()
    {
        $con = connectionString();

        $query = "SELECT * FROM transaction_fee WHERE status = 'Active'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }
}