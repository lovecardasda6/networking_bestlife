<?php

class PairingBonus
{
    public static function store($package_id, $value_points, $pair_per_day)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO pairing_bonuses VALUES(NULL, '{$package_id}', '{$value_points}', '{$pair_per_day}', '', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $pairing_bonus_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $pairing_bonus_id;
            }
        }
        return false;
    }
    
    public static function update($pairing_bonus_id, $package_id, $value_points, $pair_per_day)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "UPDATE pairing_bonuses SET package_id = '{$package_id}', value_points = '{$value_points}', pair_per_day = '{$pair_per_day}', created = '{$date}' WHERE id = '{$pairing_bonus_id}' AND package_id = '{$package_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
    
    public static function inactive()
    {
        $con = Connection::ConnectionString();

        $query = "UPDATE pairing_bonuses SET `status` = ''";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
    
    public static function activate($pairing_bonus_id, $package_id)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "UPDATE pairing_bonuses SET `status` = 'Active' WHERE id = '{$pairing_bonus_id}' AND package_id = '{$package_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
}
