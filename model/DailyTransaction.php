<?php

class DailyTransaction
{
    public static function fetch($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM daily_transaction WHERE account_id = '{$account_id}' ORDER BY id DESC";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($account_id, $transaction_type, $transaction_id)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO daily_transaction VALUES(NULL, '{$account_id}',  '{$transaction_type}',  '{$transaction_id}', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $direct_referral_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $direct_referral_id;
            }
        }
        return false;
    }
}
