<?php

class AccountCurrency
{

    public static function fetch($account_id){
        
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM account_currency WHERE account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($account_id, $currency)
    {
        $con = Connection::ConnectionString();

        $query = "INSERT INTO account_currency VALUES(NULL, '{$account_id}',  '{$currency}')";

        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $account_currency_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $account_currency_id;
            }
        }
        return false;
    }

    public static function update($account_id, $currency){
        $con = Connection::ConnectionString();

        $query = "UPDATE account_currency SET currency = '{$currency}' WHERE account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
}