<?php


class CashoutLog
{
    public static function store($account_id, $wallet_balance, $amount, $transaction_fee)
    {
        $con = connectionString();
        $date = date('Y-m-d H:i:s');

        $percentage = $transaction_fee / 100;
        $deduction = $amount * $percentage;
        $receivable_amount = $amount - $deduction;

        $query = "INSERT INTO cashout_logs VALUES(NULL, '{$account_id}', '{$wallet_balance}','{$amount}', '{$transaction_fee}', '{$receivable_amount}', '{$date}')";
        $res = $con->query($query);

        if ($res) :
            if (mysqli_affected_rows($con)) :
                $cashout_log_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $cashout_log_id;
            endif;
        endif;

        return false;
    }

}