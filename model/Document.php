<?php

class Document
{
    static function fetch($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM documents WHERE account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }
    static function store($account_id, $document_type, $file_name)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO documents VALUES (NULL, '{$account_id}', '{$document_type}', '{$file_name}', 'Pending', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $document_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $document_id;
            }
        }
        return false;
    }

    static function confirm_document($account_id, $document_id)
    {
        $con = Connection::ConnectionString();

        $query = "UPDATE documents SET `status` = 'Confirmed' WHERE id = '{$document_id}' AND account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
}
