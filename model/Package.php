<?php
class Package
{

    public static function fetch($package_id)
    {
        $con = connectionString();

        $query = "SELECT * FROM packages WHERE id = '{$package_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function all($package_id)
    {
        $con = connectionString();

        $query = "SELECT * FROM packages";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($package, $price)
    {
        $con = connectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO packages VALUES(NULL, '{$package}', '{$price}', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $package_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $package_id;
            }
        }
        return false;
    }

    public static function update($package_id, $package, $price)
    {
        $con = connectionString();

        $query = "UPDATE packages SET package = '{$package}', price = '{$price}' WHERE id = '{$package_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }

    public static function remove($package_id)
    {
        $con = connectionString();

        $query = "DELETE packages WHERE id = '{$package_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
}
