<?php

class SystemUser
{

    public static function fetch($system_admin_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM system_users WHERE id = '{$system_admin_id}'";
        $res = $con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            endif;
        endif;

        return false;
    }

    public static function all()
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM system_users";
        $res = $con->query($query);

        if ($res) :
            if (mysqli_num_rows($res)) :
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            endif;
        endif;

        return false;
    }

    public static function store($username, $password, $lastname, $firstname, $middlename, $roles)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $password = password_hash($password, PASSWORD_BCRYPT);

        $query = "INSERT INTO system_users VALUES(NULL, '{$username}', '{$password}', '{$lastname}', '{$firstname}', '{$middlename}', '{$roles}', '', '{$date}', '{$date}', 'Active')";
        $res = $con->query($query);

        if ($res) :
            if (mysqli_affected_rows($con)) :
                mysqli_close($con);
                return true;
            endif;
        endif;

        return false;
    }
}
