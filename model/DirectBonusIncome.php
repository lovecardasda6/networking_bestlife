<?php

class DirectBonusIncome
{
    public static function fetch($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM direct_bonus_income WHERE account_id = '{$account_id}' ORDER BY id DESC";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }
}
