<?php

class Encashment
{
    public static function fetch($encashment_mode, $encashment_center)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM encashments WHERE encashment = '{$encashment_mode}' AND encashment_center  = '{$encashment_center}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function fetch_by_id($encashment_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM encashments WHERE id = '{$encashment_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function fetch_by_encashment_mode($encashment_mode)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM encashments WHERE encashment = '{$encashment_mode}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function all()
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM encashments ORDER BY encashment ASC";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function fetch_logo($encashment_mode, $encashment_center)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT logo FROM encashments WHERE encashment = '{$encashment_mode}' AND encashment_center  = '{$encashment_center}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                list('logo' => $logo) = $datas;
                mysqli_close($con);
                return $logo;
            }
        }
        return false;
    }

    public static function store($encashment_mode, $encashment_center, $logo)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO encashments VALUES(NULL, '{$encashment_mode}', '{$encashment_center}', '{$logo}', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $encashment_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $encashment_id;
            }
        }
        return false;
    }

    public static function update($encashment_id, $encashment_mode, $encashment_center, $logo)
    {
        $con = Connection::ConnectionString();

        $query = "UPDATE encashments SET encashment = '{$encashment_mode}', encashment_center = '{$encashment_center}', logo = '{$logo}' WHERE id = '{$encashment_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }

    public static function remove($encashment_id)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "DELETE FROM encashments WHERE id = '{$encashment_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
}
