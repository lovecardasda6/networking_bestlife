<?php
class Wallet
{
    public static function fetch($account_id)
    {
        $con = connectionString();
        $date = date('Y-m-d H:i:s');

        $query = "SELECT * FROM wallet WHERE account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($account_id)
    {
        $con = connectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO wallet VALUES(NULL, '{$account_id}', '0', '0', '{$date}', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $wallet_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $wallet_id;
            }
        }
        return false;
    }

    public static function deduct($account_id, $amount, $gc)
    {
        $con = connectionString();
        $date = date('Y-m-d H:i:s');
        
        $amount = str_replace(',', '', $amount);
        $gc = str_replace(',', '', $gc);

        $query = "UPDATE wallet SET balance = balance - '{$amount}', gc = gc - '{$gc}', created = '{$date}', updated = '{$date}' WHERE account_id = '{$account_id}' ";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }


    public static function add($amount, $gc, $account_id)
    {
        $con = connectionString();
        $date = date('Y-m-d H:i:s');

        $amount = str_replace(',', '', $amount);
        $gc = str_replace(',', '', $gc);

        $query = "UPDATE wallet SET balance = balance + '{$amount}', gc = balance + '{$gc}', created = '{$date}', updated = '{$date}' WHERE account_id = '{$account_id}' ";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
}
