<?php

class Account
{

    public static function fetch($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM accounts WHERE id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }

        return false;
    }    

    public static function fetch_account($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT accounts.*, accounts.created as `registered_on`, accounts.id as `account_id`, packages.*, registration_codes.*, users.*, users.id as `user_id` FROM accounts INNER JOIN registration_codes ON registration_codes.id = accounts.registration_code_id INNER JOIN packages ON packages.id = accounts.package_id INNER JOIN users ON users.account_id = accounts.id WHERE accounts.id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }

        return false;
    }

    public static function validation($reference_id, $upline_id, $position)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM accounts WHERE reference_id = '{$reference_id}' AND upline_id  = '{$upline_id}' AND position = '{$position}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }

        return false;
    }

    public static function all_accounts()
    {
        $con = Connection::ConnectionString();

        $query = "SELECT accounts.*, accounts.created as `registered_on`, accounts.id as `account_id`, packages.*, registration_codes.* FROM accounts INNER JOIN registration_codes ON registration_codes.id = accounts.registration_code_id INNER JOIN packages ON packages.id = accounts.package_id";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }

        return false;
    }

    public static function all()
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM accounts";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }

        return false;
    }

    public static function store($lastname, $firstname, $middlename, $contact_no, $gender, $address, $date_of_birth, $place_of_birth, $referral_id, $package_id, $registration_code_id, $upline_id, $position)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO accounts VALUES (NULL, '{$lastname}', '{$firstname}', '{$middlename}', '{$contact_no}', '{$gender}', '{$address}', '{$date_of_birth}', '{$place_of_birth}', '{$referral_id}', '{$package_id}', '{$registration_code_id}', '{$upline_id}', '{$position}', '{$date}')";
        $res = $con->query($query);

        if ($res) :
            if (mysqli_affected_rows($con)) :
                $account_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $account_id;
            endif;
        endif;
        return false;
    }

    static function update_account_information($account_id, $lastname, $firstname, $middlename)
    {
        $con = Connection::ConnectionString();

        $query = "UPDATE accounts SET lastname = '{$lastname}', firstname = '{$firstname}', middlename = '{$middlename}' WHERE id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }

    static function update_basic_information($account_id, $contact, $gender, $address, $date_of_birth, $place_of_birth)
    {
        $con = Connection::ConnectionString();

        $query = "UPDATE accounts SET contact = '{$contact}', gender = '{$gender}', address = '{$address}', date_of_birth = '{$date_of_birth}', place_of_birth = '{$place_of_birth}' WHERE id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }

    public static function max_account_per_person($lastname, $firstname, $middlename)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT id FROM accounts WHERE lastname = '{$lastname}' AND firstname = '{$firstname}' AND middlename = '{$middlename}'";
        $res = $con->query($query);

        if ($res) {
            $counts = mysqli_num_rows($res);
            mysqli_close($con);

            if ($counts > 15) {
                return true;
            }
        }

        return false;
    }

    public static function upgrade_account($account_id, $package_id, $registration_code_id)
    {
        $con = Connection::ConnectionString();

        $query = "UPDATE accounts SET package_id = '{$package_id}', registration_code_id = '{$registration_code_id}' WHERE id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }

    public static function undo($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "DELETE FROM accounts WHERE id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) :
            if (mysqli_affected_rows($con)) :
                mysqli_close($con);
                return true;
            endif;
        endif;
        return false;
    }
}
