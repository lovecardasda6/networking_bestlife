<?php

class Cashout
{
    public static function fetch($cashout_id, $account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT accounts.*, accounts.id as `account_id`, wallet.*, cashout.*, cashout.id as `cashout_id`, cashout.created as `cashout_created`, packages.* FROM cashout INNER JOIN accounts ON accounts.id = cashout.account_id INNER JOIN wallet ON wallet.account_id = cashout.account_id INNER JOIN packages ON packages.id = accounts.package_id WHERE `status` = 'Pending' AND cashout.account_id = '{$account_id}' AND cashout.id = '{$cashout_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function fetch_transaction($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM cashout WHERE account_id = '{$account_id}' ORDER BY id DESC";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function fetch_with_encashment($encashment_mode, $encashment_center)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM cashout WHERE encashment_mode = '{$encashment_mode}' AND encashment_center = '{$encashment_center}' AND `status` = 'Pending'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function all_by_date($date)
    {
        $con = Connection::ConnectionString();
        $to = $date . " 23:59:59";

        $query = "SELECT accounts.*, accounts.id as `account_id`, wallet.*, cashout.*, cashout.id as `cashout_id` FROM cashout INNER JOIN accounts ON accounts.id = cashout.account_id INNER JOIN wallet ON wallet.account_id = cashout.account_id WHERE `status` = 'Pending' AND cashout.created <= '{$to}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function all()
    {
        $con = Connection::ConnectionString();

        $query = "SELECT accounts.*, accounts.id as `account_id`, wallet.*, cashout.*, cashout.id as `cashout_id` FROM cashout INNER JOIN accounts ON accounts.id = cashout.account_id INNER JOIN wallet ON wallet.account_id = cashout.account_id";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function fetch_pending($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM cashout WHERE account_id = '{$account_id}' AND `status` = 'Pending' ORDER BY id DESC";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($account_id, $amount, $transaction_fee, $encashment_mode, $encashment_center)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $percentage = $transaction_fee / 100;
        $deduction = $amount * $percentage;
        $receivable_amount = $amount - $deduction;

        $query = "INSERT INTO cashout VALUES (NULL, '{$account_id}', '{$amount}', '{$transaction_fee}', '{$receivable_amount}', '{$encashment_mode}', '{$encashment_center}', 'Pending',  '',  '',  '{$date}', NULL)";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $cashout_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $cashout_id;
            }
        }
        return false;
    }

    public static function confirm_cashout($cashout_id, $account_id, $confirm_by_id, $remark)
    {
        $con = Connection::ConnectionString();
        $timestamp = date('Y-m-d H:i:s');

        $query = "UPDATE cashout SET `status` = 'Confirmed', confirm_id = '{$confirm_by_id}', remark = '{$remark}', updated = '{$timestamp}' WHERE id = '{$cashout_id}' AND account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) :
            if (mysqli_affected_rows($con)) :
                mysqli_close($con);
                return true;
            endif;
        endif;

        return false;
    }

    public static function export_transactions_with_encashment_details($date)
    {
        $con = Connection::ConnectionString();
        $to = $date . " 23:59:59";

        $query = "SELECT accounts.*, accounts.id as `account_id` , wallet.*, cashout.*, cashout.id as `cashout_id`, cashout.created as `cashout_created` FROM cashout INNER JOIN accounts ON accounts.id = cashout.account_id INNER JOIN wallet ON wallet.account_id = cashout.account_id WHERE cashout.created <= '{$to}' AND status = 'Pending' ORDER BY cashout.id ASC";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }
}
