<?php

class EWallet
{
    public static function fetch($account_id, $encashment_center)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM ewallet WHERE account_id = '{$account_id}' AND ewallet_center = '{$encashment_center}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }
    
    public static function fetch_ewallet($account_id, $encashment_center)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM ewallet WHERE account_id = '{$account_id}' AND ewallet_center = '{$encashment_center}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($account_id, $number, $encashment_center)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO ewallet VALUES (NULL, '{$account_id}', '{$number}', '{$encashment_center}', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $bank_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $bank_id;
            }
        }
        return false;
    }

    public static function update($ewallet_id, $account_id, $number)
    {
        $con = Connection::ConnectionString();

        $query = "UPDATE ewallet SET ewallet_number = '{$number}' WHERE account_id = '{$account_id}' AND id = '{$ewallet_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }

        return false;
    }

    public static function remove_by_center($encashment_center)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "DELETE FROM ewallet WHERE ewallet_center = '{$encashment_center}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
}
