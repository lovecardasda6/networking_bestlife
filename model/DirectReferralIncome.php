<?php

class DirectReferralIncome
{
    public static function fetch($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM direct_referral_income WHERE account_id = '{$account_id}' ORDER BY id DESC";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($referral_id, $registered_account_id, $amount, $gc)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO direct_referral_income VALUES(NULL, '{$referral_id}',  '{$registered_account_id}',  '{$amount}',  '{$gc}',  '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $direct_referral_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $direct_referral_id;
            }
        }
        return false;
    }

    public static function fetch_direct_referral_income_with_package_id($package_id){
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM direct_referral_bonuses WHERE package_id = '{$package_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }
}
