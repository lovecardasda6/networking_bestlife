<?php
class Process
{
    public static function store($account_id, $direct_referral_income, $direct_referral_gc, $upline_account_id, $pairing_invited_account_id, $new_registered_account_id){
        
        $con = connectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO processes VALUES(NULL, '{$account_id}', '{$direct_referral_income}', '{$direct_referral_gc}', '{$upline_account_id}', '{$pairing_invited_account_id}', '{$new_registered_account_id}', 'Pending', '{$date}')";
        $res = $con->query($query);

        if($res){
            if(mysqli_affected_rows($con)){
                $process_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $process_id;
            }
        }
        return false;
    }
}
