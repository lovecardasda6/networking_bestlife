<?php

class DirectReferralBonus
{
    public static function store($package_id, $amount, $gc)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO direct_referral_bonuses VALUES(NULL, '{$package_id}', '{$amount}', '{$gc}', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $direct_referral_bonus_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $direct_referral_bonus_id;
            }
        }
        return false;
    }
    
    public static function update($direct_referral_bonus_id, $package_id, $amount, $gc)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "UPDATE direct_referral_bonuses SET package_id = '{$package_id}', earnings = '{$amount}', product = '{$gc}', created = '{$date}' WHERE id = '{$direct_referral_bonus_id}' AND package_id = '{$package_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }
}
