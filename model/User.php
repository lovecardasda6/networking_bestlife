<?php

class User
{
    public static function fetch($user_id ,$account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM users WHERE id = '{$user_id}' AND account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($account_id, $username, $email, $password)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $password = password_hash($password, PASSWORD_BCRYPT);

        $query = "INSERT INTO users VALUES (NULL, '{$account_id}', '{$username}', '{$email}', '{$password}', '', '{$date}', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $user_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $user_id;
            }
        }
        return false;
    }
    
    static function update_email_address($user_id, $account_id, $email)
    {
        $con = Connection::ConnectionString();

        $password = password_hash(1234567890, PASSWORD_BCRYPT);
        $query = "UPDATE users SET email = '{$email}' WHERE id = '{$user_id}' AND account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }


    static function change_password($user_id, $account_id, $password)
    {
        $con = Connection::ConnectionString();

        $password = password_hash($password, PASSWORD_BCRYPT);
        $query = "UPDATE users SET password = '{$password}' WHERE id = '{$user_id}' AND account_id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                mysqli_close($con);
                return true;
            }
        }
        return false;
    }

    public static function fetch_user_with_username($username)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM users WHERE username = '{$username}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }
}
