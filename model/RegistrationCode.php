<?php

class RegistrationCode
{

    public static function fetch($account_id)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM registration_codes WHERE id = '{$account_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }

        return false;
    }

    public static function all()
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM registration_codes";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_all($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }

        return false;
    }

    public static function fetch_with_registration_code($registration_code)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM registration_codes WHERE registration_code = '{$registration_code}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }

        return false;
    }

    static function store($package_id, $type, $quantity)
    {

        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');


        for ($i = 1; $i <= $quantity; $i++) {
            $code  = bin2hex(random_bytes(12));
            $code = strtoupper($code);
            $code = substr($code, 0, 16);
            $query = "INSERT INTO registration_codes VALUES (NULL, '{$package_id}', '{$code}', '{$type}', 'Unused', '{$date}',NULL)";
            $con->query($query);
        }
    }

    public static function update_status_with_registration_code_id($registration_code_id)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "UPDATE registration_codes SET status = 'Used', updated = '{$date}' WHERE id = '{$registration_code_id}'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                return true;
            }
        }

        return false;
    }
}
