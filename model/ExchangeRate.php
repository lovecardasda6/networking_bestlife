<?php

class ExchangeRate{
    
    public static function fetch_php_to_usd()
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM exchange_rate WHERE currency = 'PHP'";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }
}