<?php

class Announcement
{
    public static function fetch($date)
    {
        $con = Connection::ConnectionString();

        $query = "SELECT * FROM announcement WHERE `from` <= '{$date}' AND `end` >= '{$date}' ORDER BY id ASC LIMIT 1 ";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_num_rows($res)) {
                $datas = mysqli_fetch_array($res, MYSQLI_ASSOC);
                mysqli_close($con);
                return $datas;
            }
        }
        return false;
    }

    public static function store($subject, $content, $signed_by, $position, $from, $until)
    {
        $con = Connection::ConnectionString();
        $date = date('Y-m-d H:i:s');

        $query = "INSERT INTO announcement VALUES (NULL, '{$subject}', '{$content}', '{$signed_by}', '{$position}', '{$from}', '{$until}', '{$date}')";
        $res = $con->query($query);

        if ($res) {
            if (mysqli_affected_rows($con)) {
                $bank_id = mysqli_insert_id($con);
                mysqli_close($con);
                return $bank_id;
            }
        }
        return false;
    }
}
