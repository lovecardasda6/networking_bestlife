<?php

session_start();
header("Content-Type: image/jpeg");
$captcha = $_SESSION['captcha'];
$width = 150;
$height = 30;
$font = "font.ttf";
$image = imagecreate($width, $height);

$background_color = imagecolorallocate($image, 0, 0, 0);
$text_color = imagecolorallocate($image, 255, 255, 255);

imagestring($image, 50, 50, 5,  $captcha, $text_color);
imagejpeg($image);
imagedestroy($image);
