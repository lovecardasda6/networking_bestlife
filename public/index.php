<?php
session_start();

$request = @$_GET['page'];
require_once __DIR__ . "/../App/Sanitize.php";
require_once __DIR__ . '/../App/CsrfToken.php';
require_once __DIR__ . '/../config/Connection.php';
require_once __DIR__ . '/../controllers/auth.php';
auth::validate();

require_once __DIR__ . '/../controllers/AccountRegistrationController.php';
require_once __DIR__ . '/../controllers/AccountController.php';
require_once __DIR__ . '/../controllers/WalletController.php';
require_once __DIR__ . '/../controllers/TransactionController.php';
require_once __DIR__ . '/../controllers/CashoutController.php';
require_once __DIR__ . '/../controllers/AnnouncementController.php';
require_once __DIR__ . "/../controllers/UpgradeAccountController.php";
require_once __DIR__ . "/../controllers/ExchangeRateController.php";

require_once __DIR__ . '/../model/Account.php';
require_once __DIR__ . '/../model/RegistrationCode.php';
require_once __DIR__ . '/../model/DailyTransaction.php';
require_once __DIR__ . '/../model/DirectReferralIncome.php';
require_once __DIR__ . '/../model/Process.php';
require_once __DIR__ . '/../model/User.php';
require_once __DIR__ . '/../model/Wallet.php';
require_once __DIR__ . '/../model/Bank.php';
require_once __DIR__ . '/../model/Document.php';
require_once __DIR__ . '/../model/Remittance.php';
require_once __DIR__ . '/../model/EWallet.php';
require_once __DIR__ . '/../model/AccountCurrency.php';
require_once __DIR__ . '/../model/PairingBonusIncome.php';
require_once __DIR__ . '/../model/IndirectBonusIncome.php';
require_once __DIR__ . '/../model/DirectBonusIncome.php';
require_once __DIR__ . '/../model/Cashout.php';
require_once __DIR__ . '/../model/TransactionFee.php';
require_once __DIR__ . '/../model/Encashment.php';
require_once __DIR__ . '/../model/AccountCurrency.php';
require_once __DIR__ . '/../model/CashoutLog.php';
require_once __DIR__ . '/../model/Announcement.php';
require_once __DIR__ . '/../model/ExchangeRate.php';

auth::check_wallet();

require_once __DIR__ . '/../controllers/account_logged.php';
// require_once __DIR__ . "/../controllers/client/direct_referral.php";
require_once __DIR__ . "/../controllers/client/daily_transaction.php";
require_once __DIR__ . "/../controllers/client/binaryTree.php";
// require_once __DIR__ . "/../controllers/client/downlineTree.php";
// require_once __DIR__ . "/../controllers/process.php";
// require_once __DIR__ . "/../controllers/exchange_rate.php";
// require_once __DIR__ . "/../controllers/wallet.php";
// require_once __DIR__ . "/../controllers/exchange_rate.php";
// require_once __DIR__ . '/../controllers/client/transactions.php';
// require_once __DIR__ . "/../controllers/client/accounts.php";
// require_once __DIR__ . '/../controllers/client/cashout.php';
// require_once __DIR__ . '/../controllers/client/transaction_fee.php';



require_once __DIR__ . '/../router/client.php';

?>
<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Best Life Essential</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/style_v2.css" />
    <link rel="stylesheet" type="text/css" href="./css/binarytree.css" />
    <link rel="stylesheet" type="text/css" href="./css/packages.css" />
    <link rel="stylesheet" type="text/css" href="./assets/font-awesome/css/font-awesome.css" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <script src="./js/jquery.min.js"></script>

    <style>
        @font-face{
            font-family: 'creamCake';
            src: url('./fonts/Cream Cake.ttf');
            font-style: normal;
            font-weight: 100;
        }
    </style>

    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
</head>

<body>

    <div style="display:none; margin-top: 3.8em;" class="w3-sidebar w3-bar-block w3-border-right" id="mySidebar" onclick="w3_open()">
        <div style="background-color:white; width: 60%; height: 100%!important;" onclick="event.stopPropagation();">
            <ul class="nav nav-sidebar closed" id="mobile-nav" style="font-size: 1.5em; font-weight:50;">
                <li>
                    <a href="?page=home" style="<?= ($_GET['page'] == 'dashboard') ? 'background-color:#00674b; color:white;' : '' ?>">
                        <i class="fa fa-money" aria-hidden="true" style="margin-bottom:0.2em;"></i>
                        &nbsp;&nbsp;Home
                    </a>
                </li>
                <li>
                    <a href="?page=wallet" style="<?= ($_GET['page'] == 'wallet') ? 'background-color:#00674b; color:white;' : '' ?>">
                        <i class="fa fa-money" aria-hidden="true" style="margin-bottom:0.2em;"></i>
                        &nbsp;&nbsp;My Wallets
                    </a>
                </li>
                <li>
                    <a href="?page=cashout_center" style="<?= ($_GET['page'] == 'cashout_center' || $_GET['page'] == 'cashout_amount' || $_GET['page'] == 'cashout') ? 'background-color:#00674b; color:white;' : '' ?>">
                        <i class="fa fa-credit-card" aria-hidden="true" style="margin-bottom:0.2em;"></i>
                        &nbsp;&nbsp;Cashout
                    </a>
                </li>
                <li>
                    <a href="?page=binarytree" style="<?= ($_GET['page'] == 'binarytree') ? 'background-color:#00674b; color:white;' : '' ?>">
                        <i class="fa fa-sitemap" aria-hidden="true" style="margin-bottom:0.2em;"></i>
                        &nbsp;&nbsp;Binary Tree
                    </a>
                </li>
                <li>
                    <a href="?page=account" style="<?= ($_GET['page'] == 'account') ? 'background-color:#00674b; color:white;' : '' ?>">
                        <i class="fa fa-user-o" aria-hidden="true" style="margin-bottom:0.2em;"></i>
                        &nbsp;&nbsp;Account
                    </a>
                </li>
                <li>
                    <a href="?page=about" style="<?= ($_GET['page'] == 'about') ? 'background-color:#00674b; color:white;' : '' ?>">
                        <i class="fa fa-info" aria-hidden="true" style="margin-bottom:0.2em;"></i>
                        &nbsp;&nbsp;About
                    </a>
                </li>
                <li><a href="logout.php">
                        <i class="fa fa-sign-out" aria-hidden="true"></i></i>&nbsp;&nbsp; Logout</a>
                </li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div style="width: 98%; margin: 0 auto;">
                <div class="navbar-header" style="padding:1.5em 0em; box-sizing:border-box; width: 100%;">
                    <button type="button" class="navbar-toggle collapsed" onclick="w3_open()" style="margin-right:0em; margin-left: 0em;">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img alt="Brand" src="./assets/logo2.png" id="nav-brand-logo">
                    </a>
                    <a class="navbar-brand" href="javascript:(void)" id="nav-brand-text" style="color:#00674b; margin: 0px;">Bestlife Essential</a>
                </div>

                <div style="position: absolute; right:2em; top: 2em; text-align:center;" id="desktop_logout">
                    <a href="logout.php" style="font-size:1em;">
                        <i class="fa fa-sign-out" aria-hidden="true" style="font-size:2em; margin-bottom:0.2em;"></i>
                        <br/>
                        Logout
                    </a>
                </div>
            </div>
        </div>
    </nav>

    <br />
    <br />

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1 col-lg-1" style="overflow-x:hidden"></div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10" style="overflow-x:hidden">
                <div style="margin-top: 3.2em;">
                    <div class="row menu-desktop">
                        <div class="col-sm-2 col-md-2" style="text-align:center;">
                            <a href="?page=home" style="font-size:1.5em; <?= ($_GET['page'] == 'dashboard') ? 'text-decoration:underline;' : '' ?>">
                                <i class="fa fa-home" aria-hidden="true" style="font-size:2em; margin-bottom:0.2em;"></i>
                                <br />
                                Home
                            </a>
                        </div>
                        <div class="col-sm-2 col-md-2" style="text-align:center;">
                            <a href="?page=wallet" style="font-size:1.5em; <?= ($_GET['page'] == 'wallet') ? 'text-decoration:underline;' : '' ?>">
                                <i class="fa fa-money" aria-hidden="true" style="font-size:2em; margin-bottom:0.2em;"></i>
                                <br />
                                My Wallet
                            </a>
                        </div>
                        <div class="col-sm-2 col-md-2" style="text-align:center;">
                            <a href="?page=cashout_center" style="font-size:1.5em; <?= ($_GET['page'] == 'cashout_center' || $_GET['page'] == 'cashout_amount' || $_GET['page'] == 'cashout') ? 'text-decoration:underline;' : '' ?>">
                                <i class="fa fa-credit-card" aria-hidden="true" style="font-size:2em; margin-bottom:0.2em;"></i>
                                <br />
                                Cashout
                            </a>
                        </div>
                        <div class="col-sm-2 col-md-2" style="text-align:center;">
                            <a href="?page=binarytree" style="font-size:1.5em; <?= ($_GET['page'] == 'binarytree') ? 'text-decoration:underline;' : '' ?>">
                                <i class="fa fa-sitemap" aria-hidden="true" style="font-size:2em; margin-bottom:0.2em;"></i>
                                <br />
                                Binary Tree
                            </a>
                        </div>
                        <div class="col-sm-2 col-md-2" style="text-align:center;">
                            <a href="?page=account" style="font-size:1.5em; <?= ($_GET['page'] == 'account') ? 'text-decoration:underline;' : '' ?>">
                                <i class="fa fa-user-o" aria-hidden="true" style="font-size:2em; margin-bottom:0.2em;"></i>
                                <br />
                                Account
                            </a>
                        </div>
                        <div class="col-sm-2 col-md-2" style="text-align:center;">
                            <a href="?page=about" style="font-size:1.5em; <?= ($_GET['page'] == 'about') ? 'text-decoration:underline;' : '' ?>">
                                <i class="fa fa-info" aria-hidden="true" style="font-size:2em; margin-bottom:0.2em;"></i>
                                <br />
                                About
                            </a>
                        </div>
                    </div>
                </div>
                <hr />
                <?php
                switch ($request) {
                    case "home":
                        require_once __DIR__ . "/../views/home.php";
                        require_once __DIR__ . "/../views/announcement.php";
                        break;

                    case "wallet":
                        require_once __DIR__ . "/../views/wallet.php";
                        break;

                    case "cashout_center":
                        require_once __DIR__ . "/../views/cashout_center.php";
                        break;

                    case "cashout_amount":
                        require_once __DIR__ . "/../views/cashout_amount.php";
                        break;

                    case "cashout":
                        require_once __DIR__ . "/../views/cashout.php";
                        break;

                    case "upgrade":
                        require_once __DIR__ . "/../views/upgrade.php";
                        break;

                    case "transactions":
                        require_once __DIR__ . "/../views/transactions.php";
                        break;

                    case "history":
                        require_once __DIR__ . "/../views/history.php";
                        break;

                    case "account":
                        require_once __DIR__ . "/../views/account.php";
                        break;

                    case "binarytree":
                        require_once __DIR__ . "/../views/binarytree.php";
                        break;

                    case "account_registration":
                        require_once __DIR__ . "/../views/account_registration.php";
                        break;

                    case "downline":
                        require_once __DIR__ . "/../views/downlineTree.php";
                        break;

                    case "about":
                        require_once __DIR__ . "/../views/about.php";
                        break;

                    default;
                    require_once __DIR__ . "/../error_page/404.html";
                    break;
                }
                ?>
            </div>
        </div>
    </div>
    <footer class="container-fluid" style="margin-top:20px">
        <div class="col-md-12">
        </div>
    </footer>

    <script src="./js/bootstrap.min.js"></script>
    <script>
        function w3_open() {
            if (document.getElementById("mobile-nav").className === "nav nav-sidebar closed") {
                document.getElementById("mySidebar").style.display = "block";
                document.getElementById("mobile-nav").classList.remove("closed");
                document.getElementById("mobile-nav").classList.add("open");
            } else {
                document.getElementById("mySidebar").style.display = "none";
                document.getElementById("mobile-nav").classList.remove("open");
                document.getElementById("mobile-nav").classList.add("closed");
            }
        }

        $(document).ready(function() {


            var dropdown = document.getElementsByClassName("dropdown-btn");
            var i;

            for (i = 0; i < dropdown.length; i++) {
                dropdown[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    var dropdownContent = this.nextElementSibling;
                    if (dropdownContent.style.display === "block") {
                        dropdownContent.style.display = "none";
                    } else {
                        dropdownContent.style.display = "block";
                    }
                });
            }
        });
    </script>
</body>

</html>