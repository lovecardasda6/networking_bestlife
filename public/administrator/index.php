<?php
session_start();
require_once __DIR__ . "/../../App/Sanitize.php";
require_once __DIR__ . "/../../config/Connection.php";
require_once __DIR__ . "/../../controllers/auth.php";
auth::admin_auth('Administrator');

$request = @$_GET['page'];
require_once __DIR__ . "/../../model/Account.php";
require_once __DIR__ . "/../../model/RegistrationCode.php";
require_once __DIR__ . "/../../model/Package.php";
require_once __DIR__ . "/../../model/User.php";
require_once __DIR__ . "/../../model/Wallet.php";
require_once __DIR__ . "/../../model/DirectReferralBonus.php";
require_once __DIR__ . "/../../model/PairingBonus.php";
require_once __DIR__ . "/../../model/Document.php";
require_once __DIR__ . "/../../model/Encashment.php";
require_once __DIR__ . "/../../model/Bank.php";
require_once __DIR__ . "/../../model/EWallet.php";
require_once __DIR__ . "/../../model/Remittance.php";
require_once __DIR__ . "/../../model/Announcement.php";
require_once __DIR__ . "/../../model/Cashout.php";

require_once __DIR__ . "/../../controllers/AccountController.php";
require_once __DIR__ . "/../../controllers/administrator/DirectReferralBonusController.php";
require_once __DIR__ . "/../../controllers/administrator/PackageController.php";
require_once __DIR__ . "/../../controllers/administrator/PairingBonusController.php";
require_once __DIR__ . "/../../controllers/RegistrationCodeController.php";
require_once __DIR__ . "/../../controllers/EncashmentController.php";
require_once __DIR__ . "/../../controllers/AnnouncementController.php";


require_once __DIR__ . "/../../controllers/controller.php";
// require_once __DIR__ . "/../../controllers/wallet.php";
require_once __DIR__ . "/../../controllers/administrator/transaction_fee.php";
require_once __DIR__ . "/../../controllers/administrator/system_user.php";
require_once __DIR__ . "/../../controllers/administrator/members.php";
require_once __DIR__ . "/../../controllers/administrator/search.php";
require_once __DIR__ . "/../../controllers/administrator/pairing_level.php";
require_once __DIR__ . "/../../controllers/administrator/notifications.php";
require_once __DIR__ . "/../../controllers/documents.php";
require_once __DIR__ . "/../../controllers/registrationCodes.php";
require_once __DIR__ . "/../../controllers/pairing_bonus.php";
require_once __DIR__ . "/../../router/administrator.php";





?>

<!DOCTYPE html>

<html>

<head>
    <title>Admin - Best Life Essential</title>
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link href="./../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./../css/style_v2.css" />
    <link rel="stylesheet" type="text/css" href="./../assets/font-awesome/css/font-awesome.css" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <style>
        @media (min-width: 992px) {
            .col-md-offset-3.active {
                margin-left: 0%;
            }

            .col-md-9.active {
                width: 100%;
            }
        }
    </style>

    <script src="./../js/jquery.min.js"></script>

    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
</head>

<body>
    <div class="body">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#" style="padding:5px">
                            <img alt="Brand" src="../assets/logo2.png" style="width:40px">
                        </a>
                        <button type="button" class="btn btn-primay" onclick="toogleMenu(this)" style="margin:9px"><i class="fa fa-bars" aria-hidden="true"></i></button>
                        <a class="navbar-brand" href="javascript:(void)" style="color:#00674b;font-size: 25px;font-weight: 500;">BestLife Essential</a>

                    </div>
                    <div style="text-align: right; padding: 1em;">
                        <a href="?page=notifications"><i class="fa fa-bell" aria-hidden="true" style="font-size: 1.5em; margin-right: 1.5em;">&nbsp; <?= notifications::fetch_notifications() ?></i></a>
                        <b>Welcome back, &nbsp;</b>
                        <label>Administrator</label>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container-fluid">

            <div class="row">


                <div class="col-sm-3 col-md-2 toggle-sidebar">
                    <div class="sider" id="sider" style="width: 350px; margin:0px; padding:1em 1em 1em 0em; background-color:rgb(0, 128, 92);">
                        <ul class="nav nav-sidebar" id="side_nav">
                            <li style="<?= ($request == 'package') ? 'background-color:#004a36;' : '' ?>"><a href="?page=package" style="color:white;"><i class="fa fa-inbox" aria-hidden="true"></i>&nbsp;&nbsp;Package</a></li>
                            <li style="<?= ($request == 'referral_bonus') ? 'background-color:#004a36;' : '' ?>"><a href="?page=referral_bonus" style="color:white;"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i>&nbsp;&nbsp;Referral Bonus System</a></li>
                            <li style="<?= ($request == 'pairing_bonus') ? 'background-color:#004a36;' : '' ?>"><a href="?page=pairing_bonus" style="color:white;"><i class="fa fa-object-ungroup" aria-hidden="true"></i>&nbsp;&nbsp;Pairing Bonus System</a></li>
                            <li style="<?= ($request == 'indirect_level' || $request == 'indirect_bonus') ? 'background-color:#004a36;' : '' ?>"><a class="dropdown-btn" style="color:white;">
                                    <i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp; Indirect Bonus System &nbsp;&nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                <div class="dropdown-container" style="background-color: #00674b; margin:0px; padding:0px;">
                                    <ul class="nav nav-sidebar">
                                        <li>
                                            <a href="?page=indirect_level" style="padding-left: 40px;font-weight:500; color:white;">Level</a>
                                        </li>
                                        <li>
                                            <a href="?page=indirect_bonus" style="padding-left: 40px;font-weight:500; color:white;">Indirect Bonus</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li style="<?= ($request == 'direct_bonus') ? 'background-color:#004a36;' : '' ?>"><a href="?page=direct_bonus" style="color:white;"><i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Direct Bonus System</a></li>

                            <li style="<?= ($request == 'registration_code' || $request == 'unpaid_registration_codes') ? 'background-color:#004a36;' : '' ?>"><a class="dropdown-btn" style="color:white;">
                                    <i class="fa fa-barcode" aria-hidden="true"></i>&nbsp;&nbsp; Registration Code &nbsp;&nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                <div class="dropdown-container" style="background-color: #00674b; margin:0px; padding:0px;">
                                    <ul class="nav nav-sidebar">
                                        <li>
                                            <a href="?page=registration_code" style="color:white;"><i class="fa fa-barcode" aria-hidden="true"></i>&nbsp; &nbsp; Paid codes</a>
                                        </li>
                                        <li>
                                            <a href="?page=unpaid_registration_codes" style="color:white;"><i class="fa fa-barcode" aria-hidden="true"></i>&nbsp; &nbsp; Free codes</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>


                            <li style="<?= ($request == 'administrators' || $request == 'accounting' || $request == 'leaders') ? 'background-color:#004a36;' : '' ?>"><a class="dropdown-btn" style="color:white;">
                                    <i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp; Accounts &nbsp;&nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                <div class="dropdown-container" style="background-color: #00674b; margin:0px; padding:0px;">
                                    <ul class="nav nav-sidebar">
                                        <li>
                                            <a href="?page=administrator" style="color:white;"><i class="fa fa-users" aria-hidden="true"></i>&nbsp; &nbsp; Administrator</a>
                                        </li>
                                        <li>
                                            <a href="?page=accounting" style="color:white;"><i class="fa fa-users" aria-hidden="true"></i>&nbsp; &nbsp; Accounting</a>
                                        </li>
                                        <li>
                                            <a href="?page=members" style="color:white;"><i class="fa fa-users" aria-hidden="true"></i>&nbsp; &nbsp; Members</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li style="<?= ($request == 'transaction_fee') ? 'background-color:#004a36;' : '' ?>"><a href="?page=transaction_fee" style="color:white;">
                                    <i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;&nbsp; Transaction Fee</a>
                            </li>

                            <li style="<?= ($request == 'encashment') ? 'background-color:#004a36;' : '' ?>"><a href="?page=encashment" style="color:white;"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;Encashment </a></li>

                            <li style="<?= ($request == 'announcement') ? 'background-color:#004a36;' : '' ?>"><a href="?page=announcement" style="color:white;"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;Announcement </a></li>

                            <li><a href="logout.php" style="color:white;">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i></i>&nbsp;&nbsp; Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>



                <div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-3 main" id="main" style="overflow-x:hidden;">

                    <div class="content">
                        <?php
                        switch ($request) {
                            case "package":
                                require_once __DIR__ . "/../../views/administrator/package.php";
                                break;

                            case "referral_bonus":
                                require_once __DIR__ . "/../../views/administrator/referral_bonus.php";
                                break;

                            case "pairing_bonus":
                                require_once __DIR__ . "/../../views/administrator/pairing_bonus.php";
                                break;

                            case "indirect_bonus":
                                require_once __DIR__ . "/../../views/administrator/indirect_bonus.php";
                                break;

                            case "indirect_level":
                                require_once __DIR__ . "/../../views/administrator/indirect_level.php";
                                break;

                            case "direct_bonus":
                                require_once __DIR__ . "/../../views/administrator/direct_bonus.php";
                                break;

                            case "transaction_fee":
                                require_once __DIR__ . "/../../views/administrator/transaction_fee.php";
                                break;

                            case "encashment":
                                require_once __DIR__ . "/../../views/administrator/encashment.php";
                                break;

                            case "confirm_payout":
                                require_once __DIR__ . "/../../views/accounting/confirm_payout.php";
                                break;

                            case "registration_code":
                                require_once __DIR__ . "/../../views/administrator/registration_code.php";
                                break;

                            case "unpaid_registration_codes":
                                require_once __DIR__ . "/../../views/administrator/unpaid_registration_codes.php";
                                break;

                            case "administrator":
                                require_once __DIR__ . "/../../views/administrator/administrator/administrator.php";
                                break;

                            case "create_administrator_account":
                                require_once __DIR__ . "/../../views/administrator/administrator/create_administrator_account.php";
                                break;

                            case "accounting":
                                require_once __DIR__ . "/../../views/administrator/accounting/accounting.php";
                                break;

                            case "create_accounting_account":
                                require_once __DIR__ . "/../../views/administrator/accounting/create_accounting_account.php";
                                break;

                            case "members":
                                require_once __DIR__ . "/../../views/administrator/members/members.php";
                                break;

                            case "view_members_account":
                                require_once __DIR__ . "/../../views/administrator/members/view_members_account.php";
                                break;

                            case "create_members_account":
                                require_once __DIR__ . "/../../views/administrator/members/create_members_account.php";
                                break;

                            case "notifications":
                                require_once __DIR__ . "/../../views/administrator/notifications.php";
                                break;

                            case "announcement":
                                require_once __DIR__ . "/../../views/administrator/announcement.php";
                                break;
                        }
                        ?>
                    </div>

                </div>
            </div>

        </div>


        <script src="../js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function() {
                var side_element = document.getElementById("sider");
                document.getElementById("sider").style.display = "block";

                var dropdown = document.getElementsByClassName("dropdown-btn");
                var i;

                for (i = 0; i < dropdown.length; i++) {
                    dropdown[i].addEventListener("click", function() {
                        this.classList.toggle("active");
                        var dropdownContent = this.nextElementSibling;
                        if (dropdownContent.style.display === "block") {
                            dropdownContent.style.display = "none";
                        } else {
                            dropdownContent.style.display = "block";
                        }
                    });
                }
            });

            function toogleMenu(test) {
                var side_element = document.getElementById("sider");
                var main_element = document.getElementById("main");
                var sideToogle = side_element.style.display;

                if (!sideToogle || sideToogle == "none") {
                    main_element.classList.remove("active");
                    document.getElementById("sider").style.display = "block";
                } else {
                    main_element.classList.add("active");
                    document.getElementById("sider").style.display = "none";
                }
            }


            function removeItem() {
                var remove = confirm('Are you sure you want to remove this item?');
                if (remove) {
                    return true;
                } else {
                    event.preventDefault();
                }
            }
        </script>
</body>

</html>