<?php
    session_start();
    session_destroy();
    unset($_SESSION['admin_name']);
    unset($_SESSION['username']);
    unset($_SESSION['id']);
    header("Location: login.php");
?>