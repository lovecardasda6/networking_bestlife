<?php
session_start();
require_once __DIR__ . "/../../App/Sanitize.php";
require_once __DIR__ . "/../../config/Connection.php";
require_once __DIR__ . "/../../controllers/auth.php";
auth::admin_auth('Accounting');



require_once __DIR__ . "/../../controllers/PayoutController.php";
require_once __DIR__ . "/../../model/Cashout.php";
require_once __DIR__ . "/../../model/Bank.php";
require_once __DIR__ . "/../../model/Remittance.php";
require_once __DIR__ . "/../../model/EWallet.php";


$request = @$_GET['page'];
require_once __DIR__ . "/../../controllers/controller.php";
require_once __DIR__ . "/../../controllers/controller.php";
require_once __DIR__ . "/../../controllers/accounting/payout.php";
require_once __DIR__ . "/../../controllers/accounting/confirm_payout.php";
require_once __DIR__ . "/../../controllers/accounting/reports.php";
require_once __DIR__ . "/../../controllers/administrator/transaction_fee.php";
require_once __DIR__ . "/../../controllers/wallet.php";
require_once __DIR__ . "/../../controllers/registrationCodes.php";
require_once __DIR__ . "/../../router/accounting.php";

?>

<!DOCTYPE html>

<html>

<head>
    <title>Accounting - Best Life Essential</title>
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link href="./../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./../css/style_v2.css" />
    <link rel="stylesheet" type="text/css" href="./../assets/font-awesome/css/font-awesome.css" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <style>
        @media (min-width: 992px) {
            .col-md-offset-2.active {
                margin-left: 0%;
            }

            .col-md-10.active {
                width: 100%;
            }
        }
    </style>

    <script src="./../js/jquery.min.js"></script>

</head>

<body>
    <div class="body">

        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="row">
                <div class="col-md-12">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="btn btn-primay" onshow="toogleMenut(this)" onclick="toogleMenu(this)" style="margin:9px; float:left;"><i class="fa fa-bars" aria-hidden="true"></i></button>
                            <a class="navbar-brand" href="#" style="padding:5px; margin-left: 30px;">
                                <img alt="Brand" src="../assets/logo2.png" style="width:40px">
                                <a class="navbar-brand" href="javascript:(void)" style="color:#00674b;font-size: 25px;font-weight: 500;">BestLife Essential</a>
                            </a>
                        </div>
                        <div style="text-align: right; padding: 1em;">
                            <b>Welcome, &nbsp;</b>
                            <label><?= $_SESSION['admin_name'] ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-3 col-md-2 toggle-sidebar">
                    <div class="sider" id="sider" style="margin:0px; padding:1em 0em 0em 1em; width: 280px; background-color:rgb(0, 128, 92);">
                        <ul class="nav nav-sidebar" style="width: 100%;">
                            <li style="<?= ($request == 'payout') ? 'background-color:#004a36;' : '' ?>"><a href="?page=payout" style="color:white;">
                                    <i class="fa fa-cc-mastercard" aria-hidden="true"></i></i>&nbsp;&nbsp; Payout</a>
                            </li>
                            <li style="<?= ($request == 'transaction_fee') ? 'background-color:#004a36;' : '' ?>"><a href="?page=transaction_fee" style="color:white;">
                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;&nbsp; Transaction Fee</a>
                            </li>
                            <li style="<?= ($request == 'registration_code' || $request == 'unpaid_registration_codes') ? 'background-color:#004a36;' : '' ?>"><a class="dropdown-btn" style="color:white;">
                                    <i class="fa fa-barcode" aria-hidden="true"></i>&nbsp;&nbsp; Registration Code &nbsp;&nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                                <div class="dropdown-container" style="background-color: #00674b; margin:0px; padding:0px;">
                                    <ul class="nav nav-sidebar">
                                        <li>
                                            <a href="?page=registration_code" style="color:white;"><i class="fa fa-barcode" aria-hidden="true"></i>&nbsp; &nbsp; Paid codes</a>
                                        </li>
                                        <li>
                                            <a href="?page=unpaid_registration_codes" style="color:white;"><i class="fa fa-barcode" aria-hidden="true"></i>&nbsp; &nbsp; Free codes</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li style="<?= ($request == 'report') ? 'background-color:#004a36;' : '' ?>"><a href="?page=report" style="color:white;">
                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;&nbsp; Report</a>
                            </li>
                            <li><a href="logout.php" style="color:white;">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i></i>&nbsp;&nbsp; Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 main" id="main" style="overflow-x:hidden">
                <!-- <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" id="main" style="overflow-x:hidden"> -->
                    <div class="content">
                        <?php
                        switch ($request) {
                            case "payout":
                                require_once __DIR__ . "/../../views/accounting/payout.php";
                                break;

                            case "confirm_payout":
                                require_once __DIR__ . "/../../views/accounting/confirm_payout.php";
                                break;

                            case "report":
                                require_once __DIR__ . "/../../views/accounting/report.php";
                                break;

                            case "view_report":
                                require_once __DIR__ . "/../../views/accounting/view_report.php";
                                break;

                            case "reports":
                                require_once __DIR__ . "/../../views/accounting/reports.php";
                                break;

                            case "pending_transactions":
                                require_once __DIR__ . "/../../views/accounting/pending_transactions.php";
                                break;

                            case "account_information":
                                require_once __DIR__ . "/../../views/accounting/account_information.php";
                                break;

                            case "registration_code":
                                require_once __DIR__ . "/../../views/administrator/registration_code.php";
                                break;

                            case "unpaid_registration_codes":
                                require_once __DIR__ . "/../../views/administrator/unpaid_registration_codes.php";
                                break;

                            case "transaction_fee":
                                require_once __DIR__ . "/../../views/administrator/transaction_fee.php";
                                break;

                            default:
                                header("location: ?page=payout");
                                break;
                        }
                        ?>
                    </div>

                </div>
            </div>

        </div>
        <script src="../js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function() {
                var side_element = document.getElementById("sider");
                document.getElementById("sider").style.display = "block";

                var dropdown = document.getElementsByClassName("dropdown-btn");
                var i;

                for (i = 0; i < dropdown.length; i++) {
                    dropdown[i].addEventListener("click", function() {
                        this.classList.toggle("active");
                        var dropdownContent = this.nextElementSibling;
                        if (dropdownContent.style.display === "block") {
                            dropdownContent.style.display = "none";
                        } else {
                            dropdownContent.style.display = "block";
                        }
                    });
                }

                
                document.getElementById("sider").style.display = "none";
            });

            function toogleMenu(test) {
                var side_element = document.getElementById("sider");
                var main_element = document.getElementById("main");
                var sideToogle = side_element.style.display;

                if (!sideToogle || sideToogle == "none") {
                    main_element.classList.remove("active");
                    document.getElementById("sider").style.display = "block";
                } else {
                    main_element.classList.add("active");
                    document.getElementById("sider").style.display = "none";
                }
            }
        </script>
        <!-- <script>
            $(document).ready(function() {
                var side_element = document.getElementById("sider");
                document.getElementById("sider").style.display = "block";


            });

            function toogleMenu(test) {
                var side_element = document.getElementById("sider");
                var main_element = document.getElementById("main");
                var sideToogle = side_element.style.display;

                if (!sideToogle || sideToogle == "none") {
                    main_element.classList.remove("active");
                    document.getElementById("sider").style.display = "block";
                } else {
                    main_element.classList.add("active");
                    document.getElementById("sider").style.display = "none";
                }
            }
        </script> -->
</body>

</html>